#!/bin/bash


# TRI DE BASE :
# Pour faire des stats, on ne retient que les cas avec un WCET de reference.
# * Sont exclus ceux :
#   - qui plantent au preprocess (typiquement recursion)
#   - qui plantent OTAWA (typiquement timeout)
# 
# * Dans les retenus (on a un BASIC.owcet) on regarde EN PRIORITE ce que donne pagai/poly :
#   - on a un PAGAI.owcet :
#     on affine en comptant :
#     + pagai ne trouve rien et/ou que des flow-facts redondants
#     + pagai trouve des flow-facts NON redondants et
#       - améliore le wcet
#       - n'améliore PAS le wcet
#   - on n'a PAS DE PAGAI.owcet :
#     + à cause de pagai :
#       - bug/timeout ?
#     + à cause de lp_solve :
#       - bug/timeout ?

# appeler avec -co pour les benchs avec optim
if [ "$1" == "-co" ]; then
   benchresdir=expe-compteurs-CO
   OPT=CO1
   OUTFILE=wcet_stats_CO.txt
else
   benchresdir=expe-compteurs-O0
   OPT=O0
   OUTFILE=wcet_stats_O0.txt
fi
echo "wcet_stats in $benchresdir"

echo -n "" > $OUTFILE


# BUGS 
IGNORE="\
___BUGS_IN_PAGAI \
"
#_cnt-audiobeam_cat-main-inline-noswitch-O0-nops \
#_cnt-iir_cat-main-inline-noswitch-O0-nops \
#_cnt-lms_cat-main-inline-noswitch-O0-nops \
#_cnt-ludcmp_cat-main-inline-noswitch-O0-nops \
#_cnt-minver_cat-main-inline-noswitch-O0-nops \
#_cnt-mpeg2_cat-mpeg2_field_ME-inline-noswitch-O0-nops \

# DEFINITIVELY UNBOUNDED 

function check_owcet {
	# XXXXX.wcet may contain (e.g):
	# - "Value of objective function: 251"
	# - several lines of "spx_run: Lost feasibility ...  " + "Value of objective function: xxxxx"
	#   => returns a number
	# - "This problem is unbounded"
	#   => returns 0
	# - something else 
	#   => returns -1
	ofile=$1
	if [ ! -f $ofile ]; then
		echo -3
		return
	fi
	ores=`tail -n 1 $ofile`
	owcet=`echo $ores | grep -o "[0-9]*"`
	unbound=`echo $ores | grep -o "unbounded"`
	infea=`echo $ores | grep -o "infeasible"`
	if [ -n "$owcet" ]; then
		echo $owcet
	elif [ -n "$unbound" ]; then
		echo 0
	elif [ -n "$infea" ]; then
		echo -1
	else 
		echo -2
	fi
}

nb_bug=0
nb_bad=0
nb_bench=0

function dopcent {
	part=$1
	total=$2
	pc=`echo "$part * 100 / $total" | bc -l`
	printf "%4.1f" $pc
}

function maxpcent {
	if [ "$1" = "-" ]; then
		echo "$2"
	elif [ "$2" = "-" ]; then
		echo "$1"
	elif [ `echo "$2 > $1" | bc -l` -ge 1 ]; then
		echo $2
	else
		echo $1
	fi
}

function check_time {
	tfile=$1
	tm=`cat $tfile | head -1` || tm="-"
	#format m:s.c
	res=`echo $tm | grep -e "[0-9]\+:[0-9]\+\.[0-9]\+" -o`
	if [ -z "$res" ]; then
		#format h:m:s pour les durées longues ?
		res=`echo $tm | grep -e "[0-9]\+:[0-9]\+:[0-9]\+" -o`
		if [ -z "$res" ]; then
			res="-"
		fi
	fi
	echo $res
}

function check_tracea {
	lfile=$1
	res=`tac $lfile | grep -m 1 Traceability | grep -o "(.* out of .*)" | sed -e "s/(\([0-9]\+\) *out of *\([0-9]\+\))/\1, \2/"`
	if [ -z "$res" ]; then
		res="-, -"
	fi
	echo $res
}

nb_gain=0

# on regarde les 3 cas :
# poly (base) domainoct et domainbox
function check_domains_sh {
	polybase=$1
	boxbase="$1--domainbox"
	octbase="$1--domainoct"
	# il faut un wcet de ref, on vérifie que ce soit cohérent...
	basic=`check_owcet $benchresdir/$polybase/basic.wcet`
	b2=`check_owcet $benchresdir/$octbase/basic.wcet`
	b3=`check_owcet $benchresdir/$boxbase/basic.wcet`
	# cohérent ?
	if [ $basic != $b2 -o $basic != $b3 ]; then
		>&2 echo "BUGGY BASIC WCET $polybase"
		((nb_bug++))
		return
	fi
	# on rentre pas dans le détail : on regarde les cas ou on a 1 nombre
	# les pbs ont été traités en amont
	if [ $basic -le 0 ]; then
		>&2 echo "BAD BASIC WCET ($basic)  $polybase"
		((nb_bad++))
		return
	fi
	gain=0
	poly=`check_owcet $benchresdir/$polybase/pagai.wcet`
	if [ $poly -gt 0 -a $poly -lt $basic ]; then gain=1 ; fi
	oct=`check_owcet $benchresdir/$octbase/pagai.wcet`
	if [ $oct -gt 0 -a $oct -lt $basic ]; then gain=1 ; fi
	box=`check_owcet $benchresdir/$boxbase/pagai.wcet`
	if [ $box -gt 0 -a $box -lt $basic ]; then gain=1 ; fi
	if [ $gain -gt 0 ]; then
		((nb_gain++))
	fi

	polytime=`check_time $benchresdir/$polybase/pagai.time`
	octtime=`check_time $benchresdir/$octbase/pagai.time`
	boxtime=`check_time $benchresdir/$boxbase/pagai.time`

	# on ajoute la traceabilite
	zelog=$benchresdir/log/$polybase.log
	tracea=`check_tracea $zelog`

	((nb_bench++))
	echo "$polybase, $basic, $box, $boxtime, $oct, $octtime, $poly, $polytime, $tracea" >> $OUTFILE
}

echo "#bench, basic, box wcet, box time, oct wcet, oct time, poly wcet, poly time, traced counters, all counters" > $OUTFILE

for zsh in `/bin/ls $benchresdir/sh/*-$OPT-nops*.sh | grep -v domain`; do
#>&2 echo "DOING $zsh"

	benchbase=`basename $zsh .sh`
	check_domains_sh $benchbase
done

>&2 echo "BENCH: $nb_bench IMPROVEMENT: $nb_gain (IGNORED: $nb_bug buggy, $nb_bad otawa fails)"
