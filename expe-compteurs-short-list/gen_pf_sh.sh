#/bin/bash

#PR
# adapted from gen_sh_all_fun.sh
# -> le premier argument est un couple "dir/func"

DIR_FUNC="$1"
TIMEOUT=$2
shift
shift
OPT=$@

echo DIR_FUNC=$DIR_FUNC
echo TIMEOUT=$TIMEOUT
echo OPT=$OPT

DIR=`echo $DIR_FUNC | cut -d"," -f1`
FUNC=`echo $DIR_FUNC | cut -d"," -f2`
CFILE=`basename "$DIR"`_cat.c
CFILE="bench/$DIR/$CFILE"

set x

OPT_NO_SPACE=$(echo $OPT | tr -d ' '| tr -d '/')

#FUNC_LIST=`nm $O | grep " T " | cut -d " " -f 3 | grep -v main`
#FUNC_LIST=`nm $O | grep " T " | cut -d " " -f 3`
FUNC_LIST=$FUNC

for zefunc in $FUNC_LIST ; do
##echo $zefunc
	#PR: anticipate the name of the cnt working dir
	#serves as basis for sh and log
	CNT_COMMAND="cnt_all $CFILE $zefunc  -ld  $OPT"
	CNT_DIR=`$CNT_COMMAND -dir`
##echo $CNT_DIR
	#FNAME=${BASE_FILE_NAME}-$i-$OPT_NO_SPACE ;\
	FNAME=$CNT_DIR
	SH_NAME=sh/$FNAME.sh ;\
	LOG=log/$FNAME.log ;\
#echo LOG=$LOG
#echo SH_NAME=$SH_NAME
	echo "Generating $SH_NAME (that will generate $LOG)"; \
	if [ -e "$SH_NAME" ]; then
		echo "$SH_NAME already exist."
	else
		echo "echo \"Launching cnt on $CFILE\"> $LOG" > $SH_NAME; \
		echo "echo \"With the following options $OPT\">> $LOG" >> $SH_NAME; \
		echo "  timeout $TIMEOUT $CNT_COMMAND >> $LOG 2>&1 ;">> $SH_NAME; \
		echo "  RET=\"\$?\"; if [ \"\$RET\" -gt 1 ]; then echo \"Exit \$RET\" >> $LOG 2>&1 ; fi">> $SH_NAME; \
		echo "if [ \"\$RET\" -gt 123 ]; then echo 'KO: TIMEOUT($TIMEOUT)' >> $LOG  2>&1 ; fi">> $SH_NAME ;\
	fi
done


