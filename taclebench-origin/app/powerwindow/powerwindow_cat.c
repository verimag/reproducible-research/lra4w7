/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow

 Author: CoSys-Lab, University of Antwerp

 Function: powerwindow implement the powerwindow that can be seen in cars nowadays. 
	The window can be controlled by either driver or passenger. When an object is
	detected between the window frame and the glass during the raising of the glass,
	the glass will lower down for some distance. This benchmark contains 4 tasks which includes the
	driver side powerwindow, front passenger side powerwindow, back-left passenger side powerwindow,
	back-right passenger side powerwindow. These 4 tasks can be easily adjusted to execute in
	sequential order parallel on single or muti core.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow/powerwindow.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/

#include "powerwindow_HeaderFiles/powerwindow.h" 
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_Front.h"
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackL.h"
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackR.h"
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_DRV.h"
#include "powerwindow_HeaderFiles/powerwindow_debounce.h"
#include "powerwindow_HeaderFiles/powerwindow_controlexclusion.h"       /* Control Model's header file */
#include "powerwindow_HeaderFiles/powerwindow_model_reference_types.h"
#include "powerwindow_HeaderFiles/powerwindow_powerwindow_control.h" 	/* PW passenger control Model's header file */
#include "powerwindow_HeaderFiles/powerwindow_rtwtypes.h"
#include "powerwindow_HeaderFiles/powerwindow_model_reference_types.h"
//#include <stdio.h>
/*
  Forward declaration of functions
*/


void powerwindow_Booleaninputarray_initialize(powerwindow_boolean_T*, powerwindow_boolean_T*);
void powerwindow_Uint8inputarray_initialize(powerwindow_uint8_T*, powerwindow_uint8_T*);
void powerwindow_init();
void powerwindow_main();
int powerwindow_return();
int main(void);




//DRV
void powerwindow_init_DRV(int);
void powerwindow_input_initialize_DRV(void);
void powerwindow_initialize_DRV(void);
void powerwindow_return_DRV(void);
void powerwindow_DRV_main(void);

// PSG_Front
void powerwindow_init_PSG_Front(int);
void powerwindow_input_initialize_PSG_Front(void);
void powerwindow_initialize_PSG_Front(void);
void powerwindow_return_PSG_Front(void);
void powerwindow_PSG_Front_main(void);

// PSG_BackL
void powerwindow_init_PSG_BackL(int);
void powerwindow_input_initialize_PSG_BackL(void);
void powerwindow_initialize_PSG_BackL(void);
void powerwindow_return_PSG_BackL(void);
void powerwindow_PSG_BackL_main(void);

// PSG_BackR
void powerwindow_init_PSG_BackR(int);
void powerwindow_input_initialize_PSG_BackR(void);
void powerwindow_initialize_PSG_BackR(void);
void powerwindow_return_PSG_BackR(void);
void powerwindow_PSG_BackR_main(void);



/*
  Declaration of global variables
*/

/* External inputs (root inport signals with auto storage) */

extern powerwindow_ExternalInputs_powerwindow_PW_C powerwindow_PW_Control_DRV_U;
extern powerwindow_ExternalOutputs_powerwindow_PW_ powerwindow_PW_Control_DRV_Y;
extern powerwindow_ExternalInputs_PW_Control_PSG_Front powerwindow_PW_Control_PSG_Front_U;
extern powerwindow_ExternalInputs_PW_Control_PSG_BackL powerwindow_PW_Control_PSG_BackL_U;
extern powerwindow_ExternalInputs_PW_Control_PSG_BackR powerwindow_PW_Control_PSG_BackR_U;



powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Up_Input_DRV[997];
powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Down_Input_DRV[997];
powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Up_Input_Front[997];
powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Down_Input_Front[997];
powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Up_Input_BackL[997];
powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Down_Input_BackL[997];
powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Up_Input_BackR[997];
powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Down_Input_BackR[997];
powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV[997];
powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Input_DRV[997];


extern powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Up_Input_DRV_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Down_Input_DRV_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Up_Input_Front_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Down_Input_Front_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Up_Input_BackL_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Down_Input_BackL_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Up_Input_BackR_Array[997];
extern powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Down_Input_BackR_Array[997];
extern powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_DRV_Array[997];
extern powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_DRV_Array[997];

powerwindow_boolean_T powerwindow_controlexclusion_U_Up_DRV_Input_Front[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
powerwindow_boolean_T powerwindow_controlexclusion_U_Down_DRV_Input_Front[997];
powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Up_Input_Front[997];
powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Down_Input_Front[997];
powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Input_Front[997];
powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Input_Front[997];

extern powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Up_Front_Array[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
extern powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Down_Front_Array[997];
extern powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Front_Array[997];
extern powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Front_Array[997];

powerwindow_boolean_T powerwindow_controlexclusion_U_Up_DRV_Input_BackL[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
powerwindow_boolean_T powerwindow_controlexclusion_U_Down_DRV_Input_BackL[997];
powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Up_Input_BackL[997];
powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Down_Input_BackL[997];
powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Input_BackL[997];
powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Input_BackL[997];

extern powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Up_BackL_Array[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
extern powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Down_BackL_Array[997];
extern powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_BackL_Array[997];
extern powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_BackL_Array[997];

powerwindow_boolean_T powerwindow_controlexclusion_U_Up_DRV_Input_BackR[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
powerwindow_boolean_T powerwindow_controlexclusion_U_Down_DRV_Input_BackR[997];
powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Up_Input_BackR[997];
powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Down_Input_BackR[997];
powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Input_BackR[997];
powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Input_BackR[997];

extern powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Up_BackR_Array[997]; 	/* Here applied a push-down button, the signal is high when the button is not pressed. */
extern powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Down_BackR_Array[997];
extern powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_BackR_Array[997];
extern powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_BackR_Array[997];

int powerwindow_main_inputcyclecounter;

/*
  Initialization- and return-value-related functions
*/
void powerwindow_init_DRV(int i)
{
	powerwindow_PW_Control_DRV_U.In1  = powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV[i];			/* The when the window reaches the end of the range, the endofdetectionrange changes to 0. */
	powerwindow_PW_Control_DRV_U.In3  = powerwindow_powerwindow_control_U_currentsense_Input_DRV[i];		/* When the currentsense is higher than 92 (based on experiments), one object is stuck between the window and the frame. Pinch is set to True.*/

	powerwindow_PW_Control_DRV_U.In2  = powerwindow_debounce_Driver_DRV_U_Up_Input_DRV[i];				/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
	powerwindow_PW_Control_DRV_U.In4  = powerwindow_debounce_Driver_DRV_U_Down_Input_DRV[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */

	powerwindow_PW_Control_DRV_U.In5  = powerwindow_debounce_Driver_Front_U_Up_Input_Front[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
	powerwindow_PW_Control_DRV_U.In6  = powerwindow_debounce_Driver_Front_U_Down_Input_Front[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */

	powerwindow_PW_Control_DRV_U.In9  = powerwindow_debounce_Driver_BackL_U_Up_Input_BackL[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
	powerwindow_PW_Control_DRV_U.In10 = powerwindow_debounce_Driver_BackL_U_Down_Input_BackL[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */

	powerwindow_PW_Control_DRV_U.In7  = powerwindow_debounce_Driver_BackR_U_Up_Input_BackR[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
	powerwindow_PW_Control_DRV_U.In8 = powerwindow_debounce_Driver_BackR_U_Down_Input_BackR[i];			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */

}

void powerwindow_init_PSG_Front(int i)
{


	powerwindow_PW_Control_PSG_Front_U.Up_DRV  = powerwindow_PW_Control_DRV_Y.Out6;			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
    powerwindow_PW_Control_PSG_Front_U.Down_DRV = powerwindow_PW_Control_DRV_Y.Out7;		/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lower the window. */

	powerwindow_PW_Control_PSG_Front_U.Up_PSG_Front	= powerwindow_debounce_passenger_Front_U_Up_Input_Front[i];
    powerwindow_PW_Control_PSG_Front_U.Down_PSG_Front = powerwindow_debounce_passenger_Front_U_Down_Input_Front[i];	/* '<Root>/Down'. Here applied a push-down button, the signal is high when the button is not pressed. Change to 0 to lower the window. */

    powerwindow_PW_Control_PSG_Front_U.endofdetectionrange = powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV[i];		/* The when the window reaches the end of the range, the endofdetectionrange changes to 0. */

    powerwindow_PW_Control_PSG_Front_U.currentsense = powerwindow_powerwindow_control_U_currentsense_Input_DRV[i];		/* When the currentsense is higher than 92 (based on experiments), one object is stuck between the window and the frame. Pinch is set to True.*/

}

void powerwindow_init_PSG_BackL(int i)
{


	powerwindow_PW_Control_PSG_BackL_U.Up_DRV  = powerwindow_PW_Control_DRV_Y.Out10;			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
    powerwindow_PW_Control_PSG_BackL_U.Down_DRV = powerwindow_PW_Control_DRV_Y.Out11;		/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lower the window. */

	powerwindow_PW_Control_PSG_BackL_U.Up_PSG_BackL	= powerwindow_debounce_passenger_BackL_U_Up_Input_BackL[i];
    powerwindow_PW_Control_PSG_BackL_U.Down_PSG_BackL = powerwindow_debounce_passenger_BackL_U_Down_Input_BackL[i];	/* '<Root>/Down'. Here applied a push-down button, the signal is high when the button is not pressed. Change to 0 to lower the window. */

    powerwindow_PW_Control_PSG_BackL_U.endofdetectionrange = powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV[i];		/* The when the window reaches the end of the range, the endofdetectionrange changes to 0. */

    powerwindow_PW_Control_PSG_BackL_U.currentsense = powerwindow_powerwindow_control_U_currentsense_Input_DRV[i];		/* When the currentsense is higher than 92 (based on experiments), one object is stuck between the window and the frame. Pinch is set to True.*/

}

void powerwindow_init_PSG_BackR(int i)
{


	powerwindow_PW_Control_PSG_BackR_U.Up_DRV  = powerwindow_PW_Control_DRV_Y.Out8;			/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lift the window. */
    powerwindow_PW_Control_PSG_BackR_U.Down_DRV = powerwindow_PW_Control_DRV_Y.Out9;		/* The debounced control signal from the driver. 1 when the button is not pressed, change to 0 to lower the window. */

	powerwindow_PW_Control_PSG_BackR_U.Up_PSG_BackR	= powerwindow_debounce_passenger_BackR_U_Up_Input_BackR[i];
    powerwindow_PW_Control_PSG_BackR_U.Down_PSG_BackR = powerwindow_debounce_passenger_BackR_U_Down_Input_BackR[i];	/* '<Root>/Down'. Here applied a push-down button, the signal is high when the button is not pressed. Change to 0 to lower the window. */

    powerwindow_PW_Control_PSG_BackR_U.endofdetectionrange = powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV[i];		/* The when the window reaches the end of the range, the endofdetectionrange changes to 0. */

    powerwindow_PW_Control_PSG_BackR_U.currentsense = powerwindow_powerwindow_control_U_currentsense_Input_DRV[i];		/* When the currentsense is higher than 92 (based on experiments), one object is stuck between the window and the frame. Pinch is set to True.*/

}

void powerwindow_input_initialize_DRV(void)
{

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_DRV_U_Up_Input_DRV,powerwindow_debounce_Driver_DRV_U_Up_Input_DRV_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_DRV_U_Down_Input_DRV,powerwindow_debounce_Driver_DRV_U_Down_Input_DRV_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_Front_U_Up_Input_Front,powerwindow_debounce_Driver_Front_U_Up_Input_Front_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_Front_U_Down_Input_Front,powerwindow_debounce_Driver_Front_U_Down_Input_Front_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_BackL_U_Up_Input_BackL,powerwindow_debounce_Driver_BackL_U_Up_Input_BackL_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_BackL_U_Down_Input_BackL,powerwindow_debounce_Driver_BackL_U_Down_Input_BackL_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_BackL_U_Down_Input_BackL,powerwindow_debounce_Driver_BackR_U_Up_Input_BackR_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_Driver_BackR_U_Down_Input_BackR,powerwindow_debounce_Driver_BackR_U_Down_Input_BackR_Array);
	powerwindow_Booleaninputarray_initialize(powerwindow_powerwindow_control_U_endofdetectionrange_Input_DRV,powerwindow_powerwindow_control_U_endofdetectionrange_DRV_Array);
	powerwindow_Uint8inputarray_initialize(powerwindow_powerwindow_control_U_currentsense_DRV_Array,powerwindow_powerwindow_control_U_currentsense_DRV_Array);

}



void powerwindow_input_initialize_PSG_Front(void)
{

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_Front_U_Up_Input_Front, powerwindow_debounce_passenger_Front_U_Up_Front_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_Front_U_Down_Input_Front,powerwindow_debounce_passenger_Front_U_Down_Front_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_powerwindow_control_U_endofdetectionrange_Input_Front, powerwindow_powerwindow_control_U_endofdetectionrange_Front_Array);

	powerwindow_Uint8inputarray_initialize(powerwindow_powerwindow_control_U_currentsense_Input_Front,powerwindow_powerwindow_control_U_currentsense_Front_Array);

}


void powerwindow_input_initialize_PSG_BackL(void)
{

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_BackL_U_Up_Input_BackL, powerwindow_debounce_passenger_BackL_U_Up_BackL_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_BackL_U_Down_Input_BackL,powerwindow_debounce_passenger_BackL_U_Down_BackL_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_powerwindow_control_U_endofdetectionrange_Input_BackL, powerwindow_powerwindow_control_U_endofdetectionrange_BackL_Array);

	powerwindow_Uint8inputarray_initialize(powerwindow_powerwindow_control_U_currentsense_Input_BackL,powerwindow_powerwindow_control_U_currentsense_BackL_Array);

}

void powerwindow_input_initialize_PSG_BackR(void)
{
	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_BackR_U_Up_Input_BackR, powerwindow_debounce_passenger_BackR_U_Up_BackR_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_debounce_passenger_BackR_U_Down_Input_BackR,powerwindow_debounce_passenger_BackR_U_Down_BackR_Array);

	powerwindow_Booleaninputarray_initialize(powerwindow_powerwindow_control_U_endofdetectionrange_Input_BackR, powerwindow_powerwindow_control_U_endofdetectionrange_BackR_Array);

	powerwindow_Uint8inputarray_initialize(powerwindow_powerwindow_control_U_currentsense_Input_BackR,powerwindow_powerwindow_control_U_currentsense_BackR_Array);

}

void powerwindow_Booleaninputarray_initialize(powerwindow_boolean_T* arrayA, powerwindow_boolean_T* arrayB)
{

    register int i;
//    _Pragma( "loopbound min 997 max 997" )
    for ( i = 0; i < 997; i++ )
    	arrayA[i] = arrayB[i];
}

void powerwindow_Uint8inputarray_initialize(powerwindow_uint8_T* arrayA, powerwindow_uint8_T* arrayB)
{

    register int i;
//    _Pragma( "loopbound min 997 max 997" )
    for ( i = 0; i < 997; i++ )
    	arrayA[i] = arrayB[i];
}

void powerwindow_initialize_DRV(void)
{
    /* Initialize model */
	powerwindow_PW_Control_DRV_initialize();

}

void powerwindow_initialize_PSG_Front(void)
{
    /* Initialize model */
	powerwindow_PW_Control_PSG_Front_initialize();

}

void powerwindow_initialize_PSG_BackL(void)
{
    /* Initialize model */
	powerwindow_PW_Control_PSG_BackL_initialize();

}

void powerwindow_initialize_PSG_BackR(void)
{
    /* Initialize model */
	powerwindow_PW_Control_PSG_BackR_initialize();

}


void powerwindow_return_DRV(void)
{
    /* Terminate model */
	powerwindow_PW_Control_DRV_terminate();

}

void powerwindow_return_PSG_Front(void)
{
    /* Terminate model */
	powerwindow_PW_Control_PSG_Front_terminate();

}

void powerwindow_return_PSG_BackL(void)
{
    /* Terminate model */
	powerwindow_PW_Control_PSG_BackL_terminate();

}

void powerwindow_return_PSG_BackR(void)
{
    /* Terminate model */
	powerwindow_PW_Control_PSG_BackR_terminate();

}

/*
  Main functions
*/

/*
 * Associating powerwindow_main with a real-time clock or interrupt service routine
 * is what makes the generated code "real-time".  The function powerwindow_main is
 * always associated with the base rate of the model.  Subrates are managed
 * by the base rate from inside the generated code.  Enabling/disabling
 * interrupts and floating point context switches are target specific.  This
 * example code indicates where these should take place relative to executing
 * the generated code step function.  Overrun behavior should be tailored to
 * your application needs.  This example simply sets an error status in the
 * real-time model and returns from powerwindow_main.
 */


void powerwindow_DRV_main(void)
{

    static powerwindow_boolean_T OverrunFlag = 0;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
    	powerwindow_PW_DRV_rtmSetErrorStatus(powerwindow_PW_Control_DRV_M, "Overrun"); //////////

        return;
    }


    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */


    powerwindow_PW_Control_DRV_main();

    /* Get model outputs here */


    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */

}

/*
 * The example "main" function illustrates what is required by your
 * application code to initialize, execute, and terminate the generated code.
 * Attaching powerwindow_main to a real-time clock is target specific.  This example
 * illustates how you do this relative to initializing the model.
 */

void powerwindow_PSG_Front_main(void)
{

    static powerwindow_boolean_T OverrunFlag = 0;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
    	powerwindow_PW_PSG_Front_rtmSetErrorStatus(powerwindow_PW_Control_PSG_Front_M, "Overrun");

        return;
    }


    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */


    powerwindow_PW_Control_PSG_Front_main();

    /* Get model outputs here */


    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */

}

void powerwindow_PSG_BackL_main(void)
{

    static powerwindow_boolean_T OverrunFlag = 0;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
    	powerwindow_PW_PSG_BackL_rtmSetErrorStatus(powerwindow_PW_Control_PSG_BackL_M, "Overrun");

        return;
    }


    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */


    powerwindow_PW_Control_PSG_BackL_main();

    /* Get model outputs here */


    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */

}

void powerwindow_PSG_BackR_main(void)
{

    static powerwindow_boolean_T OverrunFlag = 0;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
    	powerwindow_PW_PSG_BackR_rtmSetErrorStatus(powerwindow_PW_Control_PSG_BackR_M, "Overrun");

        return;
    }


    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */


    powerwindow_PW_Control_PSG_BackR_main();

    /* Get model outputs here */


    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */

}

void powerwindow_init(void)
{
	powerwindow_initialize_DRV();
	powerwindow_initialize_PSG_Front();
	powerwindow_initialize_PSG_BackL();
	powerwindow_initialize_PSG_BackR();
	powerwindow_main_inputcyclecounter=0;

}

void _Pragma( "entrypoint" ) powerwindow_main(void)
{
    /* Attach powerwindow_main to a timer or interrupt service routine with
     * period 0.005 seconds (the model's base sample time) here.  The
     * call syntax for powerwindow_main is
     *
     *  powerwindow_main();
     */
    //Task 1: Driver side window


    powerwindow_input_initialize_DRV();
    powerwindow_input_initialize_PSG_Front();
    powerwindow_input_initialize_PSG_BackL();
    powerwindow_input_initialize_PSG_BackR();

    while(powerwindow_main_inputcyclecounter<997)
    {

        powerwindow_init_DRV(powerwindow_main_inputcyclecounter);
        powerwindow_DRV_main();


    //Task 2: Front passenger side window


        powerwindow_init_PSG_Front(powerwindow_main_inputcyclecounter);
        powerwindow_PSG_Front_main();


    //Task 3: Back left passenger side window


        powerwindow_init_PSG_BackL(powerwindow_main_inputcyclecounter);
        powerwindow_PSG_BackL_main();


    //Task 4: Back right passenger side window


        powerwindow_init_PSG_BackR(powerwindow_main_inputcyclecounter);
        powerwindow_PSG_BackR_main();


        powerwindow_main_inputcyclecounter++;
    }


}

int powerwindow_return(void)
{
	powerwindow_return_DRV();
	powerwindow_return_PSG_Front();
	powerwindow_return_PSG_BackL();
	powerwindow_return_PSG_BackR();

	return 0;
}


int main(void){
 powerwindow_init();
 powerwindow_main();
 return powerwindow_return();
}


/*
 * File trailer for generated code.
 *
 * [EOF]
 */



/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_PW_Control_DRV.c

 Author: CoSys-Lab, University of Antwerp

 Function: PW_Control_DRV realizes the functionality of driver side powerwindow. It connects the 3 smaller modules together.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow//powerwindow_powerwindow_control.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_DRV.h"
#include "powerwindow_HeaderFiles/powerwindow_PW_Control_DRV_private.h"



/*
 Forward declaration of functions
*/

void powerwindow_PW_Control_DRV_initialize(void);
void powerwindow_PW_Control_DRV_terminate(void);
void powerwindow_PW_Control_DRV_main(void);


/* Block states (auto storage) */
powerwindow_D_Work_powerwindow_PW_Control_D powerwindow_PW_Control_DR_DWork;

/* External inputs (root inport signals with auto storage) */
powerwindow_ExternalInputs_powerwindow_PW_C powerwindow_PW_Control_DRV_U;

/* External outputs (root outports fed by signals with auto storage) */
powerwindow_ExternalOutputs_powerwindow_PW_ powerwindow_PW_Control_DRV_Y;

/* Real-time model */
powerwindow_RT_MODEL_PW_Control_DRV powerwindow_PW_Control_DRV_M_;
powerwindow_RT_MODEL_PW_Control_DRV *const powerwindow_PW_Control_DRV_M = &powerwindow_PW_Control_DRV_M_;



/* Model step function */
void powerwindow_PW_Control_DRV_main(void)
{
  /* local block i/o variables */
  powerwindow_boolean_T rtb_Debounce_Up_DRV;
  powerwindow_boolean_T rtb_Debounce_Down_DRV;

  /* ModelReference: '<S2>/Debounce_Up_DRV' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In2, &rtb_Debounce_Up_DRV,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Down_DRV' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In4, &rtb_Debounce_Down_DRV,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Up_PSG_BackL' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In9,
           &powerwindow_PW_Control_DRV_Y.Out10,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Down_PSG_BackL' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In10,
           &powerwindow_PW_Control_DRV_Y.Out11,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Up_PSG_Front' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In5, &powerwindow_PW_Control_DRV_Y.Out6,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Down_PSG_Front' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In6, &powerwindow_PW_Control_DRV_Y.Out7,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Up_PSG_BackR' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In7, &powerwindow_PW_Control_DRV_Y.Out8,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtzce));

  /* ModelReference: '<S2>/Debounce_Down_PSG_BackR' */
  powerwindow_debounce_main(&powerwindow_PW_Control_DRV_U.In8, &powerwindow_PW_Control_DRV_Y.Out9,
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtb),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtdw),
           &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtzce));

  /* ModelReference: '<S1>/PW_DRV' */
  powerwindow_powerwindow_control_main(&rtb_Debounce_Up_DRV, &rtb_Debounce_Down_DRV,
                      &powerwindow_PW_Control_DRV_U.In1,
                      &powerwindow_PW_Control_DRV_U.In3,
                      &powerwindow_PW_Control_DRV_Y.Out1,
                      &powerwindow_PW_Control_DRV_Y.Out2,
                      &powerwindow_PW_Control_DRV_Y.Out3,
                      &powerwindow_PW_Control_DRV_Y.Out4,
                      &powerwindow_PW_Control_DRV_Y.Out5,
                      &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtb),
					  &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtdw),
					  &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtzce));
}


/* Model initialize function */
void powerwindow_PW_Control_DRV_initialize(void)
{
  /* Registration code */

  /* states (dwork) */
  (void) memset((void *)&powerwindow_PW_Control_DR_DWork, 0,
                sizeof(powerwindow_D_Work_powerwindow_PW_Control_D));

  /* external inputs */
  (void) memset((void *)&powerwindow_PW_Control_DRV_U, 0,
                sizeof(powerwindow_ExternalInputs_powerwindow_PW_C));

  /* external outputs */
  (void) memset((void *)&powerwindow_PW_Control_DRV_Y, 0,
                sizeof(powerwindow_ExternalOutputs_powerwindow_PW_));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Down_DRV' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtm),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Down_PSG_BackL' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Down_PSG_BackR' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Down_PSG_Front' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Up_DRV' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Up_PSG_BackL' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Up_PSG_BackR' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/Debounce_Up_PSG_Front' */
  powerwindow_debounce_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S1>/PW_DRV' */
  powerwindow_powerwindow_control_initialize(powerwindow_PW_DRV_rtmGetErrorStatusPointer(powerwindow_PW_Control_DRV_M),
     &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtm),
	 &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtdw),
     &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtzce));

  /* Start for ModelReference: '<S2>/Debounce_Up_DRV' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Down_DRV' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Up_PSG_BackL' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Down_PSG_BackL' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Up_PSG_Front' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Down_PSG_Front' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Up_PSG_BackR' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/Debounce_Down_PSG_BackR' */
  powerwindow_debounce_Start
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtdw));

  /* Start for ModelReference: '<S1>/PW_DRV' */
  powerwindow_powerwindow_control_Start(&(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Up_DRV' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtb),
                &(powerwindow_PW_Control_DR_DWork.Debounce_Up_DRV_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Down_DRV' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtb),
                &(powerwindow_PW_Control_DR_DWork.Debounce_Down_DRV_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Up_PSG_BackL' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackL_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Down_PSG_BackL' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackL_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Up_PSG_Front' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_Front_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Down_PSG_Front' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_Front_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Up_PSG_BackR' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Up_PSG_BackR_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S2>/Debounce_Down_PSG_BackR' */
  powerwindow_debounce_Init
    (&(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtb),
     &(powerwindow_PW_Control_DR_DWork.Debounce_Down_PSG_BackR_DWORK1.rtdw));

  /* SystemInitialize for ModelReference: '<S1>/PW_DRV' */
  powerwindow_powerwindow_control_Init(&powerwindow_PW_Control_DRV_Y.Out1,
    &powerwindow_PW_Control_DRV_Y.Out2, &powerwindow_PW_Control_DRV_Y.Out3,
    &powerwindow_PW_Control_DRV_Y.Out4, &powerwindow_PW_Control_DRV_Y.Out5,
    &(powerwindow_PW_Control_DR_DWork.PW_DRV_DWORK1.rtdw));
}

/* Model terminate function */
void powerwindow_PW_Control_DRV_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_PW_Control_PSG_BackL.c

 Author: CoSys-Lab, University of Antwerp

 Function: PW_Control_DRV realizes the functionality of back-left passenger side powerwindow. It connects the 3 smaller modules together.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow//powerwindow_powerwindow_control.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/


#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackL.h"

#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackL_private.h"

/*
 Forward declaration of functions
*/

void powerwindow_PW_Control_PSG_BackL_initialize(void);
void powerwindow_PW_Control_PSG_BackL_terminate(void);
void powerwindow_PW_Control_PSG_BackL_main(void);


/* Block states (auto storage) */
powerwindow_D_Work_PW_Control_PSG_BackL powerwindow_PW_Control_PSG_BackL_DWork;

/* External inputs (root inport signals with auto storage) */
powerwindow_ExternalInputs_PW_Control_PSG_BackL powerwindow_PW_Control_PSG_BackL_U;

/* External outputs (root outports fed by signals with auto storage) */
powerwindow_ExternalOutputs_PW_Control_PSG_BackL powerwindow_PW_Control_PSG_BackL_Y;

/* Real-time model */
powerwindow_RT_MODEL_PW_Control_PSG_BackL powerwindow_PW_Control_PSG_BackL_M_;
powerwindow_RT_MODEL_PW_Control_PSG_BackL *const powerwindow_PW_Control_PSG_BackL_M = &powerwindow_PW_Control_PSG_BackL_M_;

/* Model step function */
void powerwindow_PW_Control_PSG_BackL_main(void)
{
  /* local block i/o variables */
  powerwindow_boolean_T rtb_debounce_Up;
  powerwindow_boolean_T rtb_debounce_Down;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_BackL_o1;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_BackL_o2;

  /* ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_BackL_U.Up_PSG_BackL, &rtb_debounce_Up,
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtzce));

  /* ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_BackL_U.Down_PSG_BackL, &rtb_debounce_Down,
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtzce));

  /* ModelReference: '<S1>/ControlEx_PSG_BackL' */
  powerwindow_controlexclusion_main(&powerwindow_PW_Control_PSG_BackL_U.Up_DRV, &powerwindow_PW_Control_PSG_BackL_U.Down_DRV,
                   &rtb_debounce_Up, &rtb_debounce_Down,
                   &powerwindow_rtb_ControlEx_PSG_BackL_o1, &powerwindow_rtb_ControlEx_PSG_BackL_o2);

  /* ModelReference: '<S1>/PW_PSG_BackL' */
  powerwindow_powerwindow_control_main(&powerwindow_rtb_ControlEx_PSG_BackL_o1, &powerwindow_rtb_ControlEx_PSG_BackL_o2,
                      &powerwindow_PW_Control_PSG_BackL_U.endofdetectionrange,
                      &powerwindow_PW_Control_PSG_BackL_U.currentsense,
                      &powerwindow_PW_Control_PSG_BackL_Y.window_up, &powerwindow_PW_Control_PSG_BackL_Y.window_down,
                      &powerwindow_PW_Control_PSG_BackL_Y.overcurrent, &powerwindow_PW_Control_PSG_BackL_Y.pinch,
                      &powerwindow_PW_Control_PSG_BackL_Y.wake,
                      &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtzce));
}

/* Model initialize function */
void powerwindow_PW_Control_PSG_BackL_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  powerwindow_PW_PSG_BackL_rtmSetErrorStatus(powerwindow_PW_Control_PSG_BackL_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackL_DWork, 0,
                sizeof(powerwindow_D_Work_PW_Control_PSG_BackL));

  /* external inputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackL_U, 0,
                sizeof(powerwindow_ExternalInputs_PW_Control_PSG_BackL));

  /* external outputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackL_Y, 0,
                sizeof(powerwindow_ExternalOutputs_PW_Control_PSG_BackL));

  /* Model Initialize fcn for ModelReference Block: '<S1>/ControlEx_PSG_BackL' */
  powerwindow_controlexclusion_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Down' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_BackL_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackL_M),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Up' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_BackL_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackL_M),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S1>/PW_PSG_BackL' */
  powerwindow_powerwindow_control_initialize(powerwindow_PW_PSG_BackL_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackL_M),
    &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtm),
    &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtb),
    &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtdw),
    &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtzce));

  /* Start for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtdw));

  /* Start for ModelReference: '<S1>/PW_PSG_BackL' */
  powerwindow_powerwindow_control_Start(&(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Up_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_BackL_DWork.Debounce_Down_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S1>/PW_PSG_BackL' */
  powerwindow_powerwindow_control_Init(&powerwindow_PW_Control_PSG_BackL_Y.window_up,
    &powerwindow_PW_Control_PSG_BackL_Y.window_down, &powerwindow_PW_Control_PSG_BackL_Y.overcurrent,
    &powerwindow_PW_Control_PSG_BackL_Y.pinch, &powerwindow_PW_Control_PSG_BackL_Y.wake,
    &(powerwindow_PW_Control_PSG_BackL_DWork.PW_PSG_BackL_DWORK1.rtdw));
}

/* Model terminate function */
void powerwindow_PW_Control_PSG_BackL_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_PW_Control_PSG_BackR.c

 Author: CoSys-Lab, University of Antwerp

 Function: PW_Control_DRV realizes the functionality of back-right passenger side powerwindow. It connects the 3 smaller modules together.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow//powerwindow_PW_Control_PSG_BackR.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/


#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackR.h"

#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_BackR_private.h"

/*
 Forward declaration of functions
*/

void powerwindow_PW_Control_PSG_BackR_initialize(void);
void powerwindow_PW_Control_PSG_BackR_terminate(void);
void powerwindow_PW_Control_PSG_BackR_main(void);


/* Block states (auto storage) */
powerwindow_D_Work_PW_Control_PSG_BackR powerwindow_PW_Control_PSG_BackR_DWork;

/* External inputs (root inport signals with auto storage) */
powerwindow_ExternalInputs_PW_Control_PSG_BackR powerwindow_PW_Control_PSG_BackR_U;

/* External outputs (root outports fed by signals with auto storage) */
powerwindow_ExternalOutputs_PW_Control_PSG_BackR powerwindow_PW_Control_PSG_BackR_Y;

/* Real-time model */
powerwindow_RT_MODEL_PW_Control_PSG_BackR powerwindow_PW_Control_PSG_BackR_M_;
powerwindow_RT_MODEL_PW_Control_PSG_BackR *const powerwindow_PW_Control_PSG_BackR_M = &powerwindow_PW_Control_PSG_BackR_M_;

/* Model step function */
void powerwindow_PW_Control_PSG_BackR_main(void)
{
  /* local block i/o variables */
  powerwindow_boolean_T rtb_debounce_Up;
  powerwindow_boolean_T rtb_debounce_Down;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_BackR_o1;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_BackR_o2;

  /* ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_BackR_U.Up_PSG_BackR, &rtb_debounce_Up,
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtzce));

  /* ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_BackR_U.Down_PSG_BackR, &rtb_debounce_Down,
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtzce));

  /* ModelReference: '<S1>/ControlEx_PSG_BackR' */
  powerwindow_controlexclusion_main(&powerwindow_PW_Control_PSG_BackR_U.Up_DRV, &powerwindow_PW_Control_PSG_BackR_U.Down_DRV,
                   &rtb_debounce_Up, &rtb_debounce_Down,
                   &powerwindow_rtb_ControlEx_PSG_BackR_o1, &powerwindow_rtb_ControlEx_PSG_BackR_o2);

  /* ModelReference: '<S1>/PW_PSG_BackR' */
  powerwindow_powerwindow_control_main(&powerwindow_rtb_ControlEx_PSG_BackR_o1, &powerwindow_rtb_ControlEx_PSG_BackR_o2,
                      &powerwindow_PW_Control_PSG_BackR_U.endofdetectionrange,
                      &powerwindow_PW_Control_PSG_BackR_U.currentsense,
                      &powerwindow_PW_Control_PSG_BackR_Y.window_up, &powerwindow_PW_Control_PSG_BackR_Y.window_down,
                      &powerwindow_PW_Control_PSG_BackR_Y.overcurrent, &powerwindow_PW_Control_PSG_BackR_Y.pinch,
                      &powerwindow_PW_Control_PSG_BackR_Y.wake,
                      &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtzce));
}

/* Model initialize function */
void powerwindow_PW_Control_PSG_BackR_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  powerwindow_PW_PSG_BackR_rtmSetErrorStatus(powerwindow_PW_Control_PSG_BackR_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackR_DWork, 0,
                sizeof(powerwindow_D_Work_PW_Control_PSG_BackR));

  /* external inputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackR_U, 0,
                sizeof(powerwindow_ExternalInputs_PW_Control_PSG_BackR));

  /* external outputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_BackR_Y, 0,
                sizeof(powerwindow_ExternalOutputs_PW_Control_PSG_BackR));

  /* Model Initialize fcn for ModelReference Block: '<S1>/ControlEx_PSG_BackR' */
  powerwindow_controlexclusion_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Down' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_BackR_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackR_M),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Up' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_BackR_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackR_M),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S1>/PW_PSG_BackR' */
  powerwindow_powerwindow_control_initialize(powerwindow_PW_PSG_BackR_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_BackR_M),
    &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtm),
    &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtb),
    &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtdw),
    &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtzce));

  /* Start for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtdw));

  /* Start for ModelReference: '<S1>/PW_PSG_BackR' */
  powerwindow_powerwindow_control_Start(&(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Up_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_BackR_DWork.Debounce_Down_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S1>/PW_PSG_BackR' */
  powerwindow_powerwindow_control_Init(&powerwindow_PW_Control_PSG_BackR_Y.window_up,
    &powerwindow_PW_Control_PSG_BackR_Y.window_down, &powerwindow_PW_Control_PSG_BackR_Y.overcurrent,
    &powerwindow_PW_Control_PSG_BackR_Y.pinch, &powerwindow_PW_Control_PSG_BackR_Y.wake,
    &(powerwindow_PW_Control_PSG_BackR_DWork.PW_PSG_BackR_DWORK1.rtdw));
}

/* Model terminate function */
void powerwindow_PW_Control_PSG_BackR_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_PW_Control_PSG_Front.c

 Author: CoSys-Lab, University of Antwerp

 Function: PW_Control_DRV realizes the functionality of front passenger side powerwindow. It connects the 3 smaller modules together.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow//powerwindow_powerwindow_control.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/

#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_Front.h"

#include "powerwindow_HeaderFiles/powerwindow_PW_Control_PSG_Front_private.h"

/*
 Forward declaration of functions
*/

void powerwindow_PW_Control_PSG_Front_initialize(void);
void powerwindow_PW_Control_PSG_Front_terminate(void);
void powerwindow_PW_Control_PSG_Front_main(void);


/* Block states (auto storage) */
powerwindow_D_Work_PW_Control_PSG_Front powerwindow_PW_Control_PSG_Front_DWork;

/* External inputs (root inport signals with auto storage) */
powerwindow_ExternalInputs_PW_Control_PSG_Front powerwindow_PW_Control_PSG_Front_U;

/* External outputs (root outports fed by signals with auto storage) */
powerwindow_ExternalOutputs_PW_Control_PSG_Front powerwindow_PW_Control_PSG_Front_Y;

/* Real-time model */
powerwindow_RT_MODEL_PW_Control_PSG_Front powerwindow_PW_Control_PSG_Front_M_;
powerwindow_RT_MODEL_PW_Control_PSG_Front *const powerwindow_PW_Control_PSG_Front_M = &powerwindow_PW_Control_PSG_Front_M_;

/* Model step function */
void powerwindow_PW_Control_PSG_Front_main(void)
{
  /* local block i/o variables */
  powerwindow_boolean_T rtb_debounce_Up;
  powerwindow_boolean_T rtb_debounce_Down;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_Front_Front_o1;
  powerwindow_boolean_T powerwindow_rtb_ControlEx_PSG_Front_Front_o2;

  /* ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_Front_U.Up_PSG_Front, &rtb_debounce_Up,
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtzce));

  /* ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_main(&powerwindow_PW_Control_PSG_Front_U.Down_PSG_Front, &rtb_debounce_Down,
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtb),
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtdw),
           &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtzce));

  /* ModelReference: '<S1>/ControlEx_PSG_Front_Front' */
  powerwindow_controlexclusion_main(&powerwindow_PW_Control_PSG_Front_U.Up_DRV, &powerwindow_PW_Control_PSG_Front_U.Down_DRV,
                   &rtb_debounce_Up, &rtb_debounce_Down,
                   &powerwindow_rtb_ControlEx_PSG_Front_Front_o1, &powerwindow_rtb_ControlEx_PSG_Front_Front_o2);

  /* ModelReference: '<S1>/PW_PSG_Front_Front' */
  powerwindow_powerwindow_control_main(&powerwindow_rtb_ControlEx_PSG_Front_Front_o1, &powerwindow_rtb_ControlEx_PSG_Front_Front_o2,
                      &powerwindow_PW_Control_PSG_Front_U.endofdetectionrange,
                      &powerwindow_PW_Control_PSG_Front_U.currentsense,
                      &powerwindow_PW_Control_PSG_Front_Y.window_up, &powerwindow_PW_Control_PSG_Front_Y.window_down,
                      &powerwindow_PW_Control_PSG_Front_Y.overcurrent, &powerwindow_PW_Control_PSG_Front_Y.pinch,
                      &powerwindow_PW_Control_PSG_Front_Y.wake,
                      &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtzce));
}

/* Model initialize function */
void powerwindow_PW_Control_PSG_Front_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  powerwindow_PW_PSG_Front_rtmSetErrorStatus(powerwindow_PW_Control_PSG_Front_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&powerwindow_PW_Control_PSG_Front_DWork, 0,
                sizeof(powerwindow_D_Work_PW_Control_PSG_Front));

  /* external inputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_Front_U, 0,
                sizeof(powerwindow_ExternalInputs_PW_Control_PSG_Front));

  /* external outputs */
  (void) memset((void *)&powerwindow_PW_Control_PSG_Front_Y, 0,
                sizeof(powerwindow_ExternalOutputs_PW_Control_PSG_Front));

  /* Model Initialize fcn for ModelReference Block: '<S1>/ControlEx_PSG_Front_Front' */
  powerwindow_controlexclusion_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Down' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_Front_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_Front_M),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S2>/debounce_Up' */
  powerwindow_debounce_initialize(powerwindow_PW_PSG_Front_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_Front_M),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtm),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtb),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtdw),
                      &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtzce));

  /* Model Initialize fcn for ModelReference Block: '<S1>/PW_PSG_Front_Front' */
  powerwindow_powerwindow_control_initialize(powerwindow_PW_PSG_Front_rtmGetErrorStatusPointer(powerwindow_PW_Control_PSG_Front_M),
    &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtm),
    &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtb),
    &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtdw),
    &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtzce));

  /* Start for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtdw));

  /* Start for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Start(&(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtdw));

  /* Start for ModelReference: '<S1>/PW_PSG_Front_Front' */
  powerwindow_powerwindow_control_Start(&(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Up' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Up_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S2>/debounce_Down' */
  powerwindow_debounce_Init(&(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtb),
                &(powerwindow_PW_Control_PSG_Front_DWork.Debounce_Down_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<S1>/PW_PSG_Front_Front' */
  powerwindow_powerwindow_control_Init(&powerwindow_PW_Control_PSG_Front_Y.window_up,
    &powerwindow_PW_Control_PSG_Front_Y.window_down, &powerwindow_PW_Control_PSG_Front_Y.overcurrent,
    &powerwindow_PW_Control_PSG_Front_Y.pinch, &powerwindow_PW_Control_PSG_Front_Y.wake,
    &(powerwindow_PW_Control_PSG_Front_DWork.PW_PSG_Front_Front_DWORK1.rtdw));
}

/* Model terminate function */
void powerwindow_PW_Control_PSG_Front_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_const_params.c

 Author: CoSys-Lab, University of Antwerp

 Function: the lookup table for stateflow chart in powerwindow_powerwindow_control

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow/powerwindow_const_params.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/

#include "powerwindow_HeaderFiles/powerwindow_rtwtypes.h"

extern const powerwindow_boolean_T powerwindow_rtCP_pooled_6bUUQf1tASYw[12];
const powerwindow_boolean_T powerwindow_rtCP_pooled_6bUUQf1tASYw[12] = { 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1,
  0 } ;
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_controlexclusion

 Author: CoSys-Lab, University of Antwerp

 Function: powerwindow_controlexclusion is one functionality of the power window benchmark.
 	 It takes the input signal from the driver and the passenger to determine the final control signal.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow/powerwindow_controlexclusion.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/



#include "powerwindow_HeaderFiles/powerwindow_controlexclusion.h"
#include "powerwindow_HeaderFiles/powerwindow_controlexclusion_private.h"

/*
  Forward declaration of functions
*/

void powerwindow_controlexclusion_initialize(void);
void powerwindow_controlexclusion_terminate(void);
void powerwindow_controlexclusion_main(const powerwindow_boolean_T *rtu_Up_DRV, const powerwindow_boolean_T *rtu_Down_DRV,
                      const powerwindow_boolean_T *rtu_Up_PSG, const powerwindow_boolean_T *rtu_Down_PSG,
                      powerwindow_boolean_T *rty_Up, powerwindow_boolean_T *rty_Down);

/* Model initialize function */
void powerwindow_controlexclusion_initialize(void)
{
  /* (no initialization code required) */
}

/* Model terminate function */
void powerwindow_controlexclusion_terminate(void)
{
    /* (no terminate code required) */
}


/*
 Algorithm core functions
 */

/* Output and update for referenced model: 'ControlExclusion' */
void powerwindow_controlexclusion_main(const powerwindow_boolean_T *rtu_Up_DRV, const powerwindow_boolean_T *rtu_Down_DRV,
                      const powerwindow_boolean_T *rtu_Up_PSG, const powerwindow_boolean_T *rtu_Down_PSG,
                      powerwindow_boolean_T *rty_Up, powerwindow_boolean_T *rty_Down)
{
  /* Logic: '<S2>/Logical Operator11' incorporates:
   *  Logic: '<S2>/Logical Operator2'
   *  Logic: '<S2>/Logical Operator3'
   *  Logic: '<S2>/Logical Operator5'
   *  Logic: '<S2>/Logical Operator6'
   *  Logic: '<S2>/Logical Operator7'
   */
  *rty_Up = !(((!*rtu_Up_DRV) && (*rtu_Down_DRV)) || ((*rtu_Down_DRV) &&
    (!*rtu_Up_PSG) && (*rtu_Down_PSG)));

  /* Logic: '<S2>/Logical Operator12' incorporates:
   *  Logic: '<S2>/Logical Operator1'
   *  Logic: '<S2>/Logical Operator10'
   *  Logic: '<S2>/Logical Operator4'
   *  Logic: '<S2>/Logical Operator8'
   *  Logic: '<S2>/Logical Operator9'
   */
  *rty_Down = !(((*rtu_Up_DRV) && (!*rtu_Down_DRV)) || ((*rtu_Up_DRV) &&
    (*rtu_Up_PSG) && (!*rtu_Down_PSG)));
}



/*
 * File trailer for generated code.
 *
 * [EOF]
 */
/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_debounce.c

 Author: CoSys-Lab, University of Antwerp

 Function: powerwindow_debounce_main is used to powerwindow_debounce_main the push-down button of the power window.
 	 In order to input a manual switch signal into a digital circuit,
 	 debouncing is necessary so that a single press does not appear like multiple presses.
 	 Without debouncing, pressing the button once may cause unpredictable results.
 	 powerwindow_debounce_main.c defines all the functions that will be used in debounce_PSG_Front which is a part of the Power window.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow/powerwindow_debounce.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

 */


#include "powerwindow_HeaderFiles/powerwindow_debounce.h"
#include "powerwindow_HeaderFiles/powerwindow_debounce_private.h"

/* Named constants for Chart: '<Root>/Chart' */
#define debounce_IN_debounce           ((powerwindow_uint8_T)1U)
#define debounce_IN_NO_ACTIVE_CHILD    ((powerwindow_uint8_T)0U)
#define debounce_IN_Off                ((powerwindow_uint8_T)2U)
#define debounce_IN_Off_h              ((powerwindow_uint8_T)1U)
#define debounce_IN_On                 ((powerwindow_uint8_T)3U)
#define debounce_IN_On_b               ((powerwindow_uint8_T)2U)

/*
 Forward declaration of functions
*/

void powerwindow_debounce_Init(powerwindow_rtB_debounce_T *, powerwindow_rtDW_debounce_T *);
void powerwindow_debounce_Start(powerwindow_rtDW_debounce_T *);
void powerwindow_debounce_initialize(const powerwindow_char_T **, powerwindow_RT_MODEL_debounce_T * const,
                         powerwindow_rtB_debounce_T *, powerwindow_rtDW_debounce_T *, powerwindow_rtZCE_debounce_T *);
void powerwindow_debounce_main(const powerwindow_boolean_T *, powerwindow_boolean_T *, powerwindow_rtB_debounce_T *,
              powerwindow_rtDW_debounce_T *, powerwindow_rtZCE_debounce_T *);

/*
 Initialization- and return-value-related functions
 */

/* Initial conditions for referenced model: 'powerwindow_debounce_main' */
void powerwindow_debounce_Init(powerwindow_rtB_debounce_T *localB, powerwindow_rtDW_debounce_T *localDW)
{
    /* InitializeConditions for Chart: '<Root>/Chart' */
    localDW->is_debounce = debounce_IN_NO_ACTIVE_CHILD;
    localDW->temporalCounter_i1 = 0U;
    localDW->is_active_c3_debounce = 0U;
    localDW->is_c3_debounce = debounce_IN_NO_ACTIVE_CHILD;
    localB->Q = false;
}

/* Start for referenced model: 'powerwindow_debounce_main' */
void powerwindow_debounce_Start(powerwindow_rtDW_debounce_T *localDW)
{
    /* Start for DiscretePulseGenerator: '<Root>/period of 10ms' */
    localDW->clockTickCounter = 0L;
}

/* Model initialize function */
void powerwindow_debounce_initialize(const powerwindow_char_T **rt_errorStatus, powerwindow_RT_MODEL_debounce_T *
                         const debounce_M, powerwindow_rtB_debounce_T *localB, powerwindow_rtDW_debounce_T *localDW,
                         powerwindow_rtZCE_debounce_T *localZCE)
{
    /* Registration code */

    /* initialize error status */
	powerwindow_rtmSetErrorStatusPointer(debounce_M, rt_errorStatus);

    /* block I/O */
    (void) memset(((void *) localB), 0,
                  sizeof(powerwindow_rtB_debounce_T));

    /* states (dwork) */
    (void) memset((void *)localDW, 0,
                  sizeof(powerwindow_rtDW_debounce_T));
    localZCE->Chart_Trig_ZCE = powerwindow_POS_ZCSIG;
}

/*
 Algorithm core functions
 */

/* Output and update for referenced model: 'powerwindow_debounce_main' */
void powerwindow_debounce_main(const powerwindow_boolean_T *rtu_Switch, powerwindow_boolean_T *rty_debounced_Switch,
              powerwindow_rtB_debounce_T *localB, powerwindow_rtDW_debounce_T *localDW, powerwindow_rtZCE_debounce_T
              *localZCE)
{
    powerwindow_int16_T rtb_periodof10ms;

    /* DiscretePulseGenerator: '<Root>/period of 10ms' */
    rtb_periodof10ms = (localDW->clockTickCounter < 1L) &&
                       (localDW->clockTickCounter >= 0L) ? 1 : 0;
    if (localDW->clockTickCounter >= 1L) {
        localDW->clockTickCounter = 0L;
    } else {
        localDW->clockTickCounter++;
    }

    /* End of DiscretePulseGenerator: '<Root>/period of 10ms' */

    /* Chart: '<Root>/Chart' incorporates:
     *  TriggerPort: '<S1>/ticks'
     */
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    if ((rtb_periodof10ms != 0) && (localZCE->Chart_Trig_ZCE != powerwindow_POS_ZCSIG)) {
        /* Gateway: Chart */
        if (localDW->temporalCounter_i1 < 7U) {
            localDW->temporalCounter_i1++;
        }

        /* Event: '<S1>:13' */
        /* During: Chart */
        if (localDW->is_active_c3_debounce == 0U) {
            /* Entry: Chart */
            localDW->is_active_c3_debounce = 1U;

            /* Entry Internal: Chart */
            /* Transition: '<S1>:9' */
            localDW->is_c3_debounce = debounce_IN_Off;

            /* Entry 'Off': '<S1>:1' */
            localB->Q = true;
        } else {
            switch (localDW->is_c3_debounce) {
            case debounce_IN_debounce:
                /* During 'powerwindow_debounce_main': '<S1>:6' */
                if (localDW->is_debounce == debounce_IN_Off_h) {
                    /* During 'Off': '<S1>:8' */
                    if ((powerwindow_int16_T)*rtu_Switch < 1) {
                        /* Transition: '<S1>:12' */
                        localDW->is_debounce = debounce_IN_On_b;
                        localDW->temporalCounter_i1 = 0U;
                    } else {
                        if (localDW->temporalCounter_i1 >= 3) {
                            /* Transition: '<S1>:16' */
                            localDW->is_debounce = debounce_IN_NO_ACTIVE_CHILD;
                            localDW->is_c3_debounce = debounce_IN_Off;

                            /* Entry 'Off': '<S1>:1' */
                            localB->Q = true;
                        }
                    }
                } else {
                    /* During 'On': '<S1>:7' */
                    if ((powerwindow_int16_T)*rtu_Switch > 0) {
                        /* Transition: '<S1>:11' */
                        localDW->is_debounce = debounce_IN_Off_h;
                        localDW->temporalCounter_i1 = 0U;
                    } else {
                        if (localDW->temporalCounter_i1 >= 3) {
                            /* Transition: '<S1>:14' */
                            localDW->is_debounce = debounce_IN_NO_ACTIVE_CHILD;
                            localDW->is_c3_debounce = debounce_IN_On;

                            /* Entry 'On': '<S1>:5' */
                            localB->Q = false;
                        }
                    }
                }
                break;

            case debounce_IN_Off:
                /* During 'Off': '<S1>:1' */
                if ((powerwindow_int16_T)*rtu_Switch < 1) {
                    /* Transition: '<S1>:10' */
                    localDW->is_c3_debounce = debounce_IN_debounce;
                    localDW->is_debounce = debounce_IN_On_b;
                    localDW->temporalCounter_i1 = 0U;
                }
                break;

            default:
                /* During 'On': '<S1>:5' */
                if ((powerwindow_int16_T)*rtu_Switch > 0) {
                    /* Transition: '<S1>:15' */
                    localDW->is_c3_debounce = debounce_IN_debounce;
                    localDW->is_debounce = debounce_IN_Off_h;
                    localDW->temporalCounter_i1 = 0U;
                }
                break;
            }
        }
    }

    localZCE->Chart_Trig_ZCE = (powerwindow_uint8_T)(rtb_periodof10ms != 0 ? (powerwindow_int16_T)
                                         powerwindow_POS_ZCSIG : (powerwindow_int16_T)powerwindow_ZERO_ZCSIG);

    /* End of DataTypeConversion: '<Root>/Data Type Conversion' */

    /* DataTypeConversion: '<Root>/Data Type Conversion2' */
    *rty_debounced_Switch = localB->Q;
}


/*
 * File trailer for generated code.
 *
 * [EOF]
 */
	#include "powerwindow_HeaderFiles/powerwindow_rtwtypes.h"
	
	
	powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Up_Input_DRV_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1};

	powerwindow_boolean_T powerwindow_debounce_Driver_DRV_U_Down_Input_DRV_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1};

	powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Up_Input_Front_Array[977] = {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	powerwindow_boolean_T powerwindow_debounce_Driver_Front_U_Down_Input_Front_Array[977] = {0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1};

	powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Up_Input_BackL_Array[977] = {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_boolean_T powerwindow_debounce_Driver_BackL_U_Down_Input_BackL_Array[977] = {0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Up_Input_BackR_Array[977] = {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_boolean_T powerwindow_debounce_Driver_BackR_U_Down_Input_BackR_Array[977] = {0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_DRV_Array[977] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_DRV_Array[977] = {92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 94, 92, 92, 92, 92, 92, 92, 92, 92, 92, 20, 73, 20, 73, 92, 94, 29, 72, 88, 72, 22, 93, 89, 89, 72, 93, 70, 30, 17, 72, 92, 121, 69, 24, 8, 93, 84, 94, 14, 72, 68, 84, 92, 84, 8, 92, 85, 29, 85, 72, 92, 92, 29, 93, 29, 92, 92, 72, 92, 92, 92, 92, 92, 124, 124, 92, 92, 93, 105, 72, 69, 21, 30, 88, 120, 93, 116, 16, 94, 0, 84, 116, 94, 65, 64, 94, 86, 73, 74, 72, 21, 85, 116, 92, 0, 92, 116, 88, 80, 8, 92, 84, 117, 4, 8, 29, 76, 1, 85, 72, 92, 84, 124, 84, 64, 93, 29, 93, 81, 188, 124, 124, 124, 93, 0, 29, 28, 24, 69, 8, 92, 92, 92, 116, 116, 120, 116, 93, 94, 93, 94, 124, 122, 124, 100, 116, 116, 116, 116, 116, 124, 116, 93, 93, 93, 10, 101, 94, 102, 98, 97, 1, 97, 97, 97, 117, 94, 93, 94, 93, 94, 2, 2, 93, 8, 93, 92, 88, 80, 81, 80, 100, 80, 112, 112, 117, 96, 96, 80, 93, 92, 124, 89, 84, 112, 112, 117, 118, 16, 124, 94, 94, 94, 124, 93, 8, 94, 14, 72, 104, 28, 68, 0, 72, 188, 93, 120, 93, 88, 10, 28, 20, 20, 88, 120, 88, 176, 93, 120, 88, 93, 120, 29, 120, 93, 92, 93, 93, 117, 120, 120, 93, 120, 89, 114, 197, 189, 93, 121, 112, 124, 93, 93, 197, 197, 94, 189, 197, 5, 133, 157, 197, 112, 65, 121, 25, 186, 93, 120, 122, 94, 149, 200, 149, 149, 157, 150, 145, 156, 149, 148, 146, 150, 148, 150, 130, 150, 150, 150, 150, 146, 150, 150, 149, 149, 134, 149, 130, 129, 148, 149, 148, 150, 128, 197, 132, 148, 140, 132, 8, 20, 84, 88, 76, 64, 20, 20, 68, 68, 28, 28, 28, 29, 28, 28, 8, 28, 20, 28, 28, 28, 28, 28, 13, 13, 22, 13, 70, 12, 76, 24, 24, 24, 72, 24, 28, 13, 13, 24, 24, 24, 14, 14, 13, 14, 14, 78, 13, 14, 14, 14, 13, 13, 30, 5, 8, 4, 20, 20, 4, 4, 4, 68, 28, 4, 8, 4, 12, 4, 4, 4, 68, 68, 92, 4, 68, 4, 4, 4, 28, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 76, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 12, 12, 12, 12, 12, 12, 14, 12, 72, 14, 13, 12, 14, 14, 13, 14, 72, 13, 13, 72, 93, 13, 88, 94, 72, 94, 93, 94, 94, 94, 72, 94, 93, 94, 93, 88, 88, 72, 88, 92, 12, 88, 4, 84, 84, 84, 84, 84, 84, 84, 84, 84, 68, 4, 68, 24, 24, 24, 12, 24, 88, 28, 88, 12, 72, 12, 12, 88, 12, 12, 12, 12, 12, 12, 12, 72, 12, 12, 8, 20, 20, 133, 133, 157, 157, 93, 137, 157, 93, 148, 0, 148, 144, 152, 156, 140, 137, 128, 158, 144, 129, 144, 128, 130, 129, 132, 156, 156, 72, 120, 93, 93, 93, 86, 137, 178, 113, 93, 92, 85, 5, 188, 188, 6, 188, 17, 21, 197, 22, 194, 196, 188, 10, 100, 192, 138, 154, 137, 92, 137, 200, 200, 200, 6, 133, 128, 136, 200, 0, 192, 188, 153, 140, 72, 200, 200, 92, 200, 124, 196, 124, 116, 116, 116, 124, 124, 121, 124, 124, 200, 124, 92, 185, 5, 117, 124, 120, 82, 88, 112, 188, 193, 198, 72, 20, 4, 120, 97, 97, 121, 112, 93, 121, 113, 80, 80, 93, 88, 89, 93, 88, 92, 88, 104, 89, 89, 89, 92, 88, 121, 121, 122, 88, 94, 84, 84, 124, 116, 94, 104, 124, 94, 108, 94, 88, 88, 84, 116, 80, 94, 88, 88, 92, 25, 88, 120, 104, 108, 1, 94, 96, 96, 98, 82, 104, 20, 92, 98, 14, 94, 6, 101, 109, 22, 120, 105, 93, 120, 81, 120, 2, 1, 94, 18, 18, 120, 94, 66, 2, 104, 68, 94, 73, 82, 101, 121, 93, 113, 22, 93, 120, 94, 93, 5, 13, 122, 89, 90, 5, 101, 93, 106, 94, 73, 21, 72, 89, 121, 73, 92, 93, 84, 117, 0, 21, 85, 52, 4, 6, 198, 0, 185, 192, 29, 194, 189, 36, 36, 93, 185, 52, 0, 4, 13, 188, 9, 28, 89, 86, 185, 113, 186, 186, 14, 185, 188, 186, 188, 88, 189, 188, 116, 124, 68, 188, 188, 188, 198, 84, 52, 188, 197, 185, 20, 190, 5, 6, 190, 28, 128, 189, 189, 189, 93, 189, 14, 94, 189, 68, 190, 190, 190, 157, 84, 141, 197, 189, 197, 93, 189, 37, 190, 190, 22, 190, 190, 190, 190, 86, 190, 190, 190, 189, 94, 190, 190, 190, 190, 86, 198, 190, 189, 200, 94, 94, 6, 190, 5, 86, 2, 190, 190, 33, 0, 28, 68, 16, 80, 144, 144, 49, 52, 116, 76, 84, 49, 196, 197, 93, 17, 73, 137, 185, 93, 185, 188, 188, 185, 13, 185, 186, 186, 186, 14, 188, 186, 186, 188, 6, 186, 188, 188, 188, 84, 188, 188, 188, 188, 84, 188, 188, 188, 188, 69, 188, 189, 188, 188, 84, 188, 189, 189, 189, 24, 189, 189, 189, 189, 86, 52, 189, 93, 149, 84, 189, 93, 141, 189, 84, 189, 189, 190, 190, 86, 190, 198, 142, 190, 86, 190, 142, 190, 190, 76, 118, 142, 94, 94, 86, 198, 134, 198, 198, 85, 6, 6, 46, 38, 85, 190, 190, 186, 190, 64, 69, 69, 77, 86, 88, 28, 93, 84, 116, 0, 0 };

	powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Up_Front_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_debounce_passenger_Front_U_Down_Front_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_Front_Array[977] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_Front_Array[977] = {92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 94, 92, 92, 92, 92, 92, 92, 92, 92, 92, 20, 73, 20, 73, 92, 94, 29, 72, 88, 72, 22, 93, 89, 89, 72, 93, 70, 30, 17, 72, 92, 121, 69, 24, 8, 93, 84, 94, 14, 72, 68, 84, 92, 84, 8, 92, 85, 29, 85, 72, 92, 92, 29, 93, 29, 92, 92, 72, 92, 92, 92, 92, 92, 124, 124, 92, 92, 93, 105, 72, 69, 21, 30, 88, 120, 93, 116, 16, 94, 0, 84, 116, 94, 65, 64, 94, 86, 73, 74, 72, 21, 85, 116, 92, 0, 92, 116, 88, 80, 8, 92, 84, 117, 4, 8, 29, 76, 1, 85, 72, 92, 84, 124, 84, 64, 93, 29, 93, 81, 188, 124, 124, 124, 93, 0, 29, 28, 24, 69, 8, 92, 92, 92, 116, 116, 120, 116, 93, 94, 93, 94, 124, 122, 124, 100, 116, 116, 116, 116, 116, 124, 116, 93, 93, 93, 10, 101, 94, 102, 98, 97, 1, 97, 97, 97, 117, 94, 93, 94, 93, 94, 2, 2, 93, 8, 93, 92, 88, 80, 81, 80, 100, 80, 112, 112, 117, 96, 96, 80, 93, 92, 124, 89, 84, 112, 112, 117, 118, 16, 124, 94, 94, 94, 124, 93, 8, 94, 14, 72, 104, 28, 68, 0, 72, 188, 93, 120, 93, 88, 10, 28, 20, 20, 88, 120, 88, 176, 93, 120, 88, 93, 120, 29, 120, 93, 92, 93, 93, 117, 120, 120, 93, 120, 89, 114, 197, 189, 93, 121, 112, 124, 93, 93, 197, 197, 94, 189, 197, 5, 133, 157, 197, 112, 65, 121, 25, 186, 93, 120, 122, 94, 149, 200, 149, 149, 157, 150, 145, 156, 149, 148, 146, 150, 148, 150, 130, 150, 150, 150, 150, 146, 150, 150, 149, 149, 134, 149, 130, 129, 148, 149, 148, 150, 128, 197, 132, 148, 140, 132, 8, 20, 84, 88, 76, 64, 20, 20, 68, 68, 28, 28, 28, 29, 28, 28, 8, 28, 20, 28, 28, 28, 28, 28, 13, 13, 22, 13, 70, 12, 76, 24, 24, 24, 72, 24, 28, 13, 13, 24, 24, 24, 14, 14, 13, 14, 14, 78, 13, 14, 14, 14, 13, 13, 30, 5, 8, 4, 20, 20, 4, 4, 4, 68, 28, 4, 8, 4, 12, 4, 4, 4, 68, 68, 92, 4, 68, 4, 4, 4, 28, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 76, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 12, 12, 12, 12, 12, 12, 14, 12, 72, 14, 13, 12, 14, 14, 13, 14, 72, 13, 13, 72, 93, 13, 88, 94, 72, 94, 93, 94, 94, 94, 72, 94, 93, 94, 93, 88, 88, 72, 88, 92, 12, 88, 4, 84, 84, 84, 84, 84, 84, 84, 84, 84, 68, 4, 68, 24, 24, 24, 12, 24, 88, 28, 88, 12, 72, 12, 12, 88, 12, 12, 12, 12, 12, 12, 12, 72, 12, 12, 8, 20, 20, 133, 133, 157, 157, 93, 137, 157, 93, 148, 0, 148, 144, 152, 156, 140, 137, 128, 158, 144, 129, 144, 128, 130, 129, 132, 156, 156, 72, 120, 93, 93, 93, 86, 137, 178, 113, 93, 92, 85, 5, 188, 188, 6, 188, 17, 21, 197, 22, 194, 196, 188, 10, 100, 192, 138, 154, 137, 92, 137, 200, 200, 200, 6, 133, 128, 136, 200, 0, 192, 188, 153, 140, 72, 200, 200, 92, 200, 124, 196, 124, 116, 116, 116, 124, 124, 121, 124, 124, 200, 124, 92, 185, 5, 117, 124, 120, 82, 88, 112, 188, 193, 198, 72, 20, 4, 120, 97, 97, 121, 112, 93, 121, 113, 80, 80, 93, 88, 89, 93, 88, 92, 88, 104, 89, 89, 89, 92, 88, 121, 121, 122, 88, 94, 84, 84, 124, 116, 94, 104, 124, 94, 108, 94, 88, 88, 84, 116, 80, 94, 88, 88, 92, 25, 88, 120, 104, 108, 1, 94, 96, 96, 98, 82, 104, 20, 92, 98, 14, 94, 6, 101, 109, 22, 120, 105, 93, 120, 81, 120, 2, 1, 94, 18, 18, 120, 94, 66, 2, 104, 68, 94, 73, 82, 101, 121, 93, 113, 22, 93, 120, 94, 93, 5, 13, 122, 89, 90, 5, 101, 93, 106, 94, 73, 21, 72, 89, 121, 73, 92, 93, 84, 117, 0, 21, 85, 52, 4, 6, 198, 0, 185, 192, 29, 194, 189, 36, 36, 93, 185, 52, 0, 4, 13, 188, 9, 28, 89, 86, 185, 113, 186, 186, 14, 185, 188, 186, 188, 88, 189, 188, 116, 124, 68, 188, 188, 188, 198, 84, 52, 188, 197, 185, 20, 190, 5, 6, 190, 28, 128, 189, 189, 189, 93, 189, 14, 94, 189, 68, 190, 190, 190, 157, 84, 141, 197, 189, 197, 93, 189, 37, 190, 190, 22, 190, 190, 190, 190, 86, 190, 190, 190, 189, 94, 190, 190, 190, 190, 86, 198, 190, 189, 200, 94, 94, 6, 190, 5, 86, 2, 190, 190, 33, 0, 28, 68, 16, 80, 144, 144, 49, 52, 116, 76, 84, 49, 196, 197, 93, 17, 73, 137, 185, 93, 185, 188, 188, 185, 13, 185, 186, 186, 186, 14, 188, 186, 186, 188, 6, 186, 188, 188, 188, 84, 188, 188, 188, 188, 84, 188, 188, 188, 188, 69, 188, 189, 188, 188, 84, 188, 189, 189, 189, 24, 189, 189, 189, 189, 86, 52, 189, 93, 149, 84, 189, 93, 141, 189, 84, 189, 189, 190, 190, 86, 190, 198, 142, 190, 86, 190, 142, 190, 190, 76, 118, 142, 94, 94, 86, 198, 134, 198, 198, 85, 6, 6, 46, 38, 85, 190, 190, 186, 190, 64, 69, 69, 77, 86, 88, 28, 93, 84, 116, 0, 0 };

	powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Up_BackL_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_debounce_passenger_BackL_U_Down_BackL_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_BackL_Array[977] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_BackL_Array[977] = {92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 94, 92, 92, 92, 92, 92, 92, 92, 92, 92, 20, 73, 20, 73, 92, 94, 29, 72, 88, 72, 22, 93, 89, 89, 72, 93, 70, 30, 17, 72, 92, 121, 69, 24, 8, 93, 84, 94, 14, 72, 68, 84, 92, 84, 8, 92, 85, 29, 85, 72, 92, 92, 29, 93, 29, 92, 92, 72, 92, 92, 92, 92, 92, 124, 124, 92, 92, 93, 105, 72, 69, 21, 30, 88, 120, 93, 116, 16, 94, 0, 84, 116, 94, 65, 64, 94, 86, 73, 74, 72, 21, 85, 116, 92, 0, 92, 116, 88, 80, 8, 92, 84, 117, 4, 8, 29, 76, 1, 85, 72, 92, 84, 124, 84, 64, 93, 29, 93, 81, 188, 124, 124, 124, 93, 0, 29, 28, 24, 69, 8, 92, 92, 92, 116, 116, 120, 116, 93, 94, 93, 94, 124, 122, 124, 100, 116, 116, 116, 116, 116, 124, 116, 93, 93, 93, 10, 101, 94, 102, 98, 97, 1, 97, 97, 97, 117, 94, 93, 94, 93, 94, 2, 2, 93, 8, 93, 92, 88, 80, 81, 80, 100, 80, 112, 112, 117, 96, 96, 80, 93, 92, 124, 89, 84, 112, 112, 117, 118, 16, 124, 94, 94, 94, 124, 93, 8, 94, 14, 72, 104, 28, 68, 0, 72, 188, 93, 120, 93, 88, 10, 28, 20, 20, 88, 120, 88, 176, 93, 120, 88, 93, 120, 29, 120, 93, 92, 93, 93, 117, 120, 120, 93, 120, 89, 114, 197, 189, 93, 121, 112, 124, 93, 93, 197, 197, 94, 189, 197, 5, 133, 157, 197, 112, 65, 121, 25, 186, 93, 120, 122, 94, 149, 200, 149, 149, 157, 150, 145, 156, 149, 148, 146, 150, 148, 150, 130, 150, 150, 150, 150, 146, 150, 150, 149, 149, 134, 149, 130, 129, 148, 149, 148, 150, 128, 197, 132, 148, 140, 132, 8, 20, 84, 88, 76, 64, 20, 20, 68, 68, 28, 28, 28, 29, 28, 28, 8, 28, 20, 28, 28, 28, 28, 28, 13, 13, 22, 13, 70, 12, 76, 24, 24, 24, 72, 24, 28, 13, 13, 24, 24, 24, 14, 14, 13, 14, 14, 78, 13, 14, 14, 14, 13, 13, 30, 5, 8, 4, 20, 20, 4, 4, 4, 68, 28, 4, 8, 4, 12, 4, 4, 4, 68, 68, 92, 4, 68, 4, 4, 4, 28, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 76, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 12, 12, 12, 12, 12, 12, 14, 12, 72, 14, 13, 12, 14, 14, 13, 14, 72, 13, 13, 72, 93, 13, 88, 94, 72, 94, 93, 94, 94, 94, 72, 94, 93, 94, 93, 88, 88, 72, 88, 92, 12, 88, 4, 84, 84, 84, 84, 84, 84, 84, 84, 84, 68, 4, 68, 24, 24, 24, 12, 24, 88, 28, 88, 12, 72, 12, 12, 88, 12, 12, 12, 12, 12, 12, 12, 72, 12, 12, 8, 20, 20, 133, 133, 157, 157, 93, 137, 157, 93, 148, 0, 148, 144, 152, 156, 140, 137, 128, 158, 144, 129, 144, 128, 130, 129, 132, 156, 156, 72, 120, 93, 93, 93, 86, 137, 178, 113, 93, 92, 85, 5, 188, 188, 6, 188, 17, 21, 197, 22, 194, 196, 188, 10, 100, 192, 138, 154, 137, 92, 137, 200, 200, 200, 6, 133, 128, 136, 200, 0, 192, 188, 153, 140, 72, 200, 200, 92, 200, 124, 196, 124, 116, 116, 116, 124, 124, 121, 124, 124, 200, 124, 92, 185, 5, 117, 124, 120, 82, 88, 112, 188, 193, 198, 72, 20, 4, 120, 97, 97, 121, 112, 93, 121, 113, 80, 80, 93, 88, 89, 93, 88, 92, 88, 104, 89, 89, 89, 92, 88, 121, 121, 122, 88, 94, 84, 84, 124, 116, 94, 104, 124, 94, 108, 94, 88, 88, 84, 116, 80, 94, 88, 88, 92, 25, 88, 120, 104, 108, 1, 94, 96, 96, 98, 82, 104, 20, 92, 98, 14, 94, 6, 101, 109, 22, 120, 105, 93, 120, 81, 120, 2, 1, 94, 18, 18, 120, 94, 66, 2, 104, 68, 94, 73, 82, 101, 121, 93, 113, 22, 93, 120, 94, 93, 5, 13, 122, 89, 90, 5, 101, 93, 106, 94, 73, 21, 72, 89, 121, 73, 92, 93, 84, 117, 0, 21, 85, 52, 4, 6, 198, 0, 185, 192, 29, 194, 189, 36, 36, 93, 185, 52, 0, 4, 13, 188, 9, 28, 89, 86, 185, 113, 186, 186, 14, 185, 188, 186, 188, 88, 189, 188, 116, 124, 68, 188, 188, 188, 198, 84, 52, 188, 197, 185, 20, 190, 5, 6, 190, 28, 128, 189, 189, 189, 93, 189, 14, 94, 189, 68, 190, 190, 190, 157, 84, 141, 197, 189, 197, 93, 189, 37, 190, 190, 22, 190, 190, 190, 190, 86, 190, 190, 190, 189, 94, 190, 190, 190, 190, 86, 198, 190, 189, 200, 94, 94, 6, 190, 5, 86, 2, 190, 190, 33, 0, 28, 68, 16, 80, 144, 144, 49, 52, 116, 76, 84, 49, 196, 197, 93, 17, 73, 137, 185, 93, 185, 188, 188, 185, 13, 185, 186, 186, 186, 14, 188, 186, 186, 188, 6, 186, 188, 188, 188, 84, 188, 188, 188, 188, 84, 188, 188, 188, 188, 69, 188, 189, 188, 188, 84, 188, 189, 189, 189, 24, 189, 189, 189, 189, 86, 52, 189, 93, 149, 84, 189, 93, 141, 189, 84, 189, 189, 190, 190, 86, 190, 198, 142, 190, 86, 190, 142, 190, 190, 76, 118, 142, 94, 94, 86, 198, 134, 198, 198, 85, 6, 6, 46, 38, 85, 190, 190, 186, 190, 64, 69, 69, 77, 86, 88, 28, 93, 84, 116, 0, 0 };

	powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Up_BackR_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_debounce_passenger_BackR_U_Down_BackR_Array[977] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1 };

	powerwindow_boolean_T powerwindow_powerwindow_control_U_endofdetectionrange_BackR_Array[977] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 };

	powerwindow_uint8_T powerwindow_powerwindow_control_U_currentsense_BackR_Array[977] = {92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 92, 94, 92, 92, 92, 92, 92, 92, 92, 92, 92, 20, 73, 20, 73, 92, 94, 29, 72, 88, 72, 22, 93, 89, 89, 72, 93, 70, 30, 17, 72, 92, 121, 69, 24, 8, 93, 84, 94, 14, 72, 68, 84, 92, 84, 8, 92, 85, 29, 85, 72, 92, 92, 29, 93, 29, 92, 92, 72, 92, 92, 92, 92, 92, 124, 124, 92, 92, 93, 105, 72, 69, 21, 30, 88, 120, 93, 116, 16, 94, 0, 84, 116, 94, 65, 64, 94, 86, 73, 74, 72, 21, 85, 116, 92, 0, 92, 116, 88, 80, 8, 92, 84, 117, 4, 8, 29, 76, 1, 85, 72, 92, 84, 124, 84, 64, 93, 29, 93, 81, 188, 124, 124, 124, 93, 0, 29, 28, 24, 69, 8, 92, 92, 92, 116, 116, 120, 116, 93, 94, 93, 94, 124, 122, 124, 100, 116, 116, 116, 116, 116, 124, 116, 93, 93, 93, 10, 101, 94, 102, 98, 97, 1, 97, 97, 97, 117, 94, 93, 94, 93, 94, 2, 2, 93, 8, 93, 92, 88, 80, 81, 80, 100, 80, 112, 112, 117, 96, 96, 80, 93, 92, 124, 89, 84, 112, 112, 117, 118, 16, 124, 94, 94, 94, 124, 93, 8, 94, 14, 72, 104, 28, 68, 0, 72, 188, 93, 120, 93, 88, 10, 28, 20, 20, 88, 120, 88, 176, 93, 120, 88, 93, 120, 29, 120, 93, 92, 93, 93, 117, 120, 120, 93, 120, 89, 114, 197, 189, 93, 121, 112, 124, 93, 93, 197, 197, 94, 189, 197, 5, 133, 157, 197, 112, 65, 121, 25, 186, 93, 120, 122, 94, 149, 200, 149, 149, 157, 150, 145, 156, 149, 148, 146, 150, 148, 150, 130, 150, 150, 150, 150, 146, 150, 150, 149, 149, 134, 149, 130, 129, 148, 149, 148, 150, 128, 197, 132, 148, 140, 132, 8, 20, 84, 88, 76, 64, 20, 20, 68, 68, 28, 28, 28, 29, 28, 28, 8, 28, 20, 28, 28, 28, 28, 28, 13, 13, 22, 13, 70, 12, 76, 24, 24, 24, 72, 24, 28, 13, 13, 24, 24, 24, 14, 14, 13, 14, 14, 78, 13, 14, 14, 14, 13, 13, 30, 5, 8, 4, 20, 20, 4, 4, 4, 68, 28, 4, 8, 4, 12, 4, 4, 4, 68, 68, 92, 4, 68, 4, 4, 4, 28, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 76, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 8, 12, 12, 12, 12, 12, 12, 12, 14, 12, 72, 14, 13, 12, 14, 14, 13, 14, 72, 13, 13, 72, 93, 13, 88, 94, 72, 94, 93, 94, 94, 94, 72, 94, 93, 94, 93, 88, 88, 72, 88, 92, 12, 88, 4, 84, 84, 84, 84, 84, 84, 84, 84, 84, 68, 4, 68, 24, 24, 24, 12, 24, 88, 28, 88, 12, 72, 12, 12, 88, 12, 12, 12, 12, 12, 12, 12, 72, 12, 12, 8, 20, 20, 133, 133, 157, 157, 93, 137, 157, 93, 148, 0, 148, 144, 152, 156, 140, 137, 128, 158, 144, 129, 144, 128, 130, 129, 132, 156, 156, 72, 120, 93, 93, 93, 86, 137, 178, 113, 93, 92, 85, 5, 188, 188, 6, 188, 17, 21, 197, 22, 194, 196, 188, 10, 100, 192, 138, 154, 137, 92, 137, 200, 200, 200, 6, 133, 128, 136, 200, 0, 192, 188, 153, 140, 72, 200, 200, 92, 200, 124, 196, 124, 116, 116, 116, 124, 124, 121, 124, 124, 200, 124, 92, 185, 5, 117, 124, 120, 82, 88, 112, 188, 193, 198, 72, 20, 4, 120, 97, 97, 121, 112, 93, 121, 113, 80, 80, 93, 88, 89, 93, 88, 92, 88, 104, 89, 89, 89, 92, 88, 121, 121, 122, 88, 94, 84, 84, 124, 116, 94, 104, 124, 94, 108, 94, 88, 88, 84, 116, 80, 94, 88, 88, 92, 25, 88, 120, 104, 108, 1, 94, 96, 96, 98, 82, 104, 20, 92, 98, 14, 94, 6, 101, 109, 22, 120, 105, 93, 120, 81, 120, 2, 1, 94, 18, 18, 120, 94, 66, 2, 104, 68, 94, 73, 82, 101, 121, 93, 113, 22, 93, 120, 94, 93, 5, 13, 122, 89, 90, 5, 101, 93, 106, 94, 73, 21, 72, 89, 121, 73, 92, 93, 84, 117, 0, 21, 85, 52, 4, 6, 198, 0, 185, 192, 29, 194, 189, 36, 36, 93, 185, 52, 0, 4, 13, 188, 9, 28, 89, 86, 185, 113, 186, 186, 14, 185, 188, 186, 188, 88, 189, 188, 116, 124, 68, 188, 188, 188, 198, 84, 52, 188, 197, 185, 20, 190, 5, 6, 190, 28, 128, 189, 189, 189, 93, 189, 14, 94, 189, 68, 190, 190, 190, 157, 84, 141, 197, 189, 197, 93, 189, 37, 190, 190, 22, 190, 190, 190, 190, 86, 190, 190, 190, 189, 94, 190, 190, 190, 190, 86, 198, 190, 189, 200, 94, 94, 6, 190, 5, 86, 2, 190, 190, 33, 0, 28, 68, 16, 80, 144, 144, 49, 52, 116, 76, 84, 49, 196, 197, 93, 17, 73, 137, 185, 93, 185, 188, 188, 185, 13, 185, 186, 186, 186, 14, 188, 186, 186, 188, 6, 186, 188, 188, 188, 84, 188, 188, 188, 188, 84, 188, 188, 188, 188, 69, 188, 189, 188, 188, 84, 188, 189, 189, 189, 24, 189, 189, 189, 189, 86, 52, 189, 93, 149, 84, 189, 93, 141, 189, 84, 189, 189, 190, 190, 86, 190, 198, 142, 190, 86, 190, 142, 190, 190, 76, 118, 142, 94, 94, 86, 198, 134, 198, 198, 85, 6, 6, 46, 38, 85, 190, 190, 186, 190, 64, 69, 69, 77, 86, 88, 28, 93, 84, 116, 0, 0 };

/*

 This program is part of the TACLeBench benchmark suite.
 Version V 1.x

 Name: powerwindow_powerwindow_control.c

 Author: CoSys-Lab, University of Antwerp

 Function: powerwindow_control is the main functionality of the power window benchmark.
 	 It contains 3 states: System, EndReached and Pinch, which are used to controll the
 	 position of the glass, if the window is fully closed and sensing pinch force to realize
 	 the powerwindow function.

 Source: https://github.com/tacle/tacle-bench/blob/master/bench/app/PowerWindow//powerwindow_powerwindow_control.c

 Changes: a brief summary of major functional changes and formatting)

 License: GNU General Public License

*/

#include "powerwindow_HeaderFiles/powerwindow_powerwindow_control.h"
#include "powerwindow_HeaderFiles/powerwindow_powerwindow_control_private.h"

/* Named constants for Chart: '<S2>/stateflow control model' */
#define powerwindow_powerwindow__IN_NO_ACTIVE_CHILD ((powerwindow_uint8_T)0U)
#define powerwindow_powerwindow_contr_IN_EndReached ((powerwindow_uint8_T)1U)
#define powerwindow_powerwindow_contr_IN_SensePinch ((powerwindow_uint8_T)2U)
#define powerwindow_powerwindow_control_IN_AutoDown ((powerwindow_uint8_T)1U)
#define powerwindow_powerwindow_control_IN_AutoUp  ((powerwindow_uint8_T)1U)
#define powerwindow_powerwindow_control_IN_Down    ((powerwindow_uint8_T)2U)
#define powerwindow_powerwindow_control_IN_Down_d  ((powerwindow_uint8_T)1U)
#define powerwindow_powerwindow_control_IN_InitDown ((powerwindow_uint8_T)3U)
#define powerwindow_powerwindow_control_IN_InitUp  ((powerwindow_uint8_T)2U)
#define powerwindow_powerwindow_control_IN_Neutral ((powerwindow_uint8_T)2U)
#define powerwindow_powerwindow_control_IN_Pinch   ((powerwindow_uint8_T)2U)
#define powerwindow_powerwindow_control_IN_SenseEnd ((powerwindow_uint8_T)1U)
#define powerwindow_powerwindow_control_IN_Start   ((powerwindow_uint8_T)3U)
#define powerwindow_powerwindow_control_IN_System  ((powerwindow_uint8_T)3U)
#define powerwindow_powerwindow_control_IN_Up      ((powerwindow_uint8_T)3U)

/* Forward declaration for local functions */
void powerwindow_powerwindow_control_Start(powerwindow_rtDW_PowerWindow_control *localDW);

void powerwindow_powerwindow_control_Init(powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T
  *rty_window_down, powerwindow_boolean_T *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T *
  rty_wake, powerwindow_rtDW_PowerWindow_control *localDW);

void powerwindow_powerwindow_control_Start(powerwindow_rtDW_PowerWindow_control *localDW);

void powerwindow_powerwindow_control_initialize(const powerwindow_char_T **rt_errorStatus,
  powerwindow_RT_MODEL_PowerWindow_control *const PowerWindow_control_M,
  powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW,
  powerwindow_rtZCE_PowerWindow_control *localZCE);

void powerwindow_powerwindow_control_main(const powerwindow_boolean_T *rtu_up, const powerwindow_boolean_T *rtu_down,
  const powerwindow_boolean_T *rtu_endofdetectionrange, const powerwindow_uint8_T *rtu_currentsense,
  powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T *rty_window_down, powerwindow_boolean_T
  *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T *rty_wake,
  powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW,
  powerwindow_rtZCE_PowerWindow_control *localZCE);

void powerwindow_powerwindow_con_broadcast_ticks(powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T *
  rty_window_down, powerwindow_boolean_T *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T
  *rty_wake, powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW);

/* Function for Chart: '<S2>/stateflow control model' */
void powerwindow_powerwindow_con_broadcast_ticks(powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T *
  rty_window_down, powerwindow_boolean_T *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T
  *rty_wake, powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW)
{
  /* Event: '<S3>:30' */
  /* During: PW_PSG/PWExternalClock/stateflow control model */
  if (localDW->is_active_c2_PowerWindow_contro == 0U) {
    /* Entry: PW_PSG/PWExternalClock/stateflow control model */
    localDW->is_active_c2_PowerWindow_contro = 1U;

    /* Entry Internal: PW_PSG/PWExternalClock/stateflow control model */
    /* Transition: '<S3>:102' */
    localDW->is_c2_PowerWindow_control = powerwindow_powerwindow_control_IN_System;
 
    /* Entry Internal 'System': '<S3>:94' */
    localDW->is_active_Logic = 1U;

    /* Entry Internal 'Logic': '<S3>:95' */
    /* Transition: '<S3>:82' */
    localDW->is_Logic = powerwindow_powerwindow_control_IN_Neutral;

    /* Chart: '<S2>/stateflow control model' incorporates:
     *  TriggerPort: '<S3>/ticks'
     */
    /* Entry 'Neutral': '<S3>:16' */
    *rty_window_up = false;
    *rty_window_down = false;
    *rty_wake = false;
    localDW->is_active_Sensing = 1U;

    /* Entry Internal 'Sensing': '<S3>:96' */
    /* Transition: '<S3>:153' */
    localDW->is_Sensing = powerwindow_powerwindow_control_IN_Start;
    localDW->temporalCounter_i2 = 0U;

    /* Chart: '<S2>/stateflow control model' incorporates:
     *  TriggerPort: '<S3>/ticks'
     */
    /* Entry 'Start': '<S3>:170' */
    *rty_overcurrent = false;
    *rty_pinch = false;
  } else {
    switch (localDW->is_c2_PowerWindow_control) {
     case powerwindow_powerwindow_contr_IN_EndReached:
      /* During 'EndReached': '<S3>:97' */
      if (localDW->temporalCounter_i1 >= 10) {
        /* Transition: '<S3>:101' */
        localDW->is_c2_PowerWindow_control = powerwindow_powerwindow_control_IN_System;

        /* Entry Internal 'System': '<S3>:94' */
        localDW->is_active_Logic = 1U;

        /* Entry Internal 'Logic': '<S3>:95' */
        /* Transition: '<S3>:82' */
        localDW->is_Logic = powerwindow_powerwindow_control_IN_Neutral;

        /* Chart: '<S2>/stateflow control model' incorporates:
         *  TriggerPort: '<S3>/ticks'
         */
        /* Entry 'Neutral': '<S3>:16' */
        *rty_window_up = false;
        *rty_window_down = false;
        *rty_wake = false;
        localDW->is_active_Sensing = 1U;

        /* Entry Internal 'Sensing': '<S3>:96' */
        /* Transition: '<S3>:153' */
        localDW->is_Sensing = powerwindow_powerwindow_control_IN_Start;
        localDW->temporalCounter_i2 = 0U;

        /* Chart: '<S2>/stateflow control model' incorporates:
         *  TriggerPort: '<S3>/ticks'
         */
        /* Entry 'Start': '<S3>:170' */
        *rty_overcurrent = false;
        *rty_pinch = false;
      }
      break;

     case powerwindow_powerwindow_control_IN_Pinch:
      /* During 'Pinch': '<S3>:152' */
      if (localDW->temporalCounter_i1 >= 40) {
        /* Transition: '<S3>:157' */
        localDW->is_c2_PowerWindow_control = powerwindow_powerwindow_control_IN_System;

        /* Entry Internal 'System': '<S3>:94' */
        localDW->is_active_Logic = 1U;

        /* Entry Internal 'Logic': '<S3>:95' */
        /* Transition: '<S3>:82' */
        localDW->is_Logic = powerwindow_powerwindow_control_IN_Neutral;

        /* Chart: '<S2>/stateflow control model' incorporates:
         *  TriggerPort: '<S3>/ticks'
         */
        /* Entry 'Neutral': '<S3>:16' */
        *rty_window_up = false;
        *rty_window_down = false;
        *rty_wake = false;
        localDW->is_active_Sensing = 1U;

        /* Entry Internal 'Sensing': '<S3>:96' */
        /* Transition: '<S3>:153' */
        localDW->is_Sensing = powerwindow_powerwindow_control_IN_Start;
        localDW->temporalCounter_i2 = 0U;

        /* Chart: '<S2>/stateflow control model' incorporates:
         *  TriggerPort: '<S3>/ticks'
         */
        /* Entry 'Start': '<S3>:170' */
        *rty_overcurrent = false;
        *rty_pinch = false;
      }
      break;

     default:
      /* Chart: '<S2>/stateflow control model' incorporates:
       *  TriggerPort: '<S3>/ticks'
       */
      /* During 'System': '<S3>:94' */
      if (*rty_pinch == 1) {
        /* Transition: '<S3>:155' */
        /* Exit Internal 'System': '<S3>:94' */
        /* Exit Internal 'Sensing': '<S3>:96' */
        localDW->is_Sensing = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_active_Sensing = 0U;

        /* Exit Internal 'Logic': '<S3>:95' */
        /* Exit Internal 'Down': '<S3>:18' */
        localDW->is_Down = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_Logic = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;

        /* Exit Internal 'Up': '<S3>:17' */
        localDW->is_Up = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_active_Logic = 0U;
        localDW->is_c2_PowerWindow_control = powerwindow_powerwindow_control_IN_Pinch;
        localDW->temporalCounter_i1 = 0U;

        /* Entry 'Pinch': '<S3>:152' */
        *rty_window_up = false;
        *rty_window_down = true;
      } else if (*rty_overcurrent == 1) {
        /* Transition: '<S3>:100' */
        /* Exit Internal 'System': '<S3>:94' */
        /* Exit Internal 'Sensing': '<S3>:96' */
        localDW->is_Sensing = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_active_Sensing = 0U;

        /* Exit Internal 'Logic': '<S3>:95' */
        /* Exit Internal 'Down': '<S3>:18' */
        localDW->is_Down = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_Logic = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;

        /* Exit Internal 'Up': '<S3>:17' */
        localDW->is_Up = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
        localDW->is_active_Logic = 0U;
        localDW->is_c2_PowerWindow_control = powerwindow_powerwindow_contr_IN_EndReached;
        localDW->temporalCounter_i1 = 0U;

        /* Entry 'EndReached': '<S3>:97' */
        *rty_window_up = false;
        *rty_window_down = false;
      } else {
        /* During 'Logic': '<S3>:95' */
        switch (localDW->is_Logic) {
         case powerwindow_powerwindow_control_IN_Down_d:
          /* During 'Down': '<S3>:18' */
          if (localB->map[1]) {
            /* Transition: '<S3>:169' */
            /* Exit Internal 'Down': '<S3>:18' */
            localDW->is_Down = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
            localDW->is_Logic = powerwindow_powerwindow_control_IN_Up;

            /* Entry 'Up': '<S3>:17' */
            *rty_window_up = true;
            *rty_window_down = false;
            *rty_wake = true;
            localDW->is_Up = powerwindow_powerwindow_control_IN_Up;
          } else {
            switch (localDW->is_Down) {
             case powerwindow_powerwindow_control_IN_AutoDown:
              /* During 'AutoDown': '<S3>:111' */
              break;

             case powerwindow_powerwindow_control_IN_Down:
              /* During 'Down': '<S3>:110' */
              if (localB->map[0]) {
                /* Transition: '<S3>:26' */
                localDW->is_Down = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
                localDW->is_Logic = powerwindow_powerwindow_control_IN_Neutral;

                /* Entry 'Neutral': '<S3>:16' */
                *rty_window_up = false;
                *rty_window_down = false;
                *rty_wake = false;
              }
              break;

             default:
              /* During 'InitDown': '<S3>:109' */
              if (localDW->temporalCounter_i1 >= 20) {
                /* Transition: '<S3>:119' */
                if (localB->map[0]) {
                  /* Transition: '<S3>:120' */
                  localDW->is_Down = powerwindow_powerwindow_control_IN_AutoDown;
                } else {
                  if (localB->map[2]) {
                    /* Transition: '<S3>:121' */
                    localDW->is_Down = powerwindow_powerwindow_control_IN_Down;
                  }
                }
              }
              break;
            }
          }
          break;

         case powerwindow_powerwindow_control_IN_Neutral:
          /* During 'Neutral': '<S3>:16' */
          if (localB->map[1]) {
            /* Transition: '<S3>:24' */
            localDW->is_Logic = powerwindow_powerwindow_control_IN_Up;

            /* Entry 'Up': '<S3>:17' */
            *rty_window_up = true;
            *rty_window_down = false;
            *rty_wake = true;
            localDW->is_Up = powerwindow_powerwindow_control_IN_InitUp;
            localDW->temporalCounter_i1 = 0U;
          } else {
            if (localB->map[2]) {
              /* Transition: '<S3>:25' */
              localDW->is_Logic = powerwindow_powerwindow_control_IN_Down_d;

              /* Entry 'Down': '<S3>:18' */
              *rty_window_up = false;
              *rty_window_down = true;
              *rty_wake = true;
              localDW->is_Down = powerwindow_powerwindow_control_IN_InitDown;
              localDW->temporalCounter_i1 = 0U;
            }
          }
          break;

         default:
          /* During 'Up': '<S3>:17' */
          if (localB->map[2]) {
            /* Transition: '<S3>:166' */
            /* Exit Internal 'Up': '<S3>:17' */
            localDW->is_Up = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
            localDW->is_Logic = powerwindow_powerwindow_control_IN_Down_d;

            /* Entry 'Down': '<S3>:18' */
            *rty_window_up = false;
            *rty_window_down = true;
            *rty_wake = true;
            localDW->is_Down = powerwindow_powerwindow_control_IN_Down;
          } else {
            switch (localDW->is_Up) {
             case powerwindow_powerwindow_control_IN_AutoUp:
              /* During 'AutoUp': '<S3>:108' */
              break;

             case powerwindow_powerwindow_control_IN_InitUp:
              /* During 'InitUp': '<S3>:106' */
              if (localDW->temporalCounter_i1 >= 20) {
                /* Transition: '<S3>:115' */
                if (localB->map[0]) {
                  /* Transition: '<S3>:118' */
                  localDW->is_Up = powerwindow_powerwindow_control_IN_AutoUp;
                } else {
                  if (localB->map[1]) {
                    /* Transition: '<S3>:117' */
                    localDW->is_Up = powerwindow_powerwindow_control_IN_Up;
                  }
                }
              }
              break;

             default:
              /* During 'Up': '<S3>:107' */
              if (localB->map[0]) {
                /* Transition: '<S3>:23' */
                localDW->is_Up = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
                localDW->is_Logic = powerwindow_powerwindow_control_IN_Neutral;

                /* Entry 'Neutral': '<S3>:16' */
                *rty_window_up = false;
                *rty_window_down = false;
                *rty_wake = false;
              }
              break;
            }
          }
          break;
        }

        /* During 'Sensing': '<S3>:96' */
        switch (localDW->is_Sensing) {
         case powerwindow_powerwindow_control_IN_SenseEnd:
          /* During 'SenseEnd': '<S3>:147' */
          if ((localB->LogicalOperator == 0) && (*rty_window_up == 1)) {
            /* Transition: '<S3>:173' */
            localDW->is_Sensing = powerwindow_powerwindow_control_IN_Start;
            localDW->temporalCounter_i2 = 0U;

            /* Entry 'Start': '<S3>:170' */
            *rty_overcurrent = false;
            *rty_pinch = false;
          } else {
            *rty_overcurrent = (localB->RateTransition1 > 184);
          }
          break;

         case powerwindow_powerwindow_contr_IN_SensePinch:
          /* During 'SensePinch': '<S3>:148' */
          if ((localB->LogicalOperator == 1) || (*rty_window_down == 1)) {
            /* Transition: '<S3>:150' */
            localDW->is_Sensing = powerwindow_powerwindow_control_IN_SenseEnd;
          } else {
            *rty_pinch = (localB->RateTransition1 > 92);
          }
          break;

         default:
          /* During 'Start': '<S3>:170' */
          if (localDW->temporalCounter_i2 >= 6) {
            /* Transition: '<S3>:171' */
            localDW->is_Sensing = powerwindow_powerwindow_contr_IN_SensePinch;
          }
          break;
        }
      }
      break;
    }
  }
}

/* Initial conditions for referenced model: 'powerwindow_powerwindow_control' */
void powerwindow_powerwindow_control_Init(powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T
  *rty_window_down, powerwindow_boolean_T *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T *
  rty_wake, powerwindow_rtDW_PowerWindow_control *localDW)
{
  /* InitializeConditions for Chart: '<S2>/stateflow control model' */
  localDW->is_active_Logic = 0U;
  localDW->is_Logic = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
  localDW->is_Down = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
  localDW->is_Up = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
  localDW->temporalCounter_i1 = 0U;
  localDW->is_active_Sensing = 0U;
  localDW->is_Sensing = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
  localDW->temporalCounter_i2 = 0U;
  localDW->is_active_c2_PowerWindow_contro = 0U;
  localDW->is_c2_PowerWindow_control = powerwindow_powerwindow__IN_NO_ACTIVE_CHILD;
  *rty_window_up = false;
  *rty_window_down = false;
  *rty_overcurrent = false;
  *rty_pinch = false;
  *rty_wake = false;
}

/* Start for referenced model: 'powerwindow_powerwindow_control' */
void powerwindow_powerwindow_control_Start(powerwindow_rtDW_PowerWindow_control *localDW)
{
  /* Start for DiscretePulseGenerator: '<S2>/period of 50ms' */
  localDW->clockTickCounter = 0L;
}

/* Output and update for referenced model: 'powerwindow_powerwindow_control' */
void powerwindow_powerwindow_control_main(const powerwindow_boolean_T *rtu_up, const powerwindow_boolean_T *rtu_down,
  const powerwindow_boolean_T *rtu_endofdetectionrange, const powerwindow_uint8_T *rtu_currentsense,
  powerwindow_boolean_T *rty_window_up, powerwindow_boolean_T *rty_window_down, powerwindow_boolean_T
  *rty_overcurrent, powerwindow_boolean_T *rty_pinch, powerwindow_boolean_T *rty_wake,
  powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW,
  powerwindow_rtZCE_PowerWindow_control *localZCE)
{
  powerwindow_int16_T rowIdx;
  powerwindow_int16_T rtb_periodof50ms;

  /* DiscretePulseGenerator: '<S2>/period of 50ms' */
  rtb_periodof50ms = (localDW->clockTickCounter < 5L) &&
    (localDW->clockTickCounter >= 0L) ? 1 : 0;
  if (localDW->clockTickCounter >= 9L) {
    localDW->clockTickCounter = 0L;
  } else {
    localDW->clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<S2>/period of 50ms' */

  /* Logic: '<S2>/Logical Operator' */
  localB->LogicalOperator = !*rtu_endofdetectionrange;

  /* RateTransition: '<S2>/Rate Transition1' */
  localB->RateTransition1 = *rtu_currentsense;

  /* CombinatorialLogic: '<S2>/map' */
  rowIdx = (powerwindow_int16_T)(((powerwindow_uint16_T)*rtu_up << 1) + *rtu_down);
  localB->map[0U] = rtCP_map_table[(powerwindow_uint16_T)rowIdx];
  localB->map[1U] = rtCP_map_table[rowIdx + 4U];
  localB->map[2U] = rtCP_map_table[rowIdx + 8U];

  /* Chart: '<S2>/stateflow control model' incorporates:
   *  TriggerPort: '<S3>/ticks'
   */
  /* DataTypeConversion: '<S2>/Data Type Conversion' */
  if (((rtb_periodof50ms != 0) != (localZCE->stateflowcontrolmodel_Trig_ZCE ==
        powerwindow_POS_ZCSIG)) && (localZCE->stateflowcontrolmodel_Trig_ZCE !=
                        powerwindow_UNINITIALIZED_ZCSIG)) {
    /* Gateway: PW_PSG/PWExternalClock/stateflow control model */
    if (localDW->temporalCounter_i1 < 63U) {
      localDW->temporalCounter_i1++;
    }

    if (localDW->temporalCounter_i2 < 7U) {
      localDW->temporalCounter_i2++;
    }

    powerwindow_powerwindow_con_broadcast_ticks(rty_window_up, rty_window_down,
      rty_overcurrent, rty_pinch, rty_wake, localB, localDW);
  }

  localZCE->stateflowcontrolmodel_Trig_ZCE = (powerwindow_uint8_T)(rtb_periodof50ms != 0 ?
    (powerwindow_int16_T)powerwindow_POS_ZCSIG : (powerwindow_int16_T)powerwindow_ZERO_ZCSIG);

  /* End of DataTypeConversion: '<S2>/Data Type Conversion' */
}

/* Model initialize function */
void powerwindow_powerwindow_control_initialize(const powerwindow_char_T **rt_errorStatus,
  powerwindow_RT_MODEL_PowerWindow_control *const PowerWindow_control_M,
  powerwindow_rtB_PowerWindow_control *localB, powerwindow_rtDW_PowerWindow_control *localDW,
  powerwindow_rtZCE_PowerWindow_control *localZCE)
{
  /* Registration code */

  /* initialize error status */
	powerwindow_powerwindow_control_rtmSetErrorStatusPointer(PowerWindow_control_M, rt_errorStatus);

  /* block I/O */
  (void) memset(((void *) localB), 0,
                sizeof(powerwindow_rtB_PowerWindow_control));

  /* states (dwork) */
  (void) memset((void *)localDW, 0,
                sizeof(powerwindow_rtDW_PowerWindow_control));
  localZCE->stateflowcontrolmodel_Trig_ZCE = powerwindow_UNINITIALIZED_ZCSIG;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
