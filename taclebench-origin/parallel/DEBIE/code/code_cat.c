/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   class.c
 *
 * Event-classification module.
 *
 * Based on the SSF file class.c, rev 1.10, Tue Jun 01 12:34:56 1999.
 *
 *- * --------------------------------------------------------------------------
 */

#include "keyword.h"
#include "class.h"
#include "classtab.h"
#include "su_ctrl.h"
#include "measure.h"
#define MAX_QUALITY 255

unsigned char RoughLogarithm (unsigned int x)
/* An integer approximation (0..16) of the base-2 log of x */
/* computed as the number of the most-significant non-zero bit. */
/* When x = 0, returns zero. */
/* When x > 0, returns floor(log2(x) + 1). */
/* For example, when x =     1, returns  1. */
/*              when x =     2, returns  2. */
/*              when x =     7, returns  3. */
/*              when x = x7FFF, returns 15. */
/*              when x = x8000, returns 16. */
{
   unsigned char INDIRECT_INTERNAL greatest_non_zero_bit;
   unsigned int  INDIRECT_INTERNAL shifted;

   greatest_non_zero_bit = 0;
   shifted = x;

   _Pragma("loopbound min 16 max 16")
   while (shifted)
   {
      greatest_non_zero_bit++;
      shifted >>= 1;
   }

   return greatest_non_zero_bit;
}


float GetQualityTerm(unsigned char coeff, unsigned int amplitude)
/* Purpose        : Calculates the ampltude term of the quality formula.     */
/* Interface      : inputs      - parameter coeff defines number of the      */
/*                                quality coefficient.                       */
/*                              - parameter amplitude defines the signal     */
/*                                amplitude.                                 */
/*                  outputs     - quality term value is retruned.            */
/*                  subroutines - RoughLogarithm.                            */
/* Preconditions  : quality coefficients have valid values.                  */
/* Postconditions : none.                                                    */
/* Algorithm      : quality term is calculated according to following        */
/*                  formula :                                                */
/*                     coefficient[coeff]                                    */
/*                     * RoughLogaritm(amplitude)                            */
/*                     / AMPLTUDE_DIVIDER                                    */
/*                  where coefficient[coeff] is the amplitude coefficient of */
/*                  the quality term and AMPLTUDE_DIVIDER is a scaling       */
/*                  factor whose purpose is scale the result below 5.0.      */
/*                  However if the result would become larger than that,     */
/*                  5.0 is returned.                                         */

{
   float EXTERNAL quality;

   quality =
      (float)(telemetry_data.coefficient[coeff]
              * RoughLogarithm(amplitude))
      / AMPLITUDE_DIVIDER;

   if (quality > 5.0)
   {
      quality = 5.0;
   }

   return quality;
}


void CalculateQualityNumber(event_record_t EXTERNAL *new_event)
/* Purpose        : Calculates the quality number of a particle hit event    */
/*                  and stores in the event record.                          */
/* Interface      : inputs      - event record pointed by the parameter.     */
/*                  outputs     - event record pointed by the parameter.     */
/*                  subroutines - GetQualityTerm.                            */
/* Preconditions  : All components of the event record pointed by the        */
/*                  parameter which are used as input have valid values.     */
/* Postconditions : quality_number component of the event record pointed     */
/*                  by the parameter has is calculated.                      */
/* Algorithm      : quality_number is calculated according to the following  */
/*                  formula :                                                */
/*                     25 * Class + Ai*RoughLogarithm(Si) / Divider , where  */
/*                  Class   is the class of the event,                       */
/*                  Ai      is the amplitude coefficient of the quality      */
/*                             formula                                       */
/*                  Si      is a signal amplitude from the Sensor Unit Peak  */
/*                             detector                                      */
/*                  Divider is scaling factor whose value is determined by   */
/*                             the maximum value (5) of the latter terms     */
/*                  and i goes from 1 to 5.                                  */

{
   float INDIRECT_INTERNAL quality;

   quality = 25.0 * new_event -> classification;
   /* First term of the quality formula. */

   quality += GetQualityTerm(0, VALUE_OF (new_event -> plasma_1_plus));
   /* Add amplitude term for i=1 (see function algorithm). */

   quality += GetQualityTerm(1, VALUE_OF (new_event -> plasma_1_minus));
   /* Add amplitude term for i=2 (see function algorithm). */

   quality += GetQualityTerm(2, VALUE_OF (new_event -> piezo_1));
   /* Add amplitude term for i=3 (see function algorithm). */

   quality += GetQualityTerm(3, VALUE_OF (new_event -> piezo_2));
   /* Add amplitude term for i=4 (see function algorithm). */

   quality += GetQualityTerm(4, VALUE_OF (new_event -> plasma_2_plus));
   /* Add amplitude term for i=5 (see function algorithm). */

   new_event -> quality_number = (unsigned char) (quality + 0.5);
   /* Store quality number to the event record */
}


void ClassifyEvent(event_record_t EXTERNAL *new_event)
/* Purpose        : Classifies a particle hit event and stores result        */
/*                  to the event record pointed by the parameter.            */
/* Interface      : inputs      - event record pointed by the parameter.     */
/*                  outputs     - event record pointed by the parameter.     */
/*                  subroutines - CalculateQualityNumber.                    */
/* Preconditions  : All components of the event record pointed by the        */
/*                  parameter which are used as input have valid values.     */
/* Postconditions : classification and quality_number components of the      */
/*                  event record pointed by the parameter are computed.      */
/* Algorithm      : - class index is determined by comparing signal.         */
/*                    amplitudes and time delays to classification           */
/*                    thresholds.                                            */
/*                  - class number is read from a look-up table using the    */
/*                    class index and stored in the event record.            */
/*                  - CalculateQualityNumber is called.                      */

{
   unsigned char INDIRECT_INTERNAL class_index;
   /* Index for the class look-up table. */

   SU_settings_t EXTERNAL * INDIRECT_INTERNAL limits;
   /* Pointer to the struct holding classification thresholds. */

   class_index = 0;
   /* Bits will be set below according to event attributes. */

   switch (new_event -> SU_number)
   /* Select proper classification thresholds. */
   {
      case SU_1:
         limits = &telemetry_data.sensor_unit_1;
         break;

      case SU_2:
         limits = &telemetry_data.sensor_unit_2;
         break;

      case SU_3:
         limits = &telemetry_data.sensor_unit_3;
         break;

      case SU_4:
         limits = &telemetry_data.sensor_unit_4;
         break;
    }

    if (VALUE_OF (new_event -> plasma_1_plus) >=
        ((limits -> plasma_1_plus_classification) * 256))
    {
       class_index |= PLASMA_1_PLUS_CLASS;
       /* Set classification index bit for Plasma1+ peak amplitude. */
    }

    if (VALUE_OF (new_event -> plasma_1_minus) >=
        ((limits -> plasma_1_minus_classification) * 256))
    {
       class_index |= PLASMA_1_MINUS_CLASS;
       /* Set classification index bit for Plasma1- peak amplitude. */
    }

    if (VALUE_OF (new_event -> piezo_1) >=
        ((limits -> piezo_1_classification) * 256))
    {
       class_index |= PIEZO_1_CLASS;
       /* Set classification index bit for Piezo1 peak amplitude. */
    }

    if (VALUE_OF (new_event -> piezo_2) >=
        ((limits -> piezo_2_classification) * 256))
    {
       class_index |= PIEZO_2_CLASS;
       /* Set classification index bit for Piezo2 peak amplitude. */
    }

    if (VALUE_OF (new_event -> plasma_2_plus) >=
        ((limits -> plasma_2_plus_classification) * 256))
    {
       class_index |= PLASMA_2_PLUS_CLASS;
       /* Set classification index bit for Plasma2+ peak amplitude. */
    }

    if (VALUE_OF (new_event -> delay_2) >=
        ((limits -> plasma_1_plus_to_piezo_min_time) * 16) &&
        VALUE_OF (new_event -> delay_2) <=
        ((limits -> plasma_1_plus_to_piezo_max_time) * 16))
    {
       class_index |= PLASMA_1_PLUS_TO_PIEZO_CLASS;
       /* Set classification index bit for Plasma1+ to Piezo delay. */
    }

    if (VALUE_OF (new_event -> delay_3) >=
        ((limits -> plasma_1_minus_to_piezo_min_time) * 16) &&
        VALUE_OF (new_event -> delay_3) <=
        ((limits -> plasma_1_minus_to_piezo_max_time) * 16))
    {
       class_index |= PLASMA_1_MINUS_TO_PIEZO_CLASS;
       /* Set classification index bit for Plasma1- to Piezo delay. */
    }

    if (new_event -> delay_1 <=
        limits -> plasma_1_plus_to_minus_max_time)
    {
       class_index |= PLASMA_1_PLUS_TO_MINUS_CLASS;
       /* Set classification index bit for Plasma1+ to Plasma1- delay. */
    }

    new_event -> classification = event_class[class_index];
    /* Store classification number to the event record */

    if (SU_state[new_event->SU_number - SU_1] == self_test_e)
    {
       new_event -> quality_number = MAX_QUALITY;
    }

    else
    {
       CalculateQualityNumber(new_event);
    }

}

void InitClassification(void)
/* Purpose        : Initializes classication coefficients and levels.        */
/* Interface      : inputs      - none.                                      */
/*                  outputs     - quality coefficients in telemetry_data.    */
/*                              - classification levels in telemetry_data.   */
/*                              - threshold levels in telemetry_data.        */
/*                              - min time window in telemetry_data          */
/*                              - max time window in telemetry_data          */
/*                  subroutines - Init_SU_Settings                           */
/* Preconditions  : none.                                                    */
/* Postconditions : outputs have their default values.                       */
/* Algorithm      : see below                                                */

{
   uint_least8_t EXTERNAL i;
   /* Loop variable. */

   _Pragma("loopbound min 5 max 5")
   for (i=0; i<NUM_QCOEFF; i++)
   {
      telemetry_data.coefficient[i] = DEFAULT_COEFF;
   }

   Init_SU_Settings (&telemetry_data.sensor_unit_1);
   Init_SU_Settings (&telemetry_data.sensor_unit_2);
   Init_SU_Settings (&telemetry_data.sensor_unit_3);
   Init_SU_Settings (&telemetry_data.sensor_unit_4);
   /* Default values for thresholds, classification levels and min/max times */
   /* related to classification are set here.                                */

}

void Init_SU_Settings (SU_settings_t EXTERNAL *set)
/* Purpose        : Initializes classication parameters                      */
/* Interface      : inputs      - none.                                      */
/*                  outputs     - classification levels in telemetry_data.   */
/*                              - threshold levels in telemetry_data.        */
/*                              - min time window in telemetry_data          */
/*                              - max time window in telemetry_data          */
/*                  subroutines - none                                       */
/* Preconditions  : none.                                                    */
/* Postconditions : outputs have their default values.                       */
/* Algorithm      : Sets default values to telemetry_data.                   */
{
   set -> plasma_1_plus_threshold       = DEFAULT_THRESHOLD;
   set -> plasma_1_minus_threshold      = DEFAULT_THRESHOLD;
   set -> piezo_threshold               = DEFAULT_THRESHOLD;
   set -> plasma_1_plus_classification  = DEFAULT_CLASSIFICATION_LEVEL;
   set -> plasma_1_minus_classification = DEFAULT_CLASSIFICATION_LEVEL;
   set -> piezo_1_classification        = DEFAULT_CLASSIFICATION_LEVEL;
   set -> piezo_2_classification        = DEFAULT_CLASSIFICATION_LEVEL;
   set -> plasma_2_plus_classification  = DEFAULT_CLASSIFICATION_LEVEL;
   set -> plasma_1_plus_to_minus_max_time  = DEFAULT_MAX_TIME;
   set -> plasma_1_plus_to_piezo_min_time  = DEFAULT_MIN_TIME;
   set -> plasma_1_plus_to_piezo_max_time  = DEFAULT_MAX_TIME;
   set -> plasma_1_minus_to_piezo_min_time = DEFAULT_MIN_TIME;
   set -> plasma_1_minus_to_piezo_max_time = DEFAULT_MAX_TIME;
}


/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   classtab.c
 *
 * Table for classifying event according to multiple measurements.
 *
 * Based on the SSF file classtab.c, rev 1.2, Thu Feb 18 15:12:22 1999.
 *      
 *- * --------------------------------------------------------------------------
 */

#include "keyword.h"

unsigned char EXTERNAL event_class[256] = {
         /* Que  Qui  P1  P2  Qle  dTei     dTep     dTip       */
    0,   /*  0    0    0   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  0    0    0   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  0    0    0   0   0   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  0    0    0   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   0   0   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  0    0    0   0   0   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  0    0    0   0   0   INSIDE   INSIDE   INSIDE     */
    0,   /*  0    0    0   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  0    0    0   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  0    0    0   0   1   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  0    0    0   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   0   1   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  0    0    0   0   1   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  0    0    0   0   1   INSIDE   INSIDE   INSIDE     */
    0,   /*  0    0    0   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  0    0    0   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  0    0    0   1   0   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  0    0    0   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  0    0    0   1   0   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  0    0    0   1   0   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  0    0    0   1   0   INSIDE   INSIDE   INSIDE     */
    3,   /*  0    0    0   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  0    0    0   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    3,   /*  0    0    0   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  0    0    0   1   1   OUTSIDE  INSIDE   INSIDE     */
    3,   /*  0    0    0   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  0    0    0   1   1   INSIDE   OUTSIDE  INSIDE     */
    3,   /*  0    0    0   1   1   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  0    0    0   1   1   INSIDE   INSIDE   INSIDE     */
    0,   /*  0    0    1   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  0    0    1   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  0    0    1   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  0    0    1   0   0   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  0    0    1   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  0    0    1   0   0   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  0    0    1   0   0   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  0    0    1   0   0   INSIDE   INSIDE   INSIDE     */
    3,   /*  0    0    1   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  0    0    1   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    3,   /*  0    0    1   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  0    0    1   0   1   OUTSIDE  INSIDE   INSIDE     */
    3,   /*  0    0    1   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  0    0    1   0   1   INSIDE   OUTSIDE  INSIDE     */
    3,   /*  0    0    1   0   1   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  0    0    1   0   1   INSIDE   INSIDE   INSIDE     */
    5,   /*  0    0    1   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    5,   /*  0    0    1   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    5,   /*  0    0    1   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    5,   /*  0    0    1   1   0   OUTSIDE  INSIDE   INSIDE     */
    5,   /*  0    0    1   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    5,   /*  0    0    1   1   0   INSIDE   OUTSIDE  INSIDE     */
    5,   /*  0    0    1   1   0   INSIDE   INSIDE   OUTSIDE    */
    5,   /*  0    0    1   1   0   INSIDE   INSIDE   INSIDE     */
    8,   /*  0    0    1   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    8,   /*  0    0    1   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    8,   /*  0    0    1   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    8,   /*  0    0    1   1   1   OUTSIDE  INSIDE   INSIDE     */
    8,   /*  0    0    1   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    8,   /*  0    0    1   1   1   INSIDE   OUTSIDE  INSIDE     */
    8,   /*  0    0    1   1   1   INSIDE   INSIDE   OUTSIDE    */
    8,   /*  0    0    1   1   1   INSIDE   INSIDE   INSIDE     */
    0,   /*  0    1    0   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  0    1    0   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  0    1    0   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  0    1    0   0   0   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  0    1    0   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  0    1    0   0   0   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  0    1    0   0   0   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  0    1    0   0   0   INSIDE   INSIDE   INSIDE     */
    3,   /*  0    1    0   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  0    1    0   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    3,   /*  0    1    0   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  0    1    0   0   1   OUTSIDE  INSIDE   INSIDE     */
    3,   /*  0    1    0   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  0    1    0   0   1   INSIDE   OUTSIDE  INSIDE     */
    3,   /*  0    1    0   0   1   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  0    1    0   0   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    0   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  0    1    0   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    0   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  0    1    0   1   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    0   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  0    1    0   1   0   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    0   1   0   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  0    1    0   1   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    0   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    4,   /*  0    1    0   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    0   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    4,   /*  0    1    0   1   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    0   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    4,   /*  0    1    0   1   1   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    0   1   1   INSIDE   INSIDE   OUTSIDE    */
    4,   /*  0    1    0   1   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    1   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  0    1    1   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    1   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  0    1    1   0   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    1   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  0    1    1   0   0   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    1   0   0   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  0    1    1   0   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    1   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    4,   /*  0    1    1   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    1   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    4,   /*  0    1    1   0   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    1   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    4,   /*  0    1    1   0   1   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    1   0   1   INSIDE   INSIDE   OUTSIDE    */
    4,   /*  0    1    1   0   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    1   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    5,   /*  0    1    1   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    1   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    5,   /*  0    1    1   1   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    1   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    5,   /*  0    1    1   1   0   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    1   1   0   INSIDE   INSIDE   OUTSIDE    */
    5,   /*  0    1    1   1   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  0    1    1   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    8,   /*  0    1    1   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  0    1    1   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    8,   /*  0    1    1   1   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  0    1    1   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    8,   /*  0    1    1   1   1   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  0    1    1   1   1   INSIDE   INSIDE   OUTSIDE    */
    8,   /*  0    1    1   1   1   INSIDE   INSIDE   INSIDE     */
    0,   /*  1    0    0   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    0,   /*  1    0    0   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    0,   /*  1    0    0   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    0,   /*  1    0    0   0   0   OUTSIDE  INSIDE   INSIDE     */
    0,   /*  1    0    0   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    0,   /*  1    0    0   0   0   INSIDE   OUTSIDE  INSIDE     */
    0,   /*  1    0    0   0   0   INSIDE   INSIDE   OUTSIDE    */
    0,   /*  1    0    0   0   0   INSIDE   INSIDE   INSIDE     */
    3,   /*  1    0    0   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  1    0    0   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    3,   /*  1    0    0   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  1    0    0   0   1   OUTSIDE  INSIDE   INSIDE     */
    3,   /*  1    0    0   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  1    0    0   0   1   INSIDE   OUTSIDE  INSIDE     */
    3,   /*  1    0    0   0   1   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  1    0    0   0   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    0   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    3,   /*  1    0    0   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  1    0    0   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  1    0    0   1   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    0   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    3,   /*  1    0    0   1   0   INSIDE   OUTSIDE  INSIDE     */
    1,   /*  1    0    0   1   0   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  1    0    0   1   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    0   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    0    0   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    4,   /*  1    0    0   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    4,   /*  1    0    0   1   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    0   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    1,   /*  1    0    0   1   1   INSIDE   OUTSIDE  INSIDE     */
    4,   /*  1    0    0   1   1   INSIDE   INSIDE   OUTSIDE    */
    4,   /*  1    0    0   1   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    1   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    3,   /*  1    0    1   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    3,   /*  1    0    1   0   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    1   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   0   0   INSIDE   OUTSIDE  INSIDE     */
    3,   /*  1    0    1   0   0   INSIDE   INSIDE   OUTSIDE    */
    3,   /*  1    0    1   0   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    1   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    4,   /*  1    0    1   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    4,   /*  1    0    1   0   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    1   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   0   1   INSIDE   OUTSIDE  INSIDE     */
    4,   /*  1    0    1   0   1   INSIDE   INSIDE   OUTSIDE    */
    4,   /*  1    0    1   0   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    1   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    5,   /*  1    0    1   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    5,   /*  1    0    1   1   0   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    1   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   1   0   INSIDE   OUTSIDE  INSIDE     */
    5,   /*  1    0    1   1   0   INSIDE   INSIDE   OUTSIDE    */
    5,   /*  1    0    1   1   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    0    1   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    8,   /*  1    0    1   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    8,   /*  1    0    1   1   1   OUTSIDE  INSIDE   INSIDE     */
    1,   /*  1    0    1   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    1,   /*  1    0    1   1   1   INSIDE   OUTSIDE  INSIDE     */
    8,   /*  1    0    1   1   1   INSIDE   INSIDE   OUTSIDE    */
    8,   /*  1    0    1   1   1   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    1    0   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    1    0   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  1    1    0   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    1,   /*  1    1    0   0   0   OUTSIDE  INSIDE   INSIDE     */
    6,   /*  1    1    0   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    6,   /*  1    1    0   0   0   INSIDE   OUTSIDE  INSIDE     */
    6,   /*  1    1    0   0   0   INSIDE   INSIDE   OUTSIDE    */
    6,   /*  1    1    0   0   0   INSIDE   INSIDE   INSIDE     */
    1,   /*  1    1    0   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    1,   /*  1    1    0   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    1,   /*  1    1    0   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    1,   /*  1    1    0   0   1   OUTSIDE  INSIDE   INSIDE     */
    7,   /*  1    1    0   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    7,   /*  1    1    0   0   1   INSIDE   OUTSIDE  INSIDE     */
    7,   /*  1    1    0   0   1   INSIDE   INSIDE   OUTSIDE    */
    7,   /*  1    1    0   0   1   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    0   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    0   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    0   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    0   1   0   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    0   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    0   1   0   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    0   1   0   INSIDE   INSIDE   OUTSIDE    */
    7,   /*  1    1    0   1   0   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    0   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    0   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    0   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    0   1   1   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    0   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    0   1   1   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    0   1   1   INSIDE   INSIDE   OUTSIDE    */
    9,   /*  1    1    0   1   1   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    1   0   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   0   0   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    1   0   0   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    1   0   0   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    1   0   0   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   0   0   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    1   0   0   INSIDE   INSIDE   OUTSIDE    */
    7,   /*  1    1    1   0   0   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    1   0   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   0   1   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    1   0   1   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    1   0   1   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    1   0   1   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   0   1   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    1   0   1   INSIDE   INSIDE   OUTSIDE    */
    9,   /*  1    1    1   0   1   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    1   1   0   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   1   0   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    1   1   0   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    1   1   0   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    1   1   0   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   1   0   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    1   1   0   INSIDE   INSIDE   OUTSIDE    */
    7,   /*  1    1    1   1   0   INSIDE   INSIDE   INSIDE     */
    2,   /*  1    1    1   1   1   OUTSIDE  OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   1   1   OUTSIDE  OUTSIDE  INSIDE     */
    2,   /*  1    1    1   1   1   OUTSIDE  INSIDE   OUTSIDE    */
    2,   /*  1    1    1   1   1   OUTSIDE  INSIDE   INSIDE     */
    2,   /*  1    1    1   1   1   INSIDE   OUTSIDE  OUTSIDE    */
    2,   /*  1    1    1   1   1   INSIDE   OUTSIDE  INSIDE     */
    2,   /*  1    1    1   1   1   INSIDE   INSIDE   OUTSIDE    */
    9    /*  1    1    1   1   1   INSIDE   INSIDE   INSIDE     */
     };
/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   debie.c
 *
 * Main function.
 *
 * Based on the SSF file debie.c, revision 1.4, Tue Jun 01 13:37:20 1999.
 *
 *- * --------------------------------------------------------------------------
 */


#include "reg52.h"
#include "kernobj.h"
#include "keyword.h"
#include "health.h"
#include "taskctrl.h"


int main(void)
{
   TARGET_INIT;
   /* Initialize the benchmark target system. */

   EA=0;
   /* Disable all interrupts */

   Boot();
   /* Execute boot sequence */

   EA=1;
   /* Enable 'enabled' interrupts */

   StartSystem(HEALTH_MONITORING_TASK);
   /* Start RTX kernel and first task */

   // do not run simulation for ever, just exit here
   return 0;
   _Pragma("loopbound min 0 max 0")
   while(1)
   {
      /* StartSystem has failed, so we just      */
      /* wait here for a while for the Watch Dog */
      /* to wake up.                             */
   }
}

/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   health.c
 *
 * Monitoring DEBIE DPU and system health.
 *
 * Based on the SSF file health.c, rev 1.74, Fri Oct 15 20:18:02 1999.
 *
 *- * -----------------------------------------------------------------------
*/

#include "ad_conv.h"
#include "dpu_ctrl.h"
#include "taskctrl.h"
#include "health.h"
#include "ttc_ctrl.h"
#include "measure.h"
#include "tc_hand.h"
#include "keyword.h"
#include "kernobj.h"
#include "tm_data.h"
#include "telem.h"
#include "isr_ctrl.h"
#include "version.h"
#include "class.h"


#define BOOT_WAIT_INTERVAL 55
/* Boot wait interval: 55 x 10 ms = 550 ms */

#define SYSTEM_INTERVAL 9198
/* Sets system time interval to 10 ms, assuming a clock  */
/* frequency of exactly 11.0592 MHz.                     */
/* SYSTEM_INTERVAL is in units of the processor cycle,   */
/* and is computed as follows, in principle:             */
/*     10 ms * 11.0592 MHz / 12                          */
/* where 12 is the number of clock cycles per processor  */
/* cycle.                                                */
/* The above calculation gives the value 9216 precisely. */
/* In practice this value makes the DPU time (updated by */
/* the Health Monitoring Task) retard by about 0.2%.     */
/* This is probably due to imprecise handling of timer   */
/* ticks by RTX-51. As a primitive correction, we reduce */
/* the value by 0.2% to 9198. This correction will not   */
/* be exact, especially under heavy interrupt load.      */

#define HM_INTERVAL 100
/* Health Monitoring interval wait: 100 x 10 ms = 1s     */
/* This wait time should be waited 10 times to get       */
/* period of 10s.                                        */
/* Alternatively the execution of the Health Monitoring  */
/* Task can be divided to ten steps which are executed   */
/* 1 second apart from each other and then all actions   */
/* of the task would be executed once in 10 seconds      */

#define ADC_TEMPERATURE_MAX_TRIES         255
/* When temperatures are measured this macro defines the maximum       */
/* amount of tries to be used in reading and handling an AD channel in */
/* 'Read_AD_Channel()'.                                                */

#define CONVERSION_TEMPERATURE_MAX_TRIES  255
/* When temperatures are measured this macro defines the maximum       */
/* amount of tries to be used when End Of Conversion indication is     */
/* waited in function 'Convert_AD()'.                                  */

#define ADC_VOLTAGE_MAX_TRIES             255
/* When voltages are measured this macro defines the maximum           */
/* amount of tries to be used in reading and handling an AD channel in */
/* 'Read_AD_Channel()'.                                                */

#define CONVERSION_VOLTAGE_MAX_TRIES      255
/* When voltages are measured this macro defines the maximum           */
/* amount of tries to be used when End Of Conversion indication is     */
/* waited in function 'Convert_AD()'.                                  */

#define DPU_5V_SELECTOR     3
#define SU_1_2_P5V_SELECTOR 0
#define SU_1_2_M5V_SELECTOR 4
/* DPU Self Test voltage measurement channel selectors. */
/* See MeasureVoltage function.                         */

#define SU_P5V_ANA_LOWER_LIMIT    0xBA
#define SU_P5V_ANA_UPPER_LIMIT    0xE4
#define SU_M5V_ANA_LOWER_LIMIT    0x0D
#define SU_M5V_ANA_UPPER_LIMIT    0x22
#define SU_P50V_LOWER_LIMIT       0xA8
#define SU_P50V_UPPER_LIMIT       0xE3
#define SU_M50V_LOWER_LIMIT       0x0E
#define SU_M50V_UPPER_LIMIT       0x2C
#define DPU_P5V_DIG_LOWER_LIMIT   0xBA
#define DPU_P5V_DIG_UPPER_LIMIT   0xE4
/* Upper and lower limits for monitoring */
/* SU and DPU supply voltages.           */

#define SELF_TEST_DONE     0
#define SELF_TEST_RUNNING  1
/* Used in SU self test. */

typedef struct {
   channel_t       ADC_channel;
   uint_least8_t   ADC_max_tries;
   uint_least8_t   conversion_max_tries;
   unsigned int    unsigned_ADC;
   signed   int    signed_ADC;
   unsigned char   AD_execution_result;
   sensor_number_t sensor_unit;
} ADC_parameters_t;
/* This struct is used to hold parameters for Reading AD channels.           */
/*                                                                           */
/* 'ADC_channel'          Stores the number the AD channel to be measured.   */
/*                        Includes BP_UP bit to select unipolar/bipolar mode.*/
/* 'ADC_max_tries'        Gives the function the maximum amount of tries     */
/*                        used in reading and handling an AD channel.        */
/* 'conversion_max_tries' Gives the function the maximum amount of tries     */
/*                        used in reading and handling a single AD           */
/*                        conversion.                                        */
/* 'unsigned_ADC'         These variables are used for storing an ADC        */
/* 'signed_ADC'           result.                                            */
/* 'AD_execution_result'  Indicates whether the AD measurent is a success or */
/*                        indicates what has gone wrong with following       */
/*                        values.                                            */
/*  sensor_unit           See the function DAC_SelfTest.                     */
/*  CONVERSION_ACTIVE 0                                                      */
/*  RESULT_OK         1                                                      */
/*  HIT_OCCURRED      2                                                      */


typedef enum {
        su1_e,su2_e,su3_e,su4_e
} SU_index_t;



uint_least8_t EXTERNAL temp_meas_count = TEMP_COUNT;
/* This variable is used for counting temperature measurement interval       */
/* 1/60 secs.                                                                */

uint_least8_t EXTERNAL voltage_meas_count = VOLTAGE_COUNT;
/* This variable is used for counting voltage measurement interval 1/180     */
/* secs.                                                                     */

uint_least8_t EXTERNAL checksum_count = CHECK_COUNT;
/* This variable is used for counting  checksum counter, interval 1/60       */
/* secs.                                                                     */

unsigned char EXTERNAL code_checksum;
/* This variable is used for calculating checksum from the memory.           */

unsigned char EXTERNAL self_test_flag = SELF_TEST_DONE;
/* Used with SU self test initiation.                                  */

/* enumerations */

typedef enum  {
   channel_0_e, channel_1_e, channel_2_e, channel_3_e, channel_4_e,
   channel_5_e, channel_6_e

} AD_channel_t;

unsigned char EXTERNAL ADC_channel_register = 0x80;
/* Holds value of the ADC Channel HW register       */
/* Is used by Hit Trigger ISR task and Health       */
/* Monitoring task.                                 */
/* Updating must be atomic in the Health Monitoring */
/* task, because Hit Trigger can preempt it.        */


dpu_time_t EXTERNAL internal_time;
/* DEBIE internal time counter. */


/*  Function prototypes. */

void DelayAwhile (unsigned short duration);
void Read_AD_Channel (ADC_parameters_t EXTERNAL * ADC_results);
void DAC_SelfTest(unsigned char DAC_output,
                  ADC_parameters_t EXTERNAL * ADC_test_parameters) ;
void Monitor(uint_least8_t health_mon_round);
void UpdateTime(void);
void MeasureTemperature(sensor_index_t SU_index);
void HighVoltageCurrent(sensor_index_t SU_index);
void LowVoltageCurrent(void);
void MeasureVoltage(uint_least8_t channel_selector);
void InitSystem(void);
void CalculateChecksum(uint_least8_t checksum_count);
void UpdatePeriodCounter(uint_least8_t EXTERNAL * counter,
                         uint_least8_t full_counter_value);
void Convert_AD (ADC_parameters_t EXTERNAL * ADC_parameters);
void TemperatureFailure(sensor_index_t SU_index);
void VoltageFailure(channel_t ADC_channel);

void SelfTest_SU(sensor_index_t self_test_SU_index);
void Monitor_DPU_Voltage(void);
void Monitor_SU_Voltage(sensor_index_t self_test_SU_index);
unsigned char ExceedsLimit(
   unsigned char value,
   unsigned char lower_limit,
   unsigned char upper_limit);
void RestoreSettings(sensor_index_t self_test_SU_index);
void SelfTestChannel(sensor_index_t self_test_SU_index);
void ExecuteChannelTest(
   sensor_index_t self_test_SU_index,
   unsigned char  test_channel,
   unsigned char  test_pulse_start_level);


/* Other function prototypes. */

void Set_SU_TriggerLevels (
   sensor_number_t         sensor_unit,
   SU_settings_t EXTERNAL *settings);


/*****************************************************************************/
/*                         Boot and DPU self test                            */
/*****************************************************************************/

void Clear_RTX_Errors(void)
/* Purpose        : Clears RTX error registers in telemetry          */
/* Interface      : input   -                                        */
/*                  output  - telemetry_data, rtx error registers    */
/* Preconditions  : none                                             */
/* Postconditions : RTX error registers are cleared.                 */
/* Algorithm      : See below, self explanatory.                     */
{
   telemetry_data.isr_send_message_error    = 0xFF;
   telemetry_data.os_send_message_error     = 0xFF;
   telemetry_data.os_create_task_error      = 0xFF;
   telemetry_data.os_wait_error             = 0xFF;
   telemetry_data.os_attach_interrupt_error = 0xFF;
   telemetry_data.os_enable_isr_error       = 0xFF;
   telemetry_data.os_disable_isr_error      = 0xFF;
}


void SetSoftwareError(unsigned char error) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : This function will be called always when                */
/*                  bit(s) in the software error status                     */
/*                  register are set.                                       */
/* Interface      : inputs      - software error status register            */
/*                              - measurement_error, which specifies what   */
/*                                bits are set in software error status.    */
/*                                Value is as follows,                      */
/*                                           									    */
/*                                MEASUREMENT_ERROR                         */
/*								                                                    */
/*                  outputs     - software error status register            */
/*                  subroutines - none                                      */
/* Preconditions  : none                                                    */
/* Postconditions : none                                                    */
/* Algorithm      : - Disable interrupts                                    */
/*                  - Write to software error status register               */
/*                  - Enable interrupts                                     */
{
   DISABLE_INTERRUPT_MASTER;

   telemetry_data.software_error |= error;

   ENABLE_INTERRUPT_MASTER;
}

void ClearSoftwareError()
/* Purpose        : This function will be called always when all            */
/*                  bits in the software error status                       */
/*                  register are cleared.                                   */
/* Interface      : inputs      - software error status register            */
/*                  outputs     - software error status register            */
/*                  subroutines - none                                      */
/* Preconditions  : none                                                    */
/* Postconditions : none                                                    */
/* Algorithm      :                                							    */
/*                  - Write to SoftwareErrorStatuRegister                   */

{
    telemetry_data.software_error = 0;
}

void SetModeStatusError(unsigned char mode_status_error)
        COMPACT_DATA REENTRANT_FUNC
/* Purpose        : This function will be called always when               */
/*                  error bit(s) in the mode status register are set.      */
/* Interface      : inputs      - mode status register                     */
/*                              - mode_status_error, which specifies what  */
/*                                bit(s) are to be set in                  */
/*                                mode status register. Value is one of    */
/*                                the following,                           */
/*                                           									   */
/*                                  SUPPLY_ERROR                           */
/*                                  DATA_MEMORY_ERROR                      */
/*                                  PROGRAM_MEMORY_ERROR                   */
/*                                  MEMORY_WRITE_ERROR                     */
/*                                  ADC_ERROR                              */
/*                                           									   */
/*                  outputs     - mode status register                     */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      : - Disable interrupts                                   */
/*                  - Write to Mode Status register                        */
/*                  - Enable interrupts                                    */
{
   DISABLE_INTERRUPT_MASTER;

   telemetry_data.mode_status |= (mode_status_error & (~MODE_BITS_MASK));
   /* The mode bits are secured against unintended modification by */
   /* clearing those bits in 'mode_status_error' before "or":ing   */
   /* its value to 'telemetry_data.mode_status'.                   */


   ENABLE_INTERRUPT_MASTER;
}
void ClearModeStatusError(void)
/* Purpose        : This function will be called always when all           */
/*                  error bits in the mode status register are cleared.    */
/* Interface      : inputs      - mode status register                     */
/*        								   */
/*                  outputs     - mode status register                     */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      : - Disable interrupts                                   */
/*                  - Write to Mode Status register                        */
/*                  - Enable interrupts                                    */
{
   DISABLE_INTERRUPT_MASTER;

   telemetry_data.mode_status &= MODE_BITS_MASK;
   /* Error bits in the mode status register are cleared. */

   ENABLE_INTERRUPT_MASTER;
}

void SetMode(DEBIE_mode_t mode) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : This function will be called always when                */
/*                  mode in the mode status register is set.                */
/* Interface      : inputs      - mode status register                      */
/*                                mode_bits, which specify the mode to be   */
/*                                stored in the mode status register.       */
/*                                Value is on one of the following:         */
/*                                   DPU self test                          */
/*                                   stand by                               */
/*                                   acquisition                            */
/*                                       								             */
/*                  outputs     - mode status register                      */
/*                  subroutines - none                                      */
/* Preconditions  : none                                                    */
/* Postconditions : none                                                    */
/* Algorithm      : - Disable interrupts                                    */
/*                  - Write to Mode Status register                         */
/*                  - Enable interrupts                                     */

{
   DISABLE_INTERRUPT_MASTER;

   telemetry_data.mode_status = (telemetry_data.mode_status & ~MODE_BITS_MASK)
                                   | (mode & MODE_BITS_MASK);
   /* First mode status bits are cleared, and then the given mode is set. */

   ENABLE_INTERRUPT_MASTER;
}

DEBIE_mode_t GetMode()
/* Purpose        : This function will be called always when                */
/*                  mode in the mode status register is checked.            */
/* Interface      :                                                         */
/*                  inputs      - mode status register                      */
/*									                                                 */
/*                  outputs     - mode status register                      */
/*                              - Mode bits, which specify the mode         */
/*                                stored in the ModeStatus register.        */
/*                                Value is on one of the following:         */
/*                                   DPU self test                          */
/*                                   stand by                               */
/*                                   acquisition                            */
/*   								                                                 */
/*                  subroutines - none                                      */
/* Preconditions  : none                                                    */
/* Postconditions : none                                                    */
/* Algorithm      :                					                            */
/*                  - Read Mode Status register                             */


{
   return(telemetry_data.mode_status & MODE_BITS_MASK);
   /* Return the value of the two least significant bits in */
   /* mode status register and return this value.           */
}


void Clear_SU_Error(void)
/* Purpose        : This function will be called always when all           */
/*                  error bits in the SU# status register are cleared.     */
/* Interface      : inputs      - SU# status register                      */
/*        								                                          */
/*                  outputs     - SU# status register                      */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      : - Disable interrupts                                   */
/*                  - Write to Mode Status register                        */
/*                  - Enable interrupts                                    */
{
   sensor_index_t EXTERNAL i;

   DISABLE_INTERRUPT_MASTER;
   _Pragma("loopbound min 4 max 4")
   for (i = 0; i < NUM_SU; i++)
   {
      telemetry_data.SU_status[i] &= SUPPLY_VOLTAGE_MASK;
      /* Error bits in the SU# status register are cleared. */
   }

   ENABLE_INTERRUPT_MASTER;
}


void Set_SU_Error(sensor_index_t SU_index, unsigned char SU_error)
/* Purpose        : This function will be called always when               */
/*                  error bit(s) in the SU# status register are set.       */
/* Interface      : inputs      - SU# status register                      */
/*        	                    - 'SU_index' (0-3)                         */
/*                              - 'SU_error' is one of the following:      */
/*                                                                         */
/*                                 LV_SUPPLY_ERROR                         */
/*                                 HV_SUPPLY_ERROR                         */
/*                                 TEMPERATURE_ERROR                       */
/*                                 SELF_TEST_ERROR                         */
/*                                                                         */
/*                  outputs     - SU# status register                      */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      : - Disable interrupts                                   */
/*                  - Write to SU# status register                         */
/*                  - Set corresponding SU# error bit in the               */
/*                    error status register.                               */
/*                  - Enable interrupts                                    */
{

   DISABLE_INTERRUPT_MASTER;

      telemetry_data.SU_status[SU_index] |=
         (SU_error &(~SUPPLY_VOLTAGE_MASK));
      /* Error bits in the SU# status register are cleared. */
      /* The voltage status bits in the SU# status register */
      /* are secured against unintended modification by     */
      /* clearing those bits in 'SU_error' before           */
      /* "or":ing its value to                              */
      /* 'telemetry_data.SU_status'.                        */

      SetErrorStatus(ERROR_STATUS_OFFSET << SU_index);
      /* SU# error is set in the error status register, if      */
      /* anyone of the error bits in the SU# status register    */
      /* is set.                                                */
      /* Because this subroutine enables itself the interrupts, */
      /* the call of it must be the last operation in the       */
      /* interrupt blocked area !                               */

   ENABLE_INTERRUPT_MASTER;
}


void Set_SU_TriggerLevels (
   sensor_number_t         sensor_unit,
   SU_settings_t EXTERNAL *settings)
/* Purpose        : Set all trigger-levels of one SU.                   */
/* Interface      : inputs      - SU number in 'sensor_unit'.           */
/*                              - Triggering levels in 'settings'.      */
/*                  outputs     - Hardware trigger levels.              */
/*                  subroutines - SetTriggerLevel                       */
/* Preconditions  : none                                                */
/* Postconditions : none                                                */
/* Algorithm      : - set trigger level for Plasma 1+                   */
/*                  - set trigger level for Plasma 1-                   */
/*                  - set trigger level for Piezos.                     */
{
   trigger_set_t EXTERNAL trigger;
   /* Holds parameters for SetTriggerLevel. */

   trigger.sensor_unit = sensor_unit;

   trigger.level   = settings -> plasma_1_plus_threshold;
   trigger.channel = PLASMA_1_PLUS;
   SetTriggerLevel (&trigger);

   trigger.level   = settings -> plasma_1_minus_threshold;
   trigger.channel = PLASMA_1_MINUS;
   SetTriggerLevel (&trigger);

   trigger.level   = settings -> piezo_threshold;
   trigger.channel = PZT_1_2;
   SetTriggerLevel (&trigger);
}


void SetErrorStatus(unsigned char error_source)
/* Purpose        : This function will be called always when               */
/*                  error bit(s) in the error status register are set.     */
/*                  Exceptionally TC_ERROR will be set with a separate     */
/*                  function as it is used so often.                       */
/* Interface      : inputs      - error status register                    */
/*                              - 'error_source' specifies the error bit   */
/*                                to be set. Its value is one of the       */
/*                                following,                               */
/*                                                                         */
/*                                SU4_ERROR                                */
/*                                SU3_ERROR                                */
/*                                SU2_ERROR                                */
/*                                SU1_ERROR                                */
/*                                CHECKSUM_ERROR                           */
/*                                WATCHDOG_ERROR                           */
/*                                PARITY_ERROR                             */
/*                                                                         */
/*                  outputs     - error status register                    */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      : - Disable interrupts                                   */
/*                  - Write to error status register                       */
/*                  - Enable interrupts                                    */
{

   DISABLE_INTERRUPT_MASTER;

      telemetry_data.error_status |= (error_source &(~TC_ERROR));
      /* Error bits in the error status register are set.   */
      /* The TC_ERROR bit in the error status register      */
      /* is secured against unintended modification by      */
      /* clearing that bit in 'error_source' before         */
      /* "or":ing its value to                              */
      /* 'telemetry_data.error_status'.                     */


   ENABLE_INTERRUPT_MASTER;
}


void ClearErrorStatus()
/* Purpose        : This function will be called always when               */
/*                  error bits in the error status register are cleared.   */
/* Interface      : inputs      - error status register                    */
/*                                                                         */
/*                  outputs     - error status register                    */
/*                  subroutines - none                                     */
/* Preconditions  : none                                                   */
/* Postconditions : none                                                   */
/* Algorithm      :	                               					         */
/*                  - Write to error status register                       */

{
      telemetry_data.error_status = 0;
      /* Error bits in the error status register are      */
      /* cleared.			                  */
}


void DPU_SelfTest (void)
/* Purpose        : Executes the DPU voltage self test.                      */
/* Interface      : inputs      - none                                       */
/*                                                                           */
/*                  outputs     - none                                       */
/*                  subroutines - Monitor_DPU_Voltage                        */
/* Preconditions  : none                                                     */
/* Postconditions : - Chosen supply voltages are measured.                   */
/* Algorithm      : - Chosen supply voltages are measured and monitored with */
/*                    Monitor_DPU_Voltage                                    */
{
   Monitor_DPU_Voltage();
}

void Boot(void)
/* Purpose        : Executes Boot sequence                                   */
/* Interface      : inputs      - failed_code_address                        */
/*                              - failed_data_address                        */
/*                  outputs     - intialized state of all variables          */
/*                  subroutines - SetSensorUnitOff                           */
/*                                GetResetClass                              */
/*                                SignalMemoryErros                          */
/*                                ResetDelayCounters                         */
/*                                InitClassification                         */
/*                                ClearErrorStatus                           */
/*                                ClearSoftwareError                         */
/*                                Set_SU_TriggerLevels                       */
/* Preconditions  : Init_DPU called earlier, after reset.                    */
/*                  Keil C startup code executed; xdata RAM initialised.     */
/*                  Tasks are not yet running.                               */
/* Postconditions : DAS variables initialised.                               */
/*                  All Sensor Units are in off state                        */
/*                  If boot was caused by power-up reset, TM data registers  */
/*                  and Science Data File are initialized                    */
/*                  If boot was not caused by watchdog-reset, error counters */
/*                  are cleared                                              */
/*                  DEBIE mode is DPU_SELF_TEST                              */
/* Algorithm      : see below.                                               */

{
   EXTERNAL unsigned char  execution_result;
   /* Execution result for SetSensorUnitOff function.  */

   EXTERNAL unsigned char * DIRECT_INTERNAL fill_pointer;
   /* Used for data structure initialization */

   DIRECT_INTERNAL reset_class_t reset_class;
   /* What kind of reset caused this boot ? */

   DIRECT_INTERNAL unsigned int i;
   /* Loop variable */


   SU_ctrl_register |= 0x0F;
   SET_DATA_BYTE(SU_CONTROL,SU_ctrl_register);
   /* Set all Peak detector reset signals to high */

   max_events = MAX_EVENTS;

   ResetDelayCounters();

   SetSensorUnitOff(su1_e, &execution_result);
   /* Set Sensor Unit 1 to Off state */

   SetSensorUnitOff(su2_e, &execution_result);
   /* Set Sensor Unit 2 to Off state */

   SetSensorUnitOff(su3_e, &execution_result);
   /* Set Sensor Unit 3 to Off state */

   SetSensorUnitOff(su4_e, &execution_result);
   /* Set Sensor Unit 4 to Off state */

   ADC_channel_register |= 0x80;
   UPDATE_ADC_CHANNEL_REG;
   /* ADC interleave calibration is not used. */

   reset_class = GetResetClass();
   /* Find out what caused the reset. */


   if (reset_class != warm_reset_e)
   {
      /* We are running the PROM code unpatched, either   */
      /* from PROM or from SRAM.                          */

      reference_checksum = INITIAL_CHECKSUM_VALUE;
      /* 'reference_checksum' is used as a reference when */
      /* the integrity of the code is checked by          */
      /* HealthMonitoringTask. It is set to  its initial  */
      /* value here, after program code is copied from    */
      /* PROM to RAM.                                     */
   }

   if (reset_class == power_up_reset_e)
   {
      /* Data RAM was tested and is therefore garbage. */
      /* Init TM data registers and Science Data File. */

      internal_time = 0;

      fill_pointer = (EXTERNAL unsigned char * DIRECT_INTERNAL)&telemetry_data;

      _Pragma("loopbound min 124 max 124")
      for (i=0; i < sizeof(telemetry_data); i++)
      {
         *fill_pointer = 0;
         fill_pointer++;
      }

      ResetEventQueueLength();
      /* Empty event queue. */

      ClearEvents();
      /* Clears the event counters, quality numbers  */
      /* and free_slot_index of the event records in */
      /* the science data memory.                    */

      InitClassification();
      /* Initializes thresholds, classification levels and */
      /* min/max times related to classification.          */

      Clear_RTX_Errors();
      /* RTX error indicating registers are initialized. */

   }

   else if (reset_class == watchdog_reset_e)
   {
      /* Record watchdog failure in telemetry. */

      telemetry_data.error_status |= WATCHDOG_ERROR;

      if (telemetry_data.watchdog_failures < 255)
      {
         telemetry_data.watchdog_failures++;
      }
   }

   else if (reset_class == checksum_reset_e)
   {
      /* Record checksum failure in telemetry. */

      telemetry_data.error_status |= CHECKSUM_ERROR;

      if (telemetry_data.checksum_failures < 255)
      {
         telemetry_data.checksum_failures++;
      }
   }

   else
   {
      /* Soft or Warm reset. */
      /* Preserve most of telemetry_data; clear some parts. */

      ClearErrorStatus();
      Clear_SU_Error();
      Clear_RTX_Errors();
      ClearSoftwareError();
      telemetry_data.mode_status       &= MODE_BITS_MASK;
      telemetry_data.watchdog_failures  = 0;
      telemetry_data.checksum_failures  = 0;
      telemetry_data.TC_word            = 0;
      /* Clear error status bits, error status counters */
      /* and Command Status register.                   */

      ResetEventQueueLength();
      /* Empty event queue. */

      ClearEvents();
      /* Clears the event counters, quality numbers  */
      /* and free_slot_index of the event records in */
      /* the science data memory.                    */

      InitClassification();
      /* Initializes thresholds, classification levels and */
      /* min/max times related to classification.          */

      self_test_SU_number = NO_SU;
      /* Self test SU number indicating parameter */
      /* is set to its default value.             */
   }


   telemetry_data.mode_status =
      (telemetry_data.mode_status & ~MODE_BITS_MASK) | DPU_SELF_TEST;
   /* Enter DPU self test mode. */

   /* Software version information is stored in the telemetry data. */
   telemetry_data.SW_version = SW_VERSION;

   SignalMemoryErrors();
   /* Copy results of RAM tests to telemetry_data. */

   SetTestPulseLevel(DEFAULT_TEST_PULSE_LEVEL);
   /* Initializes test pulse level. */

   Set_SU_TriggerLevels (SU_1, &telemetry_data.sensor_unit_1);
   Set_SU_TriggerLevels (SU_2, &telemetry_data.sensor_unit_2);
   Set_SU_TriggerLevels (SU_3, &telemetry_data.sensor_unit_3);
   Set_SU_TriggerLevels (SU_4, &telemetry_data.sensor_unit_4);

}


/*****************************************************************************/
/*                         Health Monitoring Task                            */
/*****************************************************************************/


uint_least8_t EXTERNAL health_mon_round = HEALTH_COUNT;
/* This variable is used for counting the ten health monitoring rounds    */
/* which altogether equal 10 secs.                                        */


void _Pragma("entrypoint") InitHealthMonitoring (void)
/* Purpose        : Initialize the health monitoring for DEBIE.              */
/* Interface      : inputs      - none                                       */
/*                                                                           */
/*                  outputs     - telemetry_data                             */
/*                                                                           */
/*                  subroutines - InitSystem()                               */
/*                                DPU_SelfTest()                             */
/*                                SetMode()                                  */
/*                                                                           */
/* Preconditions  : Debie is on                                              */
/* Postconditions : See subroutines                                          */
/* Algorithm      :                                                          */
/*                   - Executes InitSystem()                                 */
/*                   - Executes DPU_SelfTest()                               */
/*                   - Enter stand_by mode                                   */
{
   InitSystem();
   /* Initializes the system.    */

   DPU_SelfTest();
   /* Execute the DPU self test. */

   SetMode(STAND_BY);
   /* Switch to Standby mode */
}


void _Pragma("entrypoint") HandleHealthMonitoring (void)
/* Purpose        : One round of health monitoring for DEBIE.                */
/* Interface      : inputs      - telemetry_data                             */
/*                                                                           */
/*                  outputs     - telemetry_data                             */
/*                                                                           */
/*                  subroutines - UpdateTime()                               */
/*                                Monitor()                                  */
/*                                UpdatePeriodCounter()                      */
/*                                WaitInterval()                             */
/*                                                                           */
/* Preconditions  : Debie is on                                              */
/* Postconditions : See subroutines                                          */
/* Algorithm      :                                                          */
/*                  - Updates sensor unit states                             */
/*                  - Executes UpdateTime()                                  */
/*                  - Calls Monitor() function                               */
/*                  - UpdatePeriodCounter() function advances the            */
/*                    health monitoring counter                              */
/*                  - Executes WaitInterval()                                */
{
   Update_SU_State (0);
   Update_SU_State (1);
   Update_SU_State (2);
   Update_SU_State (3);

   UpdateTime();
   /* Update telemetry registers. */

   Monitor(health_mon_round);
   /* Execute current Health Monitoring Round.                            */

   UpdatePeriodCounter(&health_mon_round, HEALTH_COUNT);
   /* Decrease or reset health monitor loop counter depending on its      */
   /* current and limiting values.                                        */

   WaitInterval(HM_INTERVAL);
   /* Wait for next activation */
}


void HealthMonitoringTask(void) TASK(HEALTH_MONITORING_TASK)
                                PRIORITY(HEALTH_MONITORING_PR)
/* Purpose        : Takes care of health monitoring for DEBIE.               */
/* Interface      : inputs      - telemetry_data                             */
/*                                                                           */
/*                  outputs     - telemetry_data                             */
/*                                                                           */
/*                  subroutines - UpdateTime()                               */
/*                                Monitor()                                  */
/*                                UpdatePeriodCounter()                      */
/*                                WaitInterval()                             */
/*                                                                           */
/* Preconditions  : Debie is on                                              */
/* Postconditions : See subroutines                                          */
/* Algorithm      :                                                          */
/*                  - InitHealthMonitoring                                   */
/*                  - loop forever:                                          */
/*                  -   HandleHealthMonitoring                               */
{
   InitHealthMonitoring ();

   _Pragma("loopbound min 0 max 0")
   while(1)
   {
      HandleHealthMonitoring ();
   }
}


void Monitor(uint_least8_t health_mon_round)
/* Purpose        : Monitors DEBIE's vital signs                             */
/* Interface      : inputs      - Health Monitoring Round count              */
/*                  outputs     - none                                       */
/*                  subroutines - MeasureTemperature()                       */
/*                                CheckCurrent()                             */
/*                                MeasureVoltage()                           */
/*                                CalculateChecksum()                        */
/* Preconditions  : Health monitoring is on.                                 */
/* Postconditions : Health monitoring duties are carried out.                */
/* Algorithm      :                                                          */
/*                     - Executes the given health monitor loop round.       */
/*                     - Starts three loops:                                 */
/*                         - voltage measurement loop      180 secs          */
/*                         - temperature measurement loop  60 secs           */
/*                         - checksum count loop           60 secs           */
/*                     The values of these counters are decreased after each */
/*                     loop cycle.                                           */
/*                                                                           */
/*                     Health monitoring loop which lasts 10 secs and is     */
/*                     divided into 10 individual rounds. On each round      */
/*                     some specific Health Monitoring duties are carried    */
/*                     out. For example the Watchdog counter is resetted.    */
/*                                                                           */
/*                     Temperature measurement loop lasts 60 secs and        */
/*                     consists of 6 Health Monitoring loop cycles each      */
/*                     lasting 10 secs. It is executed partly on each 10 sec */
/*                     Health Monitoring cycle by measuring temperatures of  */
/*                     one SU on each cycle. Measurement starts on the second*/
/*                     Health Monitoring cycle and is completed after six    */
/*                     cycles.                                               */
/*                                                                           */
/*                     Voltage measurement loop lasts 180 secs and consists  */
/*                     of 18 Health Monitoring loop cycles each lasting 10   */
/*                     secs. On each cycle some of its duties are carried    */
/*                     out.                                                  */
/*                                                                           */
/*                     Checksum calculation loop lasts 60 secs and consists  */
/*                     of 6 Health Monitoring loop cycles each lasting 10    */
/*                     secs. It is executed  partly on each Health Monitoring*/
/*                     round.                                                */
/*                                                                           */
/*    Illustration of the process:                                           */
/*                                                                           */
/*              M                                                            */
/*     _________|_________                                                   */
/*    |                   |                                                  */
/*    |R-R-R-R-R-R-R-R-R-R> : Health Monitoring loop: M                      */
/*    <---------------------<   Round: R = 1 sec                             */
/*                              M = 10*R = 10 secs                           */
/*                                                                           */
/*          T                                                                */
/*     _____|_____                                                           */
/*    |           |                                                          */
/*    |M-M-M-M-M-M>   Temperature Measurement loop: T                        */
/*    <-----------<   Health Monitoring loop: M = 10 sec                     */
/*                    T = 6*M = 60 secs                                      */
/*                                                                           */
/*                  C                                                        */
/*     _____________|____________     Checksum count loop: C                 */
/*    |                          |                                           */
/*    |R-R-R-R-R-R-R-R-R-R>                                                  */
/*      >R-R-R-R-R-R-R-R-R-R>                                                */
/*       >R-R-R-R-R-R-R-R-R-R>                                               */
/*        >R-R-R-R-R-R-R-R-R-R>                                              */
/*         >R-R-R-R-R-R-R-R-R-R>                                             */
/*          >R-R-R-R-R-R-R-R-R-R>                                            */
/*                                Health Monitoring loop cycle: R = 1 sec    */
/*                                C = 60*R = 60 secs                         */
/*                                                                           */
/*                      V                                                    */
/*     _________________|__________________                                  */
/*    |                                   |                                  */
/*    |M-M-M-M-M-M-M-M-M-M-M-M-M-M-M-M-M-M>   Voltage Measurement loop: V    */
/*    <-----------------------------------<   Health Monitoring loop: M      */
/*                                            V = 18*M = 180 secs            */
{
   CalculateChecksum(checksum_count);
   /* A 1/60th part of the memory checksum is calculated.                 */

   UpdatePeriodCounter(&checksum_count, CHECK_COUNT);
   /* Decrease or reset checksum counter                                  */
   /* depending on its current and limiting values.                       */


   switch (health_mon_round)
   {
      case round_0_e:

         HighVoltageCurrent((sensor_index_t)health_mon_round);
         /* Overcurrent indicating bits related to sensor unit 1 in HV       */
         /* status register are checked.                                     */

         UpdatePeriodCounter(&temp_meas_count, TEMP_COUNT);
         UpdatePeriodCounter(&voltage_meas_count, VOLTAGE_COUNT);
         /* Decrease or reset temperature, checksum and voltage counters     */
         /* depending on their current and limiting values.                  */


         hit_budget_left = hit_budget;
         /* Health Monitoring period ends and new hit budget can be started. */

         if (HIT_TRIGGER_FLAG == 0)
         {
            /* Hit budget was exceeded during this ending Health Monitoring */
            /* period.                                                      */

            ResetPeakDetector(SU_1);
            ResetPeakDetector(SU_2);
            ResetPeakDetector(SU_3);
            ResetPeakDetector(SU_4);
            /* Reset all Peak detectors */

            WaitTimeout(COUNTER_RESET_MIN_DELAY);

            ENABLE_HIT_TRIGGER;
            /* Allows a later falling edge on T2EX to cause */
            /* a Hit Trigger interrupt (i.e. to set EXF2).  */

            ResetDelayCounters();
            /* Resets the SU logic that generates Hit Triggers.    */
            /* Brings T2EX to a high level, making a new falling   */
            /* edge possible.                                      */
            /* This statement must come after the above "enable",  */
            /* since T2EX edges are not remembered by the HW from  */
            /* before the "enable", unlike normal interrupt enable */
            /* and disable masking.                                */
         }

         break;

      case round_1_e:

         HighVoltageCurrent((sensor_index_t)health_mon_round);
         /* Overcurrent indicating bits related to sensor unit 2 in HV       */
         /* status register are checked.                                     */

         break;

      case round_2_e:

         HighVoltageCurrent((sensor_index_t)health_mon_round);
         /* Overcurrent indicating bits related to sensor unit 3 in HV       */
         /* status register are checked.                                     */

         break;

      case round_3_e:

         HighVoltageCurrent((sensor_index_t)health_mon_round);
         /* Overcurrent indicating bits related to sensor unit 4 in HV       */
         /* status register are checked.                                     */

         break;

      case round_4_e:

         LowVoltageCurrent();
         /* 'V_DOWN' indicator bit is checked.                               */


         break;

      case round_5_e:

         if (voltage_meas_count < 7)

         {
            /* Seven Secondary voltage channels are measured starting when   */
            /* 'voltage_meas_count' reaches a value of 6. Last measurement is*/
            /*  executed on voltage_meas_count value 0.                      */

            MeasureVoltage(voltage_meas_count);
         }

         break;

      case round_6_e:

         if ((self_test_SU_number               != NO_SU) &&
	     (SU_state[self_test_SU_number - SU1] == self_test_e))
         {
            /* SU self test sequence continues   */

            SelfTestChannel(self_test_SU_number - SU1);
            /* SU channels are monitored in this round. */

            self_test_SU_number = NO_SU;
            /* SU self test sequence ends here  */
         }

         break;

      case round_7_e:

         if (self_test_SU_number != NO_SU)
         {
            /* SU self test sequence has started   */

            self_test_flag = SELF_TEST_RUNNING;
            /* Indication of a started test. */

            SelfTest_SU(self_test_SU_number - SU1);
            /* Supply voltages and SU temperatures are monitored in this round. */

            if (self_test_SU_number != NO_SU)
            {
               SU_state[self_test_SU_number - SU1] = self_test_e;
            }

         }

         break;

      case round_8_e:

         SET_WD_RESET_HIGH;

         /* The Watch Dog time out signal state is reset HIGH state, as it is*/
         /* falling edge active.                                             */

         break;

      case round_9_e:

         if (temp_meas_count < NUM_SU)

         {
            /* Two channels of one sensor unit are measured when             */
            /* 'temp_meas_count' reaches 3 -> 2 -> 1 -> 0. I.e. measuring    */
            /*  begins after 10 secs and is finished after 50 secs.          */

            MeasureTemperature((sensor_index_t)temp_meas_count);
         }

         SET_WD_RESET_LOW;

         /* The Watch Dog timer is reset by setting WD_RESET bit low at I/O  */
         /* port 1. This is done here with 10 sec interval. The watch dog    */
         /* time-out is 12.1 secs.                                           */
         break;
   }

}

void UpdateTime(void)
/* Purpose        : advances time in the telemetry                           */
/* Interface      : inputs      - telemetry_data.time                        */
/*                  outputs     - telemetry_data.time                        */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : Time updated in telemetry_data.                          */
/* Algorithm      :                                                          */
/*                  Time in the telemetry_data.time is advanced until        */
/*                  maximum value for the variable is reached, after that it */
/*                  is implicitely wrapped-around on overflow.               */
{

   DISABLE_INTERRUPT_MASTER;
   /* Disable all interrupts.                                             */

   internal_time ++;
   /* Increment internal time. */

   ENABLE_INTERRUPT_MASTER;
   /* Enable all interrupts.                                              */

}


void MeasureTemperature(sensor_index_t SU_index)
/* Purpose        : Measures and monitors SU temperatures                    */
/* Interface      : inputs      - SU_index, sensor unit index  (0 - 3)       */
/*                                telemetry_data                             */
/*                  outputs     - telemetry_data                             */
/*                  subroutines - Read_AD_Channel()                          */
/*                                TemperatureFailure()                       */
/* Preconditions  : none                                                     */
/* Postconditions : - ADC temperature channels are measured.                 */
/*                  - In case of an overheated SU,                           */
/*                    'TemperatureFailure()' function is called.i.e.         */
/*                    all secondary supply voltages to that SU are switched  */
/*                    off, SU related Bit in the Error Status Register is set*/
/*                    and temperature error indicating bit in the SU_Status  */
/*                    register is set.                                       */
/*                  - If a measurement has failed, in addition to overheating*/
/*                    response also measurement error indication bit in mode */
/*                    status register is set                                 */
/* Algorithm      : - Temperatures of a given sensor unit are measured.      */
/*                  - If measured temperature is large enough, it is stored  */
/*                    into telemetry.                                        */
/*                                                                           */
/*                  - Else measured temperature value is too small to be     */
/*                    stored. Zero value is stored into telemetry.           */
/*                                                                           */
/*                  - If temperature of any of the Sensor Units is over      */
/*                    MAX_TEMP, 'TemperatureFailure()' function is           */
/*                    called.                                                */
/*                                                                           */
/*                  - If temperature measurement of a Sensor Unit has failed,*/
/*                    temperature error indicating bit in the SU_Status      */
/*                    register is set and TemperatureFailure()'              */
/*                    function is called.                                    */
{

   ADC_parameters_t EXTERNAL AD_temperature_parameters;
   /* This struct is used to hold parameters for Reading AD channels.        */

   unsigned char temp_limit_value;

   uint_least8_t EXTERNAL j;
   /* This variable is used in for-loop.                                     */



   _Pragma("loopbound min 2 max 2")
   for (j=0; j < NUM_TEMP; j++)

   {
      AD_temperature_parameters.ADC_channel =
         5 + (SU_index&1)*8 + (SU_index&2)*12 + j;
      /* Select the channel to be measured.                                  */

      AD_temperature_parameters.ADC_max_tries = ADC_TEMPERATURE_MAX_TRIES;
      /* When temperatures are measured this variable defines the maximum    */
      /* amount of tries to be used in reading and handling an AD channel in */
      /* 'Read_AD_Channel()'.                                                */

      AD_temperature_parameters.conversion_max_tries =
         CONVERSION_TEMPERATURE_MAX_TRIES;
      /* When temperatures are measured this variable defines the maximum    */
      /* amount of tries to be used when End Of Conversion indication is     */
      /* waited in function 'Convert_AD()'.                                  */

      Read_AD_Channel(&AD_temperature_parameters);
      /* Get ADC temperature measurement result.                             */



      if (AD_temperature_parameters.unsigned_ADC & 0x8000)

      {
         /* Temperature is stored in the telemetry.                          */

         telemetry_data.SU_temperature[SU_index][j] =
         (unsigned char)((AD_temperature_parameters.unsigned_ADC
         & 0x7FFF) >> 7);
         /* Store bits 7 .. 14 */
      }

      else

      {

         telemetry_data.SU_temperature[SU_index][j] = 0;
         /* Temperature too small -> store zero */

      }

      temp_limit_value = ( j==0 ? MAX_TEMP_1 : MAX_TEMP_2 );

      if (telemetry_data.SU_temperature[SU_index][j] > temp_limit_value)

      {
         /* Temperature has exeeded a predefined limit                       */

         TemperatureFailure(SU_index);
         /* Given SU is switched off, error and SU status registers are      */
         /* updated in telemetry.                                            */

      }

      if (AD_temperature_parameters.AD_execution_result != RESULT_OK)

      {
         /* An anomaly has occurred during the measurement.                  */

         SetSoftwareError(MEASUREMENT_ERROR);
         /* Set measurement error indication bit in   */
         /* software error status register.           */





         TemperatureFailure(SU_index);
         /* Given SU is switched off and error and SU status registers are   */
         /* updated in telemetry.                                            */

 	}

   }
}


void HighVoltageCurrent(sensor_index_t SU_index)
/* Purpose        : Monitors overcurrent indicating bits in the HV Status    */
/*                  register for a given sensor unit.                        */
/* Interface      : inputs      - SU_index, sensor unit index (0 - 3)        */
/*                                telemetry_data                             */
/*                                HV_status register                         */
/*                  outputs     - telemetry_data                             */
/*                  subroutines - SetErrorStatus()                           */
/*                                Set_SU_Error()                             */
/* Preconditions  : none                                                     */
/* Postconditions : following registers are updated in case of an error,     */
/*                  - Sensor Unit or units are switched off                  */
/*                  - Error Status register updated                          */
/*                  - SU Status register updated                             */
/* Algorithm      :                                                          */
/*                  - if any of the HV_Status bits indicate a short          */
/*                    circuit or overload, the corresponding Error Status    */
/*                    Bits in the Error Status and SU_Status Registers are   */
/*                    set.                                                   */
{
   unsigned char EXTERNAL SU_current_mask[] = {3,12,48,192};
   /* This array holds parameters for checking the HV status register.       */

   unsigned char EXTERNAL valid_value[] = {1,4,16,64};
   /* This array holds comparison parameters for checking the HV status      */
   /* register.                                                              */

   if (CHECK_CURRENT(SU_current_mask[SU_index]) !=
           valid_value[SU_index])

   {
      /* Overcurrent is detected.                                            */

      SetErrorStatus(ERROR_STATUS_OFFSET << SU_index);
      /* Set high corresponding bit for the SU in Error Status Register.     */

      Set_SU_Error(SU_index, HV_SUPPLY_ERROR);
      /* Set high HV supply error indicating bit in the SU_Status register*/

   }

}

void LowVoltageCurrent(void)
/* Purpose        : Monitors low voltage currents in Sensor Units.           */
/* Interface      : inputs      - telemetry_data                             */
/*                                        V_DOWN bit                         */
/*                  outputs     - telemetry_data                             */
/*                  subroutines - SetSensorUnitOff()                         */
/* Preconditions  : none                                                     */
/* Postconditions : Following actions are taken in case of an error,         */
/*                  - Sensor Unit or units are switched off                  */
/*                  - SU Status register updated                             */
/*                  - Error Status register updated                          */
/* Algorithm      :                                                          */
/*                  - If V_DOWN bit in I/O port 1 is LOW indicating that     */
/*                    +-5 V DC/DC converter(s) is(are) limiting the output   */
/*                    current, all SU supply voltages are                    */
/*                    switched off and corresponding bits in the Error and   */
/*                    mode Status Register are set.                          */
{

   sensor_index_t EXTERNAL i;
   /* This variable is used in a for-loop.                                   */

   unsigned char EXTERNAL exec_result;
   /* This variable is used by SetSensorUnitOff() function.                 */

   if (V_DOWN == LOW)

   {
      /*  An error is detected, output current is limited.                   */

      _Pragma("loopbound min 4 max 4")
      for (i = 0; i < NUM_SU; i++)

      {
         /* Switch off all Sensor Units. */

         SetSensorUnitOff(i,&exec_result);
         /* Switch off given sensor unit.                                 */

         Set_SU_Error(i,LV_SUPPLY_ERROR);
         /* Set high LV supply error indicating bit in the SU_Status      */
         /* register.                                                     */
      }

      SetErrorStatus(OVERALL_SU_ERROR);
     /* Set all SU error status bits in 'error status register' at telemetry.*/

   }
}

void MeasureVoltage(uint_least8_t channel_selector)
/* Purpose        : Measure secondary Sensor Unit voltages.                  */
/* Interface      : inputs      - Channel selector, values 0 - 6             */
/*                  outputs     - telemetry_data.mode_status                 */
/*                  subroutines - Read_AD_Channel()                          */
/*                                VoltageFailure()                           */
/* Preconditions  : none                                                     */
/* Postconditions : - Measurement results are written to telemetry.          */
/*                  - If a measurement has failed, measurement error         */
/*                    indication bit in mode status register is set and      */
/*                    'VoltageFailure()' function is called.i.e.             */
/*                    SUs related to failed ADC channel are switched         */
/*                    off, SU related Bit in the Error Status Register is set*/
/*                    and LV error indicating bit in the SU_Status           */
/*                    register is set.                                       */
/* Algorithm      :    - Secondary SU supply voltages are measured from      */
/*                       a given channel.                                    */
/*                     - If measurement has failed,                          */
/*                       measurement error indication bit in mode status     */
/*                       register is set and 'VoltageFailure()'              */
/*                       function is called.                                 */
/*                     - If no errors have occurred, results are stored      */
/*                       in telemetry_data.                                  */
{

   ADC_parameters_t EXTERNAL AD_voltage_parameters;
   /* This struct is used to hold parameters for Reading AD channels.        */

   unsigned char EXTERNAL voltage_channel[] = {
      0x10,0x11,0x12,0x13,0x54,0x55,0x56};
   /* This array holds parameters for setting the ADC channel for the        */
   /* measurement.                                                           */

   AD_voltage_parameters.ADC_channel = voltage_channel[channel_selector];
   /* Select the channel to be measured:                                     */
   /* channel_selector ->  ADC channel                                       */
   /*                0 ->  0x10                                              */
   /*                1 ->  0x11                                              */
   /*                2 ->  0x12                                              */
   /*                3 ->  0x13                                              */
   /*                4 ->  0x54                                              */
   /*                5 ->  0x55                                              */
   /*                6 ->  0x56                                              */

   AD_voltage_parameters.ADC_max_tries = ADC_VOLTAGE_MAX_TRIES;
   /* When voltages are measured this variable defines the maximum        */
   /* amount of tries to be used in reading and handling an AD channel in */
   /* 'Read_AD_Channel()'.                                                */

   AD_voltage_parameters.conversion_max_tries =
      CONVERSION_VOLTAGE_MAX_TRIES;
   /* When voltages are measured this variable defines the maximum        */
   /* amount of tries to be used when End Of Conversion indication is     */
   /* waited in function 'Convert_AD()'.                                  */

   Read_AD_Channel(&AD_voltage_parameters);
   /* Voltage channel is read.                                            */

   if (AD_voltage_parameters.AD_execution_result != RESULT_OK)
   {
      /* An anomaly has occurred during the measurement. */

      SetSoftwareError(MEASUREMENT_ERROR);
      /* Set measurement error indication bit in */
      /* software error status register.         */

   }

   else
   {

      switch (channel_selector)
      {
         /* Measurement result bits 8..15 from channels involving positive      */
         /* voltages are written to telemetry.                                  */

         case channel_0_e:

            telemetry_data.sensor_unit_1.plus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            telemetry_data.sensor_unit_2.plus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;

         case channel_1_e:

            telemetry_data.sensor_unit_3.plus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            telemetry_data.sensor_unit_4.plus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;

         case channel_2_e:

            telemetry_data.SU_plus_50 = AD_voltage_parameters.unsigned_ADC >> 8;

            break;

         case channel_3_e:

            telemetry_data.DPU_plus_5_digital =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;


        /* Measurement result bits 8..15 from channels involving negative      */
        /* voltages are written to telemetry.                                  */
        /* Note that even here, the "unsigned" or "raw" conversion result is   */
        /* used; this is a requirement.                                        */

         case channel_4_e:

            telemetry_data.sensor_unit_1.minus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            telemetry_data.sensor_unit_2.minus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;

         case channel_5_e:

            telemetry_data.sensor_unit_3.minus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            telemetry_data.sensor_unit_4.minus_5_voltage =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;

         case channel_6_e:

            telemetry_data.SU_minus_50 =
               AD_voltage_parameters.unsigned_ADC >> 8;

            break;
      }
   }
}

void InitSystem(void)
/* Purpose        : Initialize system after RTX system is started.           */
/* Interface      : inputs      - none                                       */
/*                  outputs     - none                                       */
/*                  subroutines - SetTimeSlice()                             */
/*                                WaitInterval()                             */
/*                                CreateTask()                               */
/* Preconditions  : RTX is on.                                               */
/* Postconditions : System initialization duties are carried out i.e. rtx    */
/*                  tasks are activated, system clock interval is set.       */
/* Algorithm      :    - Set system clock interval                           */
/*                     - Wait for automatic A/D converter calibration.       */
/*                     - Activate the Telecommand Execution task.            */
/*                     - Activate the Acquisition task.                      */
/*                     - Activate Hit Trigger Interrupt Service task.        */
{
   task_info_t EXTERNAL new_task;
   /* Parameters for creating new task */

   SetTimeSlice(SYSTEM_INTERVAL);
   /* Set system clock interval */

   WaitInterval(BOOT_WAIT_INTERVAL);
   /* Wait for automatic A/D converter calibration */

   new_task.rtx_task_number    = TC_TM_INTERFACE_TASK;
   new_task.task_main_function = TC_task;
   CreateTask(&new_task);
   /* Activate the Telecommand Execution task */

   new_task.rtx_task_number    = ACQUISITION_TASK;
   new_task.task_main_function = acq_task;
   CreateTask(&new_task);
   /* Activate the Acquisition task */

   new_task.rtx_task_number    = HIT_TRIGGER_ISR_TASK;
   new_task.task_main_function = hit_task;
   CreateTask(&new_task);
   /* Activate Hit Trigger Interrupt Service task */

}

void CalculateChecksum(uint_least8_t checksum_count)
/* Purpose        : Calculates memory checksum.                              */
/* Interface      : inputs      - 'checksum_count' gives the current         */
/*                                 checksum loop cycle value,                */
/*                                 MIN_CHECKSUM_COUNT <= 'checksum_count'    */
/*                                                    <=  MAX_CHECKSUM_COUNT */
/*                                                                           */
/*                              - 'reference_checksum' variable to verify    */
/*                                correct codememory.                        */
/*                              - global variable 'code_not_patched'         */
/*                  outputs     - 'code_checksum' is modified.               */
/*                              - value of global variable                   */
/*                                'code_not_patched' is conditionally set.   */
/*                  subroutines - Reboot()                                   */
/* Preconditions  : none                                                     */
/* Postconditions : Changes values of variable 'code_checksum' and           */
/*                  on first round variable 'code_not_patched'.              */
/*                                                                           */
/*                  - In case of a checksum error,                           */
/*                    Soft reset is executed, if 'code_not_patched'          */
/*                    indication enables this.                               */
/*                                                                           */
/* Algorithm      : - Health monitoring calculates checksum with XOR for     */
/*                    CHECK_SIZE amount of codememory bytes at a time. At    */
/*                    the end of the check cycle, i.e. 'checksum_count' = 0, */
/*                    the variable 'code_checksum'                           */
/*                    should be equal with 'reference_checksum', if no       */
/*                    errors have occurred and code has not been patched     */
/*                    during the checksum cycle.                             */
/*                                                                           */
/*                  - If an error is detected in the code memory, Reboot()   */
/*                    function is called.                                    */
/*                                                                           */
/*                  - if no anomalies have been encountered there will be no */
/*                    changes and checksum loop starts from the beginning    */
/*                    i.e. on 'checksum_count' = 59, 'code_not_patched'      */
/*                    is set and 'code_checksum' is initialised.             */
{
   code_address_t EXTERNAL i;
   /* This variable is used in a for-loop.                                   */

   code_address_t EXTERNAL check_start;
   /* This variable is used for determining the start address of a given     */
   /* check.                                                                 */

   code_address_t EXTERNAL check_end;
   /* This variable is used for determining the end address of a given check.*/

   check_start = checksum_count * CHECK_SIZE;

   if (checksum_count == MAX_CHECKSUM_COUNT)

   {
      /* This piece ends at the top memory address. */
      check_end = CODE_MEMORY_END;
   }

   else

   {
      check_end = check_start + (CHECK_SIZE - 1);
   }

   if (checksum_count == MAX_CHECKSUM_COUNT)
   {

     code_checksum = 0;
     /* This global variable is used to store the result from each XOR     */
     /* calculation during a code memory checksum round. It is cleared     */
     /* here at the beginning of a new cycle and checked at the end of     */
     /* each cycle against 'reference_checksum'.                           */

     code_not_patched = 1;
     /* This global variable shows whether code is patched during a code   */
     /* memory checksum cycle. It is set here at the beginning of a new    */
     /* cycle and checked at the end of each cycle whether it has been     */
     /* cleared as an indication of an executed code patching.             */
   }

   _Pragma("loopbound min 495 max 547")
   for (i = check_start; i <= check_end; i++)

   {
     /* It is assumed that 'CODE_MEMORY_END'  < 2^16 - 1 */
     /* Otherwise variable 'i' can never have a value    */
     /* larger than 'check_end' and this loop will never */
     /* stop.                                            */

      code_checksum ^= GET_CODE_BYTE(i);
      /* XOR is counted for code memory byte under check. */
   }

   if (    (checksum_count == MIN_CHECKSUM_COUNT)
        && (code_checksum  != reference_checksum)   )
   {
      /* Checksum mismatch due to a memory error or */
      /* code memory patch.                         */

      if (code_not_patched)
      {
         /* An anomaly has been detected in the code memory  */
         /* area. Code has not been patched during this code */
         /* memory checksum cycle.                           */

         Reboot (checksum_reset_e);
         /* Soft reset is executed, as global variable       */
         /* 'code_not_patched' enables it. Note that         */
         /* Reboot() does not return here.                   */
      }
   }

}


void UpdatePeriodCounter(
   uint_least8_t EXTERNAL * counter,
   uint_least8_t full_counter_value)
/* Purpose        : Advances counters                                        */
/* Interface      : inputs      - address to counter variable                */
/*                  outputs     - none                                       */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : counter value is adjusted                                */
/* Algorithm      : - If a given counter is not already zero, its value is   */
/*                    decreased.                                             */
/*                  - Else it set to its initialization value.               */
{
   if (*counter)

   {
      (*counter)--;
      /* Decrease temperature measurement counter. */
   }

   else
   {
      *counter = full_counter_value;
      /* Reset temperature measurement counter. */
   }
}



void Convert_AD (ADC_parameters_t EXTERNAL * ADC_parameters)
/* Purpose        : Conversion is executed on a selected AD channel          */
/* Interface      : inputs      - Address of a struct for storing the ADC    */
/*                                results.                                   */
/*                  outputs     - ADC results are stored to the given struct */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : ADC results are written to a struct                      */
/* Algorithm      : - AD Conversion is started on the selected channel.      */
/*                  - End of conversion is polled, EOC bit at I/O port 1,    */
/*                    as long as the preset limit is not exceeded.           */
/*                  - If limit has not been exeeded, resulting MSB and LSB of*/
/*                    the conversion are read from the HW registers and      */
/*                    combined into one word.                                */
/*                    Else end of conversion can no longer be waited. No     */
/*                    results are gained, instead a zero is stored with an   */
/*                    indication of this occurred anomaly.                   */
{
   unsigned char DIRECT_INTERNAL conversion_count;

   /* Counts the amount of end of conversion polls.                          */

   unsigned char DIRECT_INTERNAL  msb, lsb;
   /* MSB and LSB of the conversion result                                   */

   unsigned int DIRECT_INTERNAL word;
   /*This variable is used to combine MSB and LSB bytes into one word.       */

   START_CONVERSION;

   conversion_count = 0;

   _Pragma("loopbound min 0 max 5")
   while(conversion_count < ADC_parameters -> conversion_max_tries
          && (END_OF_ADC != CONVERSION_ACTIVE))
   {
      /* Previous conversion is still active.                                */
      conversion_count++;
   }

   /* There is a slight chance for the following occurrence:                 */
   /* Conversion has ended after max_tries has been reached but before the   */
   /* following condition loop is entered. As a result measurement is failed */
   /* even if conversion has ended in time. The effect is dimished if the    */
   /* max_tries is a large value i.e. end of conversion has been waited long */
   /* enough.                                                                */

   if (conversion_count < ADC_parameters -> conversion_max_tries  )

   {
      /* Conversion has ended. Read AD result.                               */

      msb = GET_RESULT;
      lsb = GET_RESULT;

      word = (unsigned int)msb*256+lsb;
      /* Combine MSB and LSB as type 'unsigned int' in the given struct.    */

      ADC_parameters -> signed_ADC =  (signed short int)(word^0x8000);
      /* Store result as of type 'signed int'.                            */

      ADC_parameters -> unsigned_ADC = word;
      /* Store result as of type 'unsigned int'.                             */

      ADC_parameters -> AD_execution_result = RESULT_OK;
      /* Store indication of an succesful measurement.                       */

   }

   else

   {

      /* Conversion has not ended in time. No results gained, store zero.    */
      ADC_parameters -> unsigned_ADC = 0;
      ADC_parameters -> signed_ADC = 0;

      /* Store indication of an unsuccesful measurement.                     */
      ADC_parameters -> AD_execution_result = CONVERSION_ACTIVE;

   }

}

void VoltageFailure(channel_t ADC_channel)
/* Purpose        : Takes care of resulting actions in case of a failed      */
/*                  measurement.                                             */
/* Interface      : inputs      - 'ADC_channel' related to the failed        */
/*                                measurement.                               */
/*                                                                           */
/*                  outputs     - telemetry_data.error_status register       */
/*                                telemetry_data.SU_status register          */
/*                                telemetry_data.mode_status register        */
/*                  subroutines - none                                       */
/* Preconditions  : There has been an anomaly                                */
/*                  during an ADC channel measurement.                       */
/* Postconditions : Sensor unit(s) related error indication bit(s) are set   */
/*                  in error_status, SU_status and mode_status registers.    */
/* Algorithm      :                                                          */
/*                  'ADC_channel' is the number of the channel which failed  */
/*                  measurement. The following actions are taken.            */
/*                     - Find out which sensor units are related to failed   */
/*                       measurement and store indications of these target   */
/*                       SUs in the 'SU_action[]' array.                     */
/*                     - If channel number indicates that the DPU voltage    */
/*                       measurement has failed, set high DPU +5V measurement*/
/*                       error indicating bit in the mode_status register.   */
/*                     - Set Error status bit of the related SU(s) in error  */
/*                       status register.                                    */
/*                     - If the channel number is for +- 50V, set high the   */
/*                       HV supply error indicating bit in the SU_Status     */
/*                       register for the related SU(s).                     */
/*                     - If the channel number if low voltage, set high the  */
/*                       LV supply error indicating bit in the SU_Status     */
/*                       register for the related SU(s).                     */
{
   unsigned char EXTERNAL i;
   /* This variable is used in a for-loop.                                   */

   unsigned char EXTERNAL SU_action[NUM_SU] = {LOW, LOW, LOW, LOW};
   /* This array stores actions targeted at a SU judging by the given        */
   /* ADC information.                                                       */


   /* Voltage measurement has failed, actions are taken accordingly.      */


   if (ADC_channel == 0x10 || ADC_channel == 0x54)

   {
      /* Measurement of channels related to sensor units 1 and 2 have     */
      /* failed.                                                          */

      SU_action[su1_e] = HIGH;
      SU_action[su2_e] = HIGH;

      /* Select sensor units 1 and 2 as targets in the 'SU_action' array. */

      SetModeStatusError(SUPPLY_ERROR);
      /* Set Supply Error. */


   }

   else if (ADC_channel == 0x11 || ADC_channel == 0x55)

   {
      /* Measurement of channels related to sensor units 3 and 4 have     */
      /* failed.                                                          */

      SU_action[su3_e] = HIGH;
      SU_action[su4_e] = HIGH;

      /* Select sensor units 3 and 4 as targets in the 'SU_action' array. */
   }

   else if (ADC_channel == 0x12 || ADC_channel == 0x56)

   {
      /* Measurement of channels related to all sensor units have         */
      /* failed.                                                          */

      SU_action[su1_e] = HIGH;
      SU_action[su2_e] = HIGH;
      SU_action[su3_e] = HIGH;
      SU_action[su4_e] = HIGH;

      /* Select all sensor units as targets in the 'SU_action' array.     */
   }

   else if (ADC_channel == 0x13)

   {
      /* Measurement of a channel related to DPU voltage has failed.      */

      SetModeStatusError(SUPPLY_ERROR);
      /* Set high DPU +5V supply error indicating bit in the         */
      /* mode status register.                                       */

   }

   _Pragma("loopbound min 4 max 4")
   for (i = 0; i < NUM_SU; i++)

   {
      /* Set error indications related to selected Sensor Units. */

      if (SU_action[i])

      {
         SetErrorStatus (ERROR_STATUS_OFFSET << i);
         /* Set Error status bit of the related SU in error status */
         /* register.                                              */

         if (ADC_channel == 0x12
          || ADC_channel == 0x56)
         {
            Set_SU_Error (i, HV_SUPPLY_ERROR);
            /* Set high HV supply error indicating bit in the */
            /*  SU_Status register.                           */
         }

         if (ADC_channel == 0x10
          || ADC_channel == 0x11
          || ADC_channel == 0x54
          || ADC_channel == 0x55)
         {
            Set_SU_Error (i, LV_SUPPLY_ERROR);
            /* Set high LV supply error indicating bit in the */
            /* SU_Status register.                            */
         }

      }

   }
}


void TemperatureFailure(sensor_index_t SU_index)
/* Purpose        : Takes care of resulting actions in case of a failed      */
/*                  measurement.                                             */
/* Interface      : inputs      - 'SU_index' which contains sensor unit      */
/*                                index number related to failed measurement */
/*                                or overheated SU.                          */
/*                  outputs     - telemetry_data.error_status register       */
/*                                telemetry_data.SU_status register          */
/*                  subroutines - SetSensorUnitOff()                         */
/* Preconditions  : There has been an anomaly                                */
/*                  during an ADC temperature channel measurement or         */
/*                  measurement has revealed that the given SU is            */
/*                  overheated.                                              */
/* Postconditions : Selected Sensor unit is switched off and error indication*/
/*                  bit is set in error_status register and SU_status        */
/*                  register.                                                */
/* Algorithm      : Following actions are taken,                             */
/*                     - switch off given sensor unit                        */
/*                     - Set Error status bit of the related SU in error     */
/*                       status register.                                    */
/*                     - Set high temperature error indicating bit in the    */
/*                       SU_Status register.                                 */
{

   unsigned char EXTERNAL exec_result;
   /* This variable is used by SetSensorUnitOff() function.        */

   /* Temperature measurement has failed, actions are taken accordingly. */

   SetSensorUnitOff(SU_index,&exec_result);
   /* Switch off given sensor unit. */

   SetErrorStatus(ERROR_STATUS_OFFSET << SU_index);
   /* Set Error status bit of the related SU in error status register. */

   Set_SU_Error(SU_index, TEMPERATURE_ERROR);
   /* Set high temperature error indicating bit in the SU_Status */
   /* register.                                                  */
}







/*****************************************************************************/
/*                         AD conversion routines                            */
/*****************************************************************************/


void DelayAwhile (unsigned short duration)
/* Purpose        : Delay for a (brief) duration.                            */
/* Interface      : inputs      - delay duration, in ShortDelay() units.     */
/*                  outputs     - none.                                      */
/*                  subroutines - ShortDelay()                               */
/* Preconditions  : none.                                                    */
/* Postconditions : at least "duration" time units have passed.              */
/* Algorithm      : Call ShortDelay() as many times as necessary to delay    */
/*                  for at least the desired duration.                       */
{
   _Pragma("loopbound min 1 max 3")
   while (duration > MAX_SHORT_DELAY)
   {
      ShortDelay (MAX_SHORT_DELAY);
      duration = duration - MAX_SHORT_DELAY;
      /* Since ShortDelay() has a positive constant delay term, the  */
      /* actual total delay will be a little larger than 'duration'. */
   }

   if (duration > 0)
   {
      /* Some delay left after the loop above. */
      ShortDelay ((unsigned char) duration);
   }
}


void Read_AD_Channel (ADC_parameters_t EXTERNAL * ADC_parameters)
/* Purpose        : Reading an ADC channel                                   */
/* Interface      : inputs      - Address of a struct which contains         */
/*                                parameters for this function.              */
/*                  outputs     - Results are stored to the previously       */
/*                                mentioned structure.                       */
/*                  subroutines - Convert()                                  */
/* Preconditions  : Health monitoring is on.                                 */
/*                  ADC_parameters.ADC_max_tries > 0.                        */
/* Postconditions : AD channels are measured and results written to a given  */
/*                  structure.                                               */
/* Algorithm      :                                                          */
/*                  while                                                    */
/*                     - Hit trigger interrupt indicating flag is resetted.  */
/*                     - Handling of the given channel is started in a while */
/*                       loop.                                               */
/*                     - Channel to be converted is selected by setting bits */
/*                       0 - 6 from ADC Channel register to value of channel.*/
/*                       Channel value includes BP_UP bit to select          */
/*                       unipolar/bipolar mode.                              */
/*                     - Convert_AD() function executes the conversion and   */
/*                       stores the results in the given struct mentioned    */
/*                       earlier.                                            */
/*                     - If Hit trigger interrupt flag indicates that no     */
/*                       hit trigger interrupts have occurred during the     */
/*                       channel reading, exit the while loop.               */
/*                       Else continue loop from beginning, if               */
/*                       predefined limit for executing the loop has not been*/
/*                       reached.                                            */
/*                     - If all the conversion tries have been used up and   */
/*                       Hit trigger interrupt has corrupted the results,    */
/*                       only a zero is stored with an indication of this    */
/*                       occurred anomaly.                                   */
/*                  End of loop                                              */
{
   unsigned char EXTERNAL tries_left;
   /* Number of attempts remaining to try conversion without */
   /* interference from a particle hit.                      */

   unsigned short EXTERNAL delay_limit;
   /* Delay between channel selection and start of conversion in */
   /* ShortDelay() units.                                        */

   delay_limit = DELAY_LIMIT(2000);
   /* Set delay limit to 2ms. */

   tries_left = ADC_parameters -> ADC_max_tries;
   /* Limits the number of conversion attempts repeated because */
   /* of particle hit interrupts. Assumed to be at least 1.     */

   _Pragma("loopbound min 1 max 8")
   while (tries_left > 0)
   {
      confirm_hit_result = 0;
      /* Clear interrupt indicating flag.                                    */

      ADC_channel_register = (ADC_channel_register & 0x80) |
                                 ADC_parameters -> ADC_channel;
      UPDATE_ADC_CHANNEL_REG;
      /* AD Channel register is set. */

      START_CONVERSION;
      /* Initiate dummy cycle to set AD mode to unipolar or bipolar.         */

      DelayAwhile (delay_limit);
      /* Wait for analog signal and MUX to settle. */

      Convert_AD(ADC_parameters);
      /* Start conversion and measurement.                                   */

      tries_left--;
      /* Repeat while-loop until the max number of tries. */

      if (confirm_hit_result == 0)
      {
         /* Conversion has NOT been interrupted by a hit trigger interrupt.  */
         /* Exit from the while-loop.                                        */

         tries_left = 0;
      }
   }

   if (confirm_hit_result != 0)

   {
      /* Conversion has been interrupted by a hit trigger interrupt. Discard */
      /* corrupted results.                                                  */

      ADC_parameters -> unsigned_ADC = 0;
      ADC_parameters -> signed_ADC   = 0;

      ADC_parameters -> AD_execution_result = HIT_OCCURRED;
      /* Store indication of an unsuccessful measurement.                  */

   }

   else if (ADC_parameters -> AD_execution_result == CONVERSION_ACTIVE
               && confirm_hit_result == 0)
   {
      SetModeStatusError(ADC_ERROR);
      /* ADC error indication is set because a time-out has          */
      /* occurred during AD conversion and no hit trigger interrupt  */
      /* has occurred.                                               */
   }


   /* Either RESULT_OK or CONVERSION_ACTIVE indications are already */
   /* stored in the 'ADC_parameters -> AD_execution_result' field   */
   /* as a result from conversion in the Convert() function.        */
}









void DAC_SelfTest(unsigned char DAC_output,
                  ADC_parameters_t EXTERNAL * ADC_test_parameters)
/* Purpose        : To test the analog signal chain in the circuitry.        */
/* Interface      : inputs      - Test data 'DAC_Output                      */
/*                              - struct 'ADC_test_parameters'               */
/*                  outputs     - none                                       */
/*                  subroutines - Read_AD_Channel()                          */
/* Preconditions  : none                                                     */
/* Postconditions : DAC selftest is carried out and results written to a     */
/*                  given struct.                                            */
/* Algorithm      :                                                          */
/*                  - Test data is written to HW registers.                  */
/*                  - Test channel is AD converted.                          */
/*                  - Results are stored in a given structure.               */
/*                  - If the measurement has failed,                         */
/*                                                                           */
/*                     - Set self test error indication bit in mode status   */
/*                       register                                            */
/*                     - Set measurement error indication bit in mode status */
/*                       register.                                           */
{

   SET_DAC_OUTPUT(DAC_output);
   /*test data is written to DAC output                                     */

   Read_AD_Channel(ADC_test_parameters);
   /* Start reading the channel.                                             */

	if (ADC_test_parameters -> AD_execution_result != RESULT_OK)

      {
         /* An anomaly has occurred during the measurement.                  */

         Set_SU_Error(ADC_test_parameters -> sensor_unit - SU1,
                             SELF_TEST_ERROR);
         /* Self self test error indication bit in mode status register.    */
         /* Offset in numbering is taken into account by transforming       */
         /* 'sensor_unit' (value 1-4) to a sensor unit index (value 0-3)    */

         SetSoftwareError(MEASUREMENT_ERROR);
         /* Set measurement error indication bit in */
         /* software error status register.         */

      }
}


void Monitor_DPU_Voltage(void)
/* Purpose        : Monitors DPU voltages                                    */
/* Interface      : inputs      - telemetry_data, DPU voltages               */
/*                  outputs     - telemetry_data.mode_status                 */
/*                                telemetry_data, supply voltages            */
/*                  subroutines -  SetModeStatusError                        */
/*                                 ExceedsLimit                              */
/*                                 MeasureVoltage                            */
/* Preconditions  : none                                                     */
/* Postconditions : DPU voltages are measured and monitored.                 */
/* Algorithm      :                                                          */
/*                  - Channels are checked one by one and in case of an error*/
/*                    corresponding error bit is set.                        */
{
   MeasureVoltage(DPU_5V_SELECTOR);
   MeasureVoltage(SU_1_2_P5V_SELECTOR);
   MeasureVoltage(SU_1_2_M5V_SELECTOR);

   /* Result of voltage measurement from SU_1/2 +5V is compared against */
   /* limits.          							*/

   if (ExceedsLimit(telemetry_data.sensor_unit_1.plus_5_voltage,
         SU_P5V_ANA_LOWER_LIMIT,
         SU_P5V_ANA_UPPER_LIMIT))
   {
         SetModeStatusError(SUPPLY_ERROR);
   }

   /* Result of voltage measurement from SU_1/2 -5V is compared against */
   /* limits.          							*/

   if (ExceedsLimit(telemetry_data.sensor_unit_1.minus_5_voltage,
         SU_M5V_ANA_LOWER_LIMIT,
         SU_M5V_ANA_UPPER_LIMIT))
   {
         SetModeStatusError(SUPPLY_ERROR);
   }

   /* Result of voltage measurement from DIG +5V is compared against    */
   /* limits.          							*/

   if (ExceedsLimit(telemetry_data.DPU_plus_5_digital,
         DPU_P5V_DIG_LOWER_LIMIT,
         DPU_P5V_DIG_UPPER_LIMIT))
   {
         SetModeStatusError(SUPPLY_ERROR);
   }

}


void Monitor_SU_Voltage(sensor_index_t self_test_SU_index)
/* Purpose        : Monitors SU voltages                                     */
/* Interface      : inputs      - self_test_SU_index                         */
/*                                telemetry_data, sensor voltages            */
/*                  outputs     - telemetry_data.SU_error                    */
/*                  subroutines -  ExceedsLimit                              */
/*                                 Set_SU_Error                              */
/* Preconditions  : SU voltages are measured                                 */
/* Postconditions : SU voltages are monitored.                               */
/* Algorithm      :                                                          */
/*                  - Channels are checked one by one and in case of an error*/
/*                    corresponding error bit is set.                        */
{
   switch (self_test_SU_index)
   {
      case su1_e:
      case su2_e:

         /* Result of voltage measurement from SU_1/2 +5V is compared against */
         /* limits.          						      */

         if (ExceedsLimit(telemetry_data.sensor_unit_1.plus_5_voltage,
               SU_P5V_ANA_LOWER_LIMIT,
               SU_P5V_ANA_UPPER_LIMIT))
         {
            Set_SU_Error(self_test_SU_index, LV_LIMIT_ERROR);
         }

         /* Result of voltage measurement from SU_1/2 -5V is compared against */
         /* limits.          						      */

         if (ExceedsLimit(telemetry_data.sensor_unit_1.minus_5_voltage,
               SU_M5V_ANA_LOWER_LIMIT,
               SU_M5V_ANA_UPPER_LIMIT))
         {
            Set_SU_Error(self_test_SU_index, LV_LIMIT_ERROR);
         }

         break;


      case su3_e:
      case su4_e:

         /* Result of voltage measurement from SU_3/4 +5V is compared against */
         /* limits.          						      */

         if (ExceedsLimit(telemetry_data.sensor_unit_3.plus_5_voltage,
               SU_P5V_ANA_LOWER_LIMIT,
               SU_P5V_ANA_UPPER_LIMIT))
         {
            Set_SU_Error(self_test_SU_index, LV_LIMIT_ERROR);
         }

         /* Result of voltage measurement from SU_3/4 -5V is compared against */
         /* limits.          						      */

         if (ExceedsLimit(telemetry_data.sensor_unit_3.minus_5_voltage,
               SU_M5V_ANA_LOWER_LIMIT,
               SU_M5V_ANA_UPPER_LIMIT))
         {
            Set_SU_Error(self_test_SU_index, LV_LIMIT_ERROR);
         }

         break;
   }

   /* Result of voltage measurement from SU +50V is compared against */
   /* limits.          						     */

   if (ExceedsLimit(telemetry_data.SU_plus_50,
         SU_P50V_LOWER_LIMIT,
         SU_P50V_UPPER_LIMIT))
   {
      Set_SU_Error(self_test_SU_index, HV_LIMIT_ERROR);
   }

   /* Result of voltage measurement from SU -50V is compared against */
   /* limits.          						     */

   if (ExceedsLimit(telemetry_data.SU_minus_50,
         SU_M50V_LOWER_LIMIT,
         SU_M50V_UPPER_LIMIT))
   {
      Set_SU_Error(self_test_SU_index, HV_LIMIT_ERROR);
   }
}

void SelfTest_SU(sensor_index_t self_test_SU_index)
/* Purpose        : Execute SU self tests                                    */
/* Interface      : inputs      - self_test_SU_index                         */
/*                  outputs     - none                                       */
/*                  subroutines -  LowVoltageCurrent()                       */
/*                                 MeasureVoltage()                          */
/*                                 MeasureTemperature()                      */
/*                                 HighVoltageCurrent()                      */
/* Preconditions  : none                                                     */
/* Postconditions : Part of the Self Test sequence regarding temperatures,   */
/*                  voltages and overcurrents is completed.                  */
/* Algorithm      : - V_DOWN is checked                                      */
/*                  - Voltage channels are checked one by one and in case of */
/*                    an error corresponding error bit is set.               */
/*                  - SU Temperatures and HV Status Register is checked.     */
{
   uint_least8_t EXTERNAL i;
   /* Used in a for loop */

   LowVoltageCurrent();
   /* V_DOWN is checked. */

   HighVoltageCurrent(self_test_SU_index);
   /* HV Status register is checked. */

   /* SU voltages are measured */
   _Pragma("loopbound min 7 max 7")
   for (i = channel_0_e; i <= channel_6_e; i++)
   {
      MeasureVoltage(i);
      /* All voltage channels are measured. */
   }

   Monitor_SU_Voltage(self_test_SU_index);
   /* Voltage measurement results are monitored against limits. */

   MeasureTemperature(self_test_SU_index);
   /* SU temperatures are measured and monitored. */
}

unsigned char ExceedsLimit(
   unsigned char value,
   unsigned char lower_limit,
   unsigned char upper_limit)
/* Purpose        : Tests given value against given limits.                  */
/* Interface      : inputs      - value                                      */
/*                                lower_limit                                */
/*                                upper_limit                                */
/*                  outputs     - boolean value                              */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : Given value is tested.                                   */
/* Algorithm      : See below, self explanatory.                             */
{
   return (value < lower_limit || value > upper_limit);
}


void SelfTestChannel(sensor_index_t self_test_SU_index)
/* Purpose        : Starts channel tests                                     */
/* Interface      : inputs      - self_test_SU_index                         */
/*                  outputs     - none                                       */
/*                  subroutines -  SetTestPulseLevel                         */
/*                                 SetTriggerLevel                           */
/*                                 RestoreSettings                           */
/*                                 ExecuteChannelTest                        */
/*                                 DisableAnalogSwitch                       */
/* Preconditions  : none                                                     */
/* Postconditions : SU channels are self tested.                             */
/*                  voltages and overcurrents is completed.                  */
/* Algorithm      : - Threshold level is set high.                           */
/*                  - Test pulse level for a given channel is set high.      */
/*                  - Channels are tested.                                   */
/*                  - Threshold level is set low.                            */
/*                  - Test pulse level for a given channel is set low .      */
/*                  - Channels are tested.                                   */
/*                  - A pseudo trigger is generated in order to reset peak   */
/*                    detector and delay counter in AcquisitionTask.         */
/*                  - Threshold levels are restored to the level prior to    */
/*                    the test. SU state for the SU under test is restored   */
/*                    to ON.                                                 */
{
   trigger_set_t EXTERNAL test_threshold;

   DISABLE_HIT_TRIGGER;

   /* Initial parameters for SetTriggerLevel function. */
   test_threshold.sensor_unit = self_test_SU_number;
   test_threshold.level       = MAX_PLASMA_SELF_TEST_THRESHOLD;
   test_threshold.channel     = PLASMA_1_PLUS;
   SetTriggerLevel(&test_threshold);
   test_threshold.channel     = PLASMA_1_MINUS;
   SetTriggerLevel(&test_threshold);
   test_threshold.channel     = PLASMA_2_PLUS;
   SetTriggerLevel(&test_threshold);

   test_threshold.level       = MAX_PIEZO_SELF_TEST_THRESHOLD;
   test_threshold.channel     = PZT_1_2;
   SetTriggerLevel(&test_threshold);


   /* Set initial test pulse value to 0. Test pulse value is also zeroed    */
   /* before returning from ExecuteChannelTest procedure also.              */
   SetTestPulseLevel(0);

   /* Test threshold level is set before each channel test for every channel*/
   /* and value is set back to the maximum threshold level before returning */
   /* from the following ExecuteChannelTest procedure calls.                */

   test_threshold.channel     = PLASMA_1_PLUS;
   test_threshold.level       = HIGH_PLASMA_1_PLUS_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PLASMA_1_PLUS, PLASMA_1_PLUS_HIGH);

   test_threshold.channel     = PLASMA_1_MINUS;
   test_threshold.level       = HIGH_PLASMA_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PLASMA_1_MINUS, PLASMA_1_MINUS_HIGH);

   ExecuteChannelTest(self_test_SU_index, PLASMA_2_PLUS, PLASMA_2_PLUS_HIGH);

   test_threshold.channel     = PZT_1_2;
   test_threshold.level       = HIGH_PIEZO_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PZT_1, PZT_1_HIGH);

   test_threshold.channel     = PZT_1_2;
   test_threshold.level       = HIGH_PIEZO_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PZT_2, PZT_2_HIGH);

   test_threshold.channel     = PLASMA_1_PLUS;
   test_threshold.level       = LOW_PLASMA_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PLASMA_1_PLUS, PLASMA_1_PLUS_LOW);

   test_threshold.channel     = PLASMA_1_MINUS;
   test_threshold.level       = LOW_PLASMA_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PLASMA_1_MINUS, PLASMA_1_MINUS_LOW);

   ExecuteChannelTest(self_test_SU_index, PLASMA_2_PLUS, PLASMA_2_PLUS_LOW);

   test_threshold.channel     = PZT_1_2;
   test_threshold.level       = LOW_PIEZO_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PZT_1, PZT_1_LOW);

   test_threshold.channel     = PZT_1_2;
   test_threshold.level       = LOW_PIEZO_SELF_TEST_THRESHOLD;
   SetTriggerLevel(&test_threshold);
   ExecuteChannelTest(self_test_SU_index, PZT_2, PZT_2_LOW);

   ENABLE_HIT_TRIGGER;

   SET_HIT_TRIGGER_ISR_FLAG;
   /* A pseudo trigger is generated in order to reset peak   */
   /* detector and delay counter in AcquisitionTask.         */
   /* No event is recorded i.e. event processing is disabled */
   /* because SU state is 'self_test_e'.                     */

   RestoreSettings(self_test_SU_index);
}


void ExecuteChannelTest(
   sensor_index_t self_test_SU_index,
   unsigned char test_channel,
   unsigned char test_pulse_level)
/* Purpose        : Execute SU self tests                                    */
/* Interface      : inputs      - self_test_SU_index, test_channel           */
/*                  outputs     - telemetry_data.SU# Status                  */
/*                  subroutines -  SelectSelfTestChannel                     */
/*                                 SelectStartSwitchLevel                    */
/*                                 WaitTimeout                               */
/*                                 DelayAwhile                               */
/*                                 SelectTriggerSwitchLevel                  */
/* Preconditions  : none                                                     */
/* Postconditions : Self test trigger siggnal is generated.                  */
/*                  voltages and overcurrents is completed.                  */
/* Algorithm      : - Self test channel is selected.                         */
/*                  - Analog switch starting level is selected depending on  */
/*                    the channel.                                           */
/*                  - A pseudo trigger is generated in order to reset peak   */
/*                    detector and delay counter in AcquisitionTask.         */
/*                  - Hit trigger processing is waited for 40 ms.            */
/*                  - Hit trigger processing for this self test pulse is     */
/*                    enabled by setting SU state to 'self_test_trigger_e'   */
/*                  - Analog switch triggering level is selected depending   */
/*                    on the channel (rising or falliing edge).              */
/*                  - If self test trigger pulse did not cause an interrupt, */
/*                    set SELF_TEST_ERROR indication in SU status register   */
/*                    and restore SU state to 'self_test_e'.                 */
{
   unsigned short EXTERNAL delay_limit;
   trigger_set_t  EXTERNAL test_threshold;

   if (test_channel == PLASMA_1_PLUS ||
       test_channel == PLASMA_1_MINUS ||
       test_channel == PLASMA_2_PLUS)
   {
      SelectSelfTestChannel(test_channel);

      EnableAnalogSwitch(self_test_SU_index);

      WaitTimeout(1);

      ResetPeakDetector(self_test_SU_index + SU1);

      WaitTimeout(1);

      ResetPeakDetector(self_test_SU_index + SU1);

      WaitTimeout(1);

      CLEAR_HIT_TRIGGER_ISR_FLAG;

      ResetDelayCounters();

      SU_state[self_test_SU_index] = self_test_trigger_e;
      /* Enable hit trigger processing for this self test pulse. */

      ENABLE_HIT_TRIGGER;

      SetTestPulseLevel(test_pulse_level);

      /* Set at least 1ms test pulse    */
      delay_limit = DELAY_LIMIT(1000);
      DelayAwhile(delay_limit);

      /* For plasma 1i channel triggering must take place in 1ms after */
      /* rising edge.                                                  */
      if (test_channel                 == PLASMA_1_MINUS &&
          SU_state[self_test_SU_index] == self_test_trigger_e)
      {
         /* Self test trigger pulse did not cause an interrupt. */
         Set_SU_Error(self_test_SU_index, SELF_TEST_ERROR);

         SU_state[self_test_SU_index] = self_test_e;
         /* Triggering of a self test pulse is disabled by restoring */
         /* the self_test_e state.                                   */
      }

      /* Test pulse is always at least 3ms (=1ms+2ms) */
      delay_limit = DELAY_LIMIT(2000);
      DelayAwhile(delay_limit);

      SetTestPulseLevel(0);

      if (test_channel == PLASMA_2_PLUS)
      {
	 SET_HIT_TRIGGER_ISR_FLAG;
      }

      /* If channel is plasma 1e or 2e then wait at least 1ms after */
      /* falling edge.                                              */
      if (test_channel != PLASMA_1_MINUS)
      {
         delay_limit = DELAY_LIMIT(1000);
         /* Set at least 1ms test pulse    */
         DelayAwhile(delay_limit);
      }

      DISABLE_HIT_TRIGGER;

      if (test_channel != PLASMA_2_PLUS)
      {
         test_threshold.sensor_unit = self_test_SU_index + SU1;
         test_threshold.channel     = test_channel;
         test_threshold.level       = MAX_PLASMA_SELF_TEST_THRESHOLD;
         SetTriggerLevel(&test_threshold);
      }

      DisableAnalogSwitch(self_test_SU_index);
   }
   else
   {
      SelectSelfTestChannel(test_channel);

      SetTestPulseLevel(test_pulse_level);

      WaitTimeout(1);

      ResetPeakDetector(self_test_SU_index + SU1);

      WaitTimeout(1);

      ResetPeakDetector(self_test_SU_index + SU1);

      WaitTimeout(1);

      CLEAR_HIT_TRIGGER_ISR_FLAG;

      ResetDelayCounters();

      SU_state[self_test_SU_index] = self_test_trigger_e;
      /* Enable hit trigger processing for this self test pulse. */

      ENABLE_HIT_TRIGGER;

      EnableAnalogSwitch(self_test_SU_index);

      /* Set at least 1ms test pulse    */
      delay_limit = DELAY_LIMIT(1000);
      DelayAwhile(delay_limit);

      DISABLE_HIT_TRIGGER;

      SetTestPulseLevel(0);

      DisableAnalogSwitch(self_test_SU_index);

      test_threshold.sensor_unit = self_test_SU_index + SU1;
      test_threshold.level       = MAX_PIEZO_SELF_TEST_THRESHOLD;
      test_threshold.channel     = PZT_1_2;
      SetTriggerLevel(&test_threshold);
   }

   if (SU_state[self_test_SU_index] == self_test_trigger_e)
   {
      /* Self test trigger pulse did not cause an interrupt. */
      Set_SU_Error(self_test_SU_index, SELF_TEST_ERROR);

      SU_state[self_test_SU_index] = self_test_e;
      /* Triggering of a self test pulse is disabled by restoring */
      /* the self_test_e state.                                   */
   }
}

void RestoreSettings(sensor_index_t self_test_SU_index)
/* Purpose        : Restores settings after SU self tests.                   */
/* Interface      : inputs      - self_test_SU_index,                        */
/*                                telemetry_data, SU threshold levels        */
/*                  outputs     - HW registers, thresholds                   */
/*                                SU state                                   */
/*                  subroutines -  SetTriggerLevel                           */
/*                                 Switch_SU_State                           */
/* Preconditions  : none                                                     */
/* Postconditions : - Threshold levels are restored to the level prior to    */
/*                    the test. SU state for the SU under test is restored   */
/*                    to ON.                                                 */
/* Algorithm      : - Original threshold levels are copied from              */
/*                    telemetry_data and written in to HW registers with     */
/*                    SetTriggerLevel.                                       */
/*                  - SU state is restored to ON with Switch_SU_State.       */
{
   SU_settings_t EXTERNAL *const PROGRAM setting_map_c[] = {
      &telemetry_data.sensor_unit_1,
      &telemetry_data.sensor_unit_2,
      &telemetry_data.sensor_unit_3,
      &telemetry_data.sensor_unit_4
      };
      /* Pointers to Sensor Unit configuration data in telemetry */
      /* data area.                                              */

   SU_settings_t EXTERNAL * INDIRECT_INTERNAL SU_setting;
   /* Pointer to configuration data of the Sensor Unit being */
   /* Self Tested.                                           */

   sensor_unit_t EXTERNAL SU_switch;
   trigger_set_t EXTERNAL threshold;
   /* Parameters for subroutines. */

   SU_setting = setting_map_c[self_test_SU_index];

   threshold.sensor_unit = self_test_SU_number;

   threshold.level   =
      SU_setting -> plasma_1_plus_threshold;
   threshold.channel = PLASMA_1_PLUS;
      SetTriggerLevel(&threshold);
   /* Restore Plasma 1 Plus trigger threshold. */

   threshold.level   =
      SU_setting -> plasma_1_minus_threshold;
   threshold.channel = PLASMA_1_MINUS;
   SetTriggerLevel(&threshold);
   /* Restore Plasma 1 Minus trigger threshold. */

   threshold.level   =
   SU_setting -> piezo_threshold;
   threshold.channel = PZT_1_2;
   SetTriggerLevel(&threshold);
   /* Restore Piezo trigger threshold. */

   SU_switch.SU_number             = self_test_SU_number;
   SU_switch.SU_state              = on_e;
   SU_switch.expected_source_state = self_test_e;
   Switch_SU_State(&SU_switch);
   /* Switch SU State back to On. */

}


/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   hw_if.c
 *
 * Initialization and test of hardware.
 *
 * Based on the SSF file hw_if.c, rev 1.34, Sun Jul 25 15:47:56 1999.
 *
 *-----------------------------------------------------------------------------
 */

#include "dpu_ctrl.h"
#include "su_ctrl.h"
#include "isr_ctrl.h"
#include "version.h"
#include "tc_hand.h"
#include "tm_data.h"
#include "ttc_ctrl.h"


reset_class_t EXTERNAL s_w_reset;
/* The type of the last DPU reset, as recorded in Init_DPU. */
/* Note: this variable must _not_ be initialised here (in   */
/* its declaration), since this would overwrite the value   */
/* set in Init_DPU, which is called from the startup module */
/* before the variable initialisation code.                 */


unsigned char EXTERNAL SU_ctrl_register     = 0;
unsigned char EXTERNAL SU_self_test_channel = 0;
/* These variables store values of these write-only registers. */

unsigned char EXTERNAL LOCATION(0xFF00) forbidden_area[256];
/* Last 256 bytes of the external data memory are reserved for     */
/* memory mapped registers. No variables are allowed in that area. */


/* Results of the RAM test done at boot time: */

code_address_t EXTERNAL failed_code_address;
data_address_t EXTERNAL failed_data_address;
#define NO_RAM_FAILURE 0xFFFF
/* The test records the address of the first failed cell in the */
/* code (program) RAM and the external data RAM.                */
/* If no problem is found, the address is NO_RAM_FAILURE.       */
/* Note: these variables must _not_ be initialised here (at     */
/* declaration), since this initialisation would be done after  */
/* the RAM test and so would destroy its results.               */
/* These variables are set by Init_DPU, which is called from    */
/* the startup module.                                          */


/* Other memory-control variables: */

memory_configuration_t EXTERNAL memory_mode;
/* The current memory mapping mode. */

const unsigned char PROGRAM checksum_nullifier = CODE_CHECKSUM;
/* Sets the checksum of the unpatched program code to zero. */

unsigned char EXTERNAL reference_checksum;
/* Expected code checksum. Zero for unpatched code. */

unsigned char EXTERNAL code_not_patched;
/* Initial value is 1, set in Init_DPU. Value is 1 when code */
/* checksum value is valid, cleared to 0 when code memory is */
/* patched, set to 1 when next checksum calculation          */
/* period is started.                                        */

unsigned char EXTERNAL analog_switch_bit[NUM_SU] = {0x10, 0x20, 0x40, 0x80};
/* This array stores the value to be used when analog switch bit */
/* corresponding to a given SU is set.                           */

/* Function prototypes. */
void CopyProgramCode(void);
code_address_t InitCode_RAM (reset_class_t reset_class);
void InitData_RAM (
   reset_class_t  reset_class,
   code_address_t code_address);

/*****************************************************************************/
/*                               dpu_ctrl.h                                  */
/*****************************************************************************/

unsigned short int Check_RAM (
   data_address_t start_address,
   data_address_t end_address)
/* Purpose        : Check the working of an area of external RAM.            */
/* Interface      : inputs      - range of memory addresses.                 */
/*                  outputs     - first failed address.                      */
/*                                NO_RAM_FAILURE indicates "no failure".     */
/*                  subroutines - TestMemBits                                */
/*                              - TestMemSeq                                 */
/*                              - TestMemData                                */
/* Preconditions  : - start_address  <=  end_address.                        */
/*                  - end_address < 0xFFFF.                                  */
/*                  - External Data RAM is not initialised.                  */
/* Postconditions : The addresses in the given range are tested up to the    */
/*                  ending address or the first failing address.             */
/*                  The original contents of the memory area are lost.       */
/* Algorithm      : - The first memory address is tested with TestMemBits.   */
/*                  - The address range is traversed in pieces acceptable    */
/*                    to TestMemSeq/Data (max 255 bytes at a time).          */
/*                  - For each piece, TestMemSeq/Data is called.             */
/*                  - If an error is found, the failing address              */
/*                    is returned and the test is interrupted.               */
/*                  - If no failure is found, 0xFFFF is returned.            */
/*                                                                           */
/* This function must not rely on any constants in data RAM, since           */
/* it is called before the C start-up system initialises data.               */

{
   /* Note that the local variables of this function should all be */
   /* located in the internal data memory. Otherwise they may be   */
   /* altered unexpectedly as a side effect of testing memory.     */

   data_address_t INDIRECT_INTERNAL start;
   /* The starting address for TestMemSeq and TestMemData. */

   uint_least16_t INDIRECT_INTERNAL range;
   /* The remaining range (ending address - starting address). */

   uint_least8_t INDIRECT_INTERNAL bytes;
   /* The number of bytes to check, for TestMemSeq/Data. */

   uint_least8_t INDIRECT_INTERNAL bytes_left;
   /* The number of bytes left, returned from TestMemSeq/Data. */

   start = start_address;

   if (TestMemBits(start) != 0)
   {
      /* Failure in data bus, probably. */
      return start;
   }

   _Pragma("loopbound min 0 max 0")
   while (start <= end_address)
   {
      range = end_address - start;
      /* Number of bytes to check, less one. */

      if (range < 255)
      {
         /* One call of TestMemSeq/Data covers the remaining range. */
         bytes = (uint_least8_t)(range + 1);
      }
      else
      {
         /* One call cannot cover the remaining range. */
         /* Cover as much as possible for one call.    */
         bytes = 255;
      }

      bytes_left = TestMemSeq (start, bytes);

      if (bytes_left == 0)
      {
         /* TestMemSeq succeeded. Try TestMemData. */
         bytes_left = TestMemData (start, bytes);
      }

      if (bytes_left > 0)
      {
         /* Memory error. Return failing address. */
         return (bytes - bytes_left) + start;
      }

      start = start + bytes;
      /* Next address to check, or end_address+1 if all done.  */
      /* Wrap-around cannot happen since end_address < 0xFFFF. */
   }

   /* Memory is OK. */
   return NO_RAM_FAILURE;
}


void Init_DPU (reset_class_t reset_class)
/* Purpose        : DEBIE-specific DPU and I/O initialisation at reset.      */
/* Interface      : inputs      - reset class                                */
/*                  outputs     - s_w_reset                                  */
/*                              - code_not_patched                           */
/*                              - failed_code_address                        */
/*                              - failed_data_address                        */
/*                              - memory_mode                                */
/*                  subroutines - InitCode_RAM                               */
/*                              - InitData_RAM                               */
/* Preconditions  : Basic startup initialisation done.                       */
/*                  Code is being executed from PROM.                        */
/*                  Tasks are not yet running.                               */
/*                  External Data RAM is not initialised.                    */
/* Postconditions : see algorithm                                            */
/* Algorithm      : see below, self-explanatory.                             */

/* This function is called by the startup assembly code.           */
/* This function must not rely on any constants in data RAM, since */
/* it is called before the C start-up system initialises data.     */

{
   /* Note: be careful with use of external Data RAM in this function. */
   /* If InitData_RAM tests the RAM, it will destroy the contents.     */

   volatile reset_class_t INDIRECT_INTERNAL safe_reset_class;
   /* Copy of reset_class in internal RAM, safe from RAM test. */

   code_address_t INDIRECT_INTERNAL code_address;
   /* Result of InitCode_RAM. */


   safe_reset_class = reset_class;
   /* Copy to internal RAM. */

   SET_WD_RESET_LOW;

   /* The Watch Dog timer is reset by setting WD_RESET bit low at I/O  */
   /* port 1.                                                          */

   SET_INTERRUPT_PRIORITIES;
   /* Define the high/low priority of each interrupt. */

   SET_INT_TYPE1_EDGE;
   /* The interrupt control type 1 bit is set to 'falling edge' state. */

   SET_INT_TYPE0_EDGE;
   /* The interrupt control type 1 bit is set to 'falling edge' state. */

   STOP_TC_TIMER;
   SET_TC_TIMER_MODE;
   DISABLE_TC_TIMER_ISR;
   SET_TC_TIMER_OVERFLOW_FLAG;
   /* Prepare TC timer. */

   CLEAR_TC_INTERRUPT_FLAG;
   CLEAR_TM_INTERRUPT_FLAG;
   CLEAR_HIT_TRIGGER_ISR_FLAG;
   /* Clear pending interrupts. */

   /* RAM tests and code copying: */

   code_address = InitCode_RAM (safe_reset_class);

   InitData_RAM (safe_reset_class, code_address);

   /* Record RAM test results in external data as follows. */
   /* They will be safe now, since RAM test is over.       */
   /* Note, InitData_RAM already set failed_data_address,  */
   /* and InitCode_RAM selected the memory mode; here we   */
   /* just record the selection in memory_mode.            */
   /* The failed_code/data_addresses are not yet set in    */
   /* telemetry_data, since the latter will be cleared in  */
   /* a later step of the boot sequence.                   */

   s_w_reset           = safe_reset_class;
   code_not_patched    = 1;
   failed_code_address = code_address;

   if (code_address == NO_RAM_FAILURE)
   {
      memory_mode = SRAM_e;
   }
   else
   {
      memory_mode = PROM_e;
   }

   #ifdef USE_ALWAYS_PROM
      memory_mode = PROM_e;
   #endif

   SET_WD_RESET_HIGH;
   /* The Watch Dog time out signal state is reset HIGH state, as it is*/
   /* falling edge active.                                             */
}


code_address_t InitCode_RAM (reset_class_t reset_class)
/* Purpose        : Initialise Program Memory at reset.                      */
/* Interface      : inputs      - reset class                                */
/*                              - failed_code_address (if Warm Reset)        */
/*                  outputs     - returns new value for failed_code_address  */
/*                  subroutines - Check_RAM                                  */
/*                              - CopyProgramCode                            */
/* Preconditions  : Basic startup initialisation done.                       */
/*                  Code is being executed from PROM.                        */
/*                  Tasks are not yet running.                               */
/*                  External Data RAM is not initialised (except in case     */
/*                  of a Warm Reset).                                        */
/* Postconditions : For a reset that is not a Warm Reset:                    */
/*                  - Code SRAM tested, result in return value.              */
/*                  - Code copied from PROM to SRAM.                         */
/*                  - Running from SRAM if Code RAM is good.                 */
/*                  - Running from PROM if error found in Code RAM.          */
/*                  For a Warm Reset:                                        */
/*                  - Code in SRAM not modified.                             */
/*                  - Running from SRAM or PROM depending on the recorded    */
/*                    result of an earlier Code RAM test (from the global    */
/*                    variable failed_code_address).                         */
/*                  - failed_code_address unchanged and returned.            */
/* Algorithm      : see below.                                               */

/* Note: This initialisation may use external RAM, but any data it */
/* stores in external RAM will be destroyed by the RAM test,       */
/* which is done after this step in the reset sequence.            */
/* This function must not rely on any constants in data RAM, since */
/* it is called before the C start-up system initialises data.     */

{
   code_address_t INDIRECT_INTERNAL code_address;
   /* Value returned by Check_RAM, now or earlier. */

   if (reset_class == warm_reset_e)
   {
      /* Warm Reset: Do not copy PROM code to RAM. */
      /* Use result of memory test from earlier (non-warm) reset. */

      code_address = failed_code_address;
   }
   else
   {
      /* HW Reset, Soft Reset or Checksum Reset. */

      code_address = Check_RAM ( BEGIN_SRAM1, END_SRAM1 );
      /* TBC that this does not rely on data RAM constants. */

      if (code_address == NO_RAM_FAILURE)
      {
         #ifndef USE_ALWAYS_PROM
            CopyProgramCode();
         #endif
         /* Code RAM is good. Copy code to it. */
         /* Later in the boot sequence, the reference checksum */
         /* must be reset to its initial value, to erase its   */
         /* memory of any code patches, or to initialise it in */
         /* case of a power-up reset or test of data RAM.      */
      }
   }

   if (code_address == NO_RAM_FAILURE)
   {
      #ifndef USE_ALWAYS_PROM
         SET_MEM_CONF_SRAM;
      #endif
      /* Code RAM is good. Run program from it.             */
      /* For a Warm Reset, the Code RAM may contain patches */
      /* relative to the PROM code, and the reference       */
      /* checksum should also retain a memory of them.      */
   }

   return code_address;
}


void InitData_RAM (
   reset_class_t  reset_class,
   code_address_t code_address)
/* Purpose        : Initialise Data Memory at reset.                         */
/* Interface      : inputs      - reset class                                */
/*                              - failed code address (in some cases)        */
/*                  outputs     - failed_data_address                        */
/*                  subroutines - Check_RAM                                  */
/* Preconditions  : Basic startup initialisation done.                       */
/*                  Tasks are not yet running.                               */
/*                  External Data RAM is not initialised (except in case     */
/*                  of a Warm Reset).                                        */
/* Postconditions : For a Power-Up Reset:                                    */
/*                  - Data SRAM tested, result in failed_data_address.       */
/*                    If the Code RAM test failed (as shown by code_address) */
/*                    only the upper half of the Data space is tested here.  */
/*                  - The data in the tested Data RAM is garbage.            */
/*                  For other kinds of reset:                                */
/*                  - Data in SRAM not modified.                             */
/* Algorithm      : see below.                                               */

/* This function must not rely on any constants in data RAM, since */
/* it is called before the C start-up system initialises data.     */

{
   if (reset_class == power_up_reset_e)
   {
      if (code_address == NO_RAM_FAILURE)
      {
         /* The Code RAM is good, so we have a fresh lower-half  */
         /* of the Data RAM to check, as well as the upper-half. */

         failed_data_address = Check_RAM (BEGIN_DATA_RAM, END_SRAM3);
      }
      else
      {
         /* The Code RAM is bad, and is still mapped to the     */
         /* lower-half of the Data space. Check only the upper  */
         /* half of the data space.                             */

         failed_data_address = Check_RAM (BEGIN_SRAM3, END_SRAM3);
      }
   }
}


void CopyProgramCode(void)
/* Purpose        : Copies program code from PROM to SRAM                    */
/* Interface      : -inputs: PROM                                            */
/*                  -outputs: SRAM1                                          */
/* Preconditions  : Program code is executed from PROM.                      */
/*                  External data RAM not initialised.                       */
/* Postconditions : SRAM1 holds same program as PROM.                        */
/* Algorithm      : Bytes are copied from code memory to same address in     */
/*                  external data memory in loop that goes through code      */
/*                  memory                                                   */
/* This function must not rely on any constants in data RAM, since */
/* it is called before the C start-up system initialises data.     */

{
   code_address_t i;
   INDIRECT_INTERNAL unsigned char code_byte;

   _Pragma("loopbound min 28672 max 28672")
   for (i = PROGRAM_COPY_START; i < PROGRAM_COPY_END; i++)
   {
      code_byte = GET_CODE_BYTE(i);
      SET_DATA_BYTE((data_address_t)i, code_byte);
   }
}


reset_class_t  GetResetClass(void)
/* Purpose        : Reset class is returned.                                 */
/* Interface      : - inputs:  s_w_reset, type of the occurred reset.        */
/*                  - outputs: s_w_reset                                     */
/* Preconditions  : Valid only when called first time after reset in boot    */
/*                  sequence.                                                */
/* Postconditions : s_w_reset is set to error_e value.                       */
/* Algorithm      : value of s_w_reset is returned and s_w_reset is set to   */
/*                  error value.                                             */
{
   register reset_class_t occurred_reset;

   occurred_reset = s_w_reset;
   s_w_reset      = error_e;
   return occurred_reset;
}


void SignalMemoryErrors (void)
/* Purpose        : Copy results of RAM test to telemetry_data.              */
/* Interface      : - inputs:  failed_code_address, failed_data_address      */
/*                  - outputs: telemetry_data fields:                        */
/*                                failed_code_address                        */
/*                                failed_data_address                        */
/*                                mode_status bits for:                      */
/*                                   PROGRAM_MEMORY_ERROR                    */
/*                                   DATA_MEMORY_ERROR                       */
/* Preconditions  : Init_DPU called since reset.                             */
/* Postconditions : telemetry_data reflects the results of the memory tests  */
/*                  done in Init_DPU, as recorded in failed_code_address     */
/*                  and failed_data_address.                                 */
/*                  Note that the TM addresses are zero for "no failure".    */
/* Algorithm      : see below.                                               */
{
   if (failed_code_address == NO_RAM_FAILURE)
   {
      telemetry_data.mode_status        &= ~PROGRAM_MEMORY_ERROR;
      telemetry_data.failed_code_address = 0x0000;
   }
   else
   {
      telemetry_data.mode_status        |= PROGRAM_MEMORY_ERROR;
      telemetry_data.failed_code_address = failed_code_address;
   }

   if (failed_data_address == NO_RAM_FAILURE)
   {
      telemetry_data.mode_status        &= ~DATA_MEMORY_ERROR;
      telemetry_data.failed_data_address = 0x0000;
   }
   else
   {
      telemetry_data.mode_status        |= DATA_MEMORY_ERROR;
      telemetry_data.failed_data_address = failed_data_address;
   }
}


void SetMemoryConfiguration (memory_configuration_t memory)
/* Purpose        : External program memory is selected to be either PROM or */
/*                  SRAM1.                                                   */
/* Interface      : Port 1 is used.                                          */
/*                  output: memory_mode                                      */
/* Preconditions  : After power-up reset PROM is always selected.            */
/* Postconditions : External program memory is set depending on the given    */
/*                  parameter.                                               */
/*                  memory_mode contains the selected mode.                  */
/* Algorithm      : Memory configuration is selected with the output at the  */
/*                  I/O port 1.                                              */

{
   switch (memory)
   {
      case PROM_e:
         SET_MEM_CONF_PROM;
         break;
      case SRAM_e:
         SET_MEM_CONF_SRAM;
         break;
   }
   memory_mode = memory;
}


void PatchCode(memory_patch_variables_t EXTERNAL *patch_variables)
/* Purpose        :  Code memory patching.                                   */
/* Interface      :  Following parameters are given: Address from where to   */
/*                   copy, address where to copy and the amount of bytes to  */
/*                   be copied. Execution result is returned. Variables used */
/*                   in this function are stored in a struct. Pointer to a   */
/*                   variable which stores an execution result of the        */
/*                   function SetMemoryConfiguration is passed on.           */
/* Preconditions  :  Source and destination addresses should be valid.       */
/* Postconditions :  Desired part of the memory is copied.                   */
/* Algorithm      :  Bytes are copied.                                       */

{

   fptr_t patch_function;
   /* Function pointer to the patched memory area. */

   unsigned char INDIRECT_INTERNAL old_checksum;
   /* Checksum calculated from the old contents of the pachted memory. */

   unsigned char INDIRECT_INTERNAL new_checksum;
   /* Checksum calculated from the new conrents of the patched memory. */

   unsigned char INDIRECT_INTERNAL patch_value;
   /* New value of a patched code memory byte. */

   unsigned char EXTERNAL temp_configuration;
   /* Original memory configuration. */

   uint_least8_t INDIRECT_INTERNAL i;
   /* Loop variable. */


   temp_configuration = GetMemoryConfiguration();
   /* State of the current memory configuration is stored. */

   DISABLE_INTERRUPT_MASTER;
   /* Disable all interrupts. */

   SetMemoryConfiguration (PROM_e);
   /* Enable code patching. */

   new_checksum = 0;
   old_checksum = 0;

   /* Memory block is copied from SRAM3 to SRAM1. */

   _Pragma("loopbound min 32 max 32")
   for (i=0 ; i < patch_variables -> data_amount ; i++)
   {
      old_checksum ^= GET_DATA_BYTE(patch_variables -> destination + i);
      patch_value   = *(patch_variables -> source + i);
      new_checksum ^= patch_value;

      SET_DATA_BYTE(patch_variables -> destination + i, patch_value);
   }

   reference_checksum ^= (old_checksum ^ new_checksum);

   SetMemoryConfiguration (temp_configuration);
   /* The initial memory configuration is restored. */

   switch (patch_variables -> execution_command)
   {
      case 0:
         /* Continue normally. */

         break;

      case 0x09:
         /* Execute soft reset. */

        Reboot (soft_reset_e);
         /* Function does not return. */
         break;

      case 0x37:
         /* Execute warm reset. */

         Reboot (warm_reset_e);
         /* Function deos not return. */
         break;

      case 0x5A:
         /* Jump to the patched memory. */

         patch_function = (fptr_t)(patch_variables -> destination);

         CALL_PATCH(patch_function);
         /* Called code may or may not return. */

         /* TC_state is selected upon return. */

         break;
   }
   ENABLE_INTERRUPT_MASTER;
   /* Enable all 'enabled' interrupts. */

}


memory_configuration_t GetMemoryConfiguration(void) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : Information about selected program memory is acquired    */
/*                  and returned.                                            */
/* Interface      : input: memory_mode                                       */
/* Preconditions  : none                                                     */
/* Postconditions : none                                                     */
/* Algorithm      : Information about current memory  configuration is       */
/*                  stored in a variable.                                    */

{
   return memory_mode;
   /*Information about current memory configuration is stored in a global    */
   /*variable, which is returned.                                            */
}


/*****************************************************************************/
/*                                su_ctrl.h                                  */
/*****************************************************************************/

/* Sensor Unit power control                                                 */

void Switch_SU_On  (
   sensor_number_t SU_Number,
   unsigned char EXTERNAL *execution_result) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Given Sensor Unit is switched on.                       */
/* Interface      :  Execution result is stored in a variable.               */
/* Preconditions  :  SU_Number should be 1,2,3 or 4                          */
/* Postconditions :  Given Sensor Unit is switced on.                        */
/* Algorithm      :  The respective bit is set high in the SU on/off control */
/*                   register with XBYTE.                                    */
{
   switch (SU_Number)
{
   case SU_1:

      SU_ctrl_register |= 0x10;
      *execution_result = SU_1_ON;
      /* Set high bit 4 in the SU on/off control register,                   */
      /* preserves other bits.                                               */
      break;

   case SU_2:

      SU_ctrl_register |= 0x20;
      *execution_result = SU_2_ON;
      /* Set high bit 5 in the SU on/off control register,                   */
      /* preserves other bits.                                               */
      break;

   case SU_3:

      SU_ctrl_register |= 0x40;
      *execution_result = SU_3_ON;
      /* Set high bit 6 in the SU on/off control register,                   */
      /* preserves other bits.                                               */
      break;

   case SU_4:

      SU_ctrl_register |= 0x80;
      *execution_result = SU_4_ON;
      /* Set high bit 7 in the SU on/off control register,                   */
      /* preserves other bits.                                               */
      break;

   default:
       *execution_result = SU_NOT_ACTIVATED;
       /*Incorrect SU number has caused an error.                            */
       break;
   }

   SET_DATA_BYTE(SU_CONTROL,SU_ctrl_register);

   telemetry_data.SU_status[SU_Number - SU_1] |= SU_ONOFF_MASK;
   /* SU_status register is updated to indicate that SU is switched on. */
   /* Other bits in this register are preserved.                        */
}


void Switch_SU_Off (
   sensor_number_t SU_Number,
   unsigned char EXTERNAL *execution_result) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Given Sensor Unit is switced off.                       */
/* Interface      :  Execution result is stored in a variable.               */
/* Preconditions  :  SU_Number should be 1,2,3 or 4.                         */
/* Postconditions :  Given Sensor Unit is switced off.                       */
/* Algorithm      :  The respective bit is set low with XBYTE.               */
{
   switch (SU_Number)
{
   case SU_1:

      SU_ctrl_register &= ~0x10;
      *execution_result = SU_1_OFF;
      /* Set low bit 4 in the SU on/off control register,                    */
      /* preserves other bits.                                               */
      break;

   case SU_2:

      SU_ctrl_register &= ~0x20;
      *execution_result = SU_2_OFF;
      /* Set low bit 5 in the SU on/off control register,                    */
      /* preserves other bits.                                               */
      break;

   case SU_3:

      SU_ctrl_register &= ~0x40;
      *execution_result = SU_3_OFF;
      /* Set low bit 6 in the SU on/off control register,                    */
      /* preserves other bits.                                               */
      break;

   case SU_4:

      SU_ctrl_register &= ~0x80;
      *execution_result = SU_4_OFF;
      /* Set low bit 7 in the SU on/off control register,                    */
      /* preserves other bits.                                               */
      break;

   default:
       *execution_result = SU_NOT_DEACTIVATED;
       /*Incorrect SU number has caused an error.                            */
       break;
   }

   SET_DATA_BYTE(SU_CONTROL,SU_ctrl_register);

   telemetry_data.SU_status[SU_Number - SU_1] &= (~SU_ONOFF_MASK);
   /* SU_status register is updated to indicate that SU is switched off. */
   /* Other bits in this register are preserved.                         */
}


void EnableAnalogSwitch(sensor_index_t self_test_SU_index)
/* Purpose        : The analog switch output is enabled in the               */
/*                  self test channel register.                              */
/* Interface      : inputs      - self_test_SU_index                         */
/*                  outputs     - SU_self_test_channel, HW register          */
/*                  subroutines -  none                                      */
/* Preconditions  : none                                                     */
/* Postconditions : The analog switch output is enabled for a given          */
/*                  self test SU in the SU_self_test_channel register.       */
/* Algorithm      : - The respective bit is set in the SU_self_test_channel  */
/*                    variable and written to HW.                            */
{
   SU_self_test_channel |= analog_switch_bit[self_test_SU_index];
   /* The respective bit is set in the variable, preserve other bits.  */

   SET_SU_SELF_TEST_CH(SU_self_test_channel);
}

void DisableAnalogSwitch(sensor_index_t self_test_SU_index)
/* Purpose        : The analog switch output is disabled in the              */
/*                  self test channel register.                              */
/* Interface      : inputs      - self_test_SU_index                         */
/*                  outputs     - SU_self_test_channel, HW register          */
/*                  subroutines -  none                                      */
/* Preconditions  : none                                                     */
/* Postconditions : The analog switch output is disabled for a given         */
/*                  self test SU in the SU_self_test_channel register.       */
/* Algorithm      : - The respective bit is reset in the SU_self_test_channel*/
/*                    variable and written to HW.                            */
{
   SU_self_test_channel &= ~analog_switch_bit[self_test_SU_index];
   /* The respective bit is set in the variable, preserve other bits.  */

   SET_SU_SELF_TEST_CH(SU_self_test_channel);
}

void SelectSelfTestChannel(unsigned char channel)
/* Purpose        : A self test channel is selected in the                   */
/*                  self test channel register.                              */
/* Interface      : inputs      - channel                                    */
/*                  outputs     - SU_self_test_channel, HW register          */
/*                  subroutines -  none                                      */
/* Preconditions  : none                                                     */
/* Postconditions : The given self test channel is selected.                 */
/*                  self test SU in the SU_self_test_channel register.       */
/* Algorithm      : - The respective bit is set in the self test channel     */
/*                    register and written to HW.                            */
{
   unsigned char EXTERNAL channel_selector_value[NUM_CH];
   /* This array stores the selector bit states related to a given channel. */

   channel_selector_value[PLASMA_1_PLUS]    = 0x00;
   channel_selector_value[PLASMA_1_MINUS]   = 0x01;
   channel_selector_value[PZT_1]            = 0x02;
   channel_selector_value[PZT_2]            = 0x03;
   channel_selector_value[PLASMA_2_PLUS]    = 0x04;

   SU_self_test_channel =
      (SU_self_test_channel & 0xF8) | channel_selector_value[channel];
   /* Set chosen bits preserve others. */

   SET_SU_SELF_TEST_CH(SU_self_test_channel);
}


void ReadDelayCounters (delays_t EXTERNAL *delay)
/* Purpose        :  Read delay counters.                                    */
/* Interface      :  Results are stored into a given struct.                 */
/* Preconditions  :                                                          */
/* Postconditions :  Counters are read.                                      */
/* Algorithm      :  MSB and LSB are combined to form an 16 bit int.         */
{
   unsigned char msb, lsb;

   msb = GET_MSB_COUNTER & 0x0F;
   /* Correct set of four bits are selected in the MSB. */
   lsb = GET_LSB1_COUNTER;

   delay -> FromPlasma1Plus = (msb << 8) | lsb;

   msb = GET_MSB_COUNTER >> 4;
   /* Correct set of four bits are selected in the MSB.  */
   lsb = GET_LSB2_COUNTER;

   delay -> FromPlasma1Minus = (msb << 8) | lsb;
}


unsigned char ReadRiseTimeCounter(void) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Plasma1+ rise time counter is read from the specified   */
/*                   address.                                                */
/* Interface      :  Result is returned as an unsigned char.                 */
/* Preconditions  :                                                          */
/* Postconditions :  Data is gained.                                         */
/* Algorithm      :  Counter is read with XBYTE.                             */
{

   return GET_DATA_BYTE(RISE_TIME_COUNTER);

}

void ResetDelayCounters(void) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Delay counters are reset.                               */
/* Interface      :  Port 1 is used.                                         */
/* Preconditions  :  Resetting takes place after acquisition.                */
/* Postconditions :  Delay counters are reset.                               */
/* Algorithm      :  The counter reset output bit at the I/O port 1 is set   */
/*                   first and then high.                                    */

{
   SET_COUNTER_RESET(LOW);
   /* Counters are reset by setting CNTR_RS bit to low in port 1 */

   SET_COUNTER_RESET(HIGH);
   /* The bit is set back to high */
}


void SetTriggerLevel(trigger_set_t EXTERNAL *setting)
        COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Given trigger level is set.                             */
/* Interface      :  Execution result is stored in a variable.               */
/* Preconditions  :  SU_Number should be 1-4 and channel number 1-5 levels   */
/* Postconditions :  Given level is set for specific unit and channel.       */
/* Algorithm      :  The respective memory address is written into.          */
/*                                                                           */
/* This function is used by TelecomandExecutionTask and                      */
/* HealthMonitoringTask. despite the fact that it is of type re-enrant       */
/* the two tasks should not use it simultaniously. When                      */
/* HealthMonitoringTask is conducting self test and uses                     */
/* SetTriggerLevel, TelecomandExecutionTask is able to interrupt and         */
/* possibly set another trigger levels which would foul up the self          */
/* test. On the other hand when TelecomandExecutionTask is setting           */
/* trigger levels HealthMonitoringTask is disabled due to its lower          */
/* priority.                                                                 */


{

   setting -> execution_result = TRIGGER_SET_OK;

   switch (setting -> sensor_unit)
   /*sensor unit is selected*/
   {
      case SU_1:
      {
         setting -> base = SU_1_TRIGGER_BASE;
         break;
      }
      case SU_2:
      {
         setting -> base = SU_2_TRIGGER_BASE;
         break;
      }
      case SU_3:
      {
         setting -> base = SU_3_TRIGGER_BASE;
         break;
      }
      case SU_4:
      {
         setting -> base = SU_4_TRIGGER_BASE;
         break;
      }
      default:
      {
         setting -> execution_result = SU_NOT_SELECTED;
         /*Sensor Unit number is invalid.                                    */
         break;
      }
   }

   if (setting -> execution_result != SU_NOT_SELECTED)
   {
      switch (setting -> channel)
      /*channel is selected*/
      {
         case PLASMA_1_PLUS:
         {
            SET_DATA_BYTE(setting -> base + 0, setting -> level);
            break;
         }
         case PLASMA_1_MINUS:
         {
            SET_DATA_BYTE(setting -> base + 1, setting -> level);
            break;
         }
         case PZT_1_2:
         {
            SET_DATA_BYTE(setting -> base + 2, setting -> level);
            break;
         }
         default:
         {
            setting -> execution_result = CHANNEL_NOT_SELECTED;
            /*Given channel parameter is invalid.                            */
            break;
         }
      }
   }

}


void SetTestPulseLevel(unsigned char level) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Testpulse level is set.                                 */
/* Interface      :  input:  - Desired test pulse level.                     */
/* Preconditions  :  none.                                                   */
/* Postconditions :  Level is set.                                           */
/* Algorithm      :  Level is written into memory-mapped port address.       */
{
   SET_TEST_PULSE_LEVEL(level);
}


void GetVoltageStatus(voltage_status_t EXTERNAL *v_status)
        COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  Voltage status data is gained.                          */
/* Interface      :  Port 1 is used.                                         */
/* Preconditions  :                                                          */
/* Postconditions :  Data is acquired.                                       */
/* Algorithm      :  HV status register is read into a struct with XBYTE.    */
/*                   V_DOWN bit is read from port 1.                         */
{
   v_status -> V_down_bit =  V_DOWN;
   v_status -> HV_status = GET_DATA_BYTE(HV_STATUS);
}

void ResetPeakDetector(sensor_number_t unit)
/* Purpose        :  Peak detector  is reset.                             */
/* Interface      :  -'Sensor unit on/off control register' is used       */
/* Preconditions  :  Resetting takes place after acquisition.             */
/* Postconditions :  Peak detector  is reset.                             */
/* Algorithm      :  - Interrupts are disabled                            */
/*                   - SignalPeakDetectorReset function is called         */
/*                   - Interrupts are enabled                             */
/*                                                                        */
/* This function is used by Acquisition and HealthMonitoringTask.         */
/* However, it does not have to be of re-entrant type because collision   */
/* is avoided through design, as follows.                                 */
/* HealthMonitoring task uses ResetPeakDetector when Hit Budget has been  */
/* exeeded. This means that Acquisitiontask is disabled. When Acquisition */
/* task uses  ResetPeakDetector HealthMonitoringTask is disabled because  */
/* it is of lower priority .                                              */


{
     DISABLE_INTERRUPT_MASTER;
     /* Disable all interrupts */

     SignalPeakDetectorReset(
        SU_ctrl_register & ~(1 << (unit - SU_1)),
        SU_ctrl_register);
     /* Generate reset pulse. */

     ENABLE_INTERRUPT_MASTER;
}

void SelectStartSwitchLevel(
   unsigned char  test_channel,
   sensor_index_t self_test_SU_index)
/* Purpose        : Select analog switch output level.                       */
/* Interface      : inputs      - self_test_SU_index, test_channel           */
/*                  outputs     - none                                       */
/*                  subroutines -  EnableAnalogSwitch                        */
/*                                 DisableAnalogSwitch                       */
/* Preconditions  : none                                                     */
/* Postconditions : Analog switch output level is selected depending on the  */
/*                  given channel.                                           */
/* Algorithm      : - Wanted level is selected.                              */
{

   if (test_channel == PLASMA_1_PLUS || test_channel == PLASMA_2_PLUS)
   {
      /* Channel triggered by falling edge. */
      EnableAnalogSwitch(self_test_SU_index);
      /* Set analog switch output level for the given channel. */
   }

   else
   {
      /* Rest of the channels triggered by rising edge. */
      DisableAnalogSwitch(self_test_SU_index);
      /* Set analog switch output level for the given channel. */
   }
}

void SelectTriggerSwitchLevel(
   unsigned char  test_channel,
   sensor_index_t self_test_SU_index)
/* Purpose        : Select analog switch output level.                       */
/* Interface      : inputs      - self_test_SU_index, test_channel           */
/*                  outputs     - none                                       */
/*                  subroutines -  EnableAnalogSwitch                        */
/*                                 DisableAnalogSwitch                       */
/* Preconditions  : none                                                     */
/* Postconditions : Analog switch output level is selected depending on the  */
/*                  given channel.                                           */
/* Algorithm      : - Wanted level is selected.                              */
/*                  - SW triggering is needed with channel PLASMA_2_PLUS.    */
{

   if (test_channel == PLASMA_1_PLUS)
   {
      /* Channel triggered by falling edge. */

      DisableAnalogSwitch(self_test_SU_index);
      /* Set analog switch output level for the given channel. */
   }

   else if (test_channel == PLASMA_2_PLUS)
   {
       /* Channel triggered by falling edge. SW trigger needed. */

       DisableAnalogSwitch(self_test_SU_index);
       /* Set analog switch output level for the given channel. */

       SET_SU_SELF_TEST_CH(SU_self_test_channel);

       SET_HIT_TRIGGER_ISR_FLAG;
       /* SW trigger required. */
   }

   else
   {
       /* Rest of the channels triggered by rising edge. */

       EnableAnalogSwitch(self_test_SU_index);
       /* Set analog switch output level for the given channel. */
   }
}

/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   measure.c
 *
 * Event Measurement module.
 *
 * Based on the SSF file measure.c, rev 1.51, Wed Oct 13 19:48:50 1999.
 *
 *- * --------------------------------------------------------------------------
 */

#include "keyword.h"
#include "kernobj.h"
#include "tm_data.h"
#include "isr_ctrl.h"
#include "msg_ctrl.h"
#include "tc_hand.h"
#include "telem.h"
#include "ttc_ctrl.h"
#include "su_ctrl.h"
#include "health.h"
#include "ad_conv.h"
#include "measure.h"
#include "taskctrl.h"
#include "class.h"

sensor_number_t EXTERNAL self_test_SU_number = NO_SU;
/* By default this variable indicates that no SU self test   */
/* sequence is running.                                      */
/* Number of SU being self tested (SU_1, SU_2, SU_3 or SU_4) */
/* or NO_SU if no SU is being self tested.                   */

unsigned char EXTERNAL test_channel;
/* Channel being tested in SU Self Test. Valid only if triggering SU */
/* (indicated by self_test_SU) is in Self Test state.                */

SU_test_level_t EXTERNAL test_level;
/* Test level being used in SU Self Test. */

SU_state_t EXTERNAL SU_state[NUM_SU] = {off_e, off_e, off_e, off_e};

/*This array contains information about the state of a given Sensor Unit.    */
/*Default value is 'off'.                                                    */

EXTERNAL unsigned short int ADC_result[NUM_CH];
/*Used to temporarily store AD conversion results.                           */

EXTERNAL unsigned char confirm_hit_result;
/*This variable indicates a hit with a high value.                           */

uint_least8_t EXTERNAL hit_budget       = HIT_BUDGET_DEFAULT;
uint_least8_t EXTERNAL hit_budget_left  = HIT_BUDGET_DEFAULT;

#ifdef REG52_DEFINED
   #pragma REGISTERBANK(1)
#endif

void _Pragma("entrypoint") InitHitTriggerTask (void)

/* Purpose        : Initialize the global state of Hit Trigger handling      */
/* Interface      : inputs  - none                                           */
/*                  outputs - none                                           */
/* Preconditions  : none                                                     */
/* Postconditions : Calling task attached as Hit Trigger ISR.                */
/*                  Hit Trigger interrupt enabled                            */
/* Algorithm      : - attach current task as Hit Trigger ISR                 */
/*                  - enable Hit Trigger interrupt                           */

{
   AttachInterrupt(HIT_TRIGGER_ISR_SOURCE);
   /*Now 'HitTriggerTask()' will listen for Hit trigger interrupt.           */

   ENABLE_HIT_TRIGGER;
}

void _Pragma("entrypoint") HandleHitTrigger (void)

/* Purpose        : Wait for and handle one Hit Trigger interrupt            */
/* Interface      : inputs  - Five analog outputs from Peak Detectors        */
/*                  outputs - Acquisition task mailbox                       */
/*                          - Sampled ADC_result                             */
/* Preconditions  : none                                                     */
/* Postconditions : Message holding the number of triggering Sensor Unit is  */
/*                  sent to Aqcuisition task.                                */
/* Algorithm      : - wait for Hit Trigger interrupt                         */
/*                  - Read Peak Detector outputs from hardware registers.    */
/*                  - Sample and store these into a buffer.                  */
/*                  - Send number of triggering Sensor Unit to Aqcuisition   */
/*                    task mailbox.                                          */

{
   unsigned char EXTERNAL initial_delay;
   /* Delay before the first AD channel is selected in */
   /* ShortDelay() units.                              */

   unsigned char EXTERNAL delay_limit;
   /* Delay between channel selection and start of conversion in */
   /* ShortDelay() units.                                        */

   INDIRECT_INTERNAL sensor_number_t trigger;
   /*Used to store Sensor Unit number, which has beem hit.                   */

   INDIRECT_INTERNAL channel_t CH_base;
   /* First ADC channel number for the relevant Sensor Unit.                 */

   DIRECT_INTERNAL uint_least8_t i;
   /* Used in a for -loop, which reads the peak sensor outputs.              */

   DIRECT_INTERNAL unsigned char lsb, msb;
   /*These variables are used to combine two bytes into one word.            */

   DIRECT_INTERNAL uint_least8_t conversion_try_count;
   /*This variable stores the number of failed conversion starts.            */

   initial_delay = (uint_least8_t) (DELAY_LIMIT(100));
   /* Initial delay before converting first channel. */

   delay_limit = (uint_least8_t) (DELAY_LIMIT(100));
   /* The signal settling delay is 100 microseconds. */

   WaitInterrupt (HIT_TRIGGER_ISR_SOURCE, 255);
   /* Interrupt arrival is awaited.    */
   /* Execution result is not handled. */

   CLEAR_HIT_TRIGGER_ISR_FLAG;
   /* Acknowledge the interrupt.            */
   /* This bit must be cleared by software. */

   if (hit_budget_left == 0)
   {
      /* Too many hit triggers during one Health Monitoring period. */

      if (telemetry_data.hit_budget_exceedings < 255)
      {
         telemetry_data.hit_budget_exceedings++;
      }

      DISABLE_HIT_TRIGGER;
      /* No more hit triggers will be handled before next Health */
      /* Monitoring period starts (or DEBIE is reset).           */
   }
   else
   {
      /* Some hit budget left; this hit will be handled. */

      hit_budget_left--;

      confirm_hit_result = 1;
      /*This variable indicates a hit with a high value.                  */

      ADC_channel_register &= BP_DOWN;
      UPDATE_ADC_CHANNEL_REG;
      /*AD converter is set to unipolar mode                              */

      START_CONVERSION;
      /*Dummy cycle to set unipolar mode.                                 */

      conversion_try_count = 0;

      _Pragma("loopbound min 0 max 25")
      while (   conversion_try_count < ADC_MAX_TRIES
             && END_OF_ADC != CONVERSION_ACTIVE )
      {
         conversion_try_count++;
         /*Conversion try counter is increased. If this counter exeeds the*/
         /*maximum number of conversion start tries the conversion will be*/
         /*dropped.                                                       */
      }

      if (self_test_SU_number != NO_SU)
      {
         /* Some Sensor Unit is being Self Tested. */
         trigger = self_test_SU_number;

         if (SU_state[self_test_SU_number - SU1] == self_test_e)
         {
            /* Some Sensor Unit is being Self Tested but this is */
            /* not the right self test pulse.                    */

            trigger |= HIT_SELF_TEST_RESET	;
            /* Self test pulse is incorrect and an indication     */
            /* of this is stored in to 'trigger' variable.        */
            /* The AcquisitionTask will adjust its operation      */
            /* based on this indication result.                   */
         }

         else if (SU_state[self_test_SU_number - SU1] == self_test_trigger_e)
         {
            /* Some Sensor Unit is being Self Tested  and this is the correct. */
            /* self test pulse.                                                */

            SU_state[self_test_SU_number - SU1] = self_test_e;
            /* Indication of a succesfully received self test pulse */
         }
      }

      else
      {
         /* There is no Sensor Unit Self Test in progress. */

         trigger = ((int)TRIGGER_SOURCE_0
                      + 2
                      * (int)TRIGGER_SOURCE_1)
                      + SU1;
         /* Sensor Unit which caused the hit trigger is resolved. */
      }

      CH_base =
         ((int)(trigger - SU_1)&2) * 12 + ((int)(trigger - SU_1)&1) * 8;
      /* First channel address for the given SU is calculated. */

      ShortDelay(initial_delay);
      /* Delay before converting first channel. */

      ADC_channel_register =
         (ADC_channel_register & 0xC0) | CH_base;
      UPDATE_ADC_CHANNEL_REG;
      /* First channel is selected. */

      ShortDelay(delay_limit);
      /* Delay of 100 microseconds (+ function call overhead). */


      _Pragma("loopbound min 5 max 5")
      for (i = 0; i < NUM_CH; i++)
      {

         ShortDelay(delay_limit);
         /* Delay of 100 microseconds (+ function call overhead). */

         START_CONVERSION;
         /* AD conversion for the selected channel is started. */

         ADC_channel_register =
            (ADC_channel_register & 0xC0) | (CH_base + i + 1);
         UPDATE_ADC_CHANNEL_REG;
         /* Next channel is selected. */

         conversion_try_count = 0;

         _Pragma("loopbound min 0 max 25")
         while (   conversion_try_count < ADC_MAX_TRIES
                && END_OF_ADC != CONVERSION_ACTIVE )
         {
            conversion_try_count++;
            /*Conversion try counter is increased. If this counter exeeds */
            /*the maximum number of conversion start tries the conversion */
            /*will be dropped.                                            */
         }

         if (conversion_try_count < ADC_MAX_TRIES)
         {
            msb = GET_RESULT;
            /*Most significant byte is read from ADC result address.      */

            lsb = GET_RESULT;
            /*Least significant byte is read from ADC result address.     */

            ADC_result[i] =
               ((unsigned int)msb << 8) | (unsigned int)lsb;
            /*Msb and lsb are combined into one word.                     */
         }

         else
         {
            trigger |= HIT_ADC_ERROR;
            /*Conversion has failed and an indication of this is stored in*/
            /*to 'trigger' variable by setting the Most Significant Bit   */
            /*(MSB) high. The AcquisitionTask will adjust its operation   */
            /*based on this indication result.                            */

            ADC_result[i] = 0;
         }

      }

      SendTaskMail(ACQUISITION_MAILBOX,trigger, 0);
      /*The number of the Sensor unit that has caused the hit trigger     */
      /*interrupt is sent to a mailbox for the acquisition task.          */

   }  /* end if (hit budget left) */

}

void HitTriggerTask(void) TASK(HIT_TRIGGER_ISR_TASK) PRIORITY(HIT_TRIGGER_PR)

/* Purpose        : Handles the Hit Trigger interrupts                       */
/* Interface      : inputs  - Five analog outputs from Peak Detectors        */
/*                  outputs - Acquisition task mailbox                       */
/*                          - Sampled ADC_result                             */
/* Preconditions  : Aqcuisition enabled                                      */
/* Postconditions : Message holding the number of triggering Sensor Unit is  */
/*                  sent to Aqcuisition task.                                */
/* Algorithm      : - InitHitTriggerTask                                     */
/*                  - loop forever:                                          */
/*                  -    HandleHitTrigger                                    */

{
   InitHitTriggerTask ();

   _Pragma("loopbound min 0 max 0")
   while(1)
   {
      HandleHitTrigger ();
   }
}


#ifdef REG52_DEFINED
   #pragma REGISTERBANK(0)
#endif


static EXTERNAL incoming_mail_t ACQ_mail;
/* Holds parameters for the mail waiting function.                        */
/* Must be in xdata memory because parameter of subroutine is pointer     */
/* to xdata area.                                                         */

static EXTERNAL uint16_t trigger_unit;
/* Number of the triggering Sensor Unit.                                  */


void _Pragma("entrypoint") InitAcquisitionTask (void)
/* Purpose        : Initialize the global state of the Acquisition task.     */
/* Interface      : inputs      - none                                       */
/*                  outputs     - ACQ_mail static fields.                    */
/* Preconditions  : none                                                     */
/* Postconditions : AcqusitionTask is operational.                           */
/* Algorithm      : - initialize task variables                              */
{
   /* ACQ_mail struct fields are set.                                        */
   ACQ_mail.mailbox_number = ACQUISITION_MAILBOX;
   ACQ_mail.message        = &trigger_unit;
   ACQ_mail.timeout        = 0;
}


void _Pragma("entrypoint") HandleAcquisition (void)

/* Purpose        : Acquires the data for one hit event.                     */
/* Interface      : inputs      - Acquisition task mailbox                   */
/*                              - Mail from Hit Trigger interrupt service    */
/*                              - Buffer with sampled Peak detector outputs  */
/*                              - Housekeeping Telemetry registers           */
/*                  outputs     - Science data                               */
/* Preconditions  : none                                                     */
/* Postconditions : one message processed from Acquisition task mailbox      */
/* Algorithm      : - wait for mail to Acquisition task mailbox              */
/*                  - if mail is "SU_NUMBER"                                 */
/*                    - get Peak Detector Outputs sampled by the interrupt   */
/*                      service                                              */
/*                    - measure Pulse Rise Time                              */
/*                    - measure delays between trigger signals               */
/*                    - get measurement time                                 */
/*                    - get sensor unit temperatures from Housekeeping       */
/*                      Telemetry registers                                  */
/*                    - calculate time difference between Plasma1- and       */
/*                      Plasma1+ trigger signals                             */
/*                    - calculate quality number                             */
/*                    - call RecordEvent()                                   */

{
   EXTERNAL unsigned char * EXTERNAL checksum_pointer;
   unsigned char EXTERNAL event_checksum;
   uint_least8_t EXTERNAL i;
   /* These variables are used when checksum is computed for a given event   */
   /* before storing it to Science data area.                                */

   event_record_t EXTERNAL *event;
   /* Pointer to the new event record.                                       */

   EXTERNAL delays_t delay_counters;
   /*This is a struct which stores the Delay Counter time data.              */

   DIRECT_INTERNAL signed int time_delay;
   /*This variable is used to store the delay from plasma 1+ to plasma 1-.   */

   SU_state_t EXTERNAL state = off_e;
   /* Used to store sensor unit state. */

   WaitMail(&ACQ_mail);

   if (trigger_unit & HIT_ADC_ERROR)
   {
      /* There has been an  error in AD conversion */
      /* in Hit trigger processing.                */
      SetModeStatusError(ADC_ERROR);
   }

   if(trigger_unit == SU_1 || trigger_unit == SU_2 ||
      trigger_unit == SU_3 || trigger_unit == SU_4)

   {
      state = SU_state[trigger_unit - SU_1];

      if ((state == self_test_e || state == acquisition_e)
           && (EVENT_FLAG == ACCEPT_EVENT))
      {
         event = GetFreeRecord();
         /* Get pointer to the new event record. */


         /*Number of the Sensor Unit, which has been hit, is stored into  */
         /*Event Record.                                                  */
         event -> SU_number = (unsigned char)(trigger_unit & 0xFF);

         /*Contents of a temporary buffer is stored into Event Record.    */
         COPY (event -> plasma_1_plus ,ADC_result[0]);
         COPY (event -> plasma_1_minus,ADC_result[1]);
         COPY (event -> piezo_1       ,ADC_result[2]);
         COPY (event -> piezo_2       ,ADC_result[3]);
         COPY (event -> plasma_2_plus ,ADC_result[4]);

         /*Rise time counter is read in to Event Record.                  */
         event -> rise_time = ReadRiseTimeCounter();

         /*Delay counters are read in to a struct.                        */
         ReadDelayCounters(&delay_counters);

         /*Delay from plasma 1+ to PZT 1/2 is stored into Event Record.   */
         COPY (event -> delay_2,delay_counters.FromPlasma1Plus);

         /*Delay from plasma 1- to PZT 1/2 is stored into Event Record.   */
         COPY (event -> delay_3,delay_counters.FromPlasma1Minus);

         /*Delay from plasma 1+ to plasma 1- is calculated and stored into*/
         /*Event Record.                                                  */

         time_delay = delay_counters.FromPlasma1Plus
                   - delay_counters.FromPlasma1Minus;
         if(time_delay > 127)
         {
            event -> delay_1 = 127;
            /*If the delay from plasma 1+ to plasma 1- is positive and    */
            /*doesn't fit into signed char 'event_record.delay_1', then   */
            /*the largest value for the signed char is stored instead.    */
         }

         else if(time_delay < -128)
         {
            event -> delay_1 = -128;
            /*If the delay from plasma 1+ to plasma 1- is negative and    */
            /*doesn't fit into signed char 'event_record.delay_1', then   */
            /*the smallest value for the signed char is stored instead.   */
         }

         else
         {
            event -> delay_1 = time_delay;
            /*Delay from plasma 1+ to plasma 1- is calculated and stored  */
            /*into Event Record.                                          */
         }

         /*Measurement time is stored into Event Record.                  */
         COPY (event -> hit_time, internal_time);

         /*Unit temperatures are stored into Event Record.                */

         event -> SU_temperature_1 =
            telemetry_data.SU_temperature[trigger_unit - SU1][0];

         event -> SU_temperature_2 =
            telemetry_data.SU_temperature[trigger_unit - SU1][1];

         ClassifyEvent(event);
         /* New event is classified. */

         checksum_pointer = (EXTERNAL unsigned char *)event;
         event_checksum = 0;

         _Pragma("loopbound min 27 max 27")
         for (i = 1; i < sizeof(event_record_t); i++)
         {
            event_checksum ^= *checksum_pointer;
            checksum_pointer++;
         }

         event -> checksum = event_checksum;

         /* After the event record is filled up, it is stored into science*/
         /* data.                                                         */
         RecordEvent();
      }
   }

   else
   {
      /*The received mail contained an invalid Sensor unit number.        */
   }

   trigger_unit &= SU_NUMBER_MASK;
   /* Delete possible error bits. */

   WaitTimeout(PEAK_RESET_MIN_DELAY);

   ResetPeakDetector(trigger_unit);
   /*Peak detector for this Sensor Unit is resetted. */

   WaitTimeout(PEAK_RESET_MIN_DELAY);

   ResetPeakDetector(trigger_unit);
   /*Peak detector for this Sensor Unit is resetted again. */

   WaitTimeout(COUNTER_RESET_MIN_DELAY);

   ResetDelayCounters();
   /*The Delay Counters are resetted. */
}


void AcquisitionTask(void) TASK(ACQUISITION_TASK) PRIORITY(ACQUISITION_PR)
/* Purpose        : Implements the Acquisition task.                         */
/* Interface      : inputs      - Acquisition task mailbox                   */
/*                              - Mail from Hit Trigger interrupt service    */
/*                              - Buffer with sampled Peak detector outputs  */
/*                              - Housekeeping Telemetry registers           */
/*                  outputs     - Science data                               */
/* Preconditions  : none                                                     */
/* Postconditions : This function does not return.                           */
/* Algorithm      : - InitAcquisitionTask                                    */
/*                  - loop forever:                                          */
/*                    -   HandleAcquisition                                  */
{
   InitAcquisitionTask ();

   _Pragma("loopbound min 0 max 0")
   while(1)
   {
      HandleAcquisition ();
   }
}


/*Assign pointers to tasks*/
void (* EXTERNAL hit_task)(void) = HitTriggerTask;
void (* EXTERNAL acq_task)(void) = AcquisitionTask;

void Switch_SU_State(sensor_unit_t EXTERNAL *SU_setting)
        COMPACT_DATA REENTRANT_FUNC
/* Purpose        : Used when only the SU_state variable must be modified.   */
/* Interface      : inputs      - SU_state                                   */
/*                              - An Address of 'sensor_unit_t' type of a    */
/*                                struct.                                    */
/*                  outputs     - SU_state                                   */
/*                              - SU_setting.execution_result                */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : SU_state variable is conditionally modified.             */
/* Algorithm      :                                                          */
/*                  - If the expected SU_state variable value related to the */
/*                    given SU_index number is not valid, variable value is  */
/*                    not changed. Error indication is recorded instead.     */
/*                  - Else state variable value is changed and an indication */
/*                    of this is recorded.                                   */
{
   if (SU_state[(SU_setting -> SU_number) - SU_1] !=
                 SU_setting -> expected_source_state)
   {
      /* The original SU state is wrong. */

      SU_setting -> execution_result = SU_STATE_TRANSITION_FAILED;
   }

   else if (SU_setting -> SU_state == self_test_mon_e &&
            self_test_SU_number    != NO_SU)
   {
      /* There is a self test sequence running already */

      SU_setting -> execution_result = SU_STATE_TRANSITION_FAILED;
   }


   else
   {
      /* The original SU state is correct. */

      if (SU_setting -> SU_state == self_test_mon_e)
      {
         self_test_SU_number = SU_setting -> SU_number;
         /* Number of the SU under self test is recorded. */
      }

      else if (SU_setting -> SU_number == self_test_SU_number)
      {
         self_test_SU_number = NO_SU;
         /* Reset self test state i.e. no self test is running. */
      }

      SU_state[(SU_setting -> SU_number) - SU_1] = SU_setting -> SU_state;
      SU_setting->execution_result  = SU_STATE_TRANSITION_OK;
   }
}

void Start_SU_SwitchingOn(
      sensor_index_t SU,
      unsigned char EXTERNAL *exec_result) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : Transition to SU state on.                               */
/* Interface      : inputs      - Sensor_index number                        */
/*                              - An Address of 'exec_result' variable       */
/*                              - SU_state                                   */
/*                  outputs     - SU_state                                   */
/*                              - 'exec_result'                              */
/*                  subroutines - Switch_SU_On                               */
/* Preconditions  : none                                                     */
/* Postconditions : Under valid conditions transition to 'on' state is       */
/*                  completed.                                               */
/* Algorithm      :                                                          */
/*                  - If the original SU_state variable value related to the */
/*                    given SU_index number is not valid, variable value is  */
/*                    not changed. Error indication is recorded instead.     */
/*                  - Else                                                   */
/*                    - Disable interrups                                    */
/*                    - 'Switch_SU_On' function is called and an             */
/*                      indication of this transition is recorded.           */
/*                    - Enable interrupts                                    */
{
   *exec_result  = SU_STATE_TRANSITION_OK;
   /* Default value, may be changed below. */

   if (SU_state[SU] != off_e)
   {
      /* The original SU state is wrong. */

      *exec_result = SU_STATE_TRANSITION_FAILED;
   }

   else
   {
      /* The original SU state is correct. */

      DISABLE_INTERRUPT_MASTER;

      /* SU state is still off_e, because there is only one task  */
      /* which can switch SU state from off_e to any other state. */

      Switch_SU_On(
         SU + SU_1,
         exec_result);

      if (*exec_result == SU + SU_1)
      {
         /* Transition succeeds. */

         SU_state[SU] = start_switching_e;
      }

      else
      {
         /* Transition fails. */

         *exec_result = SU_STATE_TRANSITION_FAILED;
      }

      ENABLE_INTERRUPT_MASTER;
   }
}


void SetSensorUnitOff(
         sensor_index_t SU,
         unsigned char EXTERNAL *exec_result) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : Transition to SU state off.                              */
/* Interface      : inputs      - Sensor_index number                        */
/*                              - An Address of 'exec_result' variable       */
/*                              - SU_state                                   */
/*                  outputs     - SU_state                                   */
/*                              - 'exec_result'                              */
/*                  subroutines - Switch_SU_Off                              */
/* Preconditions  : none                                                     */
/* Postconditions : Under valid conditions transition to 'off' state is      */
/*                  completed.                                               */
/* Algorithm      :                                                          */
/*                  - Disable interrups                                      */
/*                  - 'Switch_SU_Off' function is called.                    */
/*                  - If transition succeeds,                                */
/*                    - 'Off' state is recorded to 'SU_state' variable.      */
/*                    - Indication of transition is recorded to              */
/*                      'exec_result'.                                       */
/*                  - Else if transition fails,                              */
/*                    - Indication of this is recorded to 'exec_result'.     */
/*                  - Enable interrupts                                      */
{
   static sensor_unit_t EXTERNAL SU_setting;
   /* Holds parameters for "Switch_SU_State" operation                  */
   /* Must be in external memory, because the parameter to the function */
   /* is pointer to external memory                                     */

   DISABLE_INTERRUPT_MASTER;

   Switch_SU_Off(
      SU + SU_1,
      exec_result);

   if (*exec_result == SU + SU_1)
   {
      /* Transition succeeds. */

      SU_setting.SU_number = SU + SU_1;
      SU_setting.expected_source_state = SU_state[SU];
      SU_setting.SU_state = off_e;
      Switch_SU_State (&SU_setting);
      *exec_result = SU_STATE_TRANSITION_OK;
   }

   else
   {
      /* Transition fails. */

      *exec_result = SU_STATE_TRANSITION_FAILED;
   }

   ENABLE_INTERRUPT_MASTER;
}

SU_state_t  ReadSensorUnit(unsigned char SU_number) COMPACT_DATA REENTRANT_FUNC
/* Purpose        :  To find out whether given Sensor Unit is switched on or */
/*                   off.                                                    */
/* Interface      :                                                          */
/* Preconditions  :  SU_Number should be 1,2,3 or 4.                         */
/* Postconditions :  Value of state variable is returned.                    */
/* Algorithm      :  Value of state variable (on_e or off_e) is returned.    */
{
   return SU_state[SU_number - 1];
}


void Update_SU_State(sensor_index_t SU_index) COMPACT_DATA REENTRANT_FUNC
/* Purpose        : Sensor unit state is updated.                            */
/* Interface      : inputs      - SU_state                                   */
/*                              - SU_index number                            */
/*                  outputs     - SU_state                                   */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : SU_state variable is modified.                           */
/* Algorithm      : - Disable interrups                                      */
/*                  - Change SU_state variable value related to the given    */
/*                    SU_index number. Selection of the new state depends on */
/*                    the present one.                                       */
/*                  - Enable interrups                                       */
{
   DISABLE_INTERRUPT_MASTER;

   if (SU_state[SU_index] == start_switching_e)
   {
      SU_state[SU_index] = switching_e;
   }

   else if (SU_state[SU_index] == switching_e)
   {
      ResetPeakDetector(SU_index + SU_1);
      /*Peak detector for this Sensor Unit is resetted. */

      WaitTimeout(PEAK_RESET_MIN_DELAY);

      ResetPeakDetector(SU_index + SU_1);
      /*Peak detector for this Sensor Unit is resetted again. */

      SU_state[SU_index] = on_e;
   }

   ENABLE_INTERRUPT_MASTER;

}





/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   tc_hand.c
 *
 * Telecommand module.
 *
 * Based on the SSF file tc_hand.c, rev 1.66, Thu Sep 09 13:29:18 1999.
 *
 *- * --------------------------------------------------------------------------
 */

#include "keyword.h"
#include "kernobj.h"
#include "tm_data.h"
#include "isr_ctrl.h"
#include "msg_ctrl.h"
#include "tc_hand.h"
#include "telem.h"
#include "ttc_ctrl.h"
#include "measure.h"
#include "dpu_ctrl.h"
#include "health.h"
#include "class.h"
#include "taskctrl.h"

/* This file contains functions to implement Telecommand Execution Task */

#define TC_ADDRESS(TC_WORD) ((TC_WORD) >> 9)
#define TC_CODE(TC_WORD)    ((TC_WORD) &  255)

#define SET_TIME_TC_TIMEOUT 100
/* Timeout between Set Time telecommands, 100 x 10ms = 1s */

#define MEM_BUFFER_SIZE     32

/* Possible TC look-up table values:                         */

#define ALL_INVALID   0
/* used for invalid TC addresses                             */

#define ALL_VALID     1
/* used for TCs which accept all TC codes                    */

#define ONLY_EQUAL    2
/* used for TCs which require equal TC address and code      */

#define ON_OFF_TC     3
/* used for TCs which require ON, OFF or SELF_TEST parameter */

#define ONLY_EVEN     4
/* used currently only for SEND_STATUS_REGISTER TC           */


#define WRITE_MEMORY_TIMEOUT 100
/* Timeout 100 x 10 ms = 1s. */


/* Array */

SU_settings_t EXTERNAL * EXTERNAL SU_config[] = {
         &telemetry_data.sensor_unit_1,
         &telemetry_data.sensor_unit_2,
         &telemetry_data.sensor_unit_3,
         &telemetry_data.sensor_unit_4
      };

/* Type definitions */

typedef struct {
   uint16_t      TC_word;       /* Received telecommand word */
   unsigned char TC_address;    /* Telecommand address       */
   unsigned char TC_code;       /* Telecommand code          */
   } telecommand_t;

typedef enum {
   code_e, data_e
   } memory_type_t;

/* Global variables */

telecommand_t EXTERNAL previous_TC;
/* Holds previous telecommand unless a timeout has occurred */

unsigned char EXTERNAL TC_timeout = 0;
/* Time out for next telecommand, zero means no timeout */


unsigned char EXTERNAL TC_look_up[128];
/* Look-up table for all possible 128 TC address values */

TC_state_t EXTERNAL TC_state;
/* internal state of the Telecommand Execution task */

memory_type_t EXTERNAL memory_type;
/* Selection of memory write target (code/data) */

unsigned char EXTERNAL memory_transfer_buffer[MEM_BUFFER_SIZE];
/* Buffer for memory read and write telecommands */

unsigned char EXTERNAL address_MSB;
/* MSB of memory read source and write destination addresses */

unsigned char EXTERNAL address_LSB;
/* LSB of memory read source and write destination addresses. */

uint_least8_t EXTERNAL memory_buffer_index = 0;
/* Index to memory_buffer. */

unsigned char EXTERNAL write_checksum;
/* Checksum for memory write blocks. */


void InitTC_LookUp(void)
/* Purpose        : Initializes the TC look-up table                     */
/* Interface      : inputs  - none                                       */
/*                  outputs - TC_lool_up                                 */
/* Preconditions  : none                                                 */
/* Postconditions : TC_look_up is initialized                            */
/* Algorithm      : - set all elements in table to ALL_INVALID           */
/*                  - set each element corresponding valid TC address    */
/*                    to proper value                                    */
{
   DIRECT_INTERNAL uint_least8_t i;
   /* Loop variable */


   _Pragma("loopbound min 128 max 128")
   for(i=0; i<128; i++) TC_look_up[i] = ALL_INVALID;

   TC_look_up[START_ACQUISITION]            = ONLY_EQUAL;
   TC_look_up[STOP_ACQUISITION]             = ONLY_EQUAL;

   TC_look_up[ERROR_STATUS_CLEAR]           = ONLY_EQUAL;

   TC_look_up[SEND_STATUS_REGISTER]         = ONLY_EVEN;
   TC_look_up[SEND_SCIENCE_DATA_FILE]       = ONLY_EQUAL;

   TC_look_up[SET_TIME_BYTE_0]              = ALL_VALID;
   TC_look_up[SET_TIME_BYTE_1]              = ALL_VALID;
   TC_look_up[SET_TIME_BYTE_2]              = ALL_VALID;
   TC_look_up[SET_TIME_BYTE_3]              = ALL_VALID;

   TC_look_up[SOFT_RESET]                   = ONLY_EQUAL;

   TC_look_up[CLEAR_WATCHDOG_FAILURES]      = ONLY_EQUAL;
   TC_look_up[CLEAR_CHECKSUM_FAILURES]      = ONLY_EQUAL;

   TC_look_up[WRITE_CODE_MEMORY_MSB]        = ALL_VALID;
   TC_look_up[WRITE_CODE_MEMORY_LSB]        = ALL_VALID;
   TC_look_up[WRITE_DATA_MEMORY_MSB]        = ALL_VALID;
   TC_look_up[WRITE_DATA_MEMORY_LSB]        = ALL_VALID;
   TC_look_up[READ_DATA_MEMORY_MSB]         = ALL_VALID;
   TC_look_up[READ_DATA_MEMORY_LSB]         = ALL_VALID;

   TC_look_up[SWITCH_SU_1]                  = ON_OFF_TC;
   TC_look_up[SWITCH_SU_2]                  = ON_OFF_TC;
   TC_look_up[SWITCH_SU_3]                  = ON_OFF_TC;
   TC_look_up[SWITCH_SU_4]                  = ON_OFF_TC;

   TC_look_up[SET_SU_1_PLASMA_1P_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1P_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1P_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1P_THRESHOLD] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1M_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1M_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1M_THRESHOLD] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1M_THRESHOLD] = ALL_VALID;

   TC_look_up[SET_SU_1_PIEZO_THRESHOLD]     = ALL_VALID;
   TC_look_up[SET_SU_2_PIEZO_THRESHOLD]     = ALL_VALID;
   TC_look_up[SET_SU_3_PIEZO_THRESHOLD]     = ALL_VALID;
   TC_look_up[SET_SU_4_PIEZO_THRESHOLD]     = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1P_CLASS_LEVEL] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1M_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1M_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1M_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1M_CLASS_LEVEL] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_2P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_2P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_2P_CLASS_LEVEL] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_2P_CLASS_LEVEL] = ALL_VALID;

   TC_look_up[SET_SU_1_PIEZO_1_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_2_PIEZO_1_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_3_PIEZO_1_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_4_PIEZO_1_CLASS_LEVEL]   = ALL_VALID;

   TC_look_up[SET_SU_1_PIEZO_2_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_2_PIEZO_2_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_3_PIEZO_2_CLASS_LEVEL]   = ALL_VALID;
   TC_look_up[SET_SU_4_PIEZO_2_CLASS_LEVEL]   = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1E_1I_MAX_TIME]  = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1E_1I_MAX_TIME]  = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1E_1I_MAX_TIME]  = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1E_1I_MAX_TIME]  = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1E_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1E_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1E_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1E_PZT_MIN_TIME] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1E_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1E_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1E_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1E_PZT_MAX_TIME] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1I_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1I_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1I_PZT_MIN_TIME] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1I_PZT_MIN_TIME] = ALL_VALID;

   TC_look_up[SET_SU_1_PLASMA_1I_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_2_PLASMA_1I_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_3_PLASMA_1I_PZT_MAX_TIME] = ALL_VALID;
   TC_look_up[SET_SU_4_PLASMA_1I_PZT_MAX_TIME] = ALL_VALID;

   TC_look_up[SET_COEFFICIENT_1]               = ALL_VALID;
   TC_look_up[SET_COEFFICIENT_2]               = ALL_VALID;
   TC_look_up[SET_COEFFICIENT_3]               = ALL_VALID;
   TC_look_up[SET_COEFFICIENT_4]               = ALL_VALID;
   TC_look_up[SET_COEFFICIENT_5]               = ALL_VALID;

}


void TC_InterruptService (void) INTERRUPT(TC_ISR_SOURCE) USED_REG_BANK(2)
/* Purpose        : Handles the TC interrupt                             */
/* Interface      : inputs  - TC MSB and LSB hardware registers          */
/*                            TC_look_up table giving valid TC addresses */
/*                            and TC codes                               */
/*                  outputs - TM data registers for received TC and TC   */
/*                            time tag                                   */
/*                            TM data registers for error and mode       */
/*                            status                                     */
/*                            TM MSB and LSB hardware registers          */
/*                            telemetry_pointer                          */
/*                            telemetry_end_pointer                      */
/*                            Telecommand Execution task mailbox         */
/* Preconditions  : none                                                 */
/* Postconditions :                                                      */
/*                  Mail sent to Telecommand Execution task, if TC was   */
/*                  valid                                                */
/* Algorithm      : - Read TC address and code from hardware registers   */
/*                  - Calculate parity                                   */
/*                  - If parity not Ok                                   */
/*                    - Set parity error                                 */
/*                  - Else                                               */
/*                    - Clear parity error                               */
/*                    - Check TC address and code                        */
/*                    - If TC Ok                                         */
/*                         Clear TC error and send mail                  */
/*                    - Else                                             */
/*                         Set TC error                                  */
/*                  - If TC is valid Send Status Register                */
/*                    - Send first two registers defined by TC code      */
/*                  - Else if TC is valid Send Science Data File         */
/*                    - Send first teo bytes from Science Data File      */
/*                  - Else if TC responce is enabled                     */
/*                    - Send Error Status and Mode Status                */
/*                  - If TC is invalid                                   */
/*                    - Disable TC responses                             */

{
   DIRECT_INTERNAL unsigned char TC_address;
   DIRECT_INTERNAL unsigned char TC_code;
   DIRECT_INTERNAL uint16_t      TC_word;
   /* Telecommand and it's parts */

   DIRECT_INTERNAL unsigned char par8, par4, par2, par1;
   /* Parity calculation results */

   DIRECT_INTERNAL unsigned char tmp_error_status;
   /* Temporary result of TC validity check */


   if (!TC_TIMER_OVERFLOW_FLAG)
   {
      /* TC is rejected. */

      telemetry_data.error_status |= TC_ERROR;
      /* Set TC Error bit. */

      return;
      /* Abort ISR. */
   }

   if ((TC_state == SC_TM_e) || (TC_state == memory_dump_e))
   {
      return;
      /* Abort ISR. */
   }

   STOP_TC_TIMER;
   INIT_TC_TIMER_MSB;
   INIT_TC_TIMER_LSB;
   CLEAR_TC_TIMER_OVERFLOW_FLAG;
   START_TC_TIMER;

   TC_address   = READ_TC_MSB;
   TC_code      = READ_TC_LSB;
   TC_word      = TC_address * 256 + TC_code;
   /* Get TC Address, TC Code and TC Word */

   if (TC_state == memory_patch_e)
   {
      Send_ISR_Mail(0, TC_word);
      return;
      /* This is not a normal telecommand, but word containing two bytes */
      /* of memory block to be written to data or code memory.           */
   }

   if (TC_state == register_TM_e)
   {
      TC_state = TC_handling_e;
      /* Register TM state is aborted */

      ResetInterruptMask(TM_ISR_MASK);
      /* Disable TM interrupt mask. Note that DisableInterrupt */
      /* cannot be called from the C51 ISR.                    */
   }


   par8 = TC_address ^ TC_code;
   par4 = (par8 & 0x0F) ^ (par8 >> 4);
   par2 = (par4 & 0x03) ^ (par4 >> 2);
   par1 = (par2 & 0x01) ^ (par2 >> 1);
   /* Calculate parity */

   TC_address >>= 1;

   tmp_error_status = 0;

   if (par1)
   {
      /* Parity error. */

      tmp_error_status |= PARITY_ERROR;
   }

   else
   {

      switch (TC_look_up[TC_address])
      {
         case ALL_INVALID:
         /* Invalid TC Address */
            tmp_error_status |= TC_ERROR;
            break;

         case ALL_VALID:
         /* All TC Codes are valid */
            Send_ISR_Mail(0, TC_word);
            break;

         case ONLY_EQUAL:
         /* TC Code should be equal to TC Address */
            if (TC_address != TC_code)
            {
               tmp_error_status |= TC_ERROR;
            }

            else
            {
               Send_ISR_Mail(0, TC_word);
            }
            break;

         case ON_OFF_TC:
         /* TC_Code must have ON , OFF or SELF_TEST value */
            if ((TC_code != ON_VALUE) && (TC_code != OFF_VALUE) &&
               (TC_code != SELF_TEST))
            {
               tmp_error_status |= TC_ERROR;
            }

            else
            {
               Send_ISR_Mail(0, TC_word);
            }
            break;

         case ONLY_EVEN:
         /* TC_Code must be even and not too big */
            if ((TC_code & 1) || (TC_code > LAST_EVEN))
            {
               tmp_error_status |= TC_ERROR;
            }

            else
            {
               Send_ISR_Mail(0, TC_word);
            }
            break;
      }
   }

   if (((TC_address != SEND_STATUS_REGISTER)
            || (tmp_error_status))
            && ((telemetry_data.error_status & TC_OR_PARITY_ERROR) == 0))
   {
      /* Condition 1 :                                        */
      /* (TC is not SEND STATUS REGISTER or TC is invalid).   */
      /* and condition 2:                                     */
      /* no invalid telecommands are recorded                 */
      /*                                                      */
      /* First condition checks that the Command Status and   */
      /* Command Time Tag registers should be updated.        */
      /* Second condition checks that the update can be done. */


      telemetry_data.TC_word = TC_word;
      COPY (telemetry_data.TC_time_tag, internal_time);
      /* Update TC registers in HK TM data area */
   }

   if (tmp_error_status)
   {
      /* TC is invalid. */

      telemetry_data.error_status |= tmp_error_status;
      WRITE_TM_MSB (telemetry_data.error_status);
      WRITE_TM_LSB (telemetry_data.mode_status);
      /* Send response to this TC to TM */

      return;
      /* Abort ISR because TC is invalid. */
   }


   if (TC_address == SEND_STATUS_REGISTER)
   {
      /* Send Status Register TC accepted */

      COPY(telemetry_data.time, internal_time);

      telemetry_pointer     = ((EXTERNAL unsigned char *)&telemetry_data) + TC_code;
      telemetry_end_pointer = ((EXTERNAL unsigned char *)&telemetry_data) +
                              LAST_EVEN + 1;
      /* First TM register to be sent is given in TC_Code */

      CLEAR_TM_INTERRUPT_FLAG;

      WRITE_TM_MSB (*telemetry_pointer);
      telemetry_pointer++;
      WRITE_TM_LSB (*telemetry_pointer);
      telemetry_pointer++;

      TC_state = register_TM_e;

      SetInterruptMask(TM_ISR_MASK);
      /* Enable TM interrupt mask. Note that EnableInterrupt */
      /* cannot be called from the C51 ISR                   */

      if (telemetry_pointer > telemetry_end_pointer)
         telemetry_pointer = (EXTERNAL unsigned char *)&telemetry_data;
   }

   else if (TC_address == SEND_SCIENCE_DATA_FILE)
   {
      /* Send Science Data File TC accepted. */

      if ((telemetry_data.mode_status & MODE_BITS_MASK) == DPU_SELF_TEST)
      {
         /* Wrong DEBIE mode. */

         telemetry_data.error_status |= TC_ERROR;
         WRITE_TM_MSB (telemetry_data.error_status);
         WRITE_TM_LSB (telemetry_data.mode_status);
         /* Send response to this TC to TM. */
      }

      else
      {
         telemetry_pointer     = (EXTERNAL unsigned char *)&science_data;
         telemetry_end_pointer = ((EXTERNAL unsigned char *)
               &(science_data.event[free_slot_index])) - 1;
         /* Science telemetry stops to the end of the last used event */
         /* record of the Science Data memory.                        */

         science_data.length = (unsigned short int)
            (telemetry_end_pointer - telemetry_pointer + 1)/2;
         /* Store the current length of used science data. */

         CLEAR_TM_INTERRUPT_FLAG;

         WRITE_TM_MSB (*telemetry_pointer);
         telemetry_pointer++;
         WRITE_TM_LSB (*telemetry_pointer);
         telemetry_pointer++;

         TC_state = SC_TM_e;

         SetInterruptMask(TM_ISR_MASK);
         /* Enable TM interrupt mask. Note that EnableInterrupt */
         /* cannot be called from a C51 ISR.                    */
      }
   }

   else if (TC_address == READ_DATA_MEMORY_LSB)
   {
      /* Read Data Memory LSB accepted. */

      if ( (TC_state != read_memory_e) ||
           ( ((unsigned int)address_MSB << 8) + TC_code
             > (END_SRAM3 - MEM_BUFFER_SIZE) + 1 ) )
      {
         /* Wrong TC state or wrong address range. */

         telemetry_data.error_status |= TC_ERROR;
         WRITE_TM_MSB (telemetry_data.error_status);
         WRITE_TM_LSB (telemetry_data.mode_status);
         /* Send response to this TC to TM. */

         TC_state = TC_handling_e;
      }

      else
      {
         telemetry_pointer =
            DATA_POINTER((int)address_MSB * 256 + TC_code);
         telemetry_end_pointer = telemetry_pointer + MEM_BUFFER_SIZE;

         CLEAR_TM_INTERRUPT_FLAG;

         WRITE_TM_MSB (telemetry_data.error_status);
         WRITE_TM_LSB (telemetry_data.mode_status);
         /* First two bytes of Read Data Memory sequence. */

         read_memory_checksum = tmp_error_status ^ telemetry_data.mode_status;

         TC_state = memory_dump_e;

         SetInterruptMask(TM_ISR_MASK);
      }
   }

   else
   {
      /* Some other TC accepted. */

      WRITE_TM_MSB (telemetry_data.error_status);
      WRITE_TM_LSB (telemetry_data.mode_status);
      /* Send response to this TC to TM. */
   }

}


unsigned char PatchExecCommandOk (unsigned char execution_command)
{
   switch (execution_command)
   {
      case 0:
      case 0x09:
      case 0x37:
      case 0x5A:
         return 1;
   }
   return 0;
}


void MemoryPatch(telecommand_t EXTERNAL *command)
/* Purpose        : Handles received telecommand in memory patch state   */
/* Interface      : inputs  - command, received telecommand              */
/*                            address_MSB, MSB of patch area             */
/*                            address_LSB, LSB of patch area             */
/*                            memory_transfer_buffer, buffer for bytes   */
/*                               to be written to patch area             */
/*                            memory_buffer_index, index to above buffer */
/*                  outputs - memory_transfer_buffer, as above           */
/*                            memory_buffer_index, as above              */
/*                            code_not_patched, indicates if code      */
/*                               checksum is vali or not                 */
/*                            telemetry_data.mode_status, Mode Status    */
/*                               telemetry register                      */
/*                            TC_state, TC Execution task state          */
/* Preconditions  : TC_state is memory patch.                            */
/*                  Destination memory type (data/code) is memorized.    */
/* Postconditions : After last phase code or data memory patched if      */
/*                     operation succeeded.                              */
/* Algorithm      : - If memory_transfer_buffer not filled               */
/*                     - update write_checksum (XOR with MSB and LSB of  */
/*                          the telecommand word)                        */
/*                     - store TC word to memory_transfer_buffer         */
/*                  - else                                               */
/*                     - If checksum of TC block Ok                      */
/*                        - If data memory patch                         */
/*                           - copy memory_tranfer_buffer to data memory */
/*                        - else (code memory patch)                     */
/*                           - call PatchCode function                   */
/*                     - else                                            */
/*                        - set MEMORY_WRITE_ERROR bit in ModeStatus     */

{
   memory_patch_variables_t EXTERNAL patch_info;
   /* Parameters for PatchCode function. */

   data_address_t INDIRECT_INTERNAL address;
   /* Start address of the memory area to be patched. */

   uint_least8_t INDIRECT_INTERNAL i;
   /* Loop variable. */

   unsigned char INDIRECT_INTERNAL TC_msb;
   /* Most significant byte of the TC word. */

   TC_msb = (command -> TC_word) >> 8;
   write_checksum ^= TC_msb;

   if (memory_buffer_index < MEM_BUFFER_SIZE)
   {
      /* Filling the buffer bytes to be written to code or data */
      /* memory.                                                */

      write_checksum ^= command -> TC_code;

      memory_transfer_buffer[memory_buffer_index] = TC_msb;
      memory_buffer_index++;
      memory_transfer_buffer[memory_buffer_index] = command -> TC_code;
      memory_buffer_index++;

      TC_timeout = WRITE_MEMORY_TIMEOUT;
   }

   else
   {
      /* Now all bytes for memory area to be patched have been */
      /* received.                                             */

      if (write_checksum == command -> TC_code)
      {
         /* Checksum Ok. */

         address = ((unsigned int)address_MSB)*256 + address_LSB;

         switch (memory_type)
         {
            case data_e:
               /* Write to the data memory. */

               if (address <= (END_SRAM3 - MEM_BUFFER_SIZE + 1))
               {
                  _Pragma("loopbound min 32 max 32")
                  for(i=0; i<MEM_BUFFER_SIZE; i++)
                  {
                     SET_DATA_BYTE(address + i, memory_transfer_buffer[i]);
                  }
               }

               else
               {
                  Set_TC_Error();
               }

               break;

            case code_e:
               /* Write to the code memory. */

               if ((address >= BEGIN_SRAM1) &&
                   (address <= END_SRAM1 - MEM_BUFFER_SIZE + 1) &&
                   (PatchExecCommandOk (TC_msb)))

               {
                  /* Destination section resides in SRAM1.   */

                  code_not_patched = 0;
                  /* Next code checksum not valid, because code memory */
                  /* will be patched.                                  */

                  patch_info.source            = memory_transfer_buffer,
                  patch_info.destination       = address,
                  patch_info.data_amount       = MEM_BUFFER_SIZE,
                  patch_info.execution_command = TC_msb;
                  /* Set parameters for the MemoryPatch function   */
                  /* (see definition of memory_patch_variables_t). */

                  PatchCode(&patch_info);
                  /* May or may not return here. */
               }

               else
               {
                  /* Destination section out side SRAM1.   */
                  Set_TC_Error();
               }

               break;
         }
      }

      else
      {
         /* Checksum failure. */

         SetModeStatusError(MEMORY_WRITE_ERROR);
         /* Set memory write error bit in Mode Status register. */
      }

      TC_state = TC_handling_e;
   }
}


void WriteMemory(telecommand_t EXTERNAL *command)
/* Purpose        : Handles received telecommand in the WriteMemory state    */
/* Interface      : inputs  - Parameter "command" containing received        */
/*                            telecommand or memory byte                     */
/*                            memory_type defining code or data memory       */
/*                            destination                                    */
/*                  outputs - address_LSB, LSB of the patch area             */
/*                            TC_state                                       */
/* Preconditions  : TC_state is write_memory                                 */
/*                  Destination memory type (data/code) is memorized         */
/* Postconditions : LSB of the patch area address is memorized.              */
/* Algorithm      : - update write_checksum (XOR with TC word MSB and LSB)   */
/*                  - if telecommand is LSB setting of patch memory address  */
/*                    of selected memory type                                */
/*                      - set TC state to memory_patch_e                     */
/*                      - memorize address LSB                               */
/*                      - clear memory_buffer_index                          */
/*                      - set TC timeout to WRITE_MEMORY_TIMEOUT             */
/*                  - else                                                   */
/*                      - set TC state to TC_handling_e                      */

{
   /* Expecting LSB of the start address for the memory area to be patched. */

   write_checksum ^= ((command -> TC_word) >> 8) ^ (command -> TC_code);

   if (   (   (command -> TC_address == WRITE_CODE_MEMORY_LSB)
           && (memory_type           == code_e))
       || (   (command -> TC_address == WRITE_DATA_MEMORY_LSB)
           && (memory_type           == data_e)))
   {
      TC_state = memory_patch_e;

      address_LSB = command -> TC_code;
      memory_buffer_index = 0;
      TC_timeout = WRITE_MEMORY_TIMEOUT;
   }

   else
   {
      TC_state = TC_handling_e;

      Set_TC_Error();
   }
}


void UpdateTarget(telecommand_t EXTERNAL *command)
/* Purpose         : Updates a HW register or some global variable according */
/*                   to the parameter "command"                              */
/* Interface       : inputs  - Parameter "command" containing received       */
/*                             telecommand                                   */
/*                   outputs - telemetry data                                */
/* Preconditions  : The parameter "command" contains a valid telecommand     */
/* Postconditions  : A HW register or global variable is updated (depend on  */
/*                   "command")                                              */
/* Algorithm       : - switch "command"                                      */
/*                     - case Set Coefficient:                               */
/*                          set given quality coefficient value in telemetry */
/*                          according to "command"                           */
/*                     - case Min/Max Time:                                  */
/*                          set Min/Max Time according to "command"          */
/*                     - case Classification Level:                          */
/*                          set classification level according to "command"  */
/*                     - case Error Status Clear:                            */
/*                          clear error indicating bits from telemetry       */
/*                     - case Set Time Byte:                                 */
/*                          set Debie time byte# according to "command"      */
/*                     - case Clear Watchdog/Checksum Failure counter        */
/*                          clear Watchdog/Checksum Failure counter          */
/*                     - case Switch SU # On/Off/SelfTest                    */
/*                          switch SU to On/Off/SelfTest according to        */
/*                          "command"                                        */
/*                     - case Set Threshold                                  */
/*                          set Threshold according to "command"             */
{
   EXTERNAL sensor_unit_t SU_setting;
   /* Holds parameters for "SetSensorUnit" operation                    */
   /* Must be in external memory, because the parameter to the function */
   /* is pointer to external memory                                     */

   EXTERNAL trigger_set_t new_threshold;

   dpu_time_t EXTERNAL new_time;

   sensor_index_t EXTERNAL SU_index;

   SU_index = ((command -> TC_address) >> 4) - 2;


   switch (command -> TC_address)
   {
      case SET_COEFFICIENT_1:
      case SET_COEFFICIENT_2:
      case SET_COEFFICIENT_3:
      case SET_COEFFICIENT_4:
      case SET_COEFFICIENT_5:

         telemetry_data.coefficient[(command -> TC_address)&0x07] =
            command -> TC_code;
         break;

      case SET_SU_1_PLASMA_1E_1I_MAX_TIME:
      case SET_SU_2_PLASMA_1E_1I_MAX_TIME:
      case SET_SU_3_PLASMA_1E_1I_MAX_TIME:
      case SET_SU_4_PLASMA_1E_1I_MAX_TIME:

          SU_config[SU_index] -> plasma_1_plus_to_minus_max_time =
             command -> TC_code;
          break;

      case SET_SU_1_PLASMA_1E_PZT_MIN_TIME:
      case SET_SU_2_PLASMA_1E_PZT_MIN_TIME:
      case SET_SU_3_PLASMA_1E_PZT_MIN_TIME:
      case SET_SU_4_PLASMA_1E_PZT_MIN_TIME:

          SU_config[SU_index] -> plasma_1_plus_to_piezo_min_time =
             command -> TC_code;
          break;

      case SET_SU_1_PLASMA_1E_PZT_MAX_TIME:
      case SET_SU_2_PLASMA_1E_PZT_MAX_TIME:
      case SET_SU_3_PLASMA_1E_PZT_MAX_TIME:
      case SET_SU_4_PLASMA_1E_PZT_MAX_TIME:

          SU_config[SU_index] -> plasma_1_plus_to_piezo_max_time =
             command -> TC_code;
          break;

      case SET_SU_1_PLASMA_1I_PZT_MIN_TIME:
      case SET_SU_2_PLASMA_1I_PZT_MIN_TIME:
      case SET_SU_3_PLASMA_1I_PZT_MIN_TIME:
      case SET_SU_4_PLASMA_1I_PZT_MIN_TIME:

          SU_config[SU_index] -> plasma_1_minus_to_piezo_min_time =
             command -> TC_code;
          break;

      case SET_SU_1_PLASMA_1I_PZT_MAX_TIME:
      case SET_SU_2_PLASMA_1I_PZT_MAX_TIME:
      case SET_SU_3_PLASMA_1I_PZT_MAX_TIME:
      case SET_SU_4_PLASMA_1I_PZT_MAX_TIME:

          SU_config[SU_index] -> plasma_1_minus_to_piezo_max_time =
             command -> TC_code;
          break;

      case SET_SU_1_PLASMA_1P_CLASS_LEVEL:
      case SET_SU_2_PLASMA_1P_CLASS_LEVEL:
      case SET_SU_3_PLASMA_1P_CLASS_LEVEL:
      case SET_SU_4_PLASMA_1P_CLASS_LEVEL:

         SU_config[SU_index] -> plasma_1_plus_classification =
            command -> TC_code;
         break;

      case SET_SU_1_PLASMA_1M_CLASS_LEVEL:
      case SET_SU_2_PLASMA_1M_CLASS_LEVEL:
      case SET_SU_3_PLASMA_1M_CLASS_LEVEL:
      case SET_SU_4_PLASMA_1M_CLASS_LEVEL:

         SU_config[SU_index] -> plasma_1_minus_classification =
            command -> TC_code;
         break;

      case SET_SU_1_PLASMA_2P_CLASS_LEVEL:
      case SET_SU_2_PLASMA_2P_CLASS_LEVEL:
      case SET_SU_3_PLASMA_2P_CLASS_LEVEL:
      case SET_SU_4_PLASMA_2P_CLASS_LEVEL:

         SU_config[SU_index] -> plasma_2_plus_classification =
            command -> TC_code;
         break;

      case SET_SU_1_PIEZO_1_CLASS_LEVEL:
      case SET_SU_2_PIEZO_1_CLASS_LEVEL:
      case SET_SU_3_PIEZO_1_CLASS_LEVEL:
      case SET_SU_4_PIEZO_1_CLASS_LEVEL:

         SU_config[SU_index] -> piezo_1_classification =
            command -> TC_code;
         break;

      case SET_SU_1_PIEZO_2_CLASS_LEVEL:
      case SET_SU_2_PIEZO_2_CLASS_LEVEL:
      case SET_SU_3_PIEZO_2_CLASS_LEVEL:
      case SET_SU_4_PIEZO_2_CLASS_LEVEL:

         SU_config[SU_index] -> piezo_2_classification =
            command -> TC_code;
         break;

      case ERROR_STATUS_CLEAR:

         ClearErrorStatus();
         Clear_RTX_Errors();
         ClearSoftwareError();
         ClearModeStatusError();
         Clear_SU_Error();
         /* Clear Error Status register, RTX and software error indicating bits  */
         /* and Mode and SU Status registers.                                    */

         break;


      case SET_TIME_BYTE_3:

         new_time = ((dpu_time_t) command -> TC_code) << 24;
         COPY (internal_time, new_time);
         TC_timeout = SET_TIME_TC_TIMEOUT;

         break;

      case SET_TIME_BYTE_2:

         if (previous_TC.TC_address == SET_TIME_BYTE_3)
         {
            COPY (new_time, internal_time);

            new_time &= 0xFF000000;
            new_time |=
               ((dpu_time_t) command -> TC_code) << 16;

            COPY (internal_time, new_time);

            TC_timeout = SET_TIME_TC_TIMEOUT;
         }

         else
         {
            Set_TC_Error();
         }

         break;

      case SET_TIME_BYTE_1:

         if (previous_TC.TC_address == SET_TIME_BYTE_2)
         {
            COPY (new_time, internal_time);

            new_time &= 0xFFFF0000;
            new_time |=
               ((dpu_time_t) command -> TC_code) << 8;

            COPY (internal_time, new_time);

            TC_timeout = SET_TIME_TC_TIMEOUT;
         }

         else
         {
            Set_TC_Error();
         }

         break;

      case SET_TIME_BYTE_0:

         if (previous_TC.TC_address == SET_TIME_BYTE_1)
         {
            COPY (new_time, internal_time);

            new_time &= 0xFFFFFF00;
            new_time |=
               ((dpu_time_t) command -> TC_code);

            COPY (internal_time, new_time);
         }

         else
         {
            Set_TC_Error();
         }

         break;

      case CLEAR_WATCHDOG_FAILURES:

         telemetry_data.watchdog_failures = 0;
         break;

      case CLEAR_CHECKSUM_FAILURES:

         telemetry_data.checksum_failures = 0;
         break;

      case SWITCH_SU_1:
      case SWITCH_SU_2:
      case SWITCH_SU_3:
      case SWITCH_SU_4:

         if (GetMode() != ACQUISITION)
         {
            SU_setting.SU_number = SU_index + SU_1;

            switch (command -> TC_code)
            {
               case ON_VALUE:
                  Start_SU_SwitchingOn(SU_index, &
                     (SU_setting.execution_result));
                  break;

               case OFF_VALUE:
                  SetSensorUnitOff(SU_index, &(SU_setting.execution_result));
                  break;

               case SELF_TEST:
                  SU_setting.SU_state              = self_test_mon_e;
                  SU_setting.expected_source_state = on_e;
                  Switch_SU_State (&SU_setting);
                  break;
            }

            if (SU_setting.execution_result == SU_STATE_TRANSITION_FAILED)
            {
                /* The requested SU state transition failed. */

                Set_TC_Error();
            }

         }

         else

         {
            Set_TC_Error();
         }

         break;

      case SET_SU_1_PLASMA_1P_THRESHOLD:

         new_threshold.sensor_unit = SU_1;
         new_threshold.channel     = PLASMA_1_PLUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_1.plasma_1_plus_threshold =
            command -> TC_code;
         break;

      case SET_SU_2_PLASMA_1P_THRESHOLD:

         new_threshold.sensor_unit = SU_2;
         new_threshold.channel     = PLASMA_1_PLUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

        telemetry_data.sensor_unit_2.plasma_1_plus_threshold =
            command -> TC_code;
         break;

      case SET_SU_3_PLASMA_1P_THRESHOLD:

         new_threshold.sensor_unit = SU_3;
         new_threshold.channel     = PLASMA_1_PLUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_3.plasma_1_plus_threshold =
            command -> TC_code;
         break;

      case SET_SU_4_PLASMA_1P_THRESHOLD:

         new_threshold.sensor_unit = SU_4;
         new_threshold.channel     = PLASMA_1_PLUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_4.plasma_1_plus_threshold =
            command -> TC_code;
         break;

      case SET_SU_1_PLASMA_1M_THRESHOLD:

         new_threshold.sensor_unit = SU_1;
         new_threshold.channel     = PLASMA_1_MINUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_1.plasma_1_minus_threshold =
            command -> TC_code;
         break;

      case SET_SU_2_PLASMA_1M_THRESHOLD:

         new_threshold.sensor_unit = SU_2;
         new_threshold.channel     = PLASMA_1_MINUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_2.plasma_1_minus_threshold =
            command -> TC_code;
         break;

      case SET_SU_3_PLASMA_1M_THRESHOLD:

         new_threshold.sensor_unit = SU_3;
         new_threshold.channel     = PLASMA_1_MINUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_3.plasma_1_minus_threshold =
            command -> TC_code;
         break;

      case SET_SU_4_PLASMA_1M_THRESHOLD:

         new_threshold.sensor_unit = SU_4;
         new_threshold.channel     = PLASMA_1_MINUS;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_4.plasma_1_minus_threshold =
            command -> TC_code;
         break;

      case SET_SU_1_PIEZO_THRESHOLD:

         new_threshold.sensor_unit = SU_1;
         new_threshold.channel     = PZT_1_2;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_1.piezo_threshold = command -> TC_code;
         break;

      case SET_SU_2_PIEZO_THRESHOLD:

         new_threshold.sensor_unit = SU_2;
         new_threshold.channel     = PZT_1_2;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_2.piezo_threshold = command -> TC_code;
         break;

      case SET_SU_3_PIEZO_THRESHOLD:

         new_threshold.sensor_unit = SU_3;
         new_threshold.channel     = PZT_1_2;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_3.piezo_threshold = command -> TC_code;
         break;

      case SET_SU_4_PIEZO_THRESHOLD:

         new_threshold.sensor_unit = SU_4;
         new_threshold.channel     = PZT_1_2;
         new_threshold.level       = command -> TC_code;
         SetTriggerLevel(&new_threshold);

         telemetry_data.sensor_unit_4.piezo_threshold = command -> TC_code;
         break;

      default:
         /* Telecommands that will not be implemented in the Prototype SW */
         break;
   }
}


void ExecuteCommand(telecommand_t EXTERNAL *command)
/* Purpose        : Executes telecommand                                     */
/* Interface      : inputs      - Parameter "command" containing received    */
/*                                telecommand                                */
/*                  outputs     - TC_state                                   */
/*                                address_MSB                                */
/*                                memory_type                                */
/*                  subroutines - UpdateTarget                               */
/*                                StartAcquisition                           */
/*                                StopAcquisition                            */
/*                                SoftReset                                  */
/*                                Set_TC_Error                               */
/*                                Switch_SU_State                            */
/*                                Reboot                                     */
/*                                SetMode                                    */
/*                                UpdateTarget                               */
/* Preconditions  : The parameter "command" contains a valid telecommand     */
/*                  TC_state is TC_handling                                  */
/* Postconditions : Telecommand is executed                                  */
/*                  TC_state updated when appropriate (depend on "command")  */
/*                  HW registers modified when appropriate (depend on        */
/*                  "command")                                               */
/*                  Global variables modified when appropriate (depend on    */
/*                  "command")                                               */
/* Algorithm      : - switch "command"                                       */
/*                    - case Send Sciece Data File : set TC_state to SC_TM   */
/*                    - case Send Status Register  : set TC_state to         */
/*                         RegisterTM                                        */
/*                    - case Read Data Memory MSB  : memorize the address    */
/*                         MSB given in the TC_code and set TC_state to      */
/*                         read_memory                                       */
/*                    - case Write Code Memory MSB : memorize the address    */
/*                         MSB given in the TC_code, memorize code           */
/*                         destination selection and set TC_state to         */
/*                         write_memory                                      */
/*                    - case Write Data Memory MSB : memorize the address    */
/*                         MSB given in the TC_code, memorize data           */
/*                         destination selection and set TC_state to         */
/*                         write_memory                                      */
/*                    - case Read/Write Memory LSB : ignore telecommand      */
/*                    - case Soft Reset            : call SoftReset          */
/*                    - case Start Acquisition     : call StartAcquisition   */
/*                    - case Stop Acquisition      : call StopAcquisition    */
/*                    - default : call UpdateTarget                          */

{
   sensor_unit_t EXTERNAL SU_setting;
   unsigned char          error_flag;
   sensor_number_t        i;

   switch (command -> TC_address)
   {
      case SEND_SCIENCE_DATA_FILE:
         break;

      case SEND_STATUS_REGISTER:
         break;

      case READ_DATA_MEMORY_MSB:
         address_MSB = command -> TC_code;
         TC_state    = read_memory_e;
         break;

      case WRITE_CODE_MEMORY_MSB:
         if (GetMode() == STAND_BY)
         {
            address_MSB    = command -> TC_code;
            memory_type    = code_e;
            TC_timeout     = WRITE_MEMORY_TIMEOUT;
            write_checksum =
               ((command -> TC_word) >> 8) ^ (command -> TC_code);
            TC_state       = write_memory_e;
         }

         else
         {
             Set_TC_Error();
         }

         break;

      case WRITE_DATA_MEMORY_MSB:
         if (GetMode() == STAND_BY)
         {
            address_MSB = command -> TC_code;
            memory_type = data_e;
            TC_timeout     = WRITE_MEMORY_TIMEOUT;
            write_checksum =
               ((command -> TC_word) >> 8) ^ (command -> TC_code);
            TC_state    = write_memory_e;
         }

        else
         {
             Set_TC_Error();
         }

        break;

      case READ_DATA_MEMORY_LSB:
         break;

      case WRITE_CODE_MEMORY_LSB:
      case WRITE_DATA_MEMORY_LSB:
         if (TC_state != write_memory_e)
	 {
            Set_TC_Error();
	 }
         break;

      case SOFT_RESET:
         Reboot(soft_reset_e);
         /* Software is rebooted, no return to this point. */

         break;

      case START_ACQUISITION:
         error_flag = 0;

         _Pragma("loopbound min 4 max 4")
         for (i=SU_1; i<=SU_4; i++)
         {
             if ((ReadSensorUnit(i) == start_switching_e) ||
                 (ReadSensorUnit(i) == switching_e))
             {
                /* SU is being switched on. */

                error_flag = 1;
                /* StartAcquisition TC has to be rejected. */
             }
         }

         if ((GetMode() == STAND_BY) && (error_flag == 0))
         {
            SU_setting.SU_state              = acquisition_e;
            SU_setting.expected_source_state = on_e;

            SU_setting.SU_number = SU_1;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 1 to Acquisition state. */

            SU_setting.SU_number = SU_2;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 2 to Acquisition state. */

            SU_setting.SU_number = SU_3;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 3 to Acquisition state. */

            SU_setting.SU_number = SU_4;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 4 to Acquisition state. */

            CLEAR_HIT_TRIGGER_ISR_FLAG;

            ResetDelayCounters();
            /* Resets the SU logic that generates Hit Triggers.    */
            /* Brings T2EX to a high level, making a new falling   */
            /* edge possible.                                      */
            /* This statement must come after the above "clear",   */
            /* because a reversed order could create a deadlock    */
            /* situation.                                          */

            SetMode(ACQUISITION);
         }

         else
         {
            Set_TC_Error();
         }
         break;

      case STOP_ACQUISITION:
         if (GetMode() == ACQUISITION)
         {
            SU_setting.SU_state              = on_e;
            SU_setting.expected_source_state = acquisition_e;

            SU_setting.SU_number = SU_1;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 1 to On state. */

            SU_setting.SU_number = SU_2;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 2 to On state. */

            SU_setting.SU_number = SU_3;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 3 to On state. */

            SU_setting.SU_number = SU_4;
            Switch_SU_State (&SU_setting);
            /* Try to switch SU 4 to On state. */

            SetMode(STAND_BY);
         }

         else
         {
            Set_TC_Error();
         }
         break;

      default:
         UpdateTarget(command);
   }
}


static EXTERNAL incoming_mail_t TC_mail;
/* Holds parameters for the mail waiting function.                        */
/* Must be in xdata memory because parameter of subrounite is pointer     */
/* to xdata area.                                                         */

static EXTERNAL telecommand_t received_command;
/* Telecommand read form the mailbox is stored to TC_word component.      */
/* Must be in external memory because parameters of subroutines (pointers */
/* to xdata area).                                                        */


void _Pragma("entrypoint") InitTelecommandTask (void)

/* Purpose        : Initialize the global state of Telecommand Execution     */
/* Interface      : inputs      - none                                       */
/*                  outputs     - TC state                                   */
/*                  subroutines - DisableInterrupt                           */
/*                                EnableInterrupt                            */
/* Preconditions  : none                                                     */
/* Postconditions : TelecommandExecutionTask is operational.                 */
/* Algorithm      : - initialize task variables.                             */

{
   InitTC_LookUp();

   TC_state            = TC_handling_e;

   TC_mail.mailbox_number = TCTM_MAILBOX;
   TC_mail.message        = &(received_command.TC_word);
   /* Parameters for mail waiting function.   */
   /* Time-out set separately.                */

   previous_TC.TC_word    = 0;
   previous_TC.TC_address = UNUSED_TC_ADDRESS;
   previous_TC.TC_code    = 0;

   DisableInterrupt(TM_ISR_SOURCE);
   EnableInterrupt(TC_ISR_SOURCE);
}


void _Pragma("entrypoint") HandleTelecommand (void)

/* Purpose        : Waits for and handles one Telecommand from the TC ISR    */
/* Interface      : inputs      - Telecommand Execution task mailbox         */
/*                                TC state                                   */
/*                  outputs     - TC state                                   */
/*                  subroutines - ReadMemory                                 */
/*                                WriteMemory                                */
/*                                ExecuteCommand                             */
/* Preconditions  : none                                                     */
/* Postconditions : one message removed from the TC mailbox                  */
/* Algorithm      : - wait for mail to Telecommand Execution task mailbox    */
/*                  - if mail is "TM_READY" and TC state is either           */
/*                    "SC_TM_e" or "memory_dump_e".                          */
/*                    - if TC state is "SC_TM_e"                             */
/*                       - call ClearEvents function                         */
/*                    - set TC state to TC handling                          */
/*                  - else switch TC state                                   */
/*                    - case ReadMemory_e  : Check received TC's address     */
/*                    - case WriteMemory_e : call WriteMemory function       */
/*                    - case MemoryPatch_e : call MemoryPatch function       */
/*                    - case TC_Handling : call ExecuteCommand               */
/*                NOTE:   case register_TM_e is left out because             */
/*                        operation of SEND_STATUS_REGISTER TC does not      */
/*                        require any functionalites of this task.           */

{
   TC_mail.timeout = TC_timeout;

   WaitMail(&TC_mail);

   TC_timeout = 0;
   /* Default value */

   if (TC_mail.execution_result == TIMEOUT_OCCURRED)
   {
      previous_TC.TC_word    = 0;
      previous_TC.TC_address = UNUSED_TC_ADDRESS;
      previous_TC.TC_code    = 0;
      /* Forget previous telecommand. */

      if (TC_state != TC_handling_e)
      {
         /* Memory R/W time-out. */
         Set_TC_Error();
      }

      TC_state = TC_handling_e;
   }

   else if (TC_mail.execution_result == MSG_RECEIVED)
   {
      received_command.TC_address = TC_ADDRESS (received_command.TC_word);
      received_command.TC_code    = TC_CODE    (received_command.TC_word);

      if (((TC_state == SC_TM_e) || (TC_state == memory_dump_e)) &&
          (received_command.TC_word == TM_READY))

      /* Note that in order to this condition to be sufficient, only */
      /* TM interrupt service should be allowed to send mail to this */
      /* task in the TC states mentioned.                            */

      {
         DisableInterrupt(TM_ISR_SOURCE);

         if (TC_state == SC_TM_e)
         {
           ClearEvents();
         }

         TC_state = TC_handling_e;
      }
      else
      {
         switch (TC_state)
         {

            case read_memory_e:

               if (received_command.TC_address != READ_DATA_MEMORY_LSB)
               {
                  Set_TC_Error();
                  TC_state = TC_handling_e;
               }

               break;

            case write_memory_e:
               WriteMemory (&received_command);
               break;

            case memory_patch_e:
               MemoryPatch (&received_command);
               break;

            case TC_handling_e:
               ExecuteCommand (&received_command);
               break;

         }

      }

      STRUCT_ASSIGN (previous_TC, received_command, telecommand_t);
   }

   else
   {
      /* Nothing is done if WaitMail returns an error message. */
   }
}


void TelecommandExecutionTask(void) TASK(TC_TM_INTERFACE_TASK)
                                    PRIORITY(TC_TM_INTERFACE_PR)
/* Purpose        : Implements the highest level of Telecommand Execution    */
/*                  task                                                     */
/* Interface      : inputs      - Telecommand Execution task mailbox         */
/*                                TC state                                   */
/*                  outputs     - TC state                                   */
/*                  subroutines - InitTelecommandTask                        */
/*                                HandleTeleommand                           */
/* Preconditions  : none                                                     */
/* Postconditions : This function does not return.                           */
/* Algorithm      : - InitTelecommandTask                                    */
/*                  - loop forever:                                          */
/*                    - HandleTelecommand                                    */

{
   InitTelecommandTask ();

   _Pragma("loopbound min 0 max 0")
   while(1)
   {
      HandleTelecommand ();
   }
}


void Set_TC_Error(void)
/* Purpose        : This function will be called always when TC_ERROR bit in */
/*                  the ErrorStatus register is set.                         */
/* Interface      : inputs      - Error_status register                      */
/*                  outputs     - Error_status register                      */
/*                  subroutines - none                                       */
/* Preconditions  : none                                                     */
/* Postconditions : none                                                     */
/* Algorithm      : - Disable interrupts                                     */
/*                  - Write to Error Status register                         */
/*                  - Enable interrupts                                      */
{
   DISABLE_INTERRUPT_MASTER;

   telemetry_data.error_status |= TC_ERROR;

   ENABLE_INTERRUPT_MASTER;
}

/*Assign pointers to tasks*/
void (* EXTERNAL TC_task)(void) = TelecommandExecutionTask;
/*------------------------------------------------------------------------------
 *
 *    Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW
 *    Subsystem  :   DAS
 *    Module     :   telem.c
 *
 * Telemetry module.
 *
 * Based on the SSF file telem.c, rev 1.28, Wed Oct 13 19:49:34 1999.
 *
 *- * --------------------------------------------------------------------------
 */

#include "keyword.h"
#include "kernobj.h"
#include "tm_data.h"
#include "msg_ctrl.h"
#include "tc_hand.h"
#include "telem.h"
#include "ttc_ctrl.h"
#include "su_ctrl.h"
#include "dpu_ctrl.h"
#include "isr_ctrl.h"
#include "taskctrl.h"
#include "health.h"

EXTERNAL telemetry_data_t    telemetry_data;

EXTERNAL science_data_file_t LOCATION(SCIENCE_DATA_START_ADDRESS) science_data;

uint_least16_t EXTERNAL max_events;
/* This variable is used to speed up certain    */
/* Functional Test by adding the possibility    */
/* to restrict the amount of events.            */
/* It is initialised to value MAX_EVENTS at     */
/* Boot.                                        */

unsigned char EXTERNAL *telemetry_pointer;

unsigned char EXTERNAL *telemetry_end_pointer;

unsigned char EXTERNAL read_memory_checksum;
/* Checksum to be sent at the end of Read Memory sequence. */

event_record_t EXTERNAL event_queue[MAX_QUEUE_LENGTH];
/* Holds event records before they are copied to the */
/* Science Data memory. Normally there is only data  */
/* from the new event whose data is beign collected, */
/* but during Science Telemetry there can be stored  */
/* several older events which are copied to the      */
/* Science Data memory after telemetry ends.         */

uint_least8_t EXTERNAL event_queue_length;
/* Number of event records stored in the queue.    */
/* These records are stored into event_queue table */
/* in order starting from the first element.       */
/* Initialised to zero on power-up.                */

uint_least16_t EXTERNAL free_slot_index;
/* Index to the first free record in the Science         */
/* Data memory, or if it is full equals to 'max_events'. */
/* Initialised to zero on power-up.                      */


event_record_t EXTERNAL *GetFreeRecord(void)

/* Purpose        : Returns pointer to free event record in event queue.     */
/* Interface      : inputs      - event_queue_length, legnth of the event    */
/*                                queue.                                     */
/*                  outputs     - return value, pointer to the free record.  */
/*                  subroutines - none                                       */
/* Preconditions  : none.                                                    */
/* Postconditions : none.                                                    */
/* Algorithm      : -If the queue is not full                                */
/*                     -return pointer to the next free record               */
/*                  -else                                                    */
/*                     -return pointer to the last record                    */

{
   if (event_queue_length < MAX_QUEUE_LENGTH)
   {
      return &(event_queue[event_queue_length]);
   }

   else
   {
      return &(event_queue[MAX_QUEUE_LENGTH - 1]);
   }
}


void TM_InterruptService (void) INTERRUPT(TM_ISR_SOURCE) USED_REG_BANK(2)
/* Purpose        : This function handles the TM interrupts.                 */
/* Interface      : inputs  - telemetry_pointer                              */
/*                            telemetry_end_pointer                          */
/*                            TC_state                                       */
/*                            telemetry_data                                 */
/*                  outputs - telemetry_pointer                              */
/*                            TM HW reigsiters                               */
/*                            Telemcommand Execution task mailbox            */
/* Preconditions  : telemetry_pointer and telemetry_end_pointer have valid   */
/*                  values (TM interrupts should be enabled only when this   */
/*                  condition is true)                                       */
/* Postconditions : Next two bytes are written to TM HW registers and if they*/
/*                  were the last bytes to be written, a "TM_READY" mail is  */
/*                  sent to the Telecommand Execution task                   */
/* Algorithm      : - if telemetry_pointer < telemetry_end_pointer           */
/*                     - write next two bytes from location pointed by       */
/*                       telemetry_pointer and increase it by two            */
/*                  - else if TC_state == register_TM_e                      */
/*                     - write first two TM data registers and set           */
/*                       telemetry_pointer to point to the third TM data     */
/*                       register                                            */
/*                  - else                                                   */
/*                     - send TM_READY message to Telecommand Execution task */
/*                       mailbox                                             */

{
   unsigned char EXTERNAL tm_byte;

   CLEAR_TM_INTERRUPT_FLAG;
   /*The interrupt flag is put down by setting high bit 3 'INT1' in port 3.  */

   if (telemetry_pointer == (unsigned char *) &telemetry_data.time)
   {
      COPY (telemetry_data.time, internal_time);
   }

   if (telemetry_pointer < telemetry_end_pointer)
   {
      /* There are bytes left to be sent to TM. */

      tm_byte = *telemetry_pointer;
      WRITE_TM_MSB (tm_byte);
      read_memory_checksum ^= tm_byte;

      telemetry_pointer++;

      tm_byte = *telemetry_pointer;
      WRITE_TM_LSB (tm_byte);
      read_memory_checksum ^= tm_byte;

      telemetry_pointer++;
   }
   else if (TC_state == register_TM_e)
   /* Start to send TM data registers starting from the first ones */
   {
      telemetry_pointer = (EXTERNAL unsigned char *)&telemetry_data;
      WRITE_TM_MSB (*telemetry_pointer);
      telemetry_pointer++;
      WRITE_TM_LSB (*telemetry_pointer);
      telemetry_pointer++;
   }
   else if (TC_state == memory_dump_e)
   {
      WRITE_TM_MSB(0);
      WRITE_TM_LSB(read_memory_checksum);
      /* Last two bytes of Read Memory sequence. */

      Send_ISR_Mail(TCTM_MAILBOX, TM_READY);
   }
   else
   /* It is time to stop sending telemetry */
   {
      Send_ISR_Mail (TCTM_MAILBOX, TM_READY);
   }
}


dpu_time_t GetElapsedTime(unsigned int event_number)
/* Purpose        : Returns the hit time of a given event.                   */
/* Interface      : inputs      - event_number (parameter)                   */
/*                                science_data[event_number].hit_time, hit   */
/*                                time of the given event record.            */
/*                  outputs     - return value, hit time.                    */
/*                  subroutines - none                                       */
/* Preconditions  : none.                                                    */
/* Postconditions : none.                                                    */
/* Algorithm      :    -copy hit time of an event to a local variable hit    */
/*                      time                                                 */
/*                     -return the value of hit time                         */
{
   dpu_time_t INDIRECT_INTERNAL hit_time;
   /* Hit time. */

   COPY (hit_time, science_data.event[event_number].hit_time);

   return hit_time;
}



unsigned int FindMinQualityRecord(void)

/* Purpose        : Finds event with lowest quality from Science Data memory.*/
/* Interface      : inputs      - science_data.event, event records          */
/*                  outputs     - return value, index of event record with   */
/*                                   the lowest quality.                     */
/*                  subroutines - GetElapsedTime                             */
/* Preconditions  : none.                                                    */
/* Postconditions : none.                                                    */
/* Algorithm      : -Select first the first event record.                    */
/*                  -Loop from the second record to the last:                */
/*                     -if the quality of the record is lower than the       */
/*                      quality of the selected one, select the record.      */
/*                     -else if the quality of the record equals the quality */
/*                      of selected one and it is older than the selected    */
/*                      one, select the record.                              */
/*                  -End loop.                                               */
/*                  -return the index of the selected record.                */

{
   unsigned int INDIRECT_INTERNAL min_quality_number;
   /* The quality number of an event which has the lowest quality */
   /* number in the science data.                                 */

   unsigned int INDIRECT_INTERNAL min_quality_location;
   /* The location of an event which has the lowest quality number */
   /* in the science data.                                         */

   dpu_time_t DIRECT_INTERNAL min_time;
   /* Elapsed time of the oldest event. */

   dpu_time_t DIRECT_INTERNAL time;
   /* Elapsed time as previously mentioned. */

   uint_least16_t DIRECT_INTERNAL i;
   /* Loop variable. */


   min_time             = GetElapsedTime(0);
   min_quality_number   = science_data.event[0].quality_number;
   min_quality_location = 0;
   /* First event is selected and compared against */
   /* the following events in the science_data.    */

   _Pragma("loopbound min 1260 max 1260")
   for (i=1; i < max_events; i++)
   {
      time = GetElapsedTime(i);

      if(science_data.event[i].quality_number < min_quality_number)
      {
         min_time = time;
         min_quality_number = science_data.event[i].quality_number;
         min_quality_location = i;
         /* If an event in the science_data has a lower quality number than  */
         /* any of the previous events, its quality_number and location is   */
         /* stored into variables.                                           */
      }

      else if(   (science_data.event[i].quality_number == min_quality_number)
              && (time < min_time))
      {
         min_time = time;
         min_quality_location = i;
         /* If an event in the science_data has an equal quality number with */
         /* any of the previous events and it's older, event's               */
         /* quality_number and location are stored into variables.           */
      }
   }

   return min_quality_location;
}


void IncrementCounters(
   sensor_index_t sensor_unit,
   unsigned char  classification)

/* Purpose        : Increments given event counters.                         */
/* Interface      : inputs      - sensor_unit (parameter)                    */
/*                                classification (parameter)                 */
/*                  outputs     - telemetry_data.SU_hits, counter of hits of */
/*                                   given Sensor Unit                       */
/*                                science_data.event_counter, counter of     */
/*                                events with given classification and SU.   */
/*                  subroutines - none                                       */
/* Preconditions  : none.                                                    */
/* Postconditions : Given counters are incremented, if they had not their    */
/*                  maximum values.                                          */
/* Algorithm      : Increment given counters, if they are less than their    */
/*                  maximum values. Calculate checksum for event counter.    */
/*                                                                           */
/* This function is used by Acquisition and TelecommandExecutionTask.        */
/* However, it does not have to be of re-entrant type because collision      */
/* is avoided through design, as follows.                                    */
/* If Science Telemetry is in progress when Acquisition task is handling     */
/* an event, the event record cannot be written to the Science Data          */
/* memory. Instead it is left to the temporary queue which will be           */
/* copied to the Science Data memory after the Science telemetry is          */
/* completed. For the same reason  call for IncrementCounters is             */
/* disabled.                                                                 */
/* On the other hand, when Acquisition task is handling an event with        */
/* RecordEvent all interrupts are disabled i.e. TelecommandExecutionTask     */
/* cannot use IncrementCounters simultaniously.                              */



{
   unsigned char EXTERNAL counter;
   unsigned char EXTERNAL new_checksum;


   if (telemetry_data.SU_hits[sensor_unit] < 0xFFFF)
   {
      telemetry_data.SU_hits[sensor_unit]++;
      /* SU hit counter is incremented. */
   }

   if (science_data.event_counter[sensor_unit][classification] < 0xFF)
   {

      counter = science_data.event_counter[sensor_unit][classification];

      new_checksum =
         science_data.counter_checksum ^ counter;
      /* Delete effect of old counter value from the checksum. */

      counter++;

      new_checksum ^= counter;
      /* Add effect of new counter value to the checksum. */

      science_data.event_counter[sensor_unit][classification] = counter;
      /* The event counter is incremented. */

      science_data.counter_checksum = new_checksum;
      /* Event counter checksum is updated. */
   }

}


/*****************************************************************************/
/*                               tm_data.h                                   */
/*****************************************************************************/

void RecordEvent(void)
/* Purpose        : This function increments proper event counter and stores */
/*                  the new event record to the science data memory.         */
/* Interface      : inputs      - free_slot_index, index of next free event  */
/*                                   record in the Science Data memory.      */
/*                                TC_state, state of the TC Execution task.  */
/*                                event_queue_length, length of the event    */
/*                                   record queue.                           */
/*                                event_queue, event record queue.           */
/*                  outputs     - event_queue_length, as above.              */
/*                                science_data.event, event records in       */
/*                                Science Data memory.                       */
/*                                free_slot_index, as above.                 */
/*                  subroutines - FindMinQualityRecord                       */
/*                                IncrementCounters                          */
/* Preconditions  : none.                                                    */
/* Postconditions : If Science telemetry is not in progress, event data is   */
/*                  stored in its proper place in the science data,          */
/*                  otherwise event data is left in the queue and one record */
/*                  is reserved from the queue unless it is already full.    */
/* Algorithm      : If there is room in the Science Data memory, the event   */
/*                  data is tried to be stored there, otherwise the event    */
/*                  with the lowest quality is searched and tried to be      */
/*                  replaced. If the Science telemetry is in progress the    */
/*                  event data is left in the queue and the length of the    */
/*                  queue is incremented unless the queue is already full.   */
/*                  If the Science telemetry is not in progress the event    */
/*                  data is copied to the Science Data to the location       */
/*                  defined earlier as described above.                      */

{
   uint_least16_t INDIRECT_INTERNAL record_index;

   DISABLE_INTERRUPT_MASTER;

   record_index = free_slot_index;

   if (record_index >= max_events && TC_state != SC_TM_e)
   {
      /* Science Data memory was full and Science TM was not in progress */

      ENABLE_INTERRUPT_MASTER;
      record_index = FindMinQualityRecord();
      DISABLE_INTERRUPT_MASTER;
   }

   if (TC_state == SC_TM_e)
   {
      /* Science Telemetry is in progress, so the event record */
      /* cannot be written to the Science Data memory. Instead */
      /* it is left to the temporary queue which will be       */
      /* copied to the Science Data memory after the Science   */
      /* telemetry is completed.                               */

      if (event_queue_length < MAX_QUEUE_LENGTH)
      {
         /* There is still room in the queue. */

         event_queue_length++;
         /* Prevent the event data from being overwritten. */
      }
      ENABLE_INTERRUPT_MASTER;
   }

   else
   {
      if (free_slot_index < max_events)
      {
	 /* Science Data memory was not full */

         record_index = free_slot_index;
         science_data.event[record_index].quality_number = 0;
         free_slot_index++;
      }


      /* Increment event counters. */
      IncrementCounters(
         event_queue[0].SU_number - 1,
         event_queue[0].classification);

      ENABLE_INTERRUPT_MASTER;

      if (event_queue[0].quality_number >=
          science_data.event[record_index].quality_number)

      {
         STRUCT_ASSIGN (
            science_data.event[record_index],
            event_queue[0],
            event_record_t);

         /* In this state the event data is located always to */
         /* the first element of the queue.                   */
      }
   }
}


void ClearEvents(void)
/* Cleares the event counters and the quality numbers of                     */
/* the event records in the science data memory                              */

{
   DIRECT_INTERNAL uint_least8_t i;
   /* This variable is used in the for-loop which goes through  */
   /* the science data event counter.                           */

   DIRECT_INTERNAL uint_least8_t j;
   /* This variable is used in the for-loop which goes through  */
   /* the science data event counter.                           */

   /* Interrupts does not need to be disabled as long as  */
   /* Telecommand Execution task has higher priority than */
   /* Acquisition task.                                   */

   _Pragma("loopbound min 4 max 4")
   for(i=0;i<NUM_SU;i++)
   {
      telemetry_data.SU_hits[i] = 0;

      _Pragma("loopbound min 10 max 10")
      for(j=0;j<NUM_CLASSES;j++)
      {
         science_data.event_counter[i][j] = 0;
      }
      /*event counters are cleared in science_data                           */
   }

   _Pragma("loopbound min 0 max 10")
   for (i=0; i < event_queue_length; i++)
   {
      /* Events from the event queue are copied to the Science */
      /* Data memory.                                          */

      STRUCT_ASSIGN (
         science_data.event[i],
         event_queue[i],
         event_record_t);

      IncrementCounters(
         event_queue[i].SU_number - 1,
         event_queue[i].classification);

      /* One more event is stored in the Science Data memory. */
      /* NOTE that the event queue should always be smaller   */
      /* than the space reserved for event records in the     */
      /* Science Data memory.                                 */
   }

   free_slot_index    = event_queue_length;

   event_queue_length = 0;
   /* Empty the event queue. */

   science_data.counter_checksum = 0;
   science_data.not_used         = 0;
}

void ResetEventQueueLength(void)
/* Purpose        : Empty the event queue length.                            */
/* Interface      : inputs      - none                                       */
/*                  outputs     - none                                       */
/*                  subroutines - none                                       */
/* Preconditions  : none.                                                    */
/* Postconditions : none.                                                    */
/* Algorithm      : - reset event queue length.                              */
{
      event_queue_length = 0;
}










