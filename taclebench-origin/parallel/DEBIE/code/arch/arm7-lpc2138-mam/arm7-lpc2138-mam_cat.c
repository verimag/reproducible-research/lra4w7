/*
cpulib: Routines for CPU initialization and configuration.

Target  : iSYSTEM LPC2138-M minitarget board (ARM7TDMI)
Source  : iSYSTEM iF-DEV-LPC kit
Changes : Tidorum Ltd (N. Holsti)

$Id: cpulib.c,v 1.1 2008/04/08 09:44:55 niklas Exp $
*/

#include "cpulib.h"        

void cpulib_init_cpu (void)
{
   // The MAM is assumed to be initialized in crt0.s.

   // Turn off the LED:

   IO0DIR|=IO_LED;   // Set pin direction to output.
   IO0SET|=IO_LED;   // Set pin value to 1 = turn off LED.
}


void cpulib_go_fast (unsigned int mode)
{   
   // Turn on PLL from 12 MHz to 60 MHz cclk:
    
   PLLCFG=0x24;
   PLLCON=0x1;
   PLLFEED=0xAA;
   PLLFEED=0x55;
   while(!(PLLSTAT & 0x400)){};
   PLLCON=0x3;
   PLLFEED=0xAA;
   PLLFEED=0x55;    
}


void cpulib_set_led (int state)
{
   if (state)
      IO0CLR=IO_LED;   // LED on.
   else
      IO0SET=IO_LED;   // LED off.
}


static volatile int flash_timer;
/* Loop counter for timing flash_led. */


void cpulib_flash_led (void)
{
   cpulib_set_led (1);

   flash_timer = 10000;
   while (flash_timer > 0) flash_timer--;

   cpulib_set_led (0);
}


void cpulib_blink_led (void)
{
   cpulib_set_led (0);

   flash_timer = 30000;
   while (flash_timer > 0) flash_timer--;

   cpulib_set_led (1);

   flash_timer = 10000;
   while (flash_timer > 0) flash_timer--;

   cpulib_set_led (0);
}

typedef unsigned char byte;
typedef unsigned int uint;
typedef void (*pfunc)(void);

int main(int argc, char *argv[]);
extern byte __ASYST_DATA_LOAD[];
extern byte __ASYST_DATA_START[];
extern byte __ASYST_DATA_END[];
extern byte __ASYST_BSS_START[];
extern byte __ASYST_BSS_END[]; 
extern pfunc __ASYST_CTOR_START[]; 
extern pfunc __ASYST_CTOR_END[];
extern pfunc __ASYST_DTOR_START[]; 
extern pfunc __ASYST_DTOR_END[];
extern byte __ASYST_HEAP_START[];

#define __ASYST_IS_ALIGNED(P) ((((uint)(P)) & (~(sizeof(uint) -1))) == ((uint)(P)))

void __asyst_memset_byte(byte *pbyDest, byte byValue, uint uiSize)
{
  byte *pbyDestLast=pbyDest+uiSize;
  for(; pbyDest!=pbyDestLast; pbyDest++)
    pbyDest[0]=byValue;
}

void __asyst_memset_uint(uint *puiDest, uint uiValue, uint uiSize)
{
  uint *puiDestLast=puiDest+uiSize;
  for(; puiDest!=puiDestLast; puiDest++)
    puiDest[0]=uiValue;
}

void __asyst_memset(byte *pbyDest, byte byValue, uint uiSize)
{
  if(__ASYST_IS_ALIGNED(pbyDest) &&
     __ASYST_IS_ALIGNED(uiSize))
  {
    uint uiValue=0;
    __asyst_memset_byte((byte*)&uiValue, byValue, sizeof(uint));
    return __asyst_memset_uint((uint*)pbyDest, uiValue, uiSize/sizeof(uint));  
  }
  else
    return __asyst_memset_byte(pbyDest, byValue, uiSize);
  
}

void *__asyst_memcpy_uint(uint *puiDest, const uint *puiSrc, uint uiSize)
{
  uint *puiDestLast=puiDest+uiSize;
  for(; puiDest!=puiDestLast; puiSrc++,puiDest++)
    puiDest[0]=puiSrc[0];
  return puiDestLast;
}

void *__asyst_memcpy_byte(byte *pbyDest, const byte *pbySrc, uint uiSize)
{
  byte *pbyDestLast=pbyDest+uiSize;
  for(; pbyDest!=pbyDestLast; pbySrc++,pbyDest++)
    pbyDest[0]=pbySrc[0];
  return pbyDestLast;    
}

void *__asyst_memcpy(byte *pbyDest, const byte *pbySrc, uint uiSize)
{
  if(__ASYST_IS_ALIGNED(pbyDest) &&
     __ASYST_IS_ALIGNED(pbySrc) &&
     __ASYST_IS_ALIGNED(uiSize))
    return __asyst_memcpy_uint((uint*)pbyDest, (uint*)pbySrc, uiSize/sizeof(uint));
  else
    return __asyst_memcpy_byte(pbyDest, pbySrc, uiSize);    
}

void __asyst_main()
{
  pfunc *pfTable;
  // copy data
  if(__ASYST_DATA_START != __ASYST_DATA_LOAD)
    __asyst_memcpy(__ASYST_DATA_START, __ASYST_DATA_LOAD, __ASYST_DATA_END - __ASYST_DATA_START);
  // zero bss
  __asyst_memset(__ASYST_BSS_START, 0, __ASYST_BSS_END - __ASYST_BSS_START);
  
  /* Not used in C project
  // call constructors
  for(pfTable=__ASYST_CTOR_END-1; pfTable!=__ASYST_CTOR_START; pfTable--)
    pfTable[0]();
  */
  
  // main
  main(0, 0);
  
  /* Not used in C project
  // call destructors
  //for(pfTable=__ASYST_DTOR_START; pfTable!=__ASYST_DTOR_END; pfTable++)
    //pfTable[0]();
  */
  
  while(1);
}
/*------------------------------------------------------------------------------
 *
 * Copyright (C) 1998 : Space Systems Finland Ltd.
 *
 * Space Systems Finland Ltd (SSF) allows you to use this version of
 * the DEBIE-I DPU software for the specific purpose and under the
 * specific conditions set forth in the Terms Of Use document enclosed
 * with or attached to this software. In particular, the software
 * remains the property of SSF and you must not distribute the software
 * to third parties without written and signed authorization from SSF.
 *
 *    System Name:   DEBIE DPU SW, test harness for WCET analysis
 *    Subsystem  :   DNI (DEBIE Null Interface)
 *    Module     :   target.c
 *
 * Target-specific implementations of the DNI operations, specifically
 * for the iF-DEV-LPC kit and the LPC2138 processor.
 *
 * Based, with extensive changes, on parts of the SSF file rtx_if.c,
 * rev 1.13, Fri May 21 00:14:00 1999.
 *
 *- * --------------------------------------------------------------------------
 */

/* This file contains one set of operations, as follows:
 *
 * > memory operations: dpu_ctrl.h
 *
 */


#include "keyword.h"


/*    Memory operations : dpu_ctrl.h    */


#include "dpu_ctrl.h"


#define DATA_MEM_BASE 0x40000000L
/* The SRAM starts at this address. */


unsigned char *Data_Pointer (uint16_t address)
{
   return (unsigned char *)(DATA_MEM_BASE + (uint32_t)address);
}


void Set_Data_Byte (data_address_t addr, unsigned char value)
{
   /* *(Data_Pointer (addr)) = value; */
   /* Safer to do nothing. */
}


unsigned char Get_Data_Byte (data_address_t addr)
{
   return *(Data_Pointer (addr));
}


unsigned char Get_Code_Byte (code_address_t addr)
{
   return 0;
}
