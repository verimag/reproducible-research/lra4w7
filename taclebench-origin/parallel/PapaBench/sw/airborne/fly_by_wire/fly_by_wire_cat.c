/*
 * Paparazzi fly by wire adc functions
 *  
 * Copied from autopilot (autopilot.sf.net) thanx alot Trammell
 *
 * Copyright (C) 2002 Trammell Hudson <hudson@rotomotion.com>
 * Copyright (C) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

//// ADC3 MVSUP
//// ADC6 MVSERVO


#include <arch/signal.h>
#include <arch/interrupt.h>
#include <arch/io.h>
#include "adc_fbw.h"
#include "std.h"


#if defined CTL_BRD_V1_2 || defined CTL_BRD_V1_2_1

#define VOLTAGE_TIME	0x07
#define ANALOG_PORT	PORTC
#define ANALOG_PORT_DIR	DDRC


// 
#define ANALOG_VREF _BV(REFS0) | _BV(REFS1)

uint16_t		adc_samples[ NB_ADC ];

static struct adc_buf* buffers[NB_ADC];

void fbw_adc_buf_channel(uint8_t adc_channel, struct adc_buf* s) {
  buffers[adc_channel] = s;
}

void 
fbw_adc_init( void )
{
  uint8_t i;
  /* Ensure that our port is for input with no pull-ups */
  ANALOG_PORT 	= 0x00;
  ANALOG_PORT_DIR	= 0x00;

  /* Select our external voltage ref */
  ADMUX		= ANALOG_VREF;

  /* Select out clock, turn on the ADC interrupt and start conversion */
  ADCSRA = 0
    | VOLTAGE_TIME
    | _BV(ADEN )
    | _BV(ADIE )
    | _BV(ADSC );

  /* Init to 0 (usefull ?) */
  _Pragma("loopbound min 8 max 8")
  for(i = 0; i < NB_ADC; i++)
    buffers[i] = (struct adc_buf*)0;
}

/**
 * Called when the voltage conversion is finished
 * 
 *  8.913kHz on mega128@16MHz 1kHz/channel ??
*/

SIGNAL( SIG_ADC )
{
  uint8_t adc_input	= ADMUX & 0x7;
  struct adc_buf* buf = buffers[adc_input];
  uint16_t adc_value = ADCW;
  /* Store result */
  adc_samples[ adc_input ] = adc_value;

  if (buf) {
    uint8_t new_head = buf->head + 1;
    if (new_head >= AV_NB_SAMPLE) new_head = 0;
    buf->sum -= buf->values[new_head];
    buf->values[new_head] = adc_value;
    buf->sum += adc_value;
    buf->head = new_head;   
  }

  /* Find the next input */
  adc_input++;
  if (adc_input == 4)
    adc_input = 6; // ADC 4 and 5 for i2c
  if( adc_input >= 8 ) {
    adc_input = 0;
#ifdef CTL_BRD_V1_2
    adc_input = 1; // WARNING ADC0 is for rservo driver reset on v1.2.0
#endif /* CTL_BRD_V1_2 */
  }
  /* Select it */
  ADMUX = adc_input | ANALOG_VREF;
  /* Restart the conversion */
  sbi( ADCSR, ADSC );
}

#endif /* CTL_BRD_V1_2 || CTL_BRD_V1_2_1 */
/*
 * Paparazzi $Id: main.c,v 1.4 2011-01-25 10:42:14 plazar Exp $
 *  
 * Copyright (C) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

#include <inttypes.h>
#include <io.h>
#include <signal.h>
#include <interrupt.h>

#include "timer.h"
#include "servo.h"
#include "ppm.h"
#include "spi.h"
#include "link_autopilot.h"
#include "radio.h"


#include "uart.h"


#ifndef CTL_BRD_V1_1
#include "adc_fbw.h"
struct adc_buf vsupply_adc_buf;
struct adc_buf vservos_adc_buf;
#endif

uint8_t mode;
static uint8_t time_since_last_mega128;
static uint16_t time_since_last_ppm;
bool_t radio_ok, mega128_ok, radio_really_lost;

static const pprz_t failsafe[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

static uint8_t ppm_cpt, last_ppm_cpt;

#define STALLED_TIME        30  // 500ms with a 60Hz timer
#define REALLY_STALLED_TIME 300 // 5s with a 60Hz timer


/* static inline void status_transmit( void ) { */
/*   uint8_t i; */
/*   uart_transmit(7); */
/*   uart_transmit(7); */
/*   for (i=0; i<sizeof(struct inter_mcu_msg); i++)  */
/*     uart_transmit(((uint8_t*)&to_mega128)[i]); */
/*   uart_transmit('\n'); */

/*   uart_transmit(7); */
/*   uart_transmit(7); */
/*   uint8_t i; */
/*   for(i = 0; i < RADIO_CTL_NB; i++) { */
/*     extern uint16_t ppm_pulses[]; */
/*     uart_transmit(ppm_pulses[i]>>8); */
/*     uart_transmit(ppm_pulses[i] & 0xff); */
/*   } */
/*   uart_transmit('\n'); */
/* } */


/* Prepare data to be sent to mcu0 */
static void to_autopilot_from_last_radio (void) {
  uint8_t i;
  _Pragma("loopbound min 9 max 9")
  for(i = 0; i < RADIO_CTL_NB; i++)
     to_mega128.channels[i] = last_radio[i];
  to_mega128.status = (radio_ok ? _BV(STATUS_RADIO_OK) : 0);
  to_mega128.status |= (radio_really_lost ? _BV(RADIO_REALLY_LOST) : 0);
  if (last_radio_contains_avg_channels) {
    to_mega128.status |= _BV(AVERAGED_CHANNELS_SENT);
    last_radio_contains_avg_channels = FALSE;
  }
  to_mega128.ppm_cpt = last_ppm_cpt;
#ifndef CTL_BRD_V1_1
  to_mega128.vsupply = VoltageOfAdc(vsupply_adc_buf.sum/AV_NB_SAMPLE) * 10;
#else
  to_mega128.vsupply = 0;
#endif
}

void _Pragma("entrypoint") send_data_to_autopilot_task(void)
{
#ifndef WCET_ANALYSIS
   if ( !SpiIsSelected() && spi_was_interrupted ) 
   {
      spi_was_interrupted = FALSE;
      to_autopilot_from_last_radio();
      spi_reset();
   }
#endif
}

#ifdef PAPABENCH_SINGLE
	extern uint8_t _1Hz;
	extern uint8_t _20Hz;
#else
	static uint8_t _1Hz;
	static uint8_t _20Hz;
#endif

void fbw_init(void) {
  uart_init_tx();
  uart_print_string("FBW Booting $Id: main.c,v 1.4 2011-01-25 10:42:14 plazar Exp $\n");

#ifndef CTL_BRD_V1_1
  fbw_adc_init();
  fbw_adc_buf_channel(3, &vsupply_adc_buf);
  fbw_adc_buf_channel(6, &vservos_adc_buf);
#endif
  timer_init();
  servo_init();
  ppm_init();
  fbw_spi_init();
  //sei(); //FN
}

void fbw_schedule(void) {
	if (time_since_last_mega128 < STALLED_TIME)
		time_since_last_mega128++;
	if (time_since_last_ppm < REALLY_STALLED_TIME)
		time_since_last_ppm++;
	if (_1Hz == 0)  {
		last_ppm_cpt = ppm_cpt;
		ppm_cpt = 0;
	}
	test_ppm_task(); 	
	check_mega128_values_task();
	send_data_to_autopilot_task();    
	check_failsafe_task();
	if (_20Hz >= 3) 
		servo_transmit();
}

#ifndef PAPABENCH_SINGLE
int main( void )
{
	fbw_init();
#ifndef NO_MAINLOOP
  while( 1 ) 
  {
#endif
	fbw_schedule();
    if(timer_periodic()) 
    {
      _1Hz++;
      _20Hz++;
      if (_1Hz >= 60) 
      {
	_1Hz = 0;
      }
      if (_20Hz >= 3) 
      {
	_20Hz = 0;
      }
    }
#ifndef NO_MAINLOOP
  } 
#endif
  return 0;
}
#endif

void _Pragma("entrypoint") test_ppm_task(void)
{
    if( ppm_valid ) 
    {
      ppm_valid = FALSE;
      ppm_cpt++;
      radio_ok = TRUE;
      radio_really_lost = FALSE;
      time_since_last_ppm = 0;
      last_radio_from_ppm();
      if (last_radio_contains_avg_channels) 
      {
	mode = MODE_OF_PPRZ(last_radio[RADIO_MODE]);
      }
      if (mode == MODE_MANUAL) 
      {
	servo_set(last_radio);
      }
    } 
    else if (mode == MODE_MANUAL && radio_really_lost) 
    {
      mode = MODE_AUTO;
    }
    if (time_since_last_ppm >= STALLED_TIME) 
    {
      radio_ok = FALSE;
    }
    if (time_since_last_ppm >= REALLY_STALLED_TIME) 
    {
      radio_really_lost = TRUE;
    }
}
void _Pragma("entrypoint") check_failsafe_task(void)
{
    if ((mode == MODE_MANUAL && !radio_ok) ||
	(mode == MODE_AUTO && !mega128_ok)) 
    {
      servo_set(failsafe);
    }
}
void _Pragma("entrypoint") check_mega128_values_task(void)
{
#ifndef WCET_ANALYSIS
  if ( !SpiIsSelected() && spi_was_interrupted ) 
  {
    if (mega128_receive_valid)
    { 
      time_since_last_mega128 = 0;
      mega128_ok = TRUE;
      if (mode == MODE_AUTO)
        servo_set(from_mega128.channels);
    }
  }
  if (time_since_last_mega128 == STALLED_TIME) {
    mega128_ok = FALSE;
  }
#endif
}
/* $Id: ppm.c,v 1.1 2011-01-18 12:42:42 moellmer Exp $
 * Copied from autopilot (autopilot.sf.net) thanx alot Trammell
 *
 * (c) 2003 Trammell Hudson <hudson@rotomotion.com>
 * (c) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 *
 */

#include "radio.h"
#include "ppm.h"

#define AVERAGING_PERIOD (PPM_FREQ/4)

/*
 * Pulse width is computed as the difference between now and the
 * previous pulse.  If no pulse has been received between then and
 * now, the time of the last pulse will be equal to the last pulse
 * we measured.  Unfortunately, the Input Capture Flag (ICF1) will
 * not be set since the interrupt routine disables it.
 * 
 * Sync pulses are timed with Timer2, which runs at Clk/1024.  This
 * is slow enough at both 4 and 8 Mhz to measure the lengthy (10ms
 * or longer) pulse.
 *
 * Otherwise, compute the pulse width with the 16-bit timer1,
 * push the pulse width onto the stack and increment the
 * pulse counter until we have received eight pulses.
 */

uint16_t ppm_pulses[ PPM_NB_PULSES ];
pprz_t last_radio[ PPM_NB_PULSES ];
pprz_t avg_last_radio[ PPM_NB_PULSES ];
bool_t last_radio_contains_avg_channels = FALSE;
volatile bool_t ppm_valid;

/* MC3030, Trame PPM7: 25ms, 10.4 au neutre, 
   sync pulse = 16.2ms with low value on every channels */


    
  
#define RestartPpmCycle() { state = 0;  sync_start = TCNT2; return; }

#ifdef PAPABENCH_SINGLE
	SIGNAL(SIG_SPM_READY)
#else
	SIGNAL( SIG_INPUT_CAPTURE1 )
#endif
{
  static uint16_t		last;
  uint16_t		this;
  uint16_t		width;
  static uint8_t		state;
  static uint8_t		sync_start;

  this		= ICR1;
  width		= this - last;
  last		= this;
  
  if( state == 0 ) {
    uint8_t	end = TCNT2;
    uint8_t diff = (end - sync_start);
    sync_start = end;

    /* The frame period of the mc3030 seems to be 25ms. 
     * One pulse lasts from 1.05ms to 2.150ms.
     * Sync pulse is at least 7ms : (7000*CLOCK)/1024 = 109
     */
    if( diff > (uint8_t)(((uint32_t)(7000ul*CLOCK))/1024ul) ) {
      state = 1;
    }
  } 
  else {
    /* Read a data pulses */
    if( width < 700ul*CLOCK || width > 2300ul*CLOCK)
      RestartPpmCycle();
    ppm_pulses[state - 1] = width;

    if (state >= PPM_NB_PULSES) {
      ppm_valid	= 1;
      RestartPpmCycle();
    } else 
      state++;
  }
  return;
}

/* Copy from the ppm receiving buffer to the buffer sent to mcu0 */
void last_radio_from_ppm() {
  LastRadioFromPpm()
}
/*  $Id: servo.c,v 1.4 2011-01-25 10:05:41 plazar Exp $
 *
 * Copied from autopilot (autopilot.sf.net) thanx alot Trammell
 * (c) 2002 Trammell Hudson <hudson@rotomotion.com>
 * (c) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 *
 */


#include <arch/io.h>
#include <arch/signal.h>
#include "servo.h"
#include "link_autopilot.h"

#include "airframe.h"

#include "uart.h"


/*
 * Paparazzi boards have one 4017 servo driver.
 * It is driven by OCR1A (PB1) with reset on PORTD5.
 */
#define _4017_NB_CHANNELS 10

#ifdef CTL_BRD_V1_1
#define _4017_RESET_PORT        PORTD
#define _4017_RESET_DDR         DDRD
#define _4017_RESET_PIN         5
#endif /* CTL_BRD_V1_1 */ 

#ifdef CTL_BRD_V1_2
#define _4017_RESET_PORT        PORTC
#define _4017_RESET_DDR         DDRC
#define _4017_RESET_PIN         0
#endif /* CTL_BRD_V1_2 */

#ifdef CTL_BRD_V1_2_1
#define _4017_RESET_PORT        PORTD
#define _4017_RESET_DDR         DDRD
#define _4017_RESET_PIN         7
#endif /* CTL_BRD_V1_2 */

#define _4017_CLOCK_PORT        PORTB
#define _4017_CLOCK_DDR         DDRB
#define _4017_CLOCK_PIN         PB1

#define SERVO_OCR		OCR1A
#define SERVO_ENABLE		OCIE1A
#define SERVO_FLAG		OCF1A
#define SERVO_FORCE		FOC1A
#define SERVO_COM0		COM1A0
#define SERVO_COM1		COM1A1

/* Following macro is required since the compiler does not solve at
   compile-time indexation in a known array with a known index */
#define SERVO_NEUTRAL_(i) SERVOS_NEUTRALS_ ## i
#define SERVO_NEUTRAL(i) (SERVO_NEUTRAL_(i)*CLOCK)

#define SERVO_NEUTRAL_I(i) (((int[])SERVOS_NEUTRALS[i])*CLOCK)
#define SERVO_MIN_I(i) (((int[])SERVOS_MINS[i])*CLOCK)

#define SERVO_MIN (SERVO_MIN_US*CLOCK)
#define SERVO_MAX (SERVO_MAX_US*CLOCK)
#define ChopServo(x) ((x) < SERVO_MIN ? SERVO_MIN : ((x) > SERVO_MAX ? SERVO_MAX : (x)))

/* holds the servo pulses width in clock ticks */
static uint16_t servo_widths[_4017_NB_CHANNELS];

/*
 * We use the output compare registers to generate our servo pulses.
 * These should be connected to a decade counter that routes the
 * pulses to the appropriate servo.
 *
 * Initialization involves:
 *
 * - Reseting the decade counters
 * - Writing the first pulse width to the counters
 * - Setting output compare to set the clock line by calling servo_enable()
 * - Bringing down the reset lines
 *
 * Ideally, you can use two decade counters to drive 20 servos.
 */
void
servo_init( void )
{
  uint8_t			i;

  /* Configure the reset and clock lines */
  _4017_RESET_DDR |= _BV(_4017_RESET_PIN);
  _4017_CLOCK_DDR |= _BV(_4017_CLOCK_PIN);

  /* Reset the decade counter */
  sbi( _4017_RESET_PORT, _4017_RESET_PIN );

  /* Lower the regular servo line */
  cbi( _4017_CLOCK_PORT, _4017_CLOCK_PIN );

  /* Set all servos at their midpoints */
  _Pragma("loopbound min 10 max 10")
  for( i=0 ; i < _4017_NB_CHANNELS ; i++ )
    //    servo_widths[i] = SERVO_MIN;
    servo_widths[i] = (SERVO_MIN+SERVO_MAX)/2;
	
  /* Set servos to go off some long time from now */
  SERVO_OCR	= 32768ul;

  /*
   * Configure output compare to toggle the output bits.
   */
  TCCR1A |=  _BV(SERVO_COM0 );
	
  /* Clear the interrupt flags in case they are set */
  TIFR = _BV(SERVO_FLAG);

  /* Unassert the decade counter reset to start it running */
  cbi( _4017_RESET_PORT, _4017_RESET_PIN );

  /* Enable our output compare interrupts */
  TIMSK |= _BV(SERVO_ENABLE );
}


/* 
 *  Interrupt routine
 *  
 *  write the next pulse width to OCR register and 
 *  assert the servo signal. It will be cleared by
 *  the following compare match.
 */
SIGNAL( SIG_OUTPUT_COMPARE1A )
{
  static uint8_t servo = 0;
  uint16_t width;

  if (servo >= _4017_NB_CHANNELS) {
    sbi( _4017_RESET_PORT, _4017_RESET_PIN );
    servo = 0;
    // FIXME: 500 ns required by 4017 reset ???? why does it work without!
    // asm( "nop; nop; nop; nop;nop; nop; nop; nop;nop; nop; nop; nop;nop; nop; nop; nop;" );
    cbi( _4017_RESET_PORT, _4017_RESET_PIN );
  }

  width = servo_widths[servo];

  SERVO_OCR += width;

  TCCR1A |= _BV(SERVO_FORCE);

  servo++;
}

void servo_set_one(uint8_t servo, uint16_t value_us) {
  servo_widths[servo] = ChopServo(CLOCK*value_us);
}

void 
_Pragma("entrypoint") servo_transmit(void) {
  uint8_t servo;
  uart_transmit((uint8_t)0); uart_transmit((uint8_t)0);

  _Pragma("loopbound min 10 max 10")
  for(servo = 0; servo < _4017_NB_CHANNELS; servo++) {
    uart_transmit((uint8_t)(servo_widths[servo] >> 8));
    uart_transmit((uint8_t)(servo_widths[servo] & 0xff));
  }
  uart_transmit((uint8_t)'\n');
}


/*
 *
 * defines how servos react to radio control or autopilot channels
 *
 */

void servo_set(const pprz_t values[]) {
  ServoSet(values); /*Generated from airframe.xml */
}
/*
 * $Id: spi.c,v 1.2 2011-01-25 10:05:41 plazar Exp $
 *
 * Paparazzi mcu1 spi functions
 *  
 * Copyright (C) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

#include <inttypes.h>
#include <arch/io.h>
#include <arch/signal.h>
#include <arch/interrupt.h>

#include "spi.h"

#define IT_PORT PORTD
#define IT_DDR  DDRD
#define IT_PIN  7

#define SPI_DDR  DDRB
#define SPI_MOSI_PIN 3
#define SPI_MISO_PIN 4
#define SPI_SCK_PIN  5

struct inter_mcu_msg from_mega128;
struct inter_mcu_msg to_mega128;
volatile bool_t mega128_receive_valid = FALSE;
volatile bool_t spi_was_interrupted = FALSE;

static volatile uint8_t idx_buf = 0;
static volatile uint8_t xor_in, xor_out;

void spi_reset(void) {
  idx_buf = 0;
  xor_in = 0;
  xor_out = ((uint8_t*)&to_mega128)[idx_buf];
  SPDR = xor_out;
  mega128_receive_valid = FALSE;
}

void fbw_spi_init(void) {
  to_mega128.status = 0;
  to_mega128.nb_err = 0;

  /* set it pin output */
  //  IT_DDR |= _BV(IT_PIN);

  /* set MISO pin output */
  SPI_DDR |= _BV(SPI_MISO_PIN);
  /* enable SPI, slave, MSB first, sck idle low */
  SPCR = _BV(SPE);
  /* enable interrupt */
  SPCR |= _BV(SPIE);
}

SIGNAL(SIG_SPI) {
  static uint8_t tmp;
  
  idx_buf++;

  spi_was_interrupted = TRUE;

  if (idx_buf > FRAME_LENGTH)
    return;
  /* we have sent/received a complete frame */
  if (idx_buf == FRAME_LENGTH) {
    /* read checksum from receive register */
    tmp = SPDR;
    /* notify valid frame                  */
    if (tmp == xor_in)
      mega128_receive_valid = TRUE;
    else
      to_mega128.nb_err++;
    return;
  }

  /* we are sending/receiving payload       */
  if (idx_buf < FRAME_LENGTH - 1) {
    /* place new payload byte in send register */
    tmp = ((uint8_t*)&to_mega128)[idx_buf];
    SPDR = tmp;
    xor_out = xor_out ^ tmp;
  } 
  /* we are done sending the payload */
  else { // idx_buf == FRAME_LENGTH - 1
    /* place checksum in send register */
    SPDR = xor_out;
  }
  
  /* read the byte from receive register */
  tmp = SPDR;
  ((uint8_t*)&from_mega128)[idx_buf-1] = tmp;
  xor_in = xor_in ^ tmp;
}
/*
 * Paparazzi $Id: uart.c,v 1.4 2011-01-25 10:17:46 plazar Exp $
 *  
 * Copyright (C) 2003 Pascal Brisset, Antoine Drouin
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA. 
 *
 */

#include <arch/signal.h>
#include <arch/interrupt.h>
#include <arch/io.h>


#include "std.h"
#include "uart.h"

#define TX_BUF_SIZE      256
static uint8_t           tx_head; /* next free in buf */
static volatile uint8_t  tx_tail; /* next char to send */
static uint8_t           tx_buf[ TX_BUF_SIZE ];

/*
 * UART Baud rate generation settings:
 *
 * With 16.0 MHz clock,UBRR=25  => 38400 baud
 *
 */
void uart_init_tx( void ) {
#ifndef WCET_ANALYSIS
  /* Baudrate is 38.4k */
  UBRRH = 0; 
  UBRRL = 25; 
  /* single speed */ 
  UCSRA = 0; 
  /* Enable transmitter */ 
  UCSRB = _BV(TXEN); 
  /* Set frame format: 8data, 1stop bit */ 
  UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0); 
#endif
}

void uart_init_rx() {
#ifndef WCET_ANALYSIS
  /* Enable receiver               */ 
  UCSRB |= _BV(RXEN); 
  /* Enable uart receive interrupt */
  sbi( UCSRB, RXCIE ); 
#endif
}

void uart_transmit( unsigned char data ) {
#ifndef WCET_ANALYSIS
  if (UCSRB & _BV(TXCIE)) {
    /* we are waiting for the last char to be sent : buffering */
    if (tx_tail == tx_head + 1) { /* BUF_SIZE = 256 */
      /* Buffer is full (almost, but tx_head = tx_tail means "empty" */
      return;
    }
    tx_buf[tx_head] = data;
    tx_head++; /* BUF_SIZE = 256 */
  } else { /* Channel is free: just send */
    UDR = data;
    sbi(UCSRB, TXCIE);
  }
#endif
}

void uart_print_hex ( uint8_t c ) {
#ifndef WCET_ANALYSIS
  const uint8_t hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7', 
                            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  uint8_t high = (c & 0xF0)>>4;
  uint8_t low  = c & 0x0F;
  uart_transmit(hex[high]);
  uart_transmit(hex[low]);
#endif
} 

void uart_print_hex16 ( uint16_t c ) {
#ifndef WCET_ANALYSIS
  uint8_t high = (uint8_t)(c>>8);
  uint8_t low  = (uint8_t)(c);
  uart_print_hex(high);
  uart_print_hex(low);
#endif
}

void uart_print_string(const uint8_t* s) {
#ifndef WCET_ANALYSIS
  uint8_t i = 0;
  _Pragma("loopbound min 100 max 100")
  while (s[i]) {
    uart_transmit(s[i]);
    i++;
  }
#endif
}

SIGNAL(SIG_UART_TRANS) {
#ifndef WCET_ANALYSIS
  if (tx_head == tx_tail) {
    /* Nothing more to send */
    cbi(UCSRB, TXCIE); /* disable interrupt */
  } else {
    UDR = tx_buf[tx_tail];
    tx_tail++; /* warning tx_buf_len is 256 */
  }
#endif
}
