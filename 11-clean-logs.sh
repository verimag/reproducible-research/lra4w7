#!/bin/bash
#
# Pour tout sh, si le log correspondant n'existe pas -> efface le dir
# Virer les log douteux, qui n'ont pas de sh (?)
#
#
# appeler avec -co pour les benchs avec optim
if [ "$1" == "-co" ]; then
   benchresdir=expe-compteurs-CO
else
   benchresdir=expe-compteurs-O0
fi
echo "clean-logs in $benchresdir"

for zelog in $benchresdir/log/*.log; do
	bn=`basename $zelog .log`
	zesh="$benchresdir/sh/$bn.sh"	
	if [ ! -f $zesh ]; then
		echo "#SH MISSING BUT LOG EXISTS: $zesh"
	fi
done
