#!/bin/bash

# compare nos bench au tacle d'origine !
#
#
tacledir=taclebench-origin

# tous les rep qui contiennent des .c
# -> on regarde s'ils contiennent un .c avec un int main
nb_main_dirs=0
for dd in `find $tacledir/ -name "*.c"  -exec dirname {} \; | sort -u`; do
	#echo $dd
	maindir=`find $dd/ -name "*.c"  -exec grep -e "^int main.[^;]*$" {} \; | wc -l`
	if [ $maindir -gt 0 ]; then
		#echo "YES $dd is a main dir"
		((nb_main_dirs++))
		echo `basename $dd`
	fi
done | sort -u > allprgs.$$

benchresdir=expe-compteurs-O0
benchdir=$benchresdir/bench

## PAS LA BONNE METHODE !
#pl=`find $benchdir/ -name "*.c"`
#for x in $pl; do
#	d=$(dirname $x)
#	p=$(basename $d)
#	echo $p
#done | sort -u > allprgs.$$ 

pl=`ls -d $benchresdir/_cnt-*-nops | grep -e "_cnt-.*_cat" -o | sort -u`
for x in $pl; do
	p=$(echo $x | sed -e "s/_cnt-//" -e "s/_cat//")
	echo $p
done | sort -u > ourprgs.$$

nb_allprgs=`cat allprgs.$$ | wc -l`
nb_ourprgs=`cat ourprgs.$$ | wc -l`

for x in `comm allprgs.$$ ourprgs.$$ -23` ; do
	echo "NOT IN OUR BENCH: $x"
done

rm -f *.$$

echo "ALL TACLE: $nb_allprgs, RETAINED: $nb_ourprgs"
