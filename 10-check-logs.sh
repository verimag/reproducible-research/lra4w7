#!/bin/bash
#
# basique : vérifie qu'on a bien un .log pour chaque .sh
# WARNING : NE CONCERNE QUE LE O0-nops
#

# appeler avec -co
if [ "$1" == "-co" ]; then
	benchresdir=expe-compteurs-CO
else
	benchresdir=expe-compteurs-O0
fi
echo "check-logs in $benchresdir"


echo -n "" > sh.list
echo -n "" > log.list
echo -n "" > dir.list
echo -n "" > redir.list
for zesh in $benchresdir/sh/*.sh; do
	bn=`basename $zesh .sh`
	zebasesh=sh/$bn.sh
	echo $bn >> sh.list
	zelog="$benchresdir/log/$bn.log"	
	if [ -f $zelog ]; then
		echo $bn >> log.list
		reinit=`cat $zelog | grep "INIT STEP" | wc -l`
		#zebasedir=`cat $zelog | grep "working dir" | head -1 | grep "_cnt-[^ ]*" -o`
		zebasedir=$bn
		zedir="$benchresdir/$zebasedir"
		if [ -n "$zebasedir" -a -d "$zedir" ]; then
			echo $zedir >> dir.list
			if [ $reinit -gt 1 ]; then
				echo $zedir >> redir.list
				echo "REINIT: $zelog"
			fi
		else
			echo "#DIR MISSING: $zelog"
		fi
	else
		echo "#LOG MISSING: $zebasesh"
	fi 

done
nbsh=`cat sh.list | wc -l`
nblog=`cat log.list | wc -l`
nbdir=`cat dir.list | wc -l`
nbredir=`cat redir.list | wc -l`
echo "SH: $nbsh LOG: $nblog ($(expr $nbsh - $nblog) MISSING) DIR: $nbdir ($(expr $nbsh - $nbdir) MISSING $nbredir RE-INIT))"
