#!/bin/bash

# appeler avec -co pour les benchs avec optim
if [ "$1" == "-co" ]; then
   benchresdir=expe-compteurs-CO
	OPT=CO1
	OUTFILE=all_funcs_CO.txt
else
   benchresdir=expe-compteurs-O0
	OPT=O0
	OUTFILE=all_funcs_O0.txt
fi
echo "bench-dimension in $benchresdir"

benchdir=$benchresdir/bench

# erase outfile
echo -n "" > $OUTFILE

#
#
# "dimension" du test bench :
# - nombre de programmes retenus ds le tacle bench
#   on le retrouve via le nombre de _cnt.*_cat uniques
nb_prgs=`ls $benchresdir/sh/*-nops.sh | grep -e "_cnt-.*_cat" -o | sort -u | wc -l`

# - nombre de fonctions extraites (brutes)
#   = nbre de sh (sans domaine)
nb_extracted_funcs=`ls $benchresdir/sh/*-nops.sh | wc -l`

#
# Parmis ces fonctions extraites, on ne retient que celles
# qui donnent un wcet, essentiellement :
# - vire les recursives

nb_bug=0
nb_recursive=0
nb_otawa_fails=0
nb_otawa_timeout=0
nb_retained_funcs=0



function print_stats {
	echo "PRGS: $nb_prgs, FUNCS: $nb_extracted_funcs, RETAINED: $nb_retained_funcs"
	echo "  (REC: $nb_recursive, OWCET FAILS: $nb_otawa_fails, OWCET TIMEOUT: $nb_otawa_timeout, BUGS: $nb_bug)"
}

function check_owcet {
	# XXXXX.wcet may contain (e.g):
	# - "Value of objective function: 251"
	# - several lines of "spx_run: Lost feasibility ...  " + "Value of objective function: xxxxx"
	#   => returns a number
	# - "This problem is unbounded"
	#   => returns 0
	# - something else 
	#   => returns -1
	ofile=$1
	ores=`tail -n 1 $ofile`
	owcet=`echo $ores | grep -o "[0-9]*"`
	unbound=`echo $ores | grep -o "unbounded"`
	infea=`echo $ores | grep -o "infeasible"`
	if [ -n "$owcet" ]; then
		echo $owcet
	elif [ -n "$unbound" ]; then
		echo 0
	elif [ -n "$infea" ]; then
		echo -1
	else 
		echo -2
	fi
}

function check_sh {
	((nb_sh++))
	zesh=$1
	zebn=`basename $zesh .sh`
	zelog=$benchresdir/log/${zebn}.log
	if [ ! -f $zelog ]; then
		echo "BUG LOG MISSING: $zebn"
		((nb_bug++))
		return
	fi 
	reinit=`cat $zelog | grep "INIT STEP" | wc -l`
	if [ $reinit -gt 1 ]; then
		((nb_reinit++))
		echo "BUG RE INIT: $zebn"
		((nb_bug++))
		return
	fi
	zebasedir=$zebn
	zedir=$benchresdir/$zebasedir
	if [ ! -d $zedir ]; then
		echo "BUG DIR MISSING: $zebn"
		((nb_bug++))
		return
	fi 

### as soon as the dir exist, put the bench in the ref list
	prgname=`echo $zebn | cut -d'-' -f2`
	prgname=`basename $prgname _cat`
	funcname=`echo $zebn | cut -d'-' -f3`
	benchsrc=`(cd $benchdir; find . -name $prgname)`
	#remove leadind ./
	benchsrc=`echo ${benchsrc#./}`
	echo $prgname,$benchsrc,$funcname,$zebn >> $OUTFILE
###

	# ignore dir in case of recursive func !
	isrec=`cat $zelog | grep -e "Error:.*is recursive"`
	if [ -n "$isrec" ]; then
		echo "IGNORE (RECURSIVE): $zebn"
		((nb_recursive++))
		return
	fi
	# ignore dir when otawa fails (new capacity out of [1, 65535])
	otawa_bug=`cat $zelog | grep -e "ASSERT:.*Vector.h.*new capacity out.*"`
	if [ -n "$otawa_bug" ]; then
		echo "IGNORE (OTAWA FAILS): $zebn"
		((nb_otawa_fails++))
		return
	fi
	((nb_dir++))
	# check basic.wcet
	#if [ ! -f $zedir/basic.wcet ]; then
	if [ ! -s $zedir/basic.wcet ]; then
		# check otawa timeout 
		timeout=`cat $zelog | grep -o TIMEOUT`
		if [ -n "$timeout" ]; then
			echo "IGNORE (OTAWA TIMEOUT): $zebn"
			((nb_otawa_timeout++))
			return
		fi
		echo "BUG FILE BASIC.WCET MISSING: $zebn"
		((nb_bug++))
		return
	fi
	owcet=`check_owcet $zedir/basic.wcet`
	if [ $owcet -lt 0 ] ; then
		echo "BUG BASIC.OWCET EMPTY: $zebn"
		((nb_bug++))
		return
	fi
	if [ $owcet -eq 0 ] ; then
		echo "BUG UNBOUNDED BASIC OWCET: $zebn"
		((nb_bug++))
		return
	fi
	# prgname=`echo $zebn | cut -d'-' -f2`
	# prgname=`basename $prgname _cat`
	# funcname=`echo $zebn | cut -d'-' -f3`
	# benchsrc=`(cd $benchdir; find . -name $prgname)`
	# #remove leadind ./
	# benchsrc=`echo ${benchsrc#./}`
	# echo $prgname,$benchsrc,$funcname,$zebn >> $OUTFILE
	((nb_retained_funcs++))
}

echo -n > all_funcs
export LC_ALL="en_US.UTF-8"
# liste tous les sh $OPT-nops de base (poly, i.e. sans -domainxxx)
for lg in `/bin/ls $benchresdir/sh/*-$OPT-nops.sh | sort -d` ; do
    check_sh $lg
    #echo $lg
done

print_stats
