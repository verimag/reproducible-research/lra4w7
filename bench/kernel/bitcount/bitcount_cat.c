/*

  This program is part of the TACLeBench benchmark suite.
  Version V 1.x

  Name: bitcnt_1.c

  Author: Ratko Tomic

  Function: Test program for bit counting functions

  Source: http://www.snippets.org/.

  Changes: no major functional changes

  License: May be used, modified, and re-distributed freely.

*/

#include "bitops.h"

int bitcount_bit_count( long x )
{
  int n = 0;
  /*
  ** The loop will execute once for each bit of x set, this is in average
  ** twice as fast as the shift/test method.
  */
  if ( x ) {
    _Pragma( "loopbound min 3 max 8" )
    do {
      n++;
    } while ( 0 != ( x = x & ( x - 1 ) ) ) ;
  }
  return ( n );
}
/*

  This program is part of the TACLeBench benchmark suite.
  Version V 1.x

  Name: bitcnt_2.c

  Author: Bob Stout & Auke Reitsma

  Function: Test program for bit counting functions

  Source: http://www.snippets.org/

  Changes: no major functional changes

  License: May be used, modified, and re-distributed freely.

*/


#include "bitops.h"

int bitcount_bitcount( long i )
{
  i = ( ( i & 0xAAAAAAAAL ) >> 1 ) + ( i & 0x55555555L );
  i = ( ( i & 0xCCCCCCCCL ) >> 2 ) + ( i & 0x33333333L );
  i = ( ( i & 0xF0F0F0F0L ) >> 4 ) + ( i & 0x0F0F0F0FL );
  i = ( ( i & 0xFF00FF00L ) >> 8 ) + ( i & 0x00FF00FFL );
  i = ( ( i & 0xFFFF0000L ) >> 16 ) + ( i & 0x0000FFFFL );
  return ( int )i;
}
/*

  This program is part of the TACLeBench benchmark suite.
  Version V 1.x

  Name: bitcnt_3.c

  Author: Bob Stout & Auke Reitsma

  Function: Bit counting functions using table lookup

  Source: http://www.snippets.org/

  Changes: no major functional changes

  License: May be used, modified, and re-distributed freely.

*/


#include "bitops.h"

static char bitcount_bits[256];

/*
**  Count bits in each nybble
**
**  Note: Only the first 16 table entries are used, the rest could be
**        omitted.
*/

void bitcount_init3( void )
{
  int volatile i = 0;
  char bitcount_bits_tmp[256] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
  };
_Pragma( "loopbound min 256 max 256" )
  for ( i = 0; i < 256; i++ )
    bitcount_bits[i] = bitcount_bits_tmp[i];
}

int bitcount_ntbl_bitcount( long int x )
{
  return
    bitcount_bits[ ( int ) ( x & 0x0000000FUL ) ] +
    bitcount_bits[ ( int )( ( x & 0x000000F0UL ) >> 4 ) ] +
    bitcount_bits[ ( int )( ( x & 0x00000F00UL ) >> 8 ) ] +
    bitcount_bits[ ( int )( ( x & 0x0000F000UL ) >> 12 )] +
    bitcount_bits[ ( int )( ( x & 0x000F0000UL ) >> 16 )] +
    bitcount_bits[ ( int )( ( x & 0x00F00000UL ) >> 20 )] +
    bitcount_bits[ ( int )( ( x & 0x0F000000UL ) >> 24 )] +
    bitcount_bits[ ( int )( ( x & 0xF0000000UL ) >> 28 )];
}

/*
**  Count bits in each byte
**
**  by Bruce Wedding, works best on Watcom & Borland
*/

int bitcount_BW_btbl_bitcount( long int x )
{
  union {
    unsigned char ch[4];
    long y;
  } U;

  U.y = x;

  return bitcount_bits[ U.ch[0] ] + bitcount_bits[ U.ch[1] ] +
         bitcount_bits[ U.ch[3] ] + bitcount_bits[ U.ch[2] ];
}

/*
**  Count bits in each byte
**
**  by Auke Reitsma, works best on Microsoft, Symantec, and others
*/

int bitcount_AR_btbl_bitcount( long int x )
{
  unsigned char *ptr = ( unsigned char * ) & x ;
  int accu ;

  accu = bitcount_bits[ *ptr++ ];
  accu += bitcount_bits[ *ptr++ ];
  accu += bitcount_bits[ *ptr++ ];
  accu += bitcount_bits[ *ptr ];
  return accu;
}
/*

  This program is part of the TACLeBench benchmark suite.
  Version V 1.x

  Name: bitcnt_4.c

  Author: Bob Stout

  Function: Recursive bit counting functions using table lookup

  Source: http://www.snippets.org/

  Changes: no major functional changes

  License: May be used, modified, and re-distributed freely.

*/


#include "bitops.h"   /* from Snippets */

static char bitcount_bits[256];

/*
**  Count bits in each nybble
**
**  Note: Only the first 16 table entries are used, the rest could be
**        omitted.
*/

void bitcount_init4( void )
{
  int volatile i = 0;
  char bitcount_bits_tmp[256] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
  };
_Pragma( "loopbound min 256 max 256" )
  for ( i = 0; i < 256; i++ )
    bitcount_bits[i] = bitcount_bits_tmp[i];
}

int bitcount_ntbl_bitcnt( unsigned long x )
{
  int cnt = bitcount_bits[( int )( x & 0x0000000FL )];

  if ( 0L != ( x >>= 4 ) )
    cnt += bitcount_ntbl_bitcnt( x );

  return cnt;
}

/*
**  Count bits in each byte
*/

int bitcount_btbl_bitcnt( unsigned long x )
{
  int cnt = bitcount_bits[ ( ( char * ) & x )[0] & 0xFF ];

  if ( 0L != ( x >>= 8 ) )
    cnt += bitcount_btbl_bitcnt( x );
  return cnt;
}
/*

  This program is part of the TACLeBench benchmark suite.
  Version V 1.x

  Name: bitcount.c

  Author: Bob Stout & Auke Reitsma

  Function: test program for bit counting functions

  Source: www.snippest.com

  Changes: no major functional changes

  License: May be used, modified, and re-distributed freely.

*/


#include "bitops.h"

#define FUNCS  8

/*
   Global variables
*/

unsigned long bitcount_randseed;
int bitcount_res;
unsigned long bitcount_seed;
unsigned long bitcount_n;
unsigned int bitcount_iterations;

/*
   First declaration of the functions
*/
int bitcount_bit_shifter( long int x );
unsigned long bitcount_random( void );
void bitcount_main();
int bitcount_return();
void bitcount_init();
int main( void );


int bitcount_bit_shifter( long int x )
{
  int n;
  unsigned int i;

  _Pragma( "loopbound min 31 max 31" )
  for ( i = n = 0; x && ( i < ( sizeof( long ) * 8 ) ); ++i, x >>= 1 )
    n += ( int )( x & 1L );
  return n;
}

int bitcount_return()
{
  return (bitcount_n + (-1095) ) != 0;
}

void bitcount_init()
{
  bitcount_randseed = 1;
  bitcount_n = 0;
  bitcount_iterations = 10;

  bitcount_init3();
  bitcount_init4();
}

unsigned long bitcount_random( void )
{
  long x, hi, lo, t;

  /*
     Compute x[n + 1] = (7^5 * x[n]) mod (2^31 - 1).
     From "Random number generators: good ones are hard to find",
     Park and Miller, Communications of the ACM, vol. 31, no. 10,
     October 1988, p. 1195.
  */
  x = bitcount_randseed;
  hi = x / 127773;
  lo = x % 127773;
  t = 16807 * lo - 2836 * hi;
  if ( t <= 0 )
    t += 0x7fffffff;
  bitcount_randseed = t;
  return ( t );
}

void _Pragma( "entrypoint" ) bitcount_main()
{
  unsigned int i, j;
  _Pragma( "loopbound min 8 max 8" )
  for ( i = 0; i < FUNCS; i++ ) {
    _Pragma( "loopbound min 10 max 10" )
    for ( j = 0, bitcount_seed = bitcount_random(); j < bitcount_iterations;
          j++, bitcount_seed += 13 ) {
      // The original calls were done by function pointers
      switch ( i ) {
        case 0:
          bitcount_res = bitcount_bit_count( bitcount_seed );
          break;
        case 1:
          bitcount_res = bitcount_bitcount( bitcount_seed );
          break;
        case 2: {
            _Pragma( "marker call_ntbl" )
            bitcount_res = bitcount_ntbl_bitcnt( bitcount_seed );
            break;
          }
        case 3: {
            _Pragma( "marker call_btbl" )
            bitcount_res = bitcount_btbl_bitcnt( bitcount_seed );
            break;
          }
        case 4:
          bitcount_res = bitcount_ntbl_bitcount( bitcount_seed );
          break;
        case 5:
          bitcount_res = bitcount_BW_btbl_bitcount( bitcount_seed );
          break;
        case 6:
          bitcount_res = bitcount_AR_btbl_bitcount( bitcount_seed );
          break;
        case 7:
          bitcount_res = bitcount_bit_shifter( bitcount_seed );
          break;
        default:
          break;
      }
      bitcount_n += bitcount_res;
    }
  }
  _Pragma( "flowrestriction 1*ntbl_bitcount <= 8*call_ntbl" )
  _Pragma( "flowrestriction 1*btbl_bitcount <= 4*call_btbl" )
}

int main( void )
{
  bitcount_init();
  bitcount_main();

  return ( bitcount_return() );
}
