/*
  -----------------------------------------------------------------------
  Copyright (c) 2001 Dr Brian Gladman <brg@gladman.uk.net>, Worcester, UK

  TERMS

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

  This software is provided 'as is' with no guarantees of correctness or
  fitness for purpose.
  -----------------------------------------------------------------------

  FUNCTION

  The AES algorithm Rijndael implemented for block and key sizes of 128,
  bits (16 bytes) by Brian Gladman.

  This is an implementation of the AES encryption algorithm (Rijndael)
  designed by Joan Daemen and Vincent Rijmen.
*/

#include "aes.h"

#include "aestab.h"

#define four_tables(x,tab,vf,rf,c)  ( tab[0][bval(vf(x,0,c),rf(0,c))] ^ \
                                      tab[1][bval(vf(x,1,c),rf(1,c))] ^ \
                                      tab[2][bval(vf(x,2,c),rf(2,c))] ^ \
                                      tab[3][bval(vf(x,3,c),rf(3,c))] )

#define vf1(x,r,c)  (x)
#define rf1(r,c)    (r)
#define rf2(r,c)    ((r-c)&3)

#define ls_box(x,c)     four_tables(x,rijndael_enc_fl_tab,vf1,rf2,c)

#define inv_mcol(x)     four_tables(x,rijndael_enc_im_tab,vf1,rf1,0)

/*
  Subroutine to set the block size (if variable) in bytes, legal
  values being 16, 24 and 32.
*/

#define nc   (Ncol)

/*
  Initialise the key schedule from the user supplied key. The key
  length is now specified in bytes - 16, 24 or 32 as appropriate.
  This corresponds to bit lengths of 128, 192 and 256 bits, and
  to Nk values of 4, 6 and 8 respectively.
*/

#define mx(t,f) (*t++ = inv_mcol(*f),f++)
#define cp(t,f) *t++ = *f++

#define cpy(d,s)    do { cp(d,s); cp(d,s); cp(d,s); cp(d,s); } while (0)
#define mix(d,s)    do { mx(d,s); mx(d,s); mx(d,s); mx(d,s); } while (0)

aes_ret rijndael_enc_set_key( byte in_key[], const word n_bytes,
                              const enum aes_key f, struct aes *cx )
{
  word    *kf, *kt, rci;

  if ( ( n_bytes & 7 ) || n_bytes < 16 || n_bytes > 32 || ( !( f & 1 ) &&
       !( f & 2 ) ) )
    return ( n_bytes ? cx->mode &= ~0x03, aes_bad : ( aes_ret )( cx->Nkey << 2 ) );

  cx->mode = ( cx->mode & ~0x03 ) | ( ( byte )f & 0x03 );
  cx->Nkey = n_bytes >> 2;
  cx->Nrnd = Nr( cx->Nkey, ( word )nc );

  cx->e_key[0] = word_in( in_key     );
  cx->e_key[1] = word_in( in_key +  4 );
  cx->e_key[2] = word_in( in_key +  8 );
  cx->e_key[3] = word_in( in_key + 12 );

  kf = cx->e_key;
  kt = kf + nc * ( cx->Nrnd + 1 ) - cx->Nkey;
  rci = 0;

  switch ( cx->Nkey ) {
    case 4:
      _Pragma( "loopbound min 0 max 0" )
      do {
        kf[4] = kf[0] ^ ls_box( kf[3], 3 ) ^ rijndael_enc_rcon_tab[rci++];
        kf[5] = kf[1] ^ kf[4];
        kf[6] = kf[2] ^ kf[5];
        kf[7] = kf[3] ^ kf[6];
        kf += 4;
      } while ( kf < kt );
      break;

    case 6:
      cx->e_key[4] = word_in( in_key + 16 );
      cx->e_key[5] = word_in( in_key + 20 );
      _Pragma( "loopbound min 0 max 0" )
      do {
        kf[ 6] = kf[0] ^ ls_box( kf[5], 3 ) ^ rijndael_enc_rcon_tab[rci++];
        kf[ 7] = kf[1] ^ kf[ 6];
        kf[ 8] = kf[2] ^ kf[ 7];
        kf[ 9] = kf[3] ^ kf[ 8];
        kf[10] = kf[4] ^ kf[ 9];
        kf[11] = kf[5] ^ kf[10];
        kf += 6;
      } while ( kf < kt );
      break;

    case 8:
      cx->e_key[4] = word_in( in_key + 16 );
      cx->e_key[5] = word_in( in_key + 20 );
      cx->e_key[6] = word_in( in_key + 24 );
      cx->e_key[7] = word_in( in_key + 28 );
      _Pragma( "loopbound min 7 max 7" )
      do {
        kf[ 8] = kf[0] ^ ls_box( kf[7], 3 ) ^ rijndael_enc_rcon_tab[rci++];
        kf[ 9] = kf[1] ^ kf[ 8];
        kf[10] = kf[2] ^ kf[ 9];
        kf[11] = kf[3] ^ kf[10];
        kf[12] = kf[4] ^ ls_box( kf[11], 0 );
        kf[13] = kf[5] ^ kf[12];
        kf[14] = kf[6] ^ kf[13];
        kf[15] = kf[7] ^ kf[14];
        kf += 8;
      } while ( kf < kt );
      break;
  }

  if ( ( cx->mode & 3 ) != enc ) {
    word    i;

    kt = cx->d_key + nc * cx->Nrnd;
    kf = cx->e_key;

    cpy( kt, kf );
    kt -= 2 * nc;

    _Pragma( "loopbound min 0 max 0" )
    for ( i = 1; i < cx->Nrnd; ++i ) {
      mix( kt, kf );
      kt -= 2 * nc;
    }

    cpy( kt, kf );
  }

  return aes_good;
}

short rijndael_enc_encrypt( unsigned char  in_blk[], unsigned char  out_blk[],
                            const struct aes *cx )
{
  const unsigned long *kp = cx->e_key;
  if ( !( cx->mode & 1 ) )
    return 0;
  unsigned long  b0[4];
  b0[0] = *( unsigned long * )in_blk ^ kp[0];
  b0[1] = *( unsigned long * )( in_blk + 4 )^kp[1];
  b0[2] = *( unsigned long * )( in_blk + 8 )^kp[2];
  b0[3] = *( unsigned long * )( in_blk + 12 )^kp[3];
  kp += 4;
  unsigned long  b1[4];
  switch ( cx->Nrnd ) {
    case 14:
      b1[0] = kp[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = kp[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = kp[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = kp[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 4 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 4 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 4 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 4 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      kp += 8;
    case 12:
      b1[0] = kp[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = kp[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = kp[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = kp[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 4 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 4 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 4 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 4 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      kp += 8;
    case 10:
      b1[0] = kp[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = kp[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = kp[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = kp[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                        rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                        rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                        rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 4 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 4 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 4 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 4 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      b1[0] = ( kp + 8 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = ( kp + 8 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = ( kp + 8 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = ( kp + 8 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                                rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                                rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                                rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 12 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 12 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 12 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 12 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      b1[0] = ( kp + 16 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = ( kp + 16 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = ( kp + 16 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = ( kp + 16 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 20 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 20 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 20 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 20 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      b1[0] = ( kp + 24 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = ( kp + 24 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = ( kp + 24 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = ( kp + 24 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 28 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 28 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 28 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 28 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b1[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
      b1[0] = ( kp + 32 )[0] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[0] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[1] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[2] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[3] >> 24 ) )] );
      b1[1] = ( kp + 32 )[1] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[1] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[2] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[3] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[0] >> 24 ) )] );
      b1[2] = ( kp + 32 )[2] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[2] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[3] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[0] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[1] >> 24 ) )] );
      b1[3] = ( kp + 32 )[3] ^ ( rijndael_enc_ft_tab[0][( ( unsigned char )b0[3] )] ^
                                 rijndael_enc_ft_tab[1][( ( unsigned char )( b0[0] >> 8 ) )] ^
                                 rijndael_enc_ft_tab[2][( ( unsigned char )( b0[1] >> 16 ) )] ^
                                 rijndael_enc_ft_tab[3][( ( unsigned char )( b0[2] >> 24 ) )] );
      b0[0] = ( kp + 36 )[0] ^ ( rijndael_enc_fl_tab[0][( ( unsigned char )b1[0] )] ^
                                 rijndael_enc_fl_tab[1][( ( unsigned char )( b1[1] >> 8 ) )] ^
                                 rijndael_enc_fl_tab[2][( ( unsigned char )( b1[2] >> 16 ) )] ^
                                 rijndael_enc_fl_tab[3][( ( unsigned char )( b1[3] >> 24 ) )] );
      b0[1] = ( kp + 36 )[1] ^ ( rijndael_enc_fl_tab[0][( ( unsigned char )b1[1] )] ^
                                 rijndael_enc_fl_tab[1][( ( unsigned char )( b1[2] >> 8 ) )] ^
                                 rijndael_enc_fl_tab[2][( ( unsigned char )( b1[3] >> 16 ) )] ^
                                 rijndael_enc_fl_tab[3][( ( unsigned char )( b1[0] >> 24 ) )] );
      b0[2] = ( kp + 36 )[2] ^ ( rijndael_enc_fl_tab[0][( ( unsigned char )b1[2] )] ^
                                 rijndael_enc_fl_tab[1][( ( unsigned char )( b1[3] >> 8 ) )] ^
                                 rijndael_enc_fl_tab[2][( ( unsigned char )( b1[0] >> 16 ) )] ^
                                 rijndael_enc_fl_tab[3][( ( unsigned char )( b1[1] >> 24 ) )] );
      b0[3] = ( kp + 36 )[3] ^ ( rijndael_enc_fl_tab[0][( ( unsigned char )b1[3] )] ^
                                 rijndael_enc_fl_tab[1][( ( unsigned char )( b1[0] >> 8 ) )] ^
                                 rijndael_enc_fl_tab[2][( ( unsigned char )( b1[1] >> 16 ) )] ^
                                 rijndael_enc_fl_tab[3][( ( unsigned char )( b1[2] >> 24 ) )] );
  }
  *( unsigned long * )out_blk = ( b0[0] );
  *( unsigned long * )( out_blk + 4 ) = ( b0[1] );
  *( unsigned long * )( out_blk + 8 ) = ( b0[2] );
  *( unsigned long * )( out_blk + 12 ) = ( b0[3] );
  return aes_good;
}

unsigned char rijndael_enc_data[] = {
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','u','n','t','i','l','t','h','e','y','v','e','f','a',
  'd','e','d','B','u','t','t','r','u','s','t','m','e','i','n','2',
  '0','y','e','a','r','s','y','o','u','l','l','l','o','o','k','b',
  'a','c','k','a','t','p','h','o','t','o','s','o','f','y','o','u',
  'r','s','e','l','f','a','n','d','r','e','c','a','l','l','i','n',
  'a','w','a','y','y','o','u','c','a','n','t','g','r','a','s','p',
  'n','o','w','h','o','w','m','u','c','h','p','o','s','s','i','b',
  'i','l','i','t','y','l','a','y','b','e','f','o','r','e','y','o',
  'u','a','n','d','h','o','w','f','a','b','u','l','o','u','s','y',
  'o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o',
  'u','a','r','e','n','o','t','a','s','f','a','t','a','s','y','o',
  'u','i','m','a','g','i','n','e','D','o','n','t','w','o','r','r',
  'y','a','b','o','u','t','t','h','e','f','u','t','u','r','e','O',
  'r','w','o','r','r','y','b','u','t','k','n','o','w','t','h','a',
  't','K','u','r','t','V','o','n','n','e','g','u','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','K','u','r','t','V','o','n','n','e','g','u','t',
  's','C','o','m','m','e','n','c','e','m','e','n','t','A','d','d',
  'r','e','s','s','a','t','M','I','T','L','a','d','i','e','s','a',
  'n','d','g','e','n','t','l','e','m','e','n','o','f','t','h','e',
  'c','l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','s','u','n','s',
  'c','r','e','e','n','w','o','u','l','d','b','e','i','t','T','h',
  'e','l','o','n','g','t','e','r','m','b','e','n','e','f','i','t',
  's','o','f','s','u','n','s','c','r','e','e','n','h','a','v','e',
  'b','e','e','n','p','r','o','v','e','d','b','y','s','c','i','e',
  'n','t','i','s','t','s','w','h','e','r','e','a','s','t','h','e',
  'r','e','s','t','o','f','m','y','a','d','v','i','c','e','h','a',
  's','n','o','b','a','s','i','s','m','o','r','e','r','e','l','i',
  'a','b','l','e','t','h','a','n','m','y','o','w','n','m','e','a',
  'n','d','e','r','i','n','g','e','x','p','e','r','i','e','n','c',
  'e','I','w','i','l','l','d','i','s','p','e','n','s','e','t','h',
  'i','s','a','d','v','i','c','e','n','o','w','E','n','j','o','y',
  't','h','e','p','o','w','e','r','a','n','d','b','e','a','u','t',
  'y','o','f','y','o','u','r','y','o','u','t','h','O','h','n','e',
  'v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n','o',
  't','u','n','d','e','r','s','t','a','n','d','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','u','n','t','i','l','t','h','e','y',
  'v','e','f','a','d','e','d','B','u','t','t','r','u','s','t','m',
  'e','i','n','2','0','y','e','a','r','s','y','o','u','l','l','l',
  'o','o','k','b','a','c','k','a','t','p','h','o','t','o','s','o',
  'f','y','o','u','r','s','e','l','f','a','n','d','r','e','c','a',
  'l','l','i','n','a','w','a','y','y','o','u','c','a','n','t','g',
  'r','a','s','p','n','o','w','h','o','w','m','u','c','h','p','o',
  's','s','i','b','i','l','i','t','y','l','a','y','b','e','f','o',
  'r','e','y','o','u','a','n','d','h','o','w','f','a','b','u','l',
  'o','u','s','y','o','u','r','e','a','l','l','y','l','o','o','k',
  'e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a','t',
  'a','s','y','o','u','i','m','a','g','i','n','e','D','o','n','t',
  'w','o','r','r','y','a','b','o','u','t','t','h','e','f','u','t',
  'u','r','e','O','r','w','o','r','r','y','b','u','t','k','n','o',
  'w','t','h','a','t','K','u','r','t','V','o','n','n','e','g','u',
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','u','n','t','i','l','t','h','e','y','v','e','f','a',
  'd','e','d','B','u','t','t','r','u','s','t','m','e','i','n','2',
  '0','y','e','a','r','s','y','o','u','l','l','l','o','o','k','b',
  'a','c','k','a','t','p','h','o','t','o','s','o','f','y','o','u',
  'r','s','e','l','f','a','n','d','r','e','c','a','l','l','i','n',
  'a','w','a','y','y','o','u','c','a','n','t','g','r','a','s','p',
  'n','o','w','h','o','w','m','u','c','h','p','o','s','s','i','b',
  'i','l','i','t','y','l','a','y','b','e','f','o','r','e','y','o',
  'u','a','n','d','h','o','w','f','a','b','u','l','o','u','s','y',
  'o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o',
  'u','a','r','e','n','o','t','a','s','f','a','t','a','s','y','o',
  'u','i','m','a','g','i','n','e','D','o','n','t','w','o','r','r',
  'y','a','b','o','u','t','t','h','e','f','u','t','u','r','e','O',
  'r','w','o','r','r','y','b','u','t','k','n','o','w','t','h','a',
  't','K','u','r','t','V','o','n','n','e','g','u','t','s','C','o',
  'm','m','e','n','c','e','m','e','n','t','A','d','d','r','e','s',
  's','a','t','M','I','T','L','a','d','i','e','s','a','n','d','g',
  'e','n','t','l','e','m','e','n','o','f','t','h','e','c','l','a',
  's','s','o','f','9','7','W','e','a','r','s','u','n','s','c','r',
  'e','e','n','I','f','I','c','o','u','l','d','o','f','f','e','r',
  'y','o','u','o','n','l','y','o','n','e','t','i','p','f','o','r',
  't','h','e','f','u','t','u','r','e','s','u','n','s','c','r','e',
  'e','n','w','o','u','l','d','b','e','i','t','T','h','e','l','o',
  'n','g','t','e','r','m','b','e','n','e','f','i','t','s','o','f',
  's','u','n','s','c','r','e','e','n','h','a','v','e','b','e','e',
  'n','p','r','o','v','e','d','b','y','s','c','i','e','n','t','i',
  's','t','s','w','h','e','r','e','a','s','t','h','e','r','e','s',
  't','o','f','m','y','a','d','v','i','c','e','h','a','s','n','o',
  'b','a','s','i','s','m','o','r','e','r','e','l','i','a','b','l',
  'e','t','h','a','n','m','y','o','w','n','m','e','a','n','d','e',
  'r','i','n','g','e','x','p','e','r','i','e','n','c','e','I','w',
  'i','l','l','d','i','s','p','e','n','s','e','t','h','i','s','a',
  'd','v','i','c','e','n','o','w','E','n','j','o','y','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','O','h','n','e','v','e','r',
  'm','i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n',
  'd','e','r','s','t','a','n','d','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','u','n','t','i','l','t','h','e','y','v','e','f',
  'a','d','e','d','B','u','t','t','r','u','s','t','m','e','i','n',
  '2','0','y','e','a','r','s','y','o','u','l','l','l','o','o','k',
  'b','a','c','k','a','t','p','h','o','t','o','s','o','f','y','o',
  'u','r','s','e','l','f','a','n','d','r','e','c','a','l','l','i',
  'n','a','w','a','y','y','o','u','c','a','n','t','g','r','a','s',
  'p','n','o','w','h','o','w','m','u','c','h','p','o','s','s','i',
  'b','i','l','i','t','y','l','a','y','b','e','f','o','r','e','y',
  'o','u','a','n','d','h','o','w','f','a','b','u','l','o','u','s',
  'y','o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y',
  'o','u','a','r','e','n','o','t','a','s','f','a','t','a','s','y',
  'o','u','i','m','a','g','i','n','e','D','o','n','t','w','o','r',
  'r','y','a','b','o','u','t','t','h','e','f','u','t','u','r','e',
  'O','r','w','o','r','r','y','b','u','t','k','n','o','w','t','h',
  'a','t','K','u','r','t','V','o','n','n','e','g','u','t','s','C',
  'o','m','m','e','n','c','e','m','e','n','t','A','d','d','r','e',
  's','s','a','t','M','I','T','L','a','d','i','e','s','a','n','d',
  'g','e','n','t','l','e','m','e','n','o','f','t','h','e','c','l',
  'a','s','s','o','f','9','7','W','e','a','r','s','u','n','s','c',
  'r','e','e','n','I','f','I','c','o','u','l','d','o','f','f','e',
  'r','y','o','u','o','n','l','y','o','n','e','t','i','p','f','o',
  'r','t','h','e','f','u','t','u','r','e','s','u','n','s','c','r',
  'e','e','n','w','o','u','l','d','b','e','i','t','T','h','e','l',
  'o','n','g','t','e','r','m','b','e','n','e','f','i','t','s','o',
  'f','s','u','n','s','c','r','e','e','n','h','a','v','e','b','e',
  'e','n','p','r','o','v','e','d','b','y','s','c','i','e','n','t',
  'i','s','t','s','w','h','e','r','e','a','s','t','h','e','r','e',
  's','t','o','f','m','y','a','d','v','i','c','e','h','a','s','n',
  'o','b','a','s','i','s','m','o','r','e','r','e','l','i','a','b',
  'l','e','t','h','a','n','m','y','o','w','n','m','e','a','n','d',
  'e','r','i','n','g','e','x','p','e','r','i','e','n','c','e','I',
  'w','i','l','l','d','i','s','p','e','n','s','e','t','h','i','s',
  'a','d','v','i','c','e','n','o','w','E','n','j','o','y','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','O','h','n','e','v','e',
  'r','m','i','n','d','Y','o','u','w','i','l','l','n','o','t','u',
  'n','d','e','r','s','t','a','n','d','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','u','n','t','i','l','t','h','e','y','v','e',
  'f','a','d','e','d','B','u','t','t','r','u','s','t','m','e','i',
  'n','2','0','y','e','a','r','s','y','o','u','l','l','l','o','o',
  'k','b','a','c','k','a','t','p','h','o','t','o','s','o','f','y',
  'o','u','r','s','e','l','f','a','n','d','r','e','c','a','l','l',
  'i','n','a','w','a','y','y','o','u','c','a','n','t','g','r','a',
  's','p','n','o','w','h','o','w','m','u','c','h','p','o','s','s',
  'i','b','i','l','i','t','y','l','a','y','b','e','f','o','r','e',
  'y','o','u','a','n','d','h','o','w','f','a','b','u','l','o','u',
  's','y','o','u','r','e','a','l','l','y','l','o','o','k','e','d',
  'Y','o','u','a','r','e','n','o','t','a','s','f','a','t','a','s',
  'y','o','u','i','m','a','g','i','n','e','D','o','n','t','w','o',
  'r','r','y','a','b','o','u','t','t','h','e','f','u','t','u','r',
  'e','O','r','w','o','r','r','y','b','u','t','k','n','o','w','t',
  'h','a','t','t','s','C','o','m','m','e','n','c','e','m','e','n',
  't','A','d','d','r','e','s','s','a','t','M','I','T','L','a','d',
  'i','e','s','a','n','d','g','e','n','t','l','e','m','e','n','o',
  'f','t','h','e','c','l','a','s','s','o','f','9','7','W','e','a',
  'r','s','u','n','s','c','r','e','e','n','I','f','I','c','o','u',
  'l','d','o','f','f','e','r','y','o','u','o','n','l','y','o','n',
  'e','t','i','p','f','o','r','t','h','e','f','u','t','u','r','e',
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','u','n','t','i','l','t','h','e','y','v','e','f','a',
  'd','e','d','B','u','t','t','r','u','s','t','m','e','i','n','2',
  '0','y','e','a','r','s','y','o','u','l','l','l','o','o','k','b',
  'a','c','k','a','t','p','h','o','t','o','s','o','f','y','o','u',
  'r','s','e','l','f','a','n','d','r','e','c','a','l','l','i','n',
  'a','w','a','y','y','o','u','c','a','n','t','g','r','a','s','p',
  'n','o','w','h','o','w','m','u','c','h','p','o','s','s','i','b',
  'i','l','i','t','y','l','a','y','b','e','f','o','r','e','y','o',
  'u','a','n','d','h','o','w','f','a','b','u','l','o','u','s','y',
  'o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o',
  'u','a','r','e','n','o','t','a','s','f','a','t','a','s','y','o',
  'u','i','m','a','g','i','n','e','D','o','n','t','w','o','r','r',
  'y','a','b','o','u','t','t','h','e','f','u','t','u','r','e','O',
  'r','w','o','r','r','y','b','u','t','k','n','o','w','t','h','a',
  't','K','u','r','t','V','o','n','n','e','g','u','t','s','C','o',
  'm','m','e','n','c','e','m','e','n','t','A','d','d','r','e','s',
  's','a','t','M','I','T','L','a','d','i','e','s','a','n','d','g',
  'e','n','t','l','e','m','e','n','o','f','t','h','e','c','l','a',
  's','s','o','f','9','7','W','e','a','r','s','u','n','s','c','r',
  'e','e','n','I','f','I','c','o','u','l','d','o','f','f','e','r',
  'y','o','u','o','n','l','y','o','n','e','t','i','p','f','o','r',
  't','h','e','f','u','t','u','r','e','s','u','n','s','c','r','e',
  'e','n','w','o','u','l','d','b','e','i','t','T','h','e','l','o',
  'n','g','t','e','r','m','b','e','n','e','f','i','t','s','o','f',
  's','u','n','s','c','r','e','e','n','h','a','v','e','b','e','e',
  'n','p','r','o','v','e','d','b','y','s','c','i','e','n','t','i',
  's','t','s','w','h','e','r','e','a','s','t','h','e','r','e','s',
  't','o','f','m','y','a','d','v','i','c','e','h','a','s','n','o',
  'b','a','s','i','s','m','o','r','e','r','e','l','i','a','b','l',
  'e','t','h','a','n','m','y','o','w','n','m','e','a','n','d','e',
  'r','i','n','g','e','x','p','e','r','i','e','n','c','e','I','w',
  'i','l','l','d','i','s','p','e','n','s','e','t','h','i','s','a',
  'd','v','i','c','e','n','o','w','E','n','j','o','y','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','O','h','n','e','v','e','r',
  'm','i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n',
  'd','e','r','s','t','a','n','d','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','u','n','t','i','l','t','h','e','y','v','e','f',
  'a','d','e','d','B','u','t','t','r','u','s','t','m','e','i','n',
  '2','0','y','e','a','r','s','y','o','u','l','l','l','o','o','k',
  'b','a','c','k','a','t','p','h','o','t','o','s','o','f','y','o',
  'u','r','s','e','l','f','a','n','d','r','e','c','a','l','l','i',
  'n','a','w','a','y','y','o','u','c','a','n','t','g','r','a','s',
  'p','n','o','w','h','o','w','m','u','c','h','p','o','s','s','i',
  'b','i','l','i','t','y','l','a','y','b','e','f','o','r','e','y',
  'o','u','a','n','d','h','o','w','f','a','b','u','l','o','u','s',
  'y','o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y',
  'o','u','a','r','e','n','o','t','a','s','f','a','t','a','s','y',
  'o','u','i','m','a','g','i','n','e','D','o','n','t','w','o','r',
  'r','y','a','b','o','u','t','t','h','e','f','u','t','u','r','e',
  'O','r','w','o','r','r','y','b','u','t','k','n','o','w','t','h',
  'a','t','K','u','r','t','V','o','n','n','e','g','u','t','s','C',
  'o','m','m','e','n','c','e','m','e','n','t','A','d','d','r','e',
  's','s','a','t','M','I','T','L','a','d','i','e','s','a','n','d',
  'g','e','n','t','l','e','m','e','n','o','f','t','h','e','c','l',
  'a','s','s','o','f','9','7','W','e','a','r','s','u','n','s','c',
  'r','e','e','n','I','f','I','c','o','u','l','d','o','f','f','e',
  'r','y','o','u','o','n','l','y','o','n','e','t','i','p','f','o',
  'r','t','h','e','f','u','t','u','r','e','s','u','n','s','c','r',
  'e','e','n','w','o','u','l','d','b','e','i','t','T','h','e','l',
  'o','n','g','t','e','r','m','b','e','n','e','f','i','t','s','o',
  'f','s','u','n','s','c','r','e','e','n','h','a','v','e','b','e',
  'e','n','p','r','o','v','e','d','b','y','s','c','i','e','n','t',
  'i','s','t','s','w','h','e','r','e','a','s','t','h','e','r','e',
  's','t','o','f','m','y','a','d','v','i','c','e','h','a','s','n',
  'o','b','a','s','i','s','m','o','r','e','r','e','l','i','a','b',
  'l','e','t','h','a','n','m','y','o','w','n','m','e','a','n','d',
  'e','r','i','n','g','e','x','p','e','r','i','e','n','c','e','I',
  'w','i','l','l','d','i','s','p','e','n','s','e','t','h','i','s',
  'a','d','v','i','c','e','n','o','w','E','n','j','o','y','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','O','h','n','e','v','e',
  'r','m','i','n','d','Y','o','u','w','i','l','l','n','o','t','u',
  'n','d','e','r','s','t','a','n','d','t','h','e','p','o','w','e',
  'r','a','K','u','r','t','V','o','n','n','e','g','u','t','s','C',
  'o','m','m','e','n','c','e','m','e','n','t','A','d','d','r','e',
  's','s','a','t','M','I','T','L','a','d','i','e','s','a','n','d',
  'g','e','n','t','l','e','m','e','n','o','f','t','h','e','c','l',
  'a','s','s','o','f','9','7','W','e','a','r','s','u','n','s','c',
  'r','e','e','n','I','f','I','c','o','u','l','d','o','f','f','e',
  'r','y','o','u','o','n','l','y','o','n','e','t','i','p','f','o',
  'r','t','h','e','f','u','t','u','r','e','s','u','n','s','c','r',
  'e','e','n','w','o','u','l','d','b','e','i','t','T','h','e','l',
  'o','n','g','t','e','r','m','b','e','n','e','f','i','t','s','o',
  'f','s','u','n','s','c','r','e','e','n','h','a','v','e','b','e',
  'e','n','p','r','o','v','e','d','b','y','s','c','i','e','n','t',
  'i','s','t','s','w','h','e','r','e','a','s','t','h','e','r','e',
  's','t','o','f','m','y','a','d','v','i','c','e','h','a','s','n',
  'o','b','a','s','i','s','m','o','r','e','r','e','l','i','a','b',
  'l','e','t','h','a','n','m','y','o','w','n','m','e','a','n','d',
  'e','r','i','n','g','e','x','p','e','r','i','e','n','c','e','I',
  'w','i','l','l','d','i','s','p','e','n','s','e','t','h','i','s',
  'a','d','v','i','c','e','n','o','w','E','n','j','o','y','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','O','h','n','e','v','e',
  'r','m','i','n','d','Y','o','u','w','i','l','l','n','o','t','u',
  'n','d','e','r','s','t','a','n','d','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','u','n','t','i','l','t','h','e','y','v','e',
  'f','a','d','e','d','B','u','t','t','r','u','s','t','m','e','i',
  'n','2','0','y','e','a','r','s','y','o','u','l','l','l','o','o',
  'k','b','a','c','k','a','t','p','h','o','t','o','s','o','f','y',
  'o','u','r','s','e','l','f','a','n','d','r','e','c','a','l','l',
  'i','n','a','w','a','y','y','o','u','c','a','n','t','g','r','a',
  's','p','n','o','w','h','o','w','m','u','c','h','p','o','s','s',
  'i','b','i','l','i','t','y','l','a','y','b','e','f','o','r','e',
  'y','o','u','a','n','d','h','o','w','f','a','b','u','l','o','u',
  's','y','o','u','r','e','a','l','l','y','l','o','o','k','e','d',
  'Y','o','u','a','r','e','n','o','t','a','s','f','a','t','a','s',
  'y','o','u','i','m','a','g','i','n','e','D','o','n','t','w','o',
  'r','r','y','a','b','o','u','t','t','h','e','f','u','t','u','r',
  'e','O','r','w','o','r','r','y','b','u','t','k','n','o','w','t',
  'h','a','t','K','u','r','t','V','o','n','n','e','g','u','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','K',
  'u','r','t','V','o','n','n','e','g','u','t','s','C','o','m','m',
  'e','n','c','e','m','e','n','t','A','d','d','r','e','s','s','a',
  't','M','I','T','L','a','d','i','e','s','a','n','d','g','e','n',
  't','l','e','m','e','n','o','f','t','h','e','c','l','a','s','s',
  'o','f','9','7','W','e','a','r','s','u','n','s','c','r','e','e',
  'n','I','f','I','c','o','u','l','d','o','f','f','e','r','y','o',
  'u','o','n','l','y','o','n','e','t','i','p','f','o','r','t','h',
  'e','f','u','t','u','r','e','s','u','n','s','c','r','e','e','n',
  'w','o','u','l','d','b','e','i','t','T','h','e','l','o','n','g',
  't','e','r','m','b','e','n','e','f','i','t','s','o','f','s','u',
  'n','s','c','r','e','e','n','h','a','v','e','b','e','e','n','p',
  'r','o','v','e','d','b','y','s','c','i','e','n','t','i','s','t',
  's','w','h','e','r','e','a','s','t','h','e','r','e','s','t','o',
  'f','m','y','a','d','v','i','c','e','h','a','s','n','o','b','a',
  's','i','s','m','o','r','e','r','e','l','i','a','b','l','e','t',
  'h','a','n','m','y','o','w','n','m','e','a','n','d','e','r','i',
  'n','g','e','x','p','e','r','i','e','n','c','e','I','w','i','l',
  'l','d','i','s','p','e','n','s','e','t','h','i','s','a','d','v',
  'i','c','e','n','o','w','E','n','j','o','y','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','O','h','n','e','v','e','r','m','i',
  'n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d','e',
  'r','s','t','a','n','d','t','h','e','p','o','w','e','r','a','n',
  'd','b','e','a','u','t','y','o','f','y','o','u','r','y','o','u',
  't','h','u','n','t','i','l','t','h','e','y','v','e','f','a','d',
  'e','d','B','u','t','t','r','u','s','t','m','e','i','n','2','0',
  'y','e','a','r','s','y','o','u','l','l','l','o','o','k','b','a',
  'c','k','a','t','p','h','o','t','o','s','o','f','y','o','u','r',
  's','e','l','f','a','n','d','r','e','c','a','l','l','i','n','a',
  'w','a','y','y','o','u','c','a','n','t','g','r','a','s','p','n',
  'o','w','h','o','w','m','u','c','h','p','o','s','s','i','b','i',
  'l','i','t','y','l','a','y','b','e','f','o','r','e','y','o','u',
  'a','n','d','h','o','w','f','a','b','u','l','o','u','s','y','o',
  'u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u',
  'a','r','e','n','o','t','a','s','f','a','t','a','s','y','o','u',
  'i','m','a','g','i','n','e','D','o','n','t','w','o','r','r','y',
  'a','b','o','u','t','t','h','e','f','u','t','u','r','e','O','r',
  'w','o','r','r','y','b','u','t','k','n','o','w','t','h','a','t',
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','u','n','t','i','l','t','h','e','y','v','e','f','a',
  'd','e','d','B','u','t','t','r','u','s','t','m','e','i','n','2',
  '0','y','e','a','r','s','y','o','u','l','l','l','o','o','k','b',
  'a','c','k','a','t','p','h','o','t','o','s','o','f','y','o','u',
  'r','s','e','l','f','a','n','d','r','e','c','a','l','l','i','n',
  'a','w','a','y','y','o','u','c','a','n','t','g','r','a','s','p',
  'n','o','w','h','o','w','m','u','c','h','p','o','s','s','i','b',
  'i','l','i','t','y','l','a','y','b','e','f','o','r','e','y','o',
  'u','a','n','d','h','o','w','f','a','b','u','l','o','u','s','y',
  'o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o',
  'u','a','r','e','n','o','t','a','s','f','a','t','a','s','y','o',
  'u','i','m','a','g','i','n','e','D','o','n','t','w','o','r','r',
  'y','a','b','o','u','t','t','h','e','f','u','t','u','r','e','O',
  'r','w','o','r','r','y','b','u','t','k','n','o','w','t','h','a',
  't','t','s','C','o','m','m','e','n','c','e','m','e','n','t','A',
  'd','d','r','e','s','s','a','t','M','I','T','L','a','d','i','e',
  's','a','n','d','g','e','n','t','l','e','m','e','n','o','f','t',
  'h','e','c','l','a','s','s','o','f','9','7','W','e','a','r','s',
  'u','n','s','c','r','e','e','n','I','f','I','c','o','u','l','d',
  'o','f','f','e','r','y','o','u','o','n','l','y','o','n','e','t',
  'i','p','f','o','r','t','h','e','f','u','t','u','r','e','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','K',
  'u','r','t','V','o','n','n','e','g','u','t','s','C','o','m','m',
  'e','n','c','e','m','e','n','t','A','d','d','r','e','s','s','a',
  't','M','I','T','L','a','d','i','e','s','a','n','d','g','e','n',
  't','l','e','m','e','n','o','f','t','h','e','c','l','a','s','s',
  'o','f','9','7','W','e','a','r','s','u','n','s','c','r','e','e',
  'n','I','f','I','c','o','u','l','d','o','f','f','e','r','y','o',
  'u','o','n','l','y','o','n','e','t','i','p','f','o','r','t','h',
  'e','f','u','t','u','r','e','s','u','n','s','c','r','e','e','n',
  'w','o','u','l','d','b','e','i','t','T','h','e','l','o','n','g',
  't','e','r','m','b','e','n','e','f','i','t','s','o','f','s','u',
  'n','s','c','r','e','e','n','h','a','v','e','b','e','e','n','p',
  'r','o','v','e','d','b','y','s','c','i','e','n','t','i','s','t',
  's','w','h','e','r','e','a','s','t','h','e','r','e','s','t','o',
  'f','m','y','a','d','v','i','c','e','h','a','s','n','o','b','a',
  's','i','s','m','o','r','e','r','e','l','i','a','b','l','e','t',
  'h','a','n','m','y','o','w','n','m','e','a','n','d','e','r','i',
  'n','g','e','x','p','e','r','i','e','n','c','e','I','w','i','l',
  'l','d','i','s','p','e','n','s','e','t','h','i','s','a','d','v',
  'i','c','e','n','o','w','E','n','j','o','y','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','O','h','n','e','v','e','r','m','i',
  'n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d','e',
  'r','s','t','a','n','d','t','h','e','p','o','w','e','r','a','n',
  'd','b','e','a','u','t','y','o','f','y','o','u','r','y','o','u',
  't','h','u','n','t','i','l','t','h','e','y','v','e','f','a','d',
  'e','d','B','u','t','t','r','u','s','t','m','e','i','n','2','0',
  'y','e','a','r','s','y','o','u','l','l','l','o','o','k','b','a',
  'c','k','a','t','p','h','o','t','o','s','o','f','y','o','u','r',
  's','e','l','f','a','n','d','r','e','c','a','l','l','i','n','a',
  'w','a','y','y','o','u','c','a','n','t','g','r','a','s','p','n',
  'o','w','h','o','w','m','u','c','h','p','o','s','s','i','b','i',
  'l','i','t','y','l','a','y','b','e','f','o','r','e','y','o','u',
  'a','n','d','h','o','w','f','a','b','u','l','o','u','s','y','o',
  'u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u',
  'a','r','e','n','o','t','a','s','f','a','t','a','s','y','o','u',
  'i','m','a','g','i','n','e','D','o','n','t','w','o','r','r','y',
  'a','b','o','u','t','t','h','e','f','u','t','u','r','e','O','r',
  'w','o','r','r','y','b','u','t','k','n','o','w','t','h','a','t',
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'K','u','r','t','V','o','n','n','e','g','u','t','s','C','o','m',
  'm','e','n','c','e','m','e','n','t','A','d','d','r','e','s','s',
  'a','t','M','I','T','L','a','d','i','e','s','a','n','d','g','e',
  'n','t','l','e','m','e','n','o','f','t','h','e','c','l','a','s',
  's','o','f','9','7','W','e','a','r','s','u','n','s','c','r','e',
  'e','n','I','f','I','c','o','u','l','d','o','f','f','e','r','y',
  'o','u','o','n','l','y','o','n','e','t','i','p','f','o','r','t',
  'h','e','f','u','t','u','r','e','s','u','n','s','c','r','e','e',
  'n','w','o','u','l','d','b','e','i','t','T','h','e','l','o','n',
  'g','t','e','r','m','b','e','n','e','f','i','t','s','o','f','s',
  'u','n','s','c','r','e','e','n','h','a','v','e','b','e','e','n',
  'p','r','o','v','e','d','b','y','s','c','i','e','n','t','i','s',
  't','s','w','h','e','r','e','a','s','t','h','e','r','e','s','t',
  'o','f','m','y','a','d','v','i','c','e','h','a','s','n','o','b',
  'a','s','i','s','m','o','r','e','r','e','l','i','a','b','l','e',
  't','h','a','n','m','y','o','w','n','m','e','a','n','d','e','r',
  'i','n','g','e','x','p','e','r','i','e','n','c','e','I','w','i',
  'l','l','d','i','s','p','e','n','s','e','t','h','i','s','a','d',
  'v','i','c','e','n','o','w','E','n','j','o','y','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','O','h','n','e','v','e','r','m',
  'i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d',
  'e','r','s','t','a','n','d','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','u','n','t','i','l','t','h','e','y','v','e','f','a',
  'd','e','d','B','u','t','t','r','u','s','t','m','e','i','n','2',
  '0','y','e','a','r','s','y','o','u','l','l','l','o','o','k','b',
  'a','c','k','a','t','p','h','o','t','o','s','o','f','y','o','u',
  'r','s','e','l','f','a','n','d','r','e','c','a','l','l','i','n',
  'a','w','a','y','y','o','u','c','a','n','t','g','r','a','s','p',
  'n','o','w','h','o','w','m','u','c','h','p','o','s','s','i','b',
  'i','l','i','t','y','l','a','y','b','e','f','o','r','e','y','o',
  'u','a','n','d','h','o','w','f','a','b','u','l','o','u','s','y',
  'o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o',
  'u','a','r','e','n','o','t','a','s','f','a','t','a','s','y','o',
  'u','i','m','a','g','i','n','e','D','o','n','t','w','o','r','r',
  'y','a','b','o','u','t','t','h','e','f','u','t','u','r','e','O',
  'r','w','o','r','r','y','b','u','t','k','n','o','w','t','h','a',
  't','K','u','r','t','V','o','n','n','e','g','u','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','t',
  's','C','o','m','m','e','n','c','e','m','e','n','t','A','d','d',
  'r','e','s','s','a','t','M','I','T','L','a','d','i','e','s','a',
  'n','d','g','e','n','t','l','e','m','e','n','o','f','t','h','e',
  'c','l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','s',
  'u','n','s','c','r','e','e','n','w','o','u','l','d','b','e','i',
  't','T','h','e','l','o','n','g','t','e','r','m','b','e','n','e',
  'f','i','t','s','o','f','s','u','n','s','c','r','e','e','n','h',
  'a','v','e','b','e','e','n','p','r','o','v','e','d','b','y','s',
  'c','i','e','n','t','i','s','t','s','w','h','e','r','e','a','s',
  't','h','e','r','e','s','t','o','f','m','y','a','d','v','i','c',
  'e','h','a','s','n','o','b','a','s','i','s','m','o','r','e','r',
  'e','l','i','a','b','l','e','t','h','a','n','m','y','o','w','n',
  'm','e','a','n','d','e','r','i','n','g','e','x','p','e','r','i',
  'e','n','c','e','I','w','i','l','l','d','i','s','p','e','n','s',
  'e','t','h','i','s','a','d','v','i','c','e','n','o','w','E','n',
  'j','o','y','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','O',
  'h','n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l',
  'l','n','o','t','u','n','d','e','r','s','t','a','n','d','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','u','n','t','i','l','t',
  'h','e','y','v','e','f','a','d','e','d','B','u','t','t','r','u',
  's','t','m','e','i','n','2','0','y','e','a','r','s','y','o','u',
  'l','l','l','o','o','k','b','a','c','k','a','t','p','h','o','t',
  'o','s','o','f','y','o','u','r','s','e','l','f','a','n','d','r',
  'e','c','a','l','l','i','n','a','w','a','y','y','o','u','c','a',
  'n','t','g','r','a','s','p','n','o','w','h','o','w','m','u','c',
  'h','p','o','s','s','i','b','i','l','i','t','y','l','a','y','b',
  'e','f','o','r','e','y','o','u','a','n','d','h','o','w','f','a',
  'b','u','l','o','u','s','y','o','u','r','e','a','l','l','y','l',
  'o','o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s',
  'f','a','t','a','s','y','o','u','i','m','a','g','i','n','e','D',
  'o','n','t','w','o','r','r','y','a','b','o','u','t','t','h','e',
  'f','u','t','u','r','e','O','r','w','o','r','r','y','b','u','t',
  'k','n','o','w','t','h','a','t','n','d','b','e','a','u','t','y',
  'o','f','y','o','u','r','y','o','u','t','h','u','n','t','i','l',
  't','h','e','y','v','e','f','a','d','e','d','B','u','t','t','r',
  'u','s','t','m','e','i','n','2','0','y','e','a','r','s','y','o',
  'u','l','l','l','o','o','k','b','a','c','k','a','t','p','h','o',
  't','o','s','o','f','y','o','u','r','s','e','l','f','a','n','d',
  'r','e','c','a','l','l','i','n','a','w','a','y','y','o','u','c',
  'a','n','t','g','r','a','s','p','n','o','w','h','o','w','m','u',
  'c','h','p','o','s','s','i','b','i','l','i','t','y','l','a','y',
  'b','e','f','o','r','e','y','o','u','a','n','d','h','o','w','f',
  'a','b','u','l','o','u','s','y','o','u','r','e','a','l','l','y',
  'l','o','o','k','e','d','Y','o','u','a','r','e','n','o','t','a',
  's','f','a','t','a','s','y','o','u','i','m','a','g','i','n','e',
  'D','o','n','t','w','o','r','r','y','a','b','o','u','t','t','h',
  'e','f','u','t','u','r','e','O','r','w','o','r','r','y','b','u',
  't','k','n','o','w','t','h','a','t','s','u','n','s','c','r','e',
  'e','n','w','o','u','l','d','b','e','i','t','T','h','e','l','o',
  'n','g','t','e','r','m','b','e','n','e','f','i','t','s','o','f',
  's','u','n','s','c','r','e','e','n','h','a','v','e','b','e','e',
  'n','p','r','o','v','e','d','b','y','s','c','i','e','n','t','i',
  's','t','s','w','h','e','r','e','a','s','t','h','e','r','e','s',
  't','o','f','m','y','a','d','v','i','c','e','h','a','s','n','o',
  'b','a','s','i','s','m','o','r','e','r','e','l','i','a','b','l',
  'e','t','h','a','n','m','y','o','w','n','m','e','a','n','d','e',
  'r','i','n','g','e','x','p','e','r','i','e','n','c','e','I','w',
  'i','l','l','d','i','s','p','e','n','s','e','t','h','i','s','a',
  'd','v','i','c','e','n','o','w','E','n','j','o','y','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','O','h','n','e','v','e','r',
  'm','i','n','d','Y','o','u','w','i','l','l','n','o','t','u','n',
  'd','e','r','s','t','a','n','d','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','u','n','t','i','l','t','h','e','y','v','e','f',
  'a','d','e','d','B','u','t','t','r','u','s','t','m','e','i','n',
  '2','0','y','e','a','r','s','y','o','u','l','l','l','o','o','k',
  'b','a','c','k','a','t','p','h','o','t','o','s','o','f','y','o',
  'u','r','s','e','l','f','a','n','d','r','e','c','a','l','l','i',
  'n','a','w','a','y','y','o','u','c','a','n','t','g','r','a','s',
  'p','n','o','w','h','o','w','m','u','c','h','p','o','s','s','i',
  'b','i','l','i','t','y','l','a','y','b','e','f','o','r','e','y',
  'o','u','a','n','d','h','o','w','f','a','b','u','l','o','u','s',
  'y','o','u','r','e','a','l','l','y','l','o','o','k','e','d','Y',
  'o','u','a','r','e','n','o','t','a','s','f','a','t','a','s','y',
  'o','u','i','m','a','g','i','n','e','D','o','n','t','w','o','r',
  'r','y','a','b','o','u','t','t','h','e','f','u','t','u','r','e',
  'O','r','w','o','r','r','y','b','u','t','k','n','o','w','t','h',
  'a','t','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','u','n','t','i','l','t','h','e','y','v','e',
  'f','a','d','e','d','B','u','t','t','r','u','s','t','m','e','i',
  'n','2','0','y','e','a','r','s','y','o','u','l','l','l','o','o',
  'k','b','a','c','k','a','t','p','h','o','t','o','s','o','f','y',
  'o','u','r','s','e','l','f','a','n','d','r','e','c','a','l','l',
  'i','n','a','w','a','y','y','o','u','c','a','n','t','g','r','a',
  's','p','n','o','w','h','o','w','m','u','c','h','p','o','s','s',
  'i','b','i','l','i','t','y','l','a','y','b','e','f','o','r','e',
  'y','o','u','a','n','d','h','o','w','f','a','b','u','l','o','u',
  's','y','o','u','r','e','a','l','l','y','l','o','o','k','e','d',
  'Y','o','u','a','r','e','n','o','t','a','s','f','a','t','a','s',
  'y','o','u','i','m','a','g','i','n','e','D','o','n','t','w','o',
  'r','r','y','a','b','o','u','t','t','h','e','f','u','t','u','r',
  'e','O','r','w','o','r','r','y','b','u','t','k','n','o','w','t',
  'h','a','t','s','u','n','s','c','r','e','e','n','w','o','u','l',
  'd','b','e','i','t','T','h','e','l','o','n','g','t','e','r','m',
  'b','e','n','e','f','i','t','s','o','f','s','u','n','s','c','r',
  'e','e','n','h','a','v','e','b','e','e','n','p','r','o','v','e',
  'd','b','y','s','c','i','e','n','t','i','s','t','s','w','h','e',
  'r','e','a','s','t','h','e','r','e','s','t','o','f','m','y','a',
  'd','v','i','c','e','h','a','s','n','o','b','a','s','i','s','m',
  'o','r','e','r','e','l','i','a','b','l','e','t','h','a','n','m',
  'y','o','w','n','m','e','a','n','d','e','r','i','n','g','e','x',
  'p','e','r','i','e','n','c','e','I','w','i','l','l','d','i','s',
  'p','e','n','s','e','t','h','i','s','a','d','v','i','c','e','n',
  'o','w','E','n','j','o','y','t','h','e','p','o','w','e','r','a',
  'n','d','b','e','a','u','t','y','o','f','y','o','u','r','y','o',
  'u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y','o',
  'u','w','i','l','l','n','o','t','u','n','d','e','r','s','t','a',
  'n','d','t','h','e','p','o','w','e','r','a','n','d','b','e','a',
  'u','t','y','o','f','y','o','u','r','y','o','u','t','h','u','n',
  't','i','l','t','h','e','y','v','e','f','a','d','e','d','B','u',
  't','t','r','u','s','t','m','e','i','n','2','0','y','e','a','r',
  's','y','o','u','l','l','l','o','o','k','b','a','c','k','a','t',
  'p','h','o','t','o','s','o','f','y','o','u','r','s','e','l','f',
  'a','n','d','r','e','c','a','l','l','i','n','a','w','a','y','y',
  'o','u','c','a','n','t','g','r','a','s','p','n','o','w','h','o',
  'w','m','u','c','h','p','o','s','s','i','b','i','l','i','t','y',
  'l','a','y','b','e','f','o','r','e','y','o','u','a','n','d','h',
  'o','w','f','a','b','u','l','o','u','s','y','o','u','r','e','a',
  'l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e','n',
  'o','t','a','s','f','a','t','a','s','y','o','u','i','m','a','g',
  'i','n','e','D','o','n','t','w','o','r','r','y','a','b','o','u',
  't','t','h','e','f','u','t','u','r','e','O','r','w','o','r','r',
  'y','b','u','t','k','n','o','w','t','h','a','t','b','u','t','k',
  'n','o','w','t','h','a','t','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','K','u','r','t','V','o','n','n','e','g','u','t',
  's','C','o','m','m','e','n','c','e','m','e','n','t','A','d','d',
  'r','e','s','s','a','t','M','I','T','L','a','d','i','e','s','a',
  'n','d','g','e','n','t','l','e','m','e','n','o','f','t','h','e',
  'c','l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','s','u','n','s',
  'c','r','e','e','n','w','o','u','l','d','b','e','i','t','T','h',
  'e','l','o','n','g','t','e','r','m','b','e','n','e','f','i','t',
  's','o','f','s','u','n','s','c','r','e','e','n','h','a','v','e',
  'b','e','e','n','p','r','o','v','e','d','b','y','s','c','i','e',
  'n','t','i','s','t','s','w','h','e','r','e','a','s','t','h','e',
  'r','e','s','t','o','f','m','y','a','d','v','i','c','e','h','a',
  's','n','o','b','a','s','i','s','m','o','r','e','r','e','l','i',
  'a','b','l','e','t','h','a','n','m','y','o','w','n','m','e','a',
  'n','d','e','r','i','n','g','e','x','p','e','r','i','e','n','c',
  'e','I','w','i','l','l','d','i','s','p','e','n','s','e','t','h',
  'i','s','a','d','v','i','c','e','n','o','w','E','n','j','o','y',
  't','h','e','p','o','w','e','r','a','n','d','b','e','a','u','t',
  'y','o','f','y','o','u','r','y','o','u','t','h','O','h','n','e',
  'v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n','o',
  't','u','n','d','e','r','s','t','a','n','d','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','u','n','t','i','l','t','h','e','y',
  'v','e','f','a','d','e','d','B','u','t','t','r','u','s','t','m',
  'e','i','n','2','0','y','e','a','r','s','y','o','u','l','l','l',
  'o','o','k','b','a','c','k','a','t','p','h','o','t','o','s','o',
  'f','y','o','u','r','s','e','l','f','a','n','d','r','e','c','a',
  'l','l','i','n','a','w','a','y','y','o','u','c','a','n','t','g',
  'r','a','s','p','n','o','w','h','o','w','m','u','c','h','p','o',
  's','s','i','b','i','l','i','t','y','l','a','y','b','e','f','o',
  'r','e','y','o','u','a','n','d','h','o','w','f','a','b','u','l',
  'o','u','s','y','o','u','r','e','a','l','l','y','l','o','o','k',
  'e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a','t',
  'a','s','y','o','u','i','m','a','g','i','n','e','D','o','n','t',
  'w','o','r','r','y','a','b','o','u','t','t','h','e','f','u','t',
  'u','r','e','O','r','w','o','r','r','y','b','u','t','k','n','o',
  'w','t','h','a','t','K','u','r','t','V','o','n','n','e','g','u',
  't','s','C','o','m','m','e','n','c','e','m','e','n','t','A','d',
  'd','r','e','s','s','a','t','M','I','T','L','a','d','i','e','s',
  'a','n','d','g','e','n','t','l','e','m','e','n','o','f','t','h',
  'e','c','l','a','s','s','o','f','9','7','W','e','a','r','s','u',
  'n','s','c','r','e','e','n','I','f','I','c','o','u','l','d','o',
  'f','f','e','r','y','o','u','o','n','l','y','o','n','e','t','i',
  'p','f','o','r','t','h','e','f','u','t','u','r','e','s','u','n',
  's','c','r','e','e','n','w','o','u','l','d','b','e','i','t','T',
  'h','e','l','o','n','g','t','e','r','m','b','e','n','e','f','i',
  't','s','o','f','s','u','n','s','c','r','e','e','n','h','a','v',
  'e','b','e','e','n','p','r','o','v','e','d','b','y','s','c','i',
  'e','n','t','i','s','t','s','w','h','e','r','e','a','s','t','h',
  'e','r','e','s','t','o','f','m','y','a','d','v','i','c','e','h',
  'a','s','n','o','b','a','s','i','s','m','o','r','e','r','e','l',
  'i','a','b','l','e','t','h','a','n','m','y','o','w','n','m','e',
  'a','n','d','e','r','i','n','g','e','x','p','e','r','i','e','n',
  'c','e','I','w','i','l','l','d','i','s','p','e','n','s','e','t',
  'h','i','s','a','d','v','i','c','e','n','o','w','E','n','j','o',
  'y','t','h','e','p','o','w','e','r','a','n','d','b','e','a','u',
  't','y','o','f','y','o','u','r','y','o','u','t','h','O','h','n',
  'e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n',
  'o','t','u','n','d','e','r','s','t','a','n','d','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','u','n','t','i','l','t','h','e',
  'y','v','e','f','a','d','e','d','B','u','t','t','r','u','s','t',
  'm','e','i','n','2','0','y','e','a','r','s','y','o','u','l','l',
  'l','o','o','k','b','a','c','k','a','t','p','h','o','t','o','s',
  'o','f','y','o','u','r','s','e','l','f','a','n','d','r','e','c',
  'a','l','l','i','n','a','w','a','y','y','o','u','c','a','n','t',
  'g','r','a','s','p','n','o','w','h','o','w','m','u','c','h','p',
  'o','s','s','i','b','i','l','i','t','y','l','a','y','b','e','f',
  'o','r','e','y','o','u','a','n','d','h','o','w','f','a','b','u',
  'l','o','u','s','y','o','u','r','e','a','l','l','y','l','o','o',
  'k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a',
  't','a','s','y','o','u','i','m','a','g','i','n','e','D','o','n',
  't','w','o','r','r','y','a','b','o','u','t','t','h','e','f','u',
  't','u','r','e','O','r','w','o','r','r','y','b','u','t','k','n',
  'o','w','t','h','a','t','K','u','r','t','V','o','n','n','e','g',
  'u','t','s','C','o','m','m','e','n','c','e','m','e','n','t','A',
  'd','d','r','e','s','s','a','t','M','I','T','L','a','d','i','e',
  's','a','n','d','g','e','n','t','l','e','m','e','n','o','f','t',
  'h','e','c','l','a','s','s','o','f','9','7','W','e','a','r','s',
  'u','n','s','c','r','e','e','n','I','f','I','c','o','u','l','d',
  'o','f','f','e','r','y','o','u','o','n','l','y','o','n','e','t',
  'i','p','f','o','r','t','h','e','f','u','t','u','r','e','s','u',
  'n','s','c','r','e','e','n','w','o','u','l','d','b','e','i','t',
  'T','h','e','l','o','n','g','t','e','r','m','b','e','n','e','f',
  'i','t','s','o','f','s','u','n','s','c','r','e','e','n','h','a',
  'v','e','b','e','e','n','p','r','o','v','e','d','b','y','s','c',
  'i','e','n','t','i','s','t','s','w','h','e','r','e','a','s','t',
  'h','e','r','e','s','t','o','f','m','y','a','d','v','i','c','e',
  'h','a','s','n','o','b','a','s','i','s','m','o','r','e','r','e',
  'l','i','a','b','l','e','t','h','a','n','m','y','o','w','n','m',
  'e','a','n','d','e','r','i','n','g','e','x','p','e','r','i','e',
  'n','c','e','I','w','i','l','l','d','i','s','p','e','n','s','e',
  't','h','i','s','a','d','v','i','c','e','n','o','w','E','n','j',
  'o','y','t','h','e','p','o','w','e','r','a','n','d','b','e','a',
  'u','t','y','o','f','y','o','u','r','y','o','u','t','h','O','h',
  'n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l',
  'n','o','t','u','n','d','e','r','s','t','a','n','d','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','u','n','t','i','l','t','h',
  'e','y','v','e','f','a','d','e','d','B','u','t','t','r','u','s',
  't','m','e','i','n','2','0','y','e','a','r','s','y','o','u','l',
  'l','l','o','o','k','b','a','c','k','a','t','p','h','o','t','o',
  's','o','f','y','o','u','r','s','e','l','f','a','n','d','r','e',
  'c','a','l','l','i','n','a','w','a','y','y','o','u','c','a','n',
  't','g','r','a','s','p','n','o','w','h','o','w','m','u','c','h',
  'p','o','s','s','i','b','i','l','i','t','y','l','a','y','b','e',
  'f','o','r','e','y','o','u','a','n','d','h','o','w','f','a','b',
  'u','l','o','u','s','y','o','u','r','e','a','l','l','y','l','o',
  'o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f',
  'a','t','a','s','y','o','u','i','m','a','g','i','n','e','D','o',
  'n','t','w','o','r','r','y','a','b','o','u','t','t','h','e','f',
  'u','t','u','r','e','O','r','w','o','r','r','y','b','u','t','k',
  'n','o','w','t','h','a','t','s','u','n','s','c','r','e','e','n',
  'w','o','u','l','d','b','e','i','t','T','h','e','l','o','n','g',
  't','e','r','m','b','e','n','e','f','i','t','s','o','f','s','u',
  'n','s','c','r','e','e','n','h','a','v','e','b','e','e','n','p',
  'r','o','v','e','d','b','y','s','c','i','e','n','t','i','s','t',
  's','w','h','e','r','e','a','s','t','h','e','r','e','s','t','o',
  'f','m','y','a','d','v','i','c','e','h','a','s','n','o','b','a',
  's','i','s','m','o','r','e','r','e','l','i','a','b','l','e','t',
  'h','a','n','m','y','o','w','n','m','e','a','n','d','e','r','i',
  'n','g','e','x','p','e','r','i','e','n','c','e','I','w','i','l',
  'l','d','i','s','p','e','n','s','e','t','h','i','s','a','d','v',
  'i','c','e','n','o','w','E','n','j','o','y','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','O','h','n','e','v','e','r','m','i',
  'n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d','e',
  'r','s','t','a','n','d','t','h','e','p','o','w','e','r','a','n',
  'd','b','e','a','u','t','y','o','f','y','o','u','r','y','o','u',
  't','h','u','n','t','i','l','t','h','e','y','v','e','f','a','d',
  'e','d','B','u','t','t','r','u','s','t','m','e','i','n','2','0',
  'y','e','a','r','s','y','o','u','l','l','l','o','o','k','b','a',
  'c','k','a','t','p','h','o','t','o','s','o','f','y','o','u','r',
  's','e','l','f','a','n','d','r','e','c','a','l','l','i','n','a',
  'w','a','y','y','o','u','c','a','n','t','g','r','a','s','p','n',
  'o','w','h','o','w','m','u','c','h','p','o','s','s','i','b','i',
  'l','i','t','y','l','a','y','b','e','f','o','r','e','y','o','u',
  'a','n','d','h','o','w','f','a','b','u','l','o','u','s','y','o',
  'u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u',
  'a','r','e','n','o','t','a','s','f','a','t','a','s','y','o','u',
  'i','m','a','g','i','n','e','D','o','n','t','w','o','r','r','y',
  'a','b','o','u','t','t','h','e','f','u','t','u','r','e','O','r',
  'w','o','r','r','y','b','u','t','k','n','o','w','t','h','a','t',
  '\n', 'l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','s',
  'u','n','s','c','r','e','e','n','w','o','u','l','d','b','e','i',
  't','T','h','e','l','o','n','g','t','e','r','m','b','e','n','e',
  'f','i','t','s','o','f','s','u','n','s','c','r','e','e','n','h',
  'a','v','e','b','e','e','n','p','r','o','v','e','d','b','y','s',
  'c','i','e','n','t','i','s','t','s','w','h','e','r','e','a','s',
  't','h','e','r','e','s','t','o','f','m','y','a','d','v','i','c',
  'e','h','a','s','n','o','b','a','s','i','s','m','o','r','e','r',
  'e','l','i','a','b','l','e','t','h','a','n','m','y','o','w','n',
  'm','e','a','n','d','e','r','i','n','g','e','x','p','e','r','i',
  'e','n','c','e','I','w','i','l','l','d','i','s','p','e','n','s',
  'e','t','h','i','s','a','d','v','i','c','e','n','o','w','E','n',
  'j','o','y','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','O',
  'h','n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l',
  'l','n','o','t','u','n','d','e','r','s','t','a','n','d','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','u','n','t','i','l','t',
  'h','e','y','v','e','f','a','d','e','d','B','u','t','t','r','u',
  's','t','m','e','i','n','2','0','y','e','a','r','s','y','o','u',
  'l','l','l','o','o','k','b','a','c','k','a','t','p','h','o','t',
  'o','s','o','f','y','o','u','r','s','e','l','f','a','n','d','r',
  'e','c','a','l','l','i','n','a','w','a','y','y','o','u','c','a',
  'n','t','g','r','a','s','p','n','o','w','h','o','w','m','u','c',
  'h','p','o','s','s','i','b','i','l','i','t','y','l','a','y','b',
  'e','f','o','r','e','y','o','u','a','n','d','h','o','w','f','a',
  'b','u','l','o','u','s','y','o','u','r','e','a','l','l','y','l',
  'o','o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s',
  'f','a','t','a','s','y','o','u','i','m','a','g','i','n','e','D',
  'o','n','t','w','o','r','r','y','a','b','o','u','t','t','h','e',
  'f','u','t','u','r','e','O','r','w','o','r','r','y','b','u','t',
  'k','n','o','w','t','h','a','t','\n', 'l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','K','u','r','t','V','o','n','n','e','g','u','t',
  's','C','o','m','m','e','n','c','e','m','e','n','t','A','d','d',
  'r','e','s','s','a','t','M','I','T','L','a','d','i','e','s','a',
  'n','d','g','e','n','t','l','e','m','e','n','o','f','t','h','e',
  'c','l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','s','u','n','s',
  'c','r','e','e','n','w','o','u','l','d','b','e','i','t','T','h',
  'e','l','o','n','g','t','e','r','m','b','e','n','e','f','i','t',
  's','o','f','s','u','n','s','c','r','e','e','n','h','a','v','e',
  'b','e','e','n','p','r','o','v','e','d','b','y','s','c','i','e',
  'n','t','i','s','t','s','w','h','e','r','e','a','s','t','h','e',
  'r','e','s','t','o','f','m','y','a','d','v','i','c','e','h','a',
  's','n','o','b','a','s','i','s','m','o','r','e','r','e','l','i',
  'a','b','l','e','t','h','a','n','m','y','o','w','n','m','e','a',
  'n','d','e','r','i','n','g','e','x','p','e','r','i','e','n','c',
  'e','I','w','i','l','l','d','i','s','p','e','n','s','e','t','h',
  'i','s','a','d','v','i','c','e','n','o','w','E','n','j','o','y',
  't','h','e','p','o','w','e','r','a','n','d','b','e','a','u','t',
  'y','o','f','y','o','u','r','y','o','u','t','h','O','h','n','e',
  'v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n','o',
  't','u','n','d','e','r','s','t','a','n','d','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','u','n','t','i','l','t','h','e','y',
  'v','e','f','a','d','e','d','B','u','t','t','r','u','s','t','m',
  'e','i','n','2','0','y','e','a','r','s','y','o','u','l','l','l',
  'o','o','k','b','a','c','k','a','t','p','h','o','t','o','s','o',
  'f','y','o','u','r','s','e','l','f','a','n','d','r','e','c','a',
  'l','l','i','n','a','w','a','y','y','o','u','c','a','n','t','g',
  'r','a','s','p','n','o','w','h','o','w','m','u','c','h','p','o',
  's','s','i','b','i','l','i','t','y','l','a','y','b','e','f','o',
  'r','e','y','o','u','a','n','d','h','o','w','f','a','b','u','l',
  'o','u','s','y','o','u','r','e','a','l','l','y','l','o','o','k',
  'e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a','t',
  'a','s','y','o','u','i','m','a','g','i','n','e','D','o','n','t',
  'w','o','r','r','y','a','b','o','u','t','t','h','e','f','u','t',
  'u','r','e','O','r','w','o','r','r','y','b','u','t','k','n','o',
  'w','t','h','a','t','K','u','r','t','V','o','n','n','e','g','u',
  't','s','C','o','m','m','e','n','c','e','m','e','n','t','A','d',
  'd','r','e','s','s','a','t','M','I','T','L','a','d','i','e','s',
  'a','n','d','g','e','n','t','l','e','m','e','n','o','f','t','h',
  'e','c','l','a','s','s','o','f','9','7','W','e','a','r','s','u',
  'n','s','c','r','e','e','n','I','f','I','c','o','u','l','d','o',
  'f','f','e','r','y','o','u','o','n','l','y','o','n','e','t','i',
  'p','f','o','r','t','h','e','f','u','t','u','r','e','s','u','n',
  's','c','r','e','e','n','w','o','u','l','d','b','e','i','t','T',
  'h','e','l','o','n','g','t','e','r','m','b','e','n','e','f','i',
  't','s','o','f','s','u','n','s','c','r','e','e','n','h','a','v',
  'e','b','e','e','n','p','r','o','v','e','d','b','y','s','c','i',
  'e','n','t','i','s','t','s','w','h','e','r','e','a','s','t','h',
  'e','r','e','s','t','o','f','m','y','a','d','v','i','c','e','h',
  'a','s','n','o','b','a','s','i','s','m','o','r','e','r','e','l',
  'i','a','b','l','e','t','h','a','n','m','y','o','w','n','m','e',
  'a','n','d','e','r','i','n','g','e','x','p','e','r','i','e','n',
  'c','e','I','w','i','l','l','d','i','s','p','e','n','s','e','t',
  'h','i','s','a','d','v','i','c','e','n','o','w','E','n','j','o',
  'y','t','h','e','p','o','w','e','r','a','n','d','b','e','a','u',
  't','y','o','f','y','o','u','r','y','o','u','t','h','O','h','n',
  'e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n',
  'o','t','u','n','d','e','r','s','t','a','n','d','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','u','n','t','i','l','t','h','e',
  'y','v','e','f','a','d','e','d','B','u','t','t','r','u','s','t',
  'm','e','i','n','2','0','y','e','a','r','s','y','o','u','l','l',
  'l','o','o','k','b','a','c','k','a','t','p','h','o','t','o','s',
  'o','f','y','o','u','r','s','e','l','f','a','n','d','r','e','c',
  'a','l','l','i','n','a','w','a','y','y','o','u','c','a','n','t',
  'g','r','a','s','p','n','o','w','h','o','w','m','u','c','h','p',
  'o','s','s','i','b','i','l','i','t','y','l','a','y','b','e','f',
  'o','r','e','y','o','u','a','n','d','h','o','w','f','a','b','u',
  'l','o','u','s','y','o','u','r','e','a','l','l','y','l','o','o',
  'k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a',
  't','a','s','y','o','u','i','m','a','g','i','n','e','D','o','n',
  't','w','o','r','r','y','a','b','o','u','t','t','h','e','f','u',
  't','u','r','e','O','r','w','o','r','r','y','b','u','t','k','n',
  'o','w','t','h','a','t','K','u','r','t','V','o','n','n','e','g',
  'u','t','s','C','o','m','m','e','n','c','e','m','e','n','t','A',
  'd','d','r','e','s','s','a','t','M','I','T','L','a','d','i','e',
  's','a','n','d','g','e','n','t','l','e','m','e','n','o','f','t',
  'h','e','c','l','a','s','s','o','f','9','7','W','e','a','r','s',
  'u','n','s','c','r','e','e','n','I','f','I','c','o','u','l','d',
  'o','f','f','e','r','y','o','u','o','n','l','y','o','n','e','t',
  'i','p','f','o','r','t','h','e','f','u','t','u','r','e','s','u',
  'n','s','c','r','e','e','n','w','o','u','l','d','b','e','i','t',
  'T','h','e','l','o','n','g','t','e','r','m','b','e','n','e','f',
  'i','t','s','o','f','s','u','n','s','c','r','e','e','n','h','a',
  'v','e','b','e','e','n','p','r','o','v','e','d','b','y','s','c',
  'i','e','n','t','i','s','t','s','w','h','e','r','e','a','s','t',
  'h','e','r','e','s','t','o','f','m','y','a','d','v','i','c','e',
  'h','a','s','n','o','b','a','s','i','s','m','o','r','e','r','e',
  'l','i','a','b','l','e','t','h','a','n','m','y','o','w','n','m',
  'e','a','n','d','e','r','i','n','g','e','x','p','e','r','i','e',
  'n','c','e','I','w','i','l','l','d','i','s','p','e','n','s','e',
  't','h','i','s','a','d','v','i','c','e','n','o','w','E','n','j',
  'o','y','t','h','e','p','o','w','e','r','a','n','d','b','e','a',
  'u','t','y','o','f','y','o','u','r','y','o','u','t','h','O','h',
  'n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l',
  'n','o','t','u','n','d','e','r','s','t','a','n','d','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','u','n','t','i','l','t','h',
  'e','y','v','e','f','a','d','e','d','B','u','t','t','r','u','s',
  't','m','e','i','n','2','0','y','e','a','r','s','y','o','u','l',
  'l','l','o','o','k','b','a','c','k','a','t','p','h','o','t','o',
  's','o','f','y','o','u','r','s','e','l','f','a','n','d','r','e',
  'c','a','l','l','i','n','a','w','a','y','y','o','u','c','a','n',
  't','g','r','a','s','p','n','o','w','h','o','w','m','u','c','h',
  'p','o','s','s','i','b','i','l','i','t','y','l','a','y','b','e',
  'f','o','r','e','y','o','u','a','n','d','h','o','w','f','a','b',
  'u','l','o','u','s','y','o','u','r','e','a','l','l','y','l','o',
  'o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f',
  'a','t','a','s','y','o','u','i','m','a','g','i','n','e','D','o',
  'n','t','w','o','r','r','y','a','b','o','u','t','t','h','e','f',
  'u','t','u','r','e','O','r','w','o','r','r','y','b','u','t','k',
  'n','o','w','t','h','a','t','s','u','n','s','c','r','e','e','n',
  'w','o','u','l','d','b','e','i','t','T','h','e','l','o','n','g',
  't','e','r','m','b','e','n','e','f','i','t','s','o','f','s','u',
  'n','s','c','r','e','e','n','h','a','v','e','b','e','e','n','p',
  'r','o','v','e','d','b','y','s','c','i','e','n','t','i','s','t',
  's','w','h','e','r','e','a','s','t','h','e','r','e','s','t','o',
  'f','m','y','a','d','v','i','c','e','h','a','s','n','o','b','a',
  's','i','s','m','o','r','e','r','e','l','i','a','b','l','e','t',
  'h','a','n','m','y','o','w','n','m','e','a','n','d','e','r','i',
  'n','g','e','x','p','e','r','i','e','n','c','e','I','w','i','l',
  'l','d','i','s','p','e','n','s','e','t','h','i','s','a','d','v',
  'i','c','e','n','o','w','E','n','j','o','y','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','O','h','n','e','v','e','r','m','i',
  'n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d','e',
  'r','s','t','a','n','d','t','h','e','p','o','w','e','r','a','n',
  'd','b','e','a','u','t','y','o','f','y','o','u','r','y','o','u',
  't','h','u','n','t','i','l','t','h','e','y','v','e','f','a','d',
  'e','d','B','u','t','t','r','u','s','t','m','e','i','n','2','0',
  'y','e','a','r','s','y','o','u','l','l','l','o','o','k','b','a',
  'c','k','a','t','p','h','o','t','o','s','o','f','y','o','u','r',
  's','e','l','f','a','n','d','r','e','c','a','l','l','i','n','a',
  'w','a','y','y','o','u','c','a','n','t','g','r','a','s','p','n',
  'o','w','h','o','w','m','u','c','h','p','o','s','s','i','b','i',
  'l','i','t','y','l','a','y','b','e','f','o','r','e','y','o','u',
  'a','n','d','h','o','w','f','a','b','u','l','o','u','s','y','o',
  'u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u',
  'a','r','e','n','o','t','a','s','f','a','t','a','s','y','o','u',
  'i','m','a','g','i','n','e','D','o','n','t','w','o','r','r','y',
  'a','b','o','u','t','t','h','e','f','u','t','u','r','e','O','r',
  'w','o','r','r','y','b','u','t','k','n','o','w','t','h','a','t',
  '\n', 'l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','s',
  'u','n','s','c','r','e','e','n','w','o','u','l','d','b','e','i',
  't','T','h','e','l','o','n','g','t','e','r','m','b','e','n','e',
  'f','i','t','s','o','f','s','u','n','s','c','r','e','e','n','h',
  'a','v','e','b','e','e','n','p','r','o','v','e','d','b','y','s',
  'c','i','e','n','t','i','s','t','s','w','h','e','r','e','a','s',
  't','h','e','r','e','s','t','o','f','m','y','a','d','v','i','c',
  'e','h','a','s','n','o','b','a','s','i','s','m','o','r','e','r',
  'e','l','i','a','b','l','e','t','h','a','n','m','y','o','w','n',
  'm','e','a','n','d','e','r','i','n','g','e','x','p','e','r','i',
  'e','n','c','e','I','w','i','l','l','d','i','s','p','e','n','s',
  'e','t','h','i','s','a','d','v','i','c','e','n','o','w','E','n',
  'j','o','y','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','O',
  'h','n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l',
  'l','n','o','t','u','n','d','e','r','s','t','a','n','d','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','u','n','t','i','l','t',
  'h','e','y','v','e','f','a','d','e','d','B','u','t','t','r','u',
  's','t','m','e','i','n','2','0','y','e','a','r','s','y','o','u',
  'l','l','l','o','o','k','b','a','c','k','a','t','p','h','o','t',
  'o','s','o','f','y','o','u','r','s','e','l','f','a','n','d','r',
  'e','c','a','l','l','i','n','a','w','a','y','y','o','u','c','a',
  'n','t','g','r','a','s','p','n','o','w','h','o','w','m','u','c',
  'h','p','o','s','s','i','b','i','l','i','t','y','l','a','y','b',
  'e','f','o','r','e','y','o','u','a','n','d','h','o','w','f','a',
  'b','u','l','o','u','s','y','o','u','r','e','a','l','l','y','l',
  'o','o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s',
  'f','a','t','a','s','y','o','u','i','m','a','g','i','n','e','D',
  'o','n','t','w','o','r','r','y','a','b','o','u','t','t','h','e',
  'f','u','t','u','r','e','O','r','w','o','r','r','y','b','u','t',
  'k','n','o','w','t','h','a','t','\n', 'l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','K','u','r','t','V','o','n','n','e','g','u','t',
  's','C','o','m','m','e','n','c','e','m','e','n','t','A','d','d',
  'r','e','s','s','a','t','M','I','T','L','a','d','i','e','s','a',
  'n','d','g','e','n','t','l','e','m','e','n','o','f','t','h','e',
  'c','l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','s','u','n','s',
  'c','r','e','e','n','w','o','u','l','d','b','e','i','t','T','h',
  'e','l','o','n','g','t','e','r','m','b','e','n','e','f','i','t',
  's','o','f','s','u','n','s','c','r','e','e','n','h','a','v','e',
  'b','e','e','n','p','r','o','v','e','d','b','y','s','c','i','e',
  'n','t','i','s','t','s','w','h','e','r','e','a','s','t','h','e',
  'r','e','s','t','o','f','m','y','a','d','v','i','c','e','h','a',
  's','n','o','b','a','s','i','s','m','o','r','e','r','e','l','i',
  'a','b','l','e','t','h','a','n','m','y','o','w','n','m','e','a',
  'n','d','e','r','i','n','g','e','x','p','e','r','i','e','n','c',
  'e','I','w','i','l','l','d','i','s','p','e','n','s','e','t','h',
  'i','s','a','d','v','i','c','e','n','o','w','E','n','j','o','y',
  't','h','e','p','o','w','e','r','a','n','d','b','e','a','u','t',
  'y','o','f','y','o','u','r','y','o','u','t','h','O','h','n','e',
  'v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n','o',
  't','u','n','d','e','r','s','t','a','n','d','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','u','n','t','i','l','t','h','e','y',
  'v','e','f','a','d','e','d','B','u','t','t','r','u','s','t','m',
  'e','i','n','2','0','y','e','a','r','s','y','o','u','l','l','l',
  'o','o','k','b','a','c','k','a','t','p','h','o','t','o','s','o',
  'f','y','o','u','r','s','e','l','f','a','n','d','r','e','c','a',
  'l','l','i','n','a','w','a','y','y','o','u','c','a','n','t','g',
  'r','a','s','p','n','o','w','h','o','w','m','u','c','h','p','o',
  's','s','i','b','i','l','i','t','y','l','a','y','b','e','f','o',
  'r','e','y','o','u','a','n','d','h','o','w','f','a','b','u','l',
  'o','u','s','y','o','u','r','e','a','l','l','y','l','o','o','k',
  'e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a','t',
  'a','s','y','o','u','i','m','a','g','i','n','e','D','o','n','t',
  'w','o','r','r','y','a','b','o','u','t','t','h','e','f','u','t',
  'u','r','e','O','r','w','o','r','r','y','b','u','t','k','n','o',
  'w','t','h','a','t','K','u','r','t','V','o','n','n','e','g','u',
  't','s','C','o','m','m','e','n','c','e','m','e','n','t','A','d',
  'd','r','e','s','s','a','t','M','I','T','L','a','d','i','e','s',
  'a','n','d','g','e','n','t','l','e','m','e','n','o','f','t','h',
  'e','c','l','a','s','s','o','f','9','7','W','e','a','r','s','u',
  'n','s','c','r','e','e','n','I','f','I','c','o','u','l','d','o',
  'f','f','e','r','y','o','u','o','n','l','y','o','n','e','t','i',
  'p','f','o','r','t','h','e','f','u','t','u','r','e','s','u','n',
  's','c','r','e','e','n','w','o','u','l','d','b','e','i','t','T',
  'h','e','l','o','n','g','t','e','r','m','b','e','n','e','f','i',
  't','s','o','f','s','u','n','s','c','r','e','e','n','h','a','v',
  'e','b','e','e','n','p','r','o','v','e','d','b','y','s','c','i',
  'e','n','t','i','s','t','s','w','h','e','r','e','a','s','t','h',
  'e','r','e','s','t','o','f','m','y','a','d','v','i','c','e','h',
  'a','s','n','o','b','a','s','i','s','m','o','r','e','r','e','l',
  'i','a','b','l','e','t','h','a','n','m','y','o','w','n','m','e',
  'a','n','d','e','r','i','n','g','e','x','p','e','r','i','e','n',
  'c','e','I','w','i','l','l','d','i','s','p','e','n','s','e','t',
  'h','i','s','a','d','v','i','c','e','n','o','w','E','n','j','o',
  'y','t','h','e','p','o','w','e','r','a','n','d','b','e','a','u',
  't','y','o','f','y','o','u','r','y','o','u','t','h','O','h','n',
  'e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l','n',
  'o','t','u','n','d','e','r','s','t','a','n','d','t','h','e','p',
  'o','w','e','r','a','n','d','b','e','a','u','t','y','o','f','y',
  'o','u','r','y','o','u','t','h','u','n','t','i','l','t','h','e',
  'y','v','e','f','a','d','e','d','B','u','t','t','r','u','s','t',
  'm','e','i','n','2','0','y','e','a','r','s','y','o','u','l','l',
  'l','o','o','k','b','a','c','k','a','t','p','h','o','t','o','s',
  'o','f','y','o','u','r','s','e','l','f','a','n','d','r','e','c',
  'a','l','l','i','n','a','w','a','y','y','o','u','c','a','n','t',
  'g','r','a','s','p','n','o','w','h','o','w','m','u','c','h','p',
  'o','s','s','i','b','i','l','i','t','y','l','a','y','b','e','f',
  'o','r','e','y','o','u','a','n','d','h','o','w','f','a','b','u',
  'l','o','u','s','y','o','u','r','e','a','l','l','y','l','o','o',
  'k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f','a',
  't','a','s','y','o','u','i','m','a','g','i','n','e','D','o','n',
  't','w','o','r','r','y','a','b','o','u','t','t','h','e','f','u',
  't','u','r','e','O','r','w','o','r','r','y','b','u','t','k','n',
  'o','w','t','h','a','t','K','u','r','t','V','o','n','n','e','g',
  'u','t','s','C','o','m','m','e','n','c','e','m','e','n','t','A',
  'd','d','r','e','s','s','a','t','M','I','T','L','a','d','i','e',
  's','a','n','d','g','e','n','t','l','e','m','e','n','o','f','t',
  'h','e','c','l','a','s','s','o','f','9','7','W','e','a','r','s',
  'u','n','s','c','r','e','e','n','I','f','I','c','o','u','l','d',
  'o','f','f','e','r','y','o','u','o','n','l','y','o','n','e','t',
  'i','p','f','o','r','t','h','e','f','u','t','u','r','e','s','u',
  'n','s','c','r','e','e','n','w','o','u','l','d','b','e','i','t',
  'T','h','e','l','o','n','g','t','e','r','m','b','e','n','e','f',
  'i','t','s','o','f','s','u','n','s','c','r','e','e','n','h','a',
  'v','e','b','e','e','n','p','r','o','v','e','d','b','y','s','c',
  'i','e','n','t','i','s','t','s','w','h','e','r','e','a','s','t',
  'h','e','r','e','s','t','o','f','m','y','a','d','v','i','c','e',
  'h','a','s','n','o','b','a','s','i','s','m','o','r','e','r','e',
  'l','i','a','b','l','e','t','h','a','n','m','y','o','w','n','m',
  'e','a','n','d','e','r','i','n','g','e','x','p','e','r','i','e',
  'n','c','e','I','w','i','l','l','d','i','s','p','e','n','s','e',
  't','h','i','s','a','d','v','i','c','e','n','o','w','E','n','j',
  'o','y','t','h','e','p','o','w','e','r','a','n','d','b','e','a',
  'u','t','y','o','f','y','o','u','r','y','o','u','t','h','O','h',
  'n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l','l',
  'n','o','t','u','n','d','e','r','s','t','a','n','d','t','h','e',
  'p','o','w','e','r','a','n','d','b','e','a','u','t','y','o','f',
  'y','o','u','r','y','o','u','t','h','u','n','t','i','l','t','h',
  'e','y','v','e','f','a','d','e','d','B','u','t','t','r','u','s',
  't','m','e','i','n','2','0','y','e','a','r','s','y','o','u','l',
  'l','l','o','o','k','b','a','c','k','a','t','p','h','o','t','o',
  's','o','f','y','o','u','r','s','e','l','f','a','n','d','r','e',
  'c','a','l','l','i','n','a','w','a','y','y','o','u','c','a','n',
  't','g','r','a','s','p','n','o','w','h','o','w','m','u','c','h',
  'p','o','s','s','i','b','i','l','i','t','y','l','a','y','b','e',
  'f','o','r','e','y','o','u','a','n','d','h','o','w','f','a','b',
  'u','l','o','u','s','y','o','u','r','e','a','l','l','y','l','o',
  'o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s','f',
  'a','t','a','s','y','o','u','i','m','a','g','i','n','e','D','o',
  'n','t','w','o','r','r','y','a','b','o','u','t','t','h','e','f',
  'u','t','u','r','e','O','r','w','o','r','r','y','b','u','t','k',
  'n','o','w','t','h','a','t','s','u','n','s','c','r','e','e','n',
  'w','o','u','l','d','b','e','i','t','T','h','e','l','o','n','g',
  't','e','r','m','b','e','n','e','f','i','t','s','o','f','s','u',
  'n','s','c','r','e','e','n','h','a','v','e','b','e','e','n','p',
  'r','o','v','e','d','b','y','s','c','i','e','n','t','i','s','t',
  's','w','h','e','r','e','a','s','t','h','e','r','e','s','t','o',
  'f','m','y','a','d','v','i','c','e','h','a','s','n','o','b','a',
  's','i','s','m','o','r','e','r','e','l','i','a','b','l','e','t',
  'h','a','n','m','y','o','w','n','m','e','a','n','d','e','r','i',
  'n','g','e','x','p','e','r','i','e','n','c','e','I','w','i','l',
  'l','d','i','s','p','e','n','s','e','t','h','i','s','a','d','v',
  'i','c','e','n','o','w','E','n','j','o','y','t','h','e','p','o',
  'w','e','r','a','n','d','b','e','a','u','t','y','o','f','y','o',
  'u','r','y','o','u','t','h','O','h','n','e','v','e','r','m','i',
  'n','d','Y','o','u','w','i','l','l','n','o','t','u','n','d','e',
  'r','s','t','a','n','d','t','h','e','p','o','w','e','r','a','n',
  'd','b','e','a','u','t','y','o','f','y','o','u','r','y','o','u',
  't','h','u','n','t','i','l','t','h','e','y','v','e','f','a','d',
  'e','d','B','u','t','t','r','u','s','t','m','e','i','n','2','0',
  'y','e','a','r','s','y','o','u','l','l','l','o','o','k','b','a',
  'c','k','a','t','p','h','o','t','o','s','o','f','y','o','u','r',
  's','e','l','f','a','n','d','r','e','c','a','l','l','i','n','a',
  'w','a','y','y','o','u','c','a','n','t','g','r','a','s','p','n',
  'o','w','h','o','w','m','u','c','h','p','o','s','s','i','b','i',
  'l','i','t','y','l','a','y','b','e','f','o','r','e','y','o','u',
  'a','n','d','h','o','w','f','a','b','u','l','o','u','s','y','o',
  'u','r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u',
  'a','r','e','n','o','t','a','s','f','a','t','a','s','y','o','u',
  'i','m','a','g','i','n','e','D','o','n','t','w','o','r','r','y',
  'a','b','o','u','t','t','h','e','f','u','t','u','r','e','O','r',
  'w','o','r','r','y','b','u','t','k','n','o','w','t','h','a','t',
  '\n', 'l','a','s','s','o','f','9','7','W','e','a','r','s','u','n',
  's','c','r','e','e','n','I','f','I','c','o','u','l','d','o','f',
  'f','e','r','y','o','u','o','n','l','y','o','n','e','t','i','p',
  'f','o','r','t','h','e','f','u','t','u','r','e','K','u','r','t',
  'V','o','n','n','e','g','u','t','s','C','o','m','m','e','n','c',
  'e','m','e','n','t','A','d','d','r','e','s','s','a','t','M','I',
  'T','L','a','d','i','e','s','a','n','d','g','e','n','t','l','e',
  'm','e','n','o','f','t','h','e','c','l','a','s','s','o','f','9',
  '7','W','e','a','r','s','u','n','s','c','r','e','e','n','I','f',
  'I','c','o','u','l','d','o','f','f','e','r','y','o','u','o','n',
  'l','y','o','n','e','t','i','p','f','o','r','t','h','e','f','u',
  't','u','r','e','s','u','n','s','c','r','e','e','n','w','o','u',
  'l','d','b','e','i','t','T','h','e','l','o','n','g','t','e','r',
  'm','b','e','n','e','f','i','t','s','o','f','s','u','n','s','c',
  'r','e','e','n','h','a','v','e','b','e','e','n','p','r','o','v',
  'e','d','b','y','s','c','i','e','n','t','i','s','t','s','w','h',
  'e','r','e','a','s','t','h','e','r','e','s','t','o','f','m','y',
  'a','d','v','i','c','e','h','a','s','n','o','b','a','s','i','s',
  'm','o','r','e','r','e','l','i','a','b','l','e','t','h','a','n',
  'm','y','o','w','n','m','e','a','n','d','e','r','i','n','g','e',
  'x','p','e','r','i','e','n','c','e','I','w','i','l','l','d','i',
  's','p','e','n','s','e','t','h','i','s','a','d','v','i','c','e',
  'n','o','w','E','n','j','o','y','t','h','e','p','o','w','e','r',
  'a','n','d','b','e','a','u','t','y','o','f','y','o','u','r','y',
  'o','u','t','h','O','h','n','e','v','e','r','m','i','n','d','Y',
  'o','u','w','i','l','l','n','o','t','u','n','d','e','r','s','t',
  'a','n','d','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','u',
  'n','t','i','l','t','h','e','y','v','e','f','a','d','e','d','B',
  'u','t','t','r','u','s','t','m','e','i','n','2','0','y','e','a',
  'r','s','y','o','u','l','l','l','o','o','k','b','a','c','k','a',
  't','p','h','o','t','o','s','o','f','y','o','u','r','s','e','l',
  'f','a','n','d','r','e','c','a','l','l','i','n','a','w','a','y',
  'y','o','u','c','a','n','t','g','r','a','s','p','n','o','w','h',
  'o','w','m','u','c','h','p','o','s','s','i','b','i','l','i','t',
  'y','l','a','y','b','e','f','o','r','e','y','o','u','a','n','d',
  'h','o','w','f','a','b','u','l','o','u','s','y','o','u','r','e',
  'a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r','e',
  'n','o','t','a','s','f','a','t','a','s','y','o','u','i','m','a',
  'g','i','n','e','D','o','n','t','w','o','r','r','y','a','b','o',
  'u','t','t','h','e','f','u','t','u','r','e','O','r','w','o','r',
  'r','y','b','u','t','k','n','o','w','t','h','a','t','K','u','r',
  't','V','o','n','n','e','g','u','t','s','C','o','m','m','e','n',
  'c','e','m','e','n','t','A','d','d','r','e','s','s','a','t','M',
  'I','T','L','a','d','i','e','s','a','n','d','g','e','n','t','l',
  'e','m','e','n','o','f','t','h','e','c','l','a','s','s','o','f',
  '9','7','W','e','a','r','s','u','n','s','c','r','e','e','n','I',
  'f','I','c','o','u','l','d','o','f','f','e','r','y','o','u','o',
  'n','l','y','o','n','e','t','i','p','f','o','r','t','h','e','f',
  'u','t','u','r','e','s','u','n','s','c','r','e','e','n','w','o',
  'u','l','d','b','e','i','t','T','h','e','l','o','n','g','t','e',
  'r','m','b','e','n','e','f','i','t','s','o','f','s','u','n','s',
  'c','r','e','e','n','h','a','v','e','b','e','e','n','p','r','o',
  'v','e','d','b','y','s','c','i','e','n','t','i','s','t','s','w',
  'h','e','r','e','a','s','t','h','e','r','e','s','t','o','f','m',
  'y','a','d','v','i','c','e','h','a','s','n','o','b','a','s','i',
  's','m','o','r','e','r','e','l','i','a','b','l','e','t','h','a',
  'n','m','y','o','w','n','m','e','a','n','d','e','r','i','n','g',
  'e','x','p','e','r','i','e','n','c','e','I','w','i','l','l','d',
  'i','s','p','e','n','s','e','t','h','i','s','a','d','v','i','c',
  'e','n','o','w','E','n','j','o','y','t','h','e','p','o','w','e',
  'r','a','n','d','b','e','a','u','t','y','o','f','y','o','u','r',
  'y','o','u','t','h','O','h','n','e','v','e','r','m','i','n','d',
  'Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r','s',
  't','a','n','d','t','h','e','p','o','w','e','r','a','n','d','b',
  'e','a','u','t','y','o','f','y','o','u','r','y','o','u','t','h',
  'u','n','t','i','l','t','h','e','y','v','e','f','a','d','e','d',
  'B','u','t','t','r','u','s','t','m','e','i','n','2','0','y','e',
  'a','r','s','y','o','u','l','l','l','o','o','k','b','a','c','k',
  'a','t','p','h','o','t','o','s','o','f','y','o','u','r','s','e',
  'l','f','a','n','d','r','e','c','a','l','l','i','n','a','w','a',
  'y','y','o','u','c','a','n','t','g','r','a','s','p','n','o','w',
  'h','o','w','m','u','c','h','p','o','s','s','i','b','i','l','i',
  't','y','l','a','y','b','e','f','o','r','e','y','o','u','a','n',
  'd','h','o','w','f','a','b','u','l','o','u','s','y','o','u','r',
  'e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a','r',
  'e','n','o','t','a','s','f','a','t','a','s','y','o','u','i','m',
  'a','g','i','n','e','D','o','n','t','w','o','r','r','y','a','b',
  'o','u','t','t','h','e','f','u','t','u','r','e','O','r','w','o',
  'r','r','y','b','u','t','k','n','o','w','t','h','a','t','K','u',
  'r','t','V','o','n','n','e','g','u','t','s','C','o','m','m','e',
  'n','c','e','m','e','n','t','A','d','d','r','e','s','s','a','t',
  'M','I','T','L','a','d','i','e','s','a','n','d','g','e','n','t',
  'l','e','m','e','n','o','f','t','h','e','c','l','a','s','s','o',
  'f','9','7','W','e','a','r','s','u','n','s','c','r','e','e','n',
  'I','f','I','c','o','u','l','d','o','f','f','e','r','y','o','u',
  'o','n','l','y','o','n','e','t','i','p','f','o','r','t','h','e',
  'f','u','t','u','r','e','s','u','n','s','c','r','e','e','n','w',
  'o','u','l','d','b','e','i','t','T','h','e','l','o','n','g','t',
  'e','r','m','b','e','n','e','f','i','t','s','o','f','s','u','n',
  's','c','r','e','e','n','h','a','v','e','b','e','e','n','p','r',
  'o','v','e','d','b','y','s','c','i','e','n','t','i','s','t','s',
  'w','h','e','r','e','a','s','t','h','e','r','e','s','t','o','f',
  'm','y','a','d','v','i','c','e','h','a','s','n','o','b','a','s',
  'i','s','m','o','r','e','r','e','l','i','a','b','l','e','t','h',
  'a','n','m','y','o','w','n','m','e','a','n','d','e','r','i','n',
  'g','e','x','p','e','r','i','e','n','c','e','I','w','i','l','l',
  'd','i','s','p','e','n','s','e','t','h','i','s','a','d','v','i',
  'c','e','n','o','w','E','n','j','o','y','t','h','e','p','o','w',
  'e','r','a','n','d','b','e','a','u','t','y','o','f','y','o','u',
  'r','y','o','u','t','h','O','h','n','e','v','e','r','m','i','n',
  'd','Y','o','u','w','i','l','l','n','o','t','u','n','d','e','r',
  's','t','a','n','d','t','h','e','p','o','w','e','r','a','n','d',
  'b','e','a','u','t','y','o','f','y','o','u','r','y','o','u','t',
  'h','u','n','t','i','l','t','h','e','y','v','e','f','a','d','e',
  'd','B','u','t','t','r','u','s','t','m','e','i','n','2','0','y',
  'e','a','r','s','y','o','u','l','l','l','o','o','k','b','a','c',
  'k','a','t','p','h','o','t','o','s','o','f','y','o','u','r','s',
  'e','l','f','a','n','d','r','e','c','a','l','l','i','n','a','w',
  'a','y','y','o','u','c','a','n','t','g','r','a','s','p','n','o',
  'w','h','o','w','m','u','c','h','p','o','s','s','i','b','i','l',
  'i','t','y','l','a','y','b','e','f','o','r','e','y','o','u','a',
  'n','d','h','o','w','f','a','b','u','l','o','u','s','y','o','u',
  'r','e','a','l','l','y','l','o','o','k','e','d','Y','o','u','a',
  'r','e','n','o','t','a','s','f','a','t','a','s','y','o','u','i',
  'm','a','g','i','n','e','D','o','n','t','w','o','r','r','y','a',
  'b','o','u','t','t','h','e','f','u','t','u','r','e','O','r','w',
  'o','r','r','y','b','u','t','k','n','o','w','t','h','a','t','s',
  'u','n','s','c','r','e','e','n','w','o','u','l','d','b','e','i',
  't','T','h','e','l','o','n','g','t','e','r','m','b','e','n','e',
  'f','i','t','s','o','f','s','u','n','s','c','r','e','e','n','h',
  'a','v','e','b','e','e','n','p','r','o','v','e','d','b','y','s',
  'c','i','e','n','t','i','s','t','s','w','h','e','r','e','a','s',
  't','h','e','r','e','s','t','o','f','m','y','a','d','v','i','c',
  'e','h','a','s','n','o','b','a','s','i','s','m','o','r','e','r',
  'e','l','i','a','b','l','e','t','h','a','n','m','y','o','w','n',
  'm','e','a','n','d','e','r','i','n','g','e','x','p','e','r','i',
  'e','n','c','e','I','w','i','l','l','d','i','s','p','e','n','s',
  'e','t','h','i','s','a','d','v','i','c','e','n','o','w','E','n',
  'j','o','y','t','h','e','p','o','w','e','r','a','n','d','b','e',
  'a','u','t','y','o','f','y','o','u','r','y','o','u','t','h','O',
  'h','n','e','v','e','r','m','i','n','d','Y','o','u','w','i','l',
  'l','n','o','t','u','n','d','e','r','s','t','a','n','d','t','h',
  'e','p','o','w','e','r','a','n','d','b','e','a','u','t','y','o',
  'f','y','o','u','r','y','o','u','t','h','u','n','t','i','l','t',
  'h','e','y','v','e','f','a','d','e','d','B','u','t','t','r','u',
  's','t','m','e','i','n','2','0','y','e','a','r','s','y','o','u',
  'l','l','l','o','o','k','b','a','c','k','a','t','p','h','o','t',
  'o','s','o','f','y','o','u','r','s','e','l','f','a','n','d','r',
  'e','c','a','l','l','i','n','a','w','a','y','y','o','u','c','a',
  'n','t','g','r','a','s','p','n','o','w','h','o','w','m','u','c',
  'h','p','o','s','s','i','b','i','l','i','t','y','l','a','y','b',
  'e','f','o','r','e','y','o','u','a','n','d','h','o','w','f','a',
  'b','u','l','o','u','s','y','o','u','r','e','a','l','l','y','l',
  'o','o','k','e','d','Y','o','u','a','r','e','n','o','t','a','s',
  'f','a','t','a','s','y','o','u','i','m','a','g','i','n','e','D',
  'o','n','t','w','o','r','r','y','a','b','o','u','t','t','h','e',
  'f','u','t','u','r','e','O','r','w','o','r','r','y','b','u','t',
  'k','n','o','w','t','h','a','t','\n'
};
/*

  This program is part of the TACLeBench benchmark suite.
  Version V 2.0

  Name: rijndael_enc

  Author: Dr Brian Gladman

  Function: rijndael_enc is an implementation of the AES encryption
            algorithm (Rijndael).

  Source: security section of MiBench

  Changes: Add computation of a checksum, refactoring

  License: see below

*/

/*
  -----------------------------------------------------------------------
  Copyright (c) 2001 Dr Brian Gladman <brg@gladman.uk.net>, Worcester, UK

  TERMS

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

  This software is provided 'as is' with no guarantees of correctness or
  fitness for purpose.
  -----------------------------------------------------------------------
*/

#include "aes.h"
#include "rijndael_enc_libc.h"

/*
  Global variable definitions
*/
unsigned char rijndael_enc_key[32];
int rijndael_enc_key_len;

extern unsigned char rijndael_enc_data[];
struct rijndael_enc_FILE rijndael_enc_fin;

int rijndael_enc_checksum = 0;

/*
  Forward declaration of functions
*/
void rijndael_enc_init( void );
int rijndael_enc_return( void );
void rijndael_enc_fillrand( unsigned char *buf, int len );
void rijndael_enc_encfile( struct rijndael_enc_FILE *fin, struct aes *ctx );
void rijndael_enc_main( void );

void rijndael_enc_init( void )
{
  /* create a pseudo-file for the input*/
  rijndael_enc_fin.data = rijndael_enc_data;
  rijndael_enc_fin.size = 31369;
  rijndael_enc_fin.cur_pos = 0;

  unsigned i;
  volatile int x = 0;
  rijndael_enc_fin.size ^= x;
  _Pragma( "loopbound min 31369 max 31369" )
  for ( i = 0; i < rijndael_enc_fin.size; i++ )
    rijndael_enc_fin.data[i] ^= x;

  /* this is a pointer to the hexadecimal key digits  */
  const volatile char *cp =
    "1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321";
  char ch;
  int by = 0;

  i = 0;                  /* this is a count for the input digits processed */
  _Pragma( "loopbound min 64 max 64" )
  while ( i < 64 && *cp ) { /* the maximum key length is 32 bytes and       */
    /* hence at most 64 hexadecimal digits            */
    ch = rijndael_enc_toupper( *cp++ );     /* process a hexadecimal digit  */
    if ( ch >= '0' && ch <= '9' )
      by = ( by << 4 ) + ch - '0';
    else
      if ( ch >= 'A' && ch <= 'F' )
        by = ( by << 4 ) + ch - 'A' + 10;
      else {                                /* error if not hexadecimal     */
        rijndael_enc_checksum = -2;
        return;
      }

    /* store a key byte for each pair of hexadecimal digits         */
    if ( i++ & 1 )
      rijndael_enc_key[i / 2 - 1] = by & 0xff;
  }

  if ( *cp ) {
    rijndael_enc_checksum = -3;
    return;
  } else
    if ( i < 32 || ( i & 15 ) ) {
      rijndael_enc_checksum = -4;
      return;
    }

  rijndael_enc_key_len = i / 2;
}

int rijndael_enc_return( void )
{
  return ( ( rijndael_enc_checksum == ( int )249509 ) ? 0 : -1 );
}

/* A Pseudo Random Number Generator (PRNG) used for the     */
/* Initialisation Vector. The PRNG is George Marsaglia's    */
/* Multiply-With-Carry (MWC) PRNG that concatenates two     */
/* 16-bit MWC generators:                                   */
/*     x(n)=36969 * x(n-1) + carry mod 2^16                 */
/*     y(n)=18000 * y(n-1) + carry mod 2^16                 */
/* to produce a combined PRNG with a period of about 2^60.  */

#define RAND(a,b) (((a = 36969 * (a & 65535) + (a >> 16)) << 16) + (b = 18000 * (b & 65535) + (b >> 16))  )

void rijndael_enc_fillrand( unsigned char *buf, int len )
{
  static unsigned long a[2], mt = 1, count = 4;
  static char          r[4];
  int                  i;

  if ( mt ) {
    mt = 0;
    a[0] = 0xeaf3;
    a[1] = 0x35fe;
  }

  _Pragma( "loopbound min 1 max 16" )
  for ( i = 0; i < len; ++i ) {
    if ( count == 4 ) {
      *( unsigned long * )r = RAND( a[0], a[1] );
      count = 0;
    }

    buf[i] = r[count++];
  }
}

void rijndael_enc_encfile( struct rijndael_enc_FILE *fin, struct aes *ctx )
{
  unsigned char   inbuf[16], outbuf[16];
  long int        flen;
  unsigned long   i = 0, l = 0;

  rijndael_enc_fillrand( outbuf,
                         16 );    /* set an IV for CBC mode           */
  flen = fin->size;

  rijndael_enc_fillrand( inbuf,
                         1 );     /* make top 4 bits of a byte random */
  l = 15;                         /* and store the length of the last */
  /* block in the lower 4 bits        */
  inbuf[0] = ( ( char )flen & 15 ) | ( inbuf[0] & ~15 );

  /* TODO: this is necessarily an input-dependent loop bound */
  _Pragma( "loopbound min 1961 max 1961" )
  while ( !rijndael_enc_feof(
            fin ) ) {             /* loop to encrypt the input file   */
    /* input 1st 16 bytes to buf[1..16] */
    i = rijndael_enc_fread( inbuf + 16 - l, 1, l, fin ); /* on 1st round byte[0] */
    /* is the length code    */
    if ( i < l ) break;           /* if end of the input file reached */

    _Pragma( "loopbound min 16 max 16" )
    for ( i = 0; i < 16; ++i )    /* xor in previous cipher text      */
      inbuf[i] ^= outbuf[i];

    rijndael_enc_encrypt( inbuf, outbuf,
                          ctx );  /* and do the encryption            */

    rijndael_enc_checksum += outbuf[15];

    /* in all but first round read 16   */
    l = 16;                       /* bytes into the buffer            */
  }

  /* except for files of length less than two blocks we now have one  */
  /* byte from the previous block and 'i' bytes from the current one  */
  /* to encrypt and 15 - i empty buffer positions. For files of less  */
  /* than two blocks (0 or 1) we have i + 1 bytes and 14 - i empty    */
  /* buffer position to set to zero since the 'count' byte is extra   */

  if ( l == 15 )                      /* adjust for extra byte in the */
    ++i;                              /* in the first block           */

  if ( i ) {                          /* if bytes remain to be output */
    _Pragma( "loopbound min 6 max 6" )
    while ( i < 16 )                  /* clear empty buffer positions */
      inbuf[i++] = 0;

    _Pragma( "loopbound min 16 max 16" )
    for ( i = 0; i < 16; ++i )        /* xor in previous cipher text  */
      inbuf[i] ^= outbuf[i];

    rijndael_enc_encrypt( inbuf, outbuf, ctx ); /* encrypt and output it */

    rijndael_enc_checksum += outbuf[15];
  }
}

void _Pragma( "entrypoint" ) rijndael_enc_main( void )
{
  struct aes ctx[1];

  /* encryption in Cipher Block Chaining mode */
  rijndael_enc_set_key( rijndael_enc_key, rijndael_enc_key_len, enc, ctx );
  rijndael_enc_encfile( &rijndael_enc_fin, ctx );
}

int main( void )
{
  rijndael_enc_init();
  rijndael_enc_main();

  return ( rijndael_enc_return() );
}

#include "rijndael_enc_libc.h"

int rijndael_enc_toupper( int c )
{
  if ( ( c >= 'a' ) && ( c <= 'z' ) )
    return c - 'a' + 'A';
  return c;
}

unsigned long rijndael_enc_fread( void *ptr, unsigned long size,
                                  unsigned long count, struct rijndael_enc_FILE *stream )
{
  unsigned i = stream->cur_pos, i2 = 0;
  unsigned long number_of_chars_to_read =
    stream->size - stream->cur_pos >= size * count ?
    size * count : stream->size - stream->cur_pos;
  _Pragma( "loopbound min 10 max 16" )
  while ( i < stream->cur_pos + number_of_chars_to_read )
    ( ( unsigned char * )ptr )[i2++] = stream->data[i++];
  stream->cur_pos += number_of_chars_to_read;
  return number_of_chars_to_read;
}

unsigned long rijndael_enc_fwrite( const void *ptr, unsigned long size,
                                   unsigned long count, struct rijndael_enc_FILE *stream )
{
  unsigned i = stream->cur_pos, i2 = 0;
  unsigned long number_of_chars_to_write =
    stream->size - stream->cur_pos >= size * count ?
    size * count : stream->size - stream->cur_pos;
  _Pragma( "loopbound min 0 max 0" )
  while ( i < stream->cur_pos + number_of_chars_to_write )
    stream->data[i++] = ( ( unsigned char * )ptr )[i2++];
  stream->cur_pos += number_of_chars_to_write;
  return number_of_chars_to_write;
}

int rijndael_enc_fseek( struct rijndael_enc_FILE *stream, long int offset,
                        Origin origin )
{
  if ( origin == RIJNDAEL_ENC_SEEK_SET ) {
    stream->cur_pos = offset;
    return 0;
  } else
    if ( origin == RIJNDAEL_ENC_SEEK_CUR ) {
      stream->cur_pos += offset;
      return 0;
    } else
      if ( origin == RIJNDAEL_ENC_SEEK_END ) {
        stream->cur_pos = stream->size + offset;
        return 0;
      }
  return -1;
}

int rijndael_enc_fgetpos( struct rijndael_enc_FILE *stream,
                          unsigned *position )
{
  *position = stream->cur_pos;
  return 0;
}

int rijndael_enc_feof( struct rijndael_enc_FILE *stream )
{
  return stream->cur_pos == stream->size ? 1 : 0;
}
