


DNS_SERVEUR=8.8.8.8

gitlab:
	gitlab-runner exec docker  --docker-dns $(DNS_SERVEUR) lra4w7-expe

dock:
	docker run  -v "$$PWD":/current_dir -w /current_dir -it jahierwan/lra4w7-tools

clean:
	rm -rf expe-compteur*/_cnt*
	rm -rf expe-compteur*/log/*.log
	rm -rf expe-compteur*/sh/*.sh
	rm -f *.txt
	rm -f res/*
