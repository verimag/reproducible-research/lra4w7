#!/bin/bash
#
# Pour tout sh, si le log correspondant n'existe pas -> efface le dir
#
#

# appeler avec -co pour les benchs avec optim
if [ "$1" == "-co" ]; then
   benchresdir=expe-compteurs-CO
else
   benchresdir=expe-compteurs-O0
fi
echo "clean-dirs in $benchresdir"

for zesh in $benchresdir/sh/*.sh; do
	bn=`basename $zesh .sh`
	zebasesh=sh/$bn.sh
	zelog="$benchresdir/log/$bn.log"	
	zedir=$benchresdir/$bn
	if [ ! -f $zelog ]; then
		if [ -d $zedir ]; then
			echo "#LOG MISSING BUT DIR EXIST: $zedir"
			rm -rf $zedir
		fi
	fi
done
