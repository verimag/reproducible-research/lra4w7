#/bin/bash

#PR
# adapted from gen_sh_all_fun.sh
CFILE=$1
TIMEOUT=$2
shift
shift
OPT=$@

echo CFILE=$CFILE
echo TIMEOUT=$TIMEOUT
echo OPT=$OPT


set x
DIR=`dirname $CFILE`
#BASE_FILE_NAME=`basename $DIR`

OPT_NO_SPACE=$(echo $OPT | tr -d ' '| tr -d '/')

O=`dirname $CFILE`/zeobj.o

gcc -c $CFILE -o $O $CC_OPT 

#PR: NO !!! we skip the main (for the time being) as gen_sh as already generated it
#FUNC_LIST=`nm $O | grep " T " | cut -d " " -f 3 | grep -v main`
FUNC_LIST=`nm $O | grep " T " | cut -d " " -f 3`

for zefunc in $FUNC_LIST ; do
##echo $zefunc
	#PR: anticipate the name of the cnt working dir
	#serves as basis for sh and log
	CNT_COMMAND="cnt_all $CFILE $zefunc  -ld  $OPT"
	CNT_DIR=`$CNT_COMMAND -dir`
##echo $CNT_DIR
	#FNAME=${BASE_FILE_NAME}-$i-$OPT_NO_SPACE ;\
	FNAME=$CNT_DIR
	SH_NAME=sh/$FNAME.sh ;\
	LOG=log/$FNAME.log ;\
#echo LOG=$LOG
#echo SH_NAME=$SH_NAME
	echo "Generating $SH_NAME (that will generate $LOG)"; \
	if [ -e "$SH_NAME" ]; then
		echo "$SH_NAME already exist."
	else
		echo "echo \"Launching cnt on $CFILE\"> $LOG" > $SH_NAME; \
		echo "echo \"With the following options $OPT\">> $LOG" >> $SH_NAME; \
		echo "  timeout $TIMEOUT $CNT_COMMAND >> $LOG 2>&1 ;">> $SH_NAME; \
		echo "  RET=\"\$?\"; if [ \"\$RET\" -gt 1 ]; then echo \"Exit \$RET\" >> $LOG 2>&1 ; fi">> $SH_NAME; \
		echo "if [ \"\$RET\" -gt 123 ]; then echo 'KO: TIMEOUT($TIMEOUT)' >> $LOG  2>&1 ; fi">> $SH_NAME ;\
	fi
done


