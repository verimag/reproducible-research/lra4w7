#!/usr/bin/env ocaml

(*
--- VERSION SANS LE BOX
   Il faut avoir généré :
   - all_funcs_O0.txt (20-bench-dimension.sh)
   - wcet_stats_O0.txt (30-wcet-stats.sh)
   - wcet_stats_CO.txt (30-wcet-stats.sh -co)

   N.B.
   chaque fonction (couple prg/func) est identifié
   de manière unique par un "nick name" unique 
   - 2 (ou 3) lettres qui minimisent le prg
   - 1 chiffre (ordre le fonction dans le prg)

   ATTENTION:
   - en interne (caml) couple (string, int)
   - à l'affichage, ça donne "am.2", "cr.13 "
   - en LATEX on a pas droit au . et au chiffres, le trick :
     on prend les 2 ou 3 lettres du prg, suivi du numero en chiffre romains
     ça donne "amII",  "crXIII" etc

On gnère plusieurs .tex:
   bench_defs.tex : que des defs (les commandes "nick-names")
Tables tronquées aux XXXX meilleurs résultats en O0 :
   bench_labels.tex : table de correspondance nick-name <-> programme
   bench_res.tex  : table de résultats en O0

Tables complètes : pas censées être utilisée, que du débug !
   bench_labels_all.tex : table de correspondance nick-name <-> programme
   bench_res_all.tex  : table de résultats en O0

   bench_res_all_CO.tex  : table de résultats en CO

*)

#use "topfind"
#require "str"

(* #directory "/home/raymond/local/scripts" *)
#use "utils.ml"

(* LIMITE DES STATS POUR LE PAPIER *)
let _PAPER_STATS = 60

(* vérifie qu'on fait pas des bétises ... *)
let paranotab = Hashtbl.create 60
let prg_nkn p = (
   let res = match p with
      | "adpcm_dec" -> "ad"
      | "adpcm_enc" -> "ae"
      | "binarysearch" -> "bns"
      | "bitcount" -> "bic"
      | "bitonic" -> "bio"
      | "cjpeg_transupp" -> "cjt"
      | "cjpeg_wrbmp" -> "cjw"
      | "complex_updates" -> "com"
      | "countnegative" -> "cou"
      | "cover" -> "cov"
      | "filterbank" -> "fb"
      | "fir2dim" -> "fd"
      | "g723_enc" -> "gs"
      | "gsm_dec" -> "gd"
      | "gsm_encode" -> "ge"
      | "h264_dec" -> "hd"
      | "huff_dec" -> "hfd"
      | "huff_enc" -> "hfe"
      | "rijndael_dec" -> "rjd"
      | "rijndael_enc" -> "rje"
      | "recursion" -> "rec"
      | "statemate" -> "sm"
   (* default : 2 first char *)
   | _ -> String.sub p 0 2
   in
   (try let x = Hashtbl.find paranotab res in
      if (x <> p) then (
         Printf.fprintf stderr "ERROR nkn: %s for both %s and %s\n" res x p;
         exit 1
      )
    with
    | Not_found -> Hashtbl.add paranotab res p
   );
   res
)

(* pas au dela de 49, O pour 0 *)
let to_roman i =
   let rec tr = function
   | 0 -> ""
   | x when (x >= 10) -> "X"^(tr (x-10))
   | 9 -> "IX"
   | x when (x >= 5) -> "V"^(tr (x-5))
   | 4 -> "IV"
   | x -> "I"^(tr (x-1)) 
   in
   if i = 0 then "O" else (tr i)

let nkn2latex (p,i) = p^(to_roman i)
let nkn2screen (p,i) = Printf.sprintf "%s.%d" p i


(* nkn func counters *)
let tab_nkn_count = Hashtbl.create 500
let nkn_func_num nkn = (
   let cpt = try Hashtbl.find tab_nkn_count nkn
   with Not_found -> 0
   in 
   Hashtbl.replace tab_nkn_count nkn (cpt+1);
   cpt
)

(*
benchdir -> nkn ET nkn -> benchdir
seulle la partie prg-fun importe
*)
let minus_rg = Str.regexp_string "-"
let bench2key b = (
   match Str.split minus_rg b with
   | _::pn::fn::_ -> (pn,fn)
   | _ -> assert false
)
    
let bench2nkn_tab = Hashtbl.create 500
let nkn_tab = Hashtbl.create 500
let add_bench b n p f = (
   Hashtbl.replace bench2nkn_tab (bench2key b) n;
   Hashtbl.replace nkn_tab n (p,f, (bench2key b))
)
let bench2nkn b = (
   let k = bench2key b in
   try (
      Hashtbl.find bench2nkn_tab k 
   ) with Not_found -> (
      Printf.fprintf stderr "Error can't find nick name for \"%s\" (prg=%s, func=%s)\n"
         b (fst k) (snd k);
      exit 1
   )
)

let nkn2pf n =
   match Hashtbl.find nkn_tab n with
   | (p,f,_) -> (p,f)


(* remplace les _ par \_ *)
let texstr s = Str.global_replace (Str.regexp_string "_") "\\_" s

let coma=Str.regexp " *, *"

let do_bench l os = (

   let lst = Str.split coma l in
   match lst with
   | [prg; prgdir; func; benchdir] -> (
      (* Printf.printf "%s\n" (string_of_list (fun x -> x) lst)    *)
      let nk = prg_nkn prg in
      let fc = nkn_func_num nk in
      let fnkn = (nk,fc) in
      add_bench benchdir fnkn prgdir func;
      (* Printf.fprintf stderr "(%s,%d), %s, %s, %s\n" (fst fnkn) (snd fnkn) prgdir func (nkn2latex fnkn); *)
(*
\newcommand{\DEFgsmdecgsmdecLongTermSynthesisFiltering}{FUNCTION: gsm\_dec\_Long\_Term\_Synthesis\_Filtering\textCR DIRECTORY: ./sequential/gsm\_dec/ }
\def\gsmdecgsmdecLongTermSynthesisFiltering{\pdftooltip{I.8}{\DEFgsmdecgsmdecLongTermSynthesisFiltering}\stepcounter{FunctionName}}
*)
      (* on génère le tooltip et la commande qui sert à référencer *)

      Printf.fprintf os "\\newcommand{\\DEF%s}{FUNCTION: %s\\textCR DIRECTORY: %s}\n"
         (nkn2latex fnkn)
         (texstr func)
         (texstr prgdir);
      (* et la commande qui sert à afficher le nickname *)
      Printf.fprintf os "\\def\\%s{\\pdftooltip{%s}{\\DEF%s}}\n"
         (nkn2latex fnkn)
         (nkn2screen fnkn)
         (nkn2latex fnkn);
         
   )
   | _ -> assert false
)
 
(*  DATA TYPE pour les stats ce WCET
-------------------------------------
{\bf Ref} & {\bf WCET} & {\bf $\Delta$} & {\bf Imp$^t$} & {\bf Time} & {\bf $\Delta$} & {\bf Imp$^t$} & {\bf Time}  \\
\mdcmdcRRandomInit & 2648 & 0 & 0.0 & 1s & 1920 & \bf{72.5} & 1s & 1920 &  \bf{72.5} &  1s  \\ % 72.500000
*)

type pagai_stat = {
   wcet: int option;
   delta: int option;
   impr: float option;
   time: string 
}

(*
on ajoute ->
   - le meilleur improv
*)
type line_stat = {
   bench: string;
   nkn : string * int;
   bwcet : int;
   octores : pagai_stat;
   polyres : pagai_stat;
   bestimpr : float option;
   tracea : (int * int) option;
}
let dump_line_stat s =
   let t2s = function None -> "-" | Some t -> (string_of_int t) in
   let p2s = function None -> "-" | Some p -> (Printf.sprintf "%.01f" p) in
   Printf.sprintf
   "%s (%s,%d) init: %d oct: %s %s %s poly: %s %s %s best: %s"
   s.bench
   (fst s.nkn) (snd s.nkn) s.bwcet
   (t2s s.octores.delta) (p2s s.octores.impr) s.octores.time
   (t2s s.polyres.delta) (p2s s.polyres.impr) s.polyres.time
   (p2s s.bestimpr)

(* la meme en tex en suivant le style de la v1
{\bf Ref} & {\bf WCET} & {\bf $\Delta$} & {\bf Imp$^t$} & {\bf Time} & {\bf $\Delta$} & {\bf Imp$^t$} & {\bf Time}  \\
\mdcmdcRRandomInit & 2648 & 0 & 0.0 & 1s & 1920 & \bf{72.5} & 1s & 1920 &  \bf{72.5} &  1s  \\ % 72.500000

*)

(*
   on en profite pour fixer le pb du négatif -> 0 ! 
*)
let round_cycles x zeunit zesfx = 
   if x <= 0 then "0" else  (
      (* let rawval = floor((float_of_int x) /. zeunit +. 0.5) in *)
      let xf = (float_of_int x) /. zeunit in
      let xfr = xf +. 0.5 in
      if xfr < 1.0 then
         Printf.sprintf "%.2f%s" xf zesfx
      else
         Printf.sprintf "%d%s" (int_of_float (floor xfr)) zesfx
   )
   

let line_stat2latex s = (
   (* calibrage pour les GROS wcet ->
      change d'unité K ou M selon
      la taille de s.bwcet
   *)
   let (zeunit, zesfx) =
      if s.bwcet >= 10000000 then 
         (1000000., "M")
      else if s.bwcet >= 10000 then
         (1000., "K")
      else (1., "")
   in
   let t2s = function
      | None -> "-"
      | Some x ->
         (* (string_of_int t) in *)
         round_cycles x zeunit zesfx
   in
   (* en gras si best *)
   let p2s = function
      | None -> "-"
      | Some p when ((Some p) = s.bestimpr) -> (Printf.sprintf "{\\bf %.01f}" p) 
      | Some p -> (Printf.sprintf "%.01f" p) 
   in
   Printf.sprintf
   "\\%s & %s & %s & %s & %s & %s & %s & %s \\\\"
   (nkn2latex s.nkn)
   (round_cycles s.bwcet zeunit zesfx)
   (t2s s.octores.delta) (p2s s.octores.impr) s.octores.time
   (t2s s.polyres.delta) (p2s s.polyres.impr) s.polyres.time
)

(* ligne de def du label : nkn  dir  func *)
(*  \gsmencodeGsmPreprocess & sequential/gsm\_encode/  & Gsm\_Preprocess   \\ *)
(* ad hoc. on tronque le "_reaction" dans les fct du style :
   (trop long !) ammunition_divide_unsigned_integer_without_overflow_reaction
*)
let nkn_def2latex nkn = (
   let (pn, fn) = nkn2pf nkn in 
   let fn =  Str.global_replace (Str.regexp_string "_reaction") "" fn in
   Printf.sprintf
      "\\%s & %s & %s \\\\ \n"
      (nkn2latex nkn) 
      (texstr pn)
      (texstr fn)
)


(* négatif = failure = nada *)
let checkint s =
   try
      let res = int_of_string s in
      if res >= 0 then Some res
      else None
   with _ -> None

let pcent all part =
   float_of_int ((part * 1000) / all) /. 10.0

let time_msc_re = Str.regexp "\\([0-9]+\\):\\([0-9]+\\)\\.\\([0-9]+\\)"
let time_hms_re = Str.regexp "\\([0-9]+\\):\\([0-9]+\\):\\([0-9]+\\)"
let format_time pt = (
   (* déja un "-" -> stop *)
   let res = match pt with
   | "-" -> pt
   | _ when (Str.string_match time_msc_re pt 0) -> (
      let mm = int_of_string (Str.matched_group 1 pt) in
      let ss = int_of_string (Str.matched_group 2 pt) in
      let ts = 60*mm+ss in
      let fm = int_of_float (floor((float_of_int ts) /. 60.0 +. 0.5)) in
      if (fm > 0) then
         Printf.sprintf "%dm" fm
      else if (ss > 0) then
         Printf.sprintf "%ds" ss
      else "<1s"
   )
   | _ when (Str.string_match time_hms_re pt 0) -> (
      let hh = int_of_string (Str.matched_group 1 pt) in
      let mm = int_of_string (Str.matched_group 2 pt) in
      let tm = 60*hh+mm in
      Printf.sprintf "%dm" tm
   )
   | _ -> assert false
   in
   res
)

let pagai_res (bwcet: int) pw pt = (
   (* bwcet = initial, pwcet = wcet pagai, ptime = tps de calcul *)
   let (w,d,i) = match checkint pw  with
   | None  -> (None, None, None)
   | Some v ->
   (
      Some v,
      Some (bwcet - v),
      Some (pcent bwcet (bwcet - v)) 
   ) in
   let tm = format_time pt in
   {
      wcet = w;
      delta = d;
      impr = i;
      time = tm;
   }
)


let maximpr a b = match (a,b) with
   | (_, None) -> a
   | (None, _) -> b 
   | (Some x, Some y) when (y > x) -> b 
   | _ -> a 


let do_stat (l: string) : line_stat = (
   let lst = Str.split coma l in
   match lst with
   | [bench; wc; bxw; bxt; ow; ot; pw; pt; trpart; trall ] -> (
      let nkn = bench2nkn bench in

      (* le wcet initial DOIT être connu *)
      let bwcet = match checkint wc with
         | Some x -> x
         | None -> (
            Printf.fprintf stderr "ERROR: unexpected initial wcet '%s' in '%s'\n" wc bench;
            exit 1
         )
      in 

      (* on fait les calculs pour les 3 domaines ... *)
      let octores =  pagai_res bwcet ow ot in
      let bestimpr = octores.impr in
      let polyres = pagai_res bwcet pw pt in
      let bestimpr = maximpr polyres.impr bestimpr in

      (* on rajoute la traceabilité *)
      let tracea = try (
         Some (int_of_string trpart, int_of_string trall)
      ) with _ -> None
      in

      {
         bench;
         nkn = nkn;
         bwcet = bwcet; 
         octores = octores;
         polyres  = polyres;
         bestimpr = bestimpr;
         tracea = tracea;
      }
   )
   | _ -> (
      Printf.fprintf stderr "ERROR: bad res %s\n" (string_of_list (fun x -> x) lst);
      assert false
   )
)


(* prelude pour les tables de res
*)
let res_tab_prelude = 
"\\begin{longtable}{|c|l||l|l|r||l|l|r|}
\\caption{\\resTabCaption} \\\\\\hline
{\\bf } & {\\bf Initial} & \\multicolumn{3}{|c||}{\\bf Octagons} & \\multicolumn{3}{|c|}{\\bf Polyhedra}  \\\\
\\cline{3-8}
{\\bf Ref} & {\\bf WCET} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time}  \\\\
\\hline
\\endfirsthead
\\multicolumn{3}{@{}l}{The continuation of Table~\\ref{result-tbl}}\\\\\\hline
{\\bf } & {\\bf Initial} & \\multicolumn{3}{|c||}{\\bf Octagons} & \\multicolumn{3}{|c|}{\\bf Polyhedra}  \\\\
\\cline{3-8}
{\\bf Ref} & {\\bf WCET} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time}  \\\\
\\hline
\\endhead % all the lines above this will be repeated on every page
\\hline
\\multicolumn{3}{r@{}}{to be continued\\ldots}\\\\
\\endfoot
\\hline
\\endlastfoot
"
let res_tab_postlude = "\
\\hline
\\end{longtable}
"

let make_res_env_def os = (

   output_string os "\
\\newenvironment{benchResTab}[2]{
\\begin{table}
\\caption{#1}
\\label{#2}
\\begin{tabular}{|c|l||l|l|r||l|l|r|}
\\hline
{\\bf } & {\\bf Initial} & \\multicolumn{3}{|c||}{\\bf Octagons} & \\multicolumn{3}{|c|}{\\bf Polyhedra}  \\\\
\\cline{3-8}
{\\bf Ref} & {\\bf WCET} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time} & {\\bf $\\Delta$} & {\\bf Imp$^t$} & {\\bf Time}  \\\\
\\hline
}{
\\hline
\\end{tabular}
\\end{table}
}
"
)


let make_res_tab_line os sl = (
   let prdef x = (
      Printf.fprintf os "\\def\\RES%s{%s}\n"
         (nkn2latex x.nkn)
         (line_stat2latex x)
   ) in
   List.iter prdef sl;
)

let res2latex_tab os sl = (
   let otex = output_string os in

(*
la caption n'est plus en dur ici -> la définir avant avec un \def\resTabCaption
\\caption{How LRA can improve the estimated WCET of TacleBench \\label{result-tbl}} \\\\\\hline
*)

   otex res_tab_prelude ;
   List.iter (fun x -> otex (line_stat2latex x); otex "\n") sl;
   otex res_tab_postlude
)

let labels2latex_tab os sl = (
   let otex = output_string os in

   otex
"\\begin{longtable}{|c|l|l|c|}
\\caption{TacleBench functions Reference Labels \\label{Bench-ref-tbl}} \\\\
\\hline
    {\\bf Ref } & {\\bf Directory} & {\\bf  Function Names}  \\\\\\hline
\\endfirsthead
\\multicolumn{3}{@{}l}{The continuation of Table~\\ref{Bench-ref-tbl}}\\\\\\hline
    {\\bf Ref } & {\\bf TacleBench  Function Names} & {\\bf Directory} \\\\\\hline
\\endhead % all the lines above this will be repeated on every page
\\hline
\\multicolumn{3}{r@{}}{to be continued\\ldots}\\\\
\\endfoot
\\hline
\\endlastfoot
";
   List.iter (fun x -> otex (nkn_def2latex x.nkn)) sl;
   otex "\
\\hline
\\end{longtable}
"
)

(* version simplifiée, sortie csv *)
let labels2list os sl = (
   let ps s = (
      let (pn, fn) = nkn2pf s.nkn in 
      Printf.fprintf os "%s,%s\n" pn fn
   ) in
   List.iter ps sl;
)


(* compte les occurrences de cas
octo vs poly
   OP : pareil
   oP : poly meilleur
   _P : que octo
   O_ : que poly

*)

let make_res_dom_stats os sl = (
(* pour octo vs poly *)
   let cptOP = ref 0 in (* pareil *)
   let cptoP = ref 0 in (*  poly meilleur *)
   let cptO_ = ref 0 in (* que octo *)
   let cpt_P = ref 0 in (* que poly *)
   let cpt__ = ref 0 in (* aucun *)
   
   let do_stat s = (
      let _ = match (s.octores.impr, s.polyres.impr, s.bestimpr) with
      | (None,   None,   _     ) -> incr cpt__
      | (Some _, None,   _     ) -> incr cptO_ 
      | (None,   Some _, _     ) -> incr cpt_P 
      | (Some x, Some y, Some b) when (b > x) -> incr cptoP
      | (Some x, Some y, Some b) when (b = x) && (b = y) -> incr cptOP
      | _ -> ()
      in ()
   ) in
   List.iter do_stat sl;
   let pdef x v =
      Printf.fprintf os "\\def\\stat%s{%d}\n" x v
   in
   pdef "cptOP" !cptOP;
   pdef "cptoP" !cptoP ;
   pdef "cptOx" !cptO_ ;
   pdef "cptxP" !cpt_P ;
   pdef "cptxx" !cpt__;
   (* on met aussi le nombre total *)
   pdef "cptALL" (List.length sl);
)


(*
type pagai_stat = {
   wcet: int option;
   delta: int option;
   impr: float option;
   time: string 
}

type line_stat = {
   bench: string;
   nkn : string * int;
   bwcet : int;
   octores : pagai_stat;
   polyres : pagai_stat;
   bestimpr : float option;
   tracea : (int * int) option;
}
*)

(* renvoie un descripteur "bop" + la meilleure stat dans x *)
let get_best_domain_in_stat (zestat: line_stat) : string * pagai_stat = (
   let is_best (ps : pagai_stat) : bool  = (
      match ps.impr with | Some i when ((Some i) = zestat.bestimpr) -> true | _ -> false
   ) in
   let bstl = ref [] in
   let bstd = ref "" in
   let treat_dom dom desc =       
      if is_best dom then (
         bstl := dom::!bstl; 
         bstd := !bstd^desc
      ) else (
         bstd := !bstd^"-"
      )
   in
   treat_dom zestat.octores "o";
   treat_dom zestat.polyres "p";
   match !bstl with
   | x::_ -> (!bstd, x)
   | _ -> (
      Printf.fprintf stderr "ERROR: O0vsCO: BAD CO stats '%s'\n" (dump_line_stat zestat);
      exit 1
   )
)

(*-------------------------

Tableaux pour comparer O0 et CO1

---------------------------*)

(* ancienne version avec bop *)
let co_tab_cols_bop = "|c|l||l|l|r||l|l|r||l|l|r|"
let co_tab_header_bop = "
{\\bf }  & & \\multicolumn{3}{|c||}{\\bf O0} &  \\multicolumn{4}{|c|}{\\bf CO}  \\\\
\\cline{3-9}
{\\bf }     & {\\bf Best}   & {\\bf Initial}  & {\\bf Best} & {\\bf Best}    & {\\bf Initial} & {\\bf Best } & {\\bf Best} & {\\bf Traceability}    \\\\
{\\bf Ref } & {\\bf domain} & {\\bf WCET}     & {\\bf WCET} & {\\bf Imp$^t$} & {\\bf WCET}    & {\\bf WCET } & {\\bf Imp$^t$} &  \\\\
"
(* nouvelle version SANS bop avec opt speedup *)
let co_tab_cols = "|c||l|l|r||c|l|l|r|rcl|"
let co_tab_header = "
{\\bf }  & \\multicolumn{3}{|c||}{\\bf O0} &  \\multicolumn{7}{|c|}{\\bf CO}  \\\\
\\cline{2-11}
{\\bf }     & {\\bf Initial}  & {\\bf Best} & {\\bf Best}    & {\\bf Opt.}  & {\\bf Initial} & {\\bf Best } & {\\bf Best} & \\multicolumn{3}{c|}{\\bf Traceability}    \\\\
{\\bf Ref } & {\\bf WCET}     & {\\bf WCET} & {\\bf Imp$^t$} & {\\bf speedup} & {\\bf WCET}    & {\\bf WCET } & {\\bf Imp$^t$} & \\multicolumn{3}{c|}{}   \\\\
"

(* def de la mcro "petite table" a utiliser avec les macros OPTXXXX *)
let make_opt_env_def os = (

   output_string os "\
\\newenvironment{benchOptTab}[2]{
\\begin{table}
\\caption{#1}
\\label{#2}
\\begin{tabular}{"; output_string os co_tab_cols; output_string os "}
\\hline
";
   output_string os co_tab_header ;
   output_string os "\
\\hline
}{
\\hline
\\end{tabular}
\\end{table}
}
"
)

(* on met aussi des macros da def
on en proffite pour sortir qq macros:
\statoptFT nbre avec full traceability
\statoptPTg nbre avec partial traceability mais gain quand meme
\statoptPTn nbre avec partial traceability mais pas de gain 
*)
let do_O0vsCO_latex os_defs os sl optls = (
   let otex = output_string os in

   let cptFull = ref 0 in
   let cptPartGain = ref 0 in
   let cptPartNoGain = ref 0 in

   let do_line o0_stat = (
      (* le pendant dans optls ? *)
      let co_stat = try
(* Printf.printf "search CO for %s\n" (nkn2screen o0_stat.nkn); *)
         List.find (fun x -> (x.nkn = o0_stat.nkn)) optls
      with Not_found -> (
         Printf.fprintf stderr "ERROR: O0vsCO: no CO entry for %s\n" (nkn2screen o0_stat.nkn) ;
(* Printf.printf "search CO for %s\n" (nkn2screen o0_stat.nkn); *)
         exit 1
      ) in
(* Printf.printf "found CO %s\n" (dump_line_stat co_stat); *)
      let (o0_desc, o0_best) = get_best_domain_in_stat o0_stat in
      let (co_desc, co_best) = get_best_domain_in_stat co_stat in
      let (zeunit, zesfx) =
         if o0_stat.bwcet >= 10000000 then 
            (1000000., "M")
         else if o0_stat.bwcet >= 10000 then
            (1000., "K")
         else (1., "")
      in
      let c2s = function | None -> "-" | Some x -> round_cycles x zeunit zesfx in
      let p2s = function | None -> "-" | Some p -> (Printf.sprintf "%.01f" p) in
      (* pty tracabilité *)
      let y2s = function
         | None -> "-"
            (* old version ... *)
         (* | Some (p,a) -> (Printf.sprintf "%d (%d/%d)" (int_of_float (pcent a p)) p a) *)
         | Some (p,a) -> (
            let pc = (int_of_float (pcent a p)) in
            Printf.sprintf "%d\\%% & of & %d" pc a
         )
      in
      (* met a jour kles stats *)
      let _ = match (co_stat.tracea, co_best.impr) with
      | (Some (p,a), _) when (p=a) -> incr cptFull
      | (_, Some g) when (g > 0.0) -> incr cptPartGain 
      | _ -> incr cptPartNoGain
      in
      (* ancienne ligne: nkn, best dom, O0 init wcet, O0 best wcet, O0 impr, CO init wcet, CO best wcet, CO impr CO tracea *)
      (*
      let elts = [
         (nkn2screen o0_stat.nkn); o0_desc;
         (c2s (Some o0_stat.bwcet)); (c2s o0_best.wcet); (p2s o0_best.impr);
         (c2s (Some co_stat.bwcet)); (c2s co_best.wcet); (p2s co_best.impr);
         (y2s co_stat.tracea) 
      ] in
      *)
      (* nouvelle ligne: nkn, O0 init wcet, O0 best wcet, O0 impr, CO speedup, CO init wcet, CO best wcet, CO impr CO tracea *)
      let spup = (float_of_int o0_stat.bwcet) /. (float_of_int co_stat.bwcet) in
      let speedup = Printf.sprintf "%.1fx" (spup) in
      let elts = [
         (nkn2screen o0_stat.nkn);
         (* nkn o0_desc; *)
         (c2s (Some o0_stat.bwcet)); (c2s o0_best.wcet); (p2s o0_best.impr);
         speedup ;
         (c2s (Some co_stat.bwcet)); (c2s co_best.wcet); (p2s co_best.impr);
         (y2s co_stat.tracea) 
      ] in
      let res = string_of_list ~obr:"" ~cbr:"\\\\" ~sep:" & " (fun x -> x) elts in
      (* on definit une macro dans os_defs *)
      Printf.fprintf os_defs "\\def\\OPT%s{%s}\n" (nkn2latex o0_stat.nkn) (res);
      res
   ) in

   otex
"\\begin{longtable}{"; otex co_tab_cols; otex "}
\\caption{\\resTabCaption} \\\\
\\hline
";
   otex co_tab_header;
   otex "
\\hline
\\endfirsthead
";
   otex co_tab_header;
   otex "
\\hline
\\endhead % all the lines above this will be repeated on every page
\\hline
\\multicolumn{3}{r@{}}{to be continued\\ldots}\\\\
\\endfoot
\\hline
\\endlastfoot
";

   List.iter (fun x -> otex (do_line x); otex "\n") sl;

   otex "\
\\hline
\\end{longtable}
";
   (* stats ds def *)
   let pdef x v =
      Printf.fprintf os_defs "\\def\\stat%s{%d}\n" x v
   in
   pdef "optFull" !cptFull;
   pdef "optPartGain" !cptPartGain;
   pdef "optPartNoGain" !cptPartNoGain;

)


(* utile ... *)
let rec trunc_list i l = (
   match (i,l) with
   | (_, []) -> []
   | (z, _) when (z <= 0) -> []
   | (i, h::t) -> h::(trunc_list (i-1) t)
) 

(*  INFOS SUR LA DECOUVERTE DE LOOP BOUNDS
   let bounds_list = get_bounds "bounds_stats.txt" in
*)

let get_bounds zefile = (
   let inc = open_in zefile in
   let do_bound lne = (
   ) in
   (try
      while true do
         do_bound (input_line inc)
      done
   with End_of_file -> close_in inc);
)

let main () =  (
   let ch = open_in "all_funcs_O0.txt" in
   let os_defs = open_out "bench_defs.tex" in

   (* A: parcours all_funcs.txt
      pour fabriquer les nkn
      -> bench_defs.tex
   *)
   (try
      while true do
         do_bench (input_line ch) os_defs
      done
   with End_of_file -> close_in ch);

   (* parcours de wcet_stats.txt
      en 2 temps :
      - genére les infos brutes -> type res_info
      - tri éventuel
      
   *)

   (* B.0: genere l'environnement *)
   make_res_env_def os_defs;
   make_opt_env_def os_defs;

   (* B.1: parcours wcet_stats.txt
      pour fabriquer les stats 
      sous forme de liste
   *)
   let stats_of_file zefile = (
      let _res = ref [] in
      let ch = open_in zefile in
      (try
         while true do
            let l = input_line ch in
            match l.[0] with '#' -> ()
            | _ ->
               _res := (do_stat l)::!_res
         done
      with End_of_file -> close_in ch);
      List.rev !_res
   ) in
   let all_stats = stats_of_file "wcet_stats_O0.txt" in


   (* B.2: Tri et troncage des stats
   *)
   (* sort selon le plus grd impr *)
   let bestfirst a b = Pervasives.compare b.bestimpr a.bestimpr in
   let sorted_stats = List.stable_sort bestfirst all_stats in


   (* ceux qu'on garde pour le papier *)
   let paper_stats = trunc_list _PAPER_STATS sorted_stats in

   (* B.2.bis pour debug essentiellement, la meme chose pour -CO *)
   let co_all_stats = stats_of_file "wcet_stats_CO.txt" in
   let co_sorted_stats = List.stable_sort bestfirst co_all_stats in

   (* C: generer des tables de resultats
      bench_res.tex : les X meilleurs pour le papier
      bench_res_all.tex : pour debug/info   
      bench_co_res_all.tex : pour debug/info 
   *)
   let os_res_all = open_out "bench_res_all.tex" in
   res2latex_tab  os_res_all sorted_stats;
   close_out os_res_all;

   let os_res = open_out "bench_res.tex" in
   res2latex_tab  os_res paper_stats;
   close_out os_res;
   (* on genere aussi les commandes individuelles de ligne *)
   make_res_tab_line os_defs paper_stats;
   (* et qq commandes de stats *)
   make_res_dom_stats os_defs paper_stats;

   let os_co_res_all = open_out "bench_co_res_all.tex" in
   res2latex_tab  os_co_res_all co_sorted_stats;
   close_out os_co_res_all;


   (* D: tables des nik-names
      On le fait ici car on ne retient que les "paper_stats"
      bench_labels.tex : les X meilleurs pour le papier
      bench_labels_all.tex : pour debug/info   
      N.B. on le "re-trie" par ordre alphabetique sur le label
   *)
   let cmp_nkn a b = Pervasives.compare a.nkn b.nkn in
   let alpha_sorted_stats = List.stable_sort cmp_nkn sorted_stats in 
   let alpha_paper_stats = List.stable_sort cmp_nkn paper_stats in 

   let os_labels_all = open_out "bench_labels_all.tex" in
   labels2latex_tab os_labels_all alpha_sorted_stats;
   close_out os_labels_all;

   let os_labels = open_out "bench_labels.tex" in
   let os_short_list = open_out "bench_short_list.txt" in
   labels2latex_tab os_labels alpha_paper_stats;
   labels2list os_short_list alpha_paper_stats;
   close_out os_labels;
   close_out os_short_list;

   (* E: table O0 vs CO
      On le fait ici car on ne retient que les "paper_stats"
      entrées : paper_stats et  co_sorted_stats
   *)
   let os_O0vsCO = open_out "bench_O0vsCO.tex" in
   do_O0vsCO_latex os_defs os_O0vsCO paper_stats co_sorted_stats;
   close_out os_O0vsCO;

   (* on rajoute qq commandes dans os_defs *)
   (* on a un improv. avec au - une methode *)
   let has_gain x = match x.bestimpr with None -> false | Some f -> f > 0.0 in
   let nb_gain = list_count has_gain sorted_stats in

   Printf.fprintf os_defs "\\newcommand{\\benchSize}{%d}\n" (List.length sorted_stats) ;
   Printf.fprintf os_defs "\\newcommand{\\benchNbGain}{%d}\n" nb_gain ;

   close_out os_defs; 

   (* let bounds_list = get_bounds "bounds_stats.txt" in *)
   let _ = get_bounds "bounds_stats.txt" in
   ()

)

let _ = main ()
