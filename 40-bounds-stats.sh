#!/bin/bash

#
# stats sur les loop bounds ->
# - pour la liste bench_short_list.txt UNIQUEMENT
# - a partir des resultats dans expe-compteurs-short
#
# Pour les prg/func concernés on est cense avoir généré plusieurs benchs:
#
# 0) connaitre la profondeur de boucle max d'un prg
#    on détourne c2pip pour ça, qui fait du cfg -> regexp
#    et on lui demande de donner la profondeur d'étoile !
#    "c2pip -stars xx.c -f func"
#    on le fait sur le "cptr_ref_xxx_cat.c"
#    -> petite correction : il faut virer la "fausse" boucle
#       {int cptr_dummy=0;while(cptr_dummy>0){cptr_dummy--;}}
#    qui sert a faciliter le boulot/l'interpretation de pagai
# 
# 1) savoir si pagai borne les boucles sans pragma, en poly et/ou octo :
#
# -noprag-nops-inline-noswitch-O0
# -noprag-nops-inline-noswitch-O0--domainoct
# accessoirement, on peut aussi savoir si owcet SANS orange borne (i.e. pas de boucles normallement
#
# 2) pour savoir si orange boucle sans pragma, on a essayé pusieurs méthodes :
# 2.0 -orange-noprag-nops-inline-noswitch-O0
#     => irrelevant, orange pete sur les nops
# 2.1 -orange-noprag-inline-noswitch-O0
#     => un peu mieux mais très pénalisant pour orange
# 2.2 -orange-noprag-noswitch-O0
#     => un peu mieux mais encore très pénalisant pour orange
# 2.3 Change complètement de philosophie :
#     on ne cherche pas à faire un wcet "complet"
#     on lance juste orange sur le programme ORIGINAL
#     et on voit s'il produit des NOCOMP
benchresdir=expe-compteurs-short-list

# entrée : un fichier de type "xxx_cat.cpp" (et la fonction à borner)
#   c.a.d. après cpp, mais avant cil 
#   juste 1 petite correction à cause des pseudo appel "wcet_bounds"
#   
# retour: 1 (bounded), 0 (unbounded), -1 (erreur)
#
function check_orange_bounds {
	src=$1
	func=$2
	# remove wcet stuff
	bnme=`basename $src .cpp`
	tmpsrc=$$-$bnme-$func.c
	tmpffx=$$-$bnme-$func.ffx
	rm -f $tmpsrc $tmpffx
	cat "$src" | sed -e "/_wcet_loop_bound/d" > $tmpsrc
>&2 echo "-------------------------------"
>&2 echo "orange $tmpsrc $func -o $tmpffx"
	#nbu=`orange $tmpsrc "$func" | grep NOCOMP | wc -l 2> /dev/null`
	#orange $tmpsrc $func -o $tmpffx 1>&2
	orange $tmpsrc $func -o $tmpffx 1> /dev/null
	code=$?
>&2 echo "orange return code: $code"
	if [ ! -f $tmpffx -o $code -gt 0 ]; then
		res=-1
	else
		nbu=`cat $tmpffx | grep NOCOMP | wc -l`
		if [ "$nbu" -gt 0 ]; then
			res=0
		else
			res=999
		fi	
	fi
	rm -f $tmpsrc $tmpffx
>&2 echo "check_orange_bounds returns: $res"
	echo $res
}


OPT=O0
OUTFILE=bounds_stats.txt
echo "bounds_stats in $benchresdir"

echo -n "" > $OUTFILE

function check_owcet {
	# XXXXX.wcet may contain (e.g):
	# - "Value of objective function: 251"
	# - several lines of "spx_run: Lost feasibility ...  " + "Value of objective function: xxxxx"
	#   => returns a number
	# - "This problem is unbounded"
	#   => returns 0
	# - something else 
	#   => returns -1
	ofile=$1
	if [ ! -f $ofile ]; then
		echo -3
		return
	fi
	ores=`tail -n 1 $ofile`
	owcet=`echo $ores | grep -o "[0-9]*"`
	unbound=`echo $ores | grep -o "unbounded"`
	infea=`echo $ores | grep -o "infeasible"`
	if [ -n "$owcet" ]; then
		echo $owcet
	elif [ -n "$unbound" ]; then
		echo 0
	elif [ -n "$infea" ]; then
		echo -1
	else 
		echo -2
	fi
}

function dopcent {
	part=$1
	total=$2
	pc=`echo "$part * 100 / $total" | bc -l`
	printf "%4.1f" $pc
}

function maxpcent {
	if [ "$1" = "-" ]; then
		echo "$2"
	elif [ "$2" = "-" ]; then
		echo "$1"
	elif [ `echo "$2 > $1" | bc -l` -ge 1 ]; then
		echo $2
	else
		echo $1
	fi
}

function check_time {
	tfile=$1
	tm=`cat $tfile | head -1` || tm="-"
	#format m:s.c
	res=`echo $tm | grep -e "[0-9]\+:[0-9]\+\.[0-9]\+" -o`
	if [ -z "$res" ]; then
		#format h:m:s pour les durées longues ?
		res=`echo $tm | grep -e "[0-9]\+:[0-9]\+:[0-9]\+" -o`
		if [ -z "$res" ]; then
			res="-"
		fi
	fi
	echo $res
}

function check_tracea {
	lfile=$1
	res=`tac $lfile | grep -m 1 Traceability | grep -o "(.* out of .*)" | sed -e "s/(\([0-9]\+\) *out of *\([0-9]\+\))/\1, \2/"`
	if [ -z "$res" ]; then
		res="-, -"
	fi
	echo $res
}

# at least one bug
nb_bug=0
# bounded  without orange and pagai (no loop !)
nb_no_loops=0
# other cases (oranGe|Octo|Poly)
nb_GOP=0
nb_GuP=0
nb_GOu=0
nb_Guu=0
nb_uOP=0
nb_uuP=0
nb_uOu=0
nb_uuu=0

nb_pg_fails=0
nb_pg_unbound=0

function check_bounds_sh {
	# check_bounds_sh $benchbase $orangebase $octobase
	# this dir contains basic.wcet WITHOUT orange and pagai.wcet POLY
	benchbase=$1
	# prog/func part maybe usefull
	pf=`echo $benchbase | cut -d'-' -f2,3`
	prg=`echo $benchbase | cut -d'-' -f2`
	func=`echo $benchbase | cut -d'-' -f3`
	# this dir contains basic.wcet WITH orange (no pagai)
	orangebase=$2
	# this dir contains basic.wcet WITHOUT orange and pagai.wcet POLY
	octobase=$3

	# wcet de base
	basic=`check_owcet $benchresdir/$benchbase/basic.wcet`
	#------------------------------------------
	# orange
	# nouvelle méthode -> check_orange_bounds
	# orange=`check_owcet $benchresdir/$orangebase/basic.wcet`
	for cppfile in `/bin/ls $benchresdir/$benchbase/*_cat.cpp`; do
		break
	done
	if [ ! -f $cppfile ]; then
		>&2 echo "ERROR: missing c file '$cppfile'"
		exit 1
	fi
	orange=`check_orange_bounds $cppfile $func`
>&2 echo "ORANGE BOUND: $orange"
	octo=`check_owcet $benchresdir/$octobase/pagai.wcet`
	poly=`check_owcet $benchresdir/$benchbase/pagai.wcet`
	# rappel: neg si erreur, 0 si unbound, valeur sinon
	pagai=$(( (octo > poly) ? octo : poly ))

	# check if negative wcet (error) is coherent 
	# with the corresponding result without -noprag
	# if yes -> ok
	# if no -> bug
	if [ $basic -lt 0 ]; then
		# unexpected: bug
		>&2 echo "WARNING: unexpected basic wcet '$basic' for '$benchbase'"
		((nb_bug++))
	fi
	# not a bug: orange fails on too complex code
	# if [ $orange -lt 0 ]; then
		# # unexpected: probably a bug
		# >&2 echo "WARNING: unexpected orange wcet '$orange' for '$benchbase'"
		# ((nb_bug++))
	# fi
	if [ $poly -lt 0 ]; then
		# coherent with wcet_stats_O0.txt ? poly wcet is in 7th column
		orig=`cat wcet_stats_O0.txt | grep "$pf-" | cut -d',' -f 7 | tr -d '[:space:]'`
		if [ "$poly" != "$orig" ]; then
			>&2 echo "WARNING: poly wcet '$poly' was '$orig' in first stats for '$benchbase'"
			((nb_bug++))
		fi
	fi
	if [ $octo -lt 0 ]; then
		# coherent with wcet_stats_O0.txt ? octo wcet is in 5th column
		orig=`cat wcet_stats_O0.txt | grep "$pf-" | cut -d',' -f 5 | tr -d '[:space:]'`
		if [ "$octo" != "$orig" ]; then
			>&2 echo "WARNING: octo wcet '$octo' was '$orig' in first stats for '$octobase'"
			((nb_bug++))
		fi
	fi
	# from now on, negative or null -> not bounded
	if [ $basic -gt 0 ]; then
		((nb_no_loops++))
	elif [ $orange -gt 0 -a $pagai -gt 0 ]; then
		if [ $octo -le 0 ]; then
			((nb_GuP++))
		elif [ $poly -le 0 ]; then
			((nb_GOu++))
		else
			((nb_GOP++))
		fi
	elif [ $orange -le 0 -a $pagai -gt 0 ]; then
		if [ $octo -le 0 ]; then
			((nb_uuP++))
		elif [ $poly -le 0 ]; then
			((nb_uOu++))
		else
			((nb_uOP++))
		fi
	elif [ $orange -gt 0 -a $pagai -le 0 ]; then
		((nb_Guu++))
	else
		((nb_uuu++))
	fi
	if [ $pagai -lt 0 ]; then ((nb_pg_fails++)); fi
	if [ $pagai -eq 0 ]; then ((nb_pg_unbound++)); fi
	((nb_bench++))
	#
	# on utilise "c2pip -stars xx.c -f func"
	# le fichier c est le cpt_ref...c
	# MAIS il faut ignorer la boucle "artificielle"
	#   {int cptr_dummy=0;while(cptr_dummy>0){cptr_dummy--;}}
	for zecfile in `/bin/ls $benchresdir/$benchbase/cptr_ref_*.c`; do
		break
	done
	if [ ! -f $zecfile ]; then
		>&2 echo "ERROR: missing c file '$zecfile'"
		exit 1
	fi
	tmpc=ref_$$.c
	cat $zecfile | sed -e "s/while(cptr_dummy>0)/if(cptr_dummy>0)/" > $tmpc
	loop_lvl=`c2pip -stars $tmpc -f $func`
	rm -f "$tmpc"

	echo "$benchbase, $loop_lvl, $basic, $orange, $octo, $poly" >> $OUTFILE
}

echo "#bench, looplevel, basic, orange, octo, poly wcet" > $OUTFILE

# iter the NO orange sh 
#for zsh in `/bin/ls $benchresdir/sh/*.sh | grep noprag | grep -v orange | grep -v domain`; do
# NON -> on itere sur la short list
for sline in `cat bench_short_list.txt`; do
	zebdir=`echo "$sline" | cut -d',' -f1`
	zebdir=`basename "$zebdir"`
	zebfunc=`echo "$sline" | cut -d',' -f2`
	# recherche le sh 
	for zsh in `/bin/ls $benchresdir/sh/*$zebdir*$zebfunc*.sh | grep noprag | grep -v orange | grep -v domain`; do
		break
	done
	if [ ! -f $zsh ]; then
		>&2 echo "ERROR: missing bench '$zsh'"
		exit 1
	fi
	# A partir de la on garde le vieux scrtipt ...
	#>&2 echo "DOING $zsh"
	benchbase=`basename $zsh .sh`
	# get the corresponding prg/fun
	pf=`echo $benchbase | cut -d'-' -f2,3`
	# get the corresponding octagon sh
	for zeoct in `/bin/ls $benchresdir/sh/*$pf*domainoct*.sh 2>/dev/null | head -1`; do break; done
	if [ -z "$zeoct" ]; then
		>&2 echo "BUG: can't find '-domainoct' dir for '$benchbase'"
		((nb_bug++))
		break
	fi
	octobase=`basename $zeoct .sh`
	# get the corresponding ORANGE sh
   # WITH OR WITHOUT -inline
   #
	for zeora in `/bin/ls $benchresdir/sh/*$pf*orange*inline*.sh 2>/dev/null | head -1`; do break; done
	# for zeora in `/bin/ls $benchresdir/sh/*$pf*orange*.sh 2>/dev/null | grep -v inline | head -1`; do break; done
	if [ -z "$zeora" ]; then
		>&2 echo "BUG: can't find '-orange' dir for '$benchbase'"
		((nb_bug++))
		break
	fi
	orangebase=`basename $zeora .sh`
	check_bounds_sh $benchbase $orangebase $octobase
done

>&2 echo "BENCH: $nb_bench (warnings: $nb_bug)
NO LOOPS: $nb_no_loops
OTHER BOUNDED BY:
None:   $nb_uuu,
Orange: $nb_Guu,
Pagai:  $((nb_uOP + nb_uuP + nb_uOu)) (octo: $nb_uOu, poly: $nb_uuP, both: $nb_uOP)
Both:   $((nb_GOP + nb_GuP + nb_GOu)) (octo: $nb_GOu, poly: $nb_GuP, both: $nb_GOP)
PAGAI DETAILS:
Fails:   $nb_pg_fails
Unbound: $nb_pg_unbound"
