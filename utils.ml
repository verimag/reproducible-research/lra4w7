(* SOME USEFUL BASIC STUFF *)


(* SIGNATURE: ORDERED and PRINTABLE TYPES *)
module type XType =
   sig
      type t
      val compare: t -> t -> int
      val to_string: t -> string
   end;;

(* MODULE: ORDERED and PRINTABLE INT *)
module XInt : XType =
  struct
    type t = int
    let compare = Pervasives.compare
    let to_string = Pervasives.string_of_int
  end;;

(* MODULE: ORDERED and PRINTABLE STRING *)
module XString : XType =
  struct
    type t = string
    let compare = Pervasives.compare
    let to_string = fun x -> x
  end;;

(* FUNCTOR: SETS OVER ORDERED and PRINTABLE TYPES *)
module XSet (Elt: XType) =
   struct
      include Set.Make (
         struct
            type t = Elt.t
            let compare = Elt.compare
         end
      ) 
      let to_string: t -> string = fun x ->
         "{"^(String.concat ", " (List.map Elt.to_string (elements x))) ^"}"

   end;;

(* MODULE: PRINTABLE INT SET *)
module IntSet = XSet (
   struct
      type t = int
      let compare = Pervasives.compare
      let to_string = Pervasives.string_of_int
   end
);;

module StringSet = XSet (
   struct
      type t = string 
      let compare = Pervasives.compare
      let to_string = (fun x -> x)
   end
);;

(* FUNCTOR: MAPS BETWEEN ORDERED and PRINTABLE TYPES *)
module XMap (Key: XType) (Val: XType) =
   struct
      module M = Map.Make (
         struct
            type t = Key.t
            let compare = Key.compare
         end
      ) 
      open M 
      type t = Val.t M.t
      let cardinal = M.cardinal
      let empty = M.empty
      let add = M.add
      let find = M.find
      let to_string ?(mapsto:string=" -> ") (x:t) : string =
         let bs (k, v) = "("^(Key.to_string k)^mapsto^(Val.to_string v)^")" in
         "{"^(String.concat ", " (List.map bs (bindings x))) ^"}"
      let of_list: (Key.t * Val.t) list -> t = fun lst ->
         List.fold_left (fun a (k,v) -> add k v a) empty lst 
(*
      let to_string: Val.t t -> string = fun x ->
         let bs (k, v) = "("^(Key.to_string k)^" -> "^(Val.to_string v)^")" in
         "{"^(String.concat ", " (List.map bs (bindings x))) ^"}"
      let of_list: (Key.t * Val.t) list -> Val.t t = fun lst ->
         List.fold_left (fun a (k,v) -> add k v a) empty lst 
*)
   end;;

(* MODULE: INT 2 STRING MAP *)

(* ??  module Int2String = XMap (struct include XInt end) (struct include XString end);; *)

module Int2String = XMap
(
   struct
      type t = int
      let compare = Pervasives.compare
      let to_string = Pervasives.string_of_int
   end
)
(
   struct
      type t = string
      let compare = Pervasives.compare
      let to_string = fun x -> x
   end
);;

module Int2Int = XMap
(
   struct
      type t = int
      let compare = Pervasives.compare
      let to_string = Pervasives.string_of_int
   end
)
(
   struct
      type t = int
      let compare = Pervasives.compare
      let to_string = Pervasives.string_of_int
   end
);;



let string_of_list
   ?(obr: string = "[")
   ?(cbr: string = "]")
   ?(sep: string = "; ")
   (soa: 'a -> string) (al: 'a list) = 
   obr^(String.concat sep (List.map soa al))^cbr

let string_of_option (soa: 'a -> string) (ao: 'a option) = 
   match ao with
   | None -> "None"
   | Some a -> "Some("^(soa a)^")" 

let string_of_hashtab (sok: 'a -> string) (soe: 'b -> string) (abtbl: ('a,'b) Hashtbl.t) =
   let soab k v acc = ((sok k)^" -> "^(soe v)^";")^acc in
   "["^(Hashtbl.fold soab abtbl "")^"]"

type toto = int * string

module TotoSet = Set.Make (
   struct
      type t = toto
      let compare = Pervasives.compare
   end
)

let rec iter_sep (f: 'a -> unit) (g: unit -> unit) (l: 'a list) : unit =
   match l with
   | [] -> ()
   | [x] -> f x
   | x::l' -> f x; g (); iter_sep f g l'

let iteri_sep (f: int -> 'a -> unit) (g: unit -> unit) (l: 'a list) : unit =
   let rec _iteri i l =
      match l with
      | [] -> ()
      | [x] -> f i x
      | x::l' -> f i x; g (); _iteri (i+1) l'
   in
   _iteri 0 l

let rec make_list (i: int) (x:'a) : 'a list =
   if i <= 0 then [] else x::(make_list (i-1) x)

let list_count (select: 'a -> bool) (lst: 'a list) : int =
   List.fold_left (fun acc x -> if select x then acc + 1 else acc) 0 lst
